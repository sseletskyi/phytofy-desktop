# Changelog
All notable changes to the OSRAM - Phytofy RL project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# [ 0.9.2] - 2018-02-04

### Added
- Pagination for fixtures (fixtures page)
- Added placeholders for names in forms and placeholders with minimum sizes for layouts.
- Running commissioning on startup
- Permanent label next to "Add" buttons

### Fixed
- bug with timeline on recipeItemContainer - wasn't starting on 00 different timezones because linux time was being used, use date instead.
- fixed state persistence on create groups (fixtures list)
- Changed application title (on app.html changed title)
- Fixed product info according to indications
- Added fixtures to existing group now have their scheduling state set to the same as the rest of the group.
- Schedules for multiple days are now saved in a single command according to communication's specifications just provided.

### Changed
- removed logs for local and global broadcast in moxa discovery
- on reset data, after deleting all schedules, request schedule count and delete again if different from 0
- on realtime control added a small wait time (20ms) in between commands to solve merge commands issue.
- removed all occurrences of "profile" (changed to layout)
- Updated minimum length for layouts and updated error messages and hints.
- Changed primary menu (left), now it also has groups and layouts.
- Changed Recipes to Day Recipes everywhere
- Changed Day Recipes and Layout menu icons.
- Added quick guide page on left bar
- Overview empty state is now the quick guide
- About on tooltip (sabe as quick guide)
- Changed photoperiod card on Recipe form to distinguish from editable values
- no active recipe to no active schedule on overview

# [ 0.9.1 ] - 2018-01-28

### Added
- commission fixtures on mount of FixturesPage

### Fixed
- fixed size of profile svg on profile forms
- fixed problem with font of labels on profile svgs
- fixed displayed info of the libraries license checker could not fetch the license from
- typo on daily light integral units (overview)
- fixed bug on recipe form, need to init period time values
- small typo on start empty screen

### Changed
- reordered settings page cards

# [ 0.9.0 ] - 2018-01-27

### Added
- schedule overlap validator to group details page
- about section
- load fixtures from db or commission on start up

### Fixed
- import groups now updates number of groups assigned to profiles
- Fixed add schedule on group details page
- fixed delete schedules on group details page
- fixed validators on create schedules
- fixed typo on daily light integral unit (overview)

### Changed
- removed menu from browser window (windows)
- fixed formatting of schedule dates in lists
- (windows) log file is now store under log directory
- reset data now also stops scheduling for all fixtures
- recipe validator on add/edit button instead of submit
- changed overview empty screen
- removed remaining layout occurrences
- changed irradiance map tooltip to show irradiance value

# [ 0.8.3 ] - 2018-01-16

### Added
- notifications to delete schedules

### Fixed
-  recipe import, building the imported objects
- recipe import - typo on require electron-log

### Changed
- removed y axis from spectrum chart in recipes and fixtures real-time
- doubled notifications time
- removed DLI from recipes form

# [ 0.8.2 ] - 2018-01-14

### Fixed
- Irradiance channels on details
- single day schedules on overview and details
- fixed DLI calculation
- fixed bug on master level of channels
- header of sidebar is unmoved
- fixed x labels on spectrum
- allow same start and end for adjoining periods on recipes

### Changed
- removed any mention of irradiance on recipes and fixture real-time
- changed sidebar with background focus and lock
- timeline changed to 00 - 24
- AM/PM on recipe periods on Linux and Windows changed to 00:00-24:00

# [ 0.8.1 ] - 2018-01-06

### Fixed
- fixed local broadcast ip masking (moxa discovery)
- using both global and local broadcast for moxa discovery

# [ 0.8.0 ] - 2018-01-06

### Added
- daily light integral and photoperiod
- recipe spectrum to canopy card on homepage
- schedule fail warning icon on schedule list
- recipe and schedule overlapping validators
- EULA
- empty state for homepage
- master level controller for real-time
- import/export json recipes
- profile create/edit validator

### Changed
- search for fixtures and canopies now also shows listing of items
- it is now possible to create 1 day length schedules

### Fixed
- log file path on windows
- canopy occurrences
- schedule fail acknowledgement
- schedule export without canopies

## [ 0.7.1 ] - 2018-12-19

### Changed
- Moxa discovery algorithm - broadcast is now local brodcast

### Fixed
- One occurence of "canopy" on form warning.

## [ 0.7.0 ] - 2018-12-16

### Added
- CSV Import and Export functionalities
- backup and restore functionality
- added Go-to logs directory button
- real time control from fixtures
- tooltip on status icon on fixtures list

### Changed
- Last occurrences of canopies to groups
- added fuller screen view of real-time control and recipes
- new commissioning algorithm
- on primary menu, selected icon is now coloured
- real-time control now displays actual max irradiance

### Fixed
- bug on create/edit schedules for multiple canopies, would result in inability to delete schedules
- some connectivity bugs on drivers
- typo on spectrum label


## [ 0.6.2 ] - 2018-12-3

### Added
- error and warning logs

### Fixed
- group commissioning - max irradiance
- Close sidebar on schedule edit/creation
- Notification while communicating with fixtures
- Reset Data now also deletes all schedules for fixtures saved in canopies

## [ 0.6.1 ] - 2018-11-25

### Fixed
- Fixed windows installer
- Fixed Success button states
- Accordion on Recipe forms is functioning correct

## [ 0.6.0 ] - 2018-11-20
### Added
- Canopy Details View
- Irradiance Map for current active schedule
- Canopy Commissioning
- Tooltips
- Axis units to spectral graph

### Changed
- Renamed all occurrences of canopy(ies) to group(s)
- Renamed all occurrences of layout(s) to profile(s)
- Renamed devices to adapters
- Eliminated Diagnostics View

### Fixed
- Selected options should not obscure status - on group setup list
- Random negative photoperiod value on homepage cards
- Reset Data Button now deletes all data models
- Accordion bug on recipe creation/editing
- Fixed bug where white channel wasn't controllable on real-time
- Fixed duplicate fixtures on commissioning
- Fixed error on deleting schedules without groups associated with it

## [ 0.5.0 ] - 2018-09-19
### Added
- New navigation

## [ 0.4.2 ] - 2018-09-14
### Added
- Time validation on recipes forms
- Date validation on schedules forms
- Moved recipe validation when being used so the user can view the recipe

## [ 0.4.1 ] - 2018-09-03
### Added
- Schedules
- Recipes
- Irradiance on Canopies view
- Schedules remaining time on recipes view
- Canopy comissioning
- Realtime control


## [ 0.3.1 ] - 2018-08-27
### Added
- Export layouts
- Import layouts (format to be reviewed)
- Reset database
- Discover Moxas
- Comission Fixtures
- Get features temperatue, hardware viersion and firmware version

### Fixed
- Pagination issue on Canopies
- Delete canopies


## [ 0.2.1 ] - 2018-08-06
### Added
- Devices page layout for testing purposes
- Settings preferences
- Search filters by name

### Changed
- Navigation layout
- Moved canopies creation, edition, deletion to a separated canopy menu within the setup tab

### Fixed
- Removed limit (pagination) from profiles list on Canopies form
- "Configuration" filter should read "Orientation"
- Fixed application logos


## [0.1.1] - 2018-07-24
### Added
- Canopies grid view (with dummy data)
- Go To a specific element on paginated lists
- Max length validation on names fields
- Max length validation on numbers fields
- Support for floats on numbers fields
- Fields description as a placeholder

### Changed
- Renamed "Canopy Profile" to "Layout"
- Renamed "Configuration" to "Orientation"
- Merged "Layout" and "Orientation" on canopies' list
- No uppercase titles
- Application icon

### Fixed
- Removed useless items from topbar menu on Windows and Linux
- Submiting a form after a validation error was not changing the data
- "Delta" character line height
- Hide delta width/delta length for orientations that doesn't support these values
- Cosmetic issue on "3 vertical" orientation image
- Removed sidebar scrolls on windows
