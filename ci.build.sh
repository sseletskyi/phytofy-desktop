#!/bin/sh

set -e

npm cache clean --force
rm package-lock.json lib/phytofy-utils/package-lock.json lib/phytofy-design-system/package-lock.json || true

npm install

npm install -g mocha
cd lib/phytofy-drivers/tests && mocha test-moxaDiscovery.js && cd -
cd lib/phytofy-drivers/tests && mocha test-rs485protocol.js && cd -
cd lib/phytofy-drivers/tests/integration && mocha test.js && cd -
cd lib/phytofy-utils/tests && mocha test-utils.js && cd -

npm run package-win
npm run package-linux
#npm run package-mac
