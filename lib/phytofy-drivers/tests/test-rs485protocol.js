const assert = require('assert');
const compute = require('../src/compute.js');
const proto = require('../lib/rs485-protocol.js');
const logger = require('../src/logger.js');

describe('RS485 communication tests', function() {
  describe('util tests', function() {
    describe('#computeCRC() and #compareCRC()', function() {
      //test frame and result taken from phytofy documentation
      it('Should return the correct CRC value', function() {
        let msg = [0xC0,0xA8,0x01,0x77,0x00,0x00,0x00,0x01,0x00,0x03,0x00];
        assert.deepEqual(compute.computeCRC2(msg, swap=0), [0xD1,0x37]);
      });
      it('Should return the correct CRC value', function() {
        let msg = [0x77,0x01,0xA8,0xC0,0x01,0x00,0x00,0x00,0x00,0x03,0x00];
        assert.deepEqual(compute.computeCRC2(msg, swap=1), [0x64,0x33]);
      });
      let testFrame = [0x02, 0x07];
      it('Should return the correct CRC value', function() {
        assert.deepEqual(compute.computeCRC(testFrame, swap=0), [0x12, 0x41]);
      });
      it('Should return the correct CRC value (#computeCRC2())', function() {
        assert.deepEqual(compute.computeCRC2(testFrame, swap=0), [0x12, 0x41]);
      });
      it('Should return true', function() {
        let testMsg = [0x02, 0x07, 0x41, 0x12];
        assert.equal(compute.compareCRC(testMsg, swap=1), true);
      });
      it('Should return false', function() {
        let testMsg = [0x02, 0x07, 0x13, 0xA1];
        assert.equal(compute.compareCRC(testMsg, swap=1), false);
      });
    });
    describe('#byte2dec()', function() {
      //test frame and result taken from phytofy documentation
      it('Should return the correct sequence number in decimal form', function() {
        let testNum = [0x06, 0x81, 0xA2, 0xE3];
        assert.equal(compute.byte2dec(testNum), 109159139);
      });
      it('Should return the correct sequence number in decimal form', function() {
        let testNum = [0x5B, 0x75, 0xAF, 0x1D];
        assert.equal(compute.byte2dec(testNum), 1534439197);
      });

    });
    describe('#dec2byte()', function() {
      //test frame and result taken from phytofy documentation
      it('Should return [0,0,0,3]', function() {
        let testNum = 3;
        assert.deepEqual(compute.dec2byte(testNum, 4), [0,0,0,3]);
      });
      it('Should return [0x10]', function() {
        let testNum = 16;
        assert.deepEqual(compute.dec2byte(testNum, 1), [0x10]);
      });
      it('Should return [0, 0, 0x01,0xAC, 0xC5]', function() {
        let testNum = 109765;
        assert.deepEqual(compute.dec2byte(testNum, 5), [0,0,0x01,0xAC,0xC5]);
      });
      it('Should return [0, 0, 0x01,0xAC, 0xC5]', function() {
        let testNum = 1534439197;
        assert.deepEqual(compute.dec2byte(testNum, 4), [0x5B, 0x75, 0xAF, 0x1D]);
      });
    });
    describe('#byte2uint()', function() {
      it('Should return 1483230300', function() {
        let testArr = [88,104,76,92];
        assert.deepEqual(compute.byte2uint(testArr), 1483230300);
      });
    });
    describe('#uint2byte()', function() {
      it('Should return [88,104,76,27]', function() {
        assert.deepEqual(compute.uint2byte(1483230235), [88,104,76,27]);
      });
      it('Should return [88,104,76,249]', function() {
        assert.deepEqual(compute.uint2byte(1483230457), [88,104,76,249]);
      });
    });
    describe('#byte2float()', function() {
      it('Should return 32.5', function() {
        let testArr = [66, 2, 0, 0];
        assert.deepEqual(compute.byte2float(testArr), 32.5);
      });
    });
    describe('#float2byte()', function() {
      it('Should return [66, 2, 0, 0]', function() {
        let testArr = 32.5;
        assert.deepEqual(compute.float2byte(32.5).reverse(), [66, 2, 0, 0]);
      });
    });
    describe('#arrDiff()', function() {
      let argh = [1,2,3,3,2,6,8,11];
      let testAgainst = [...Array(15).keys()].map(i => i + 1);
      it('Should return [4,5,7,9,10,12,13,14,15]', function() {
        assert.deepEqual(compute.arrDiff(testAgainst, argh), [4,5,7,9,10,12,13,14,15]);
      });
    });
  });
  describe('main class tests', function() {
    let comm = new proto.RS485protocol('127.0.0.1');

    describe('#findFixture()', function() {
      comm.moxaList = [
        { ip: '192.168.1.20',
        ports: [ 4001 ],
        mac: '0:144:232:111:186:20' },
      { ip: '192.168.1.21',
        ports: [ 4001 ],
        mac: '0:144:232:111:186:99' }
      ];

      comm.fixtureMap = [
        {serial_num: 1937465, slave_addr: 1, moxa_mac: '0:144:232:111:186:20', port: 4001},
        {serial_num: 9972445, slave_addr: 2, moxa_mac: '0:144:232:111:186:20', port: 4001},
        {serial_num: 6052331, slave_addr: 1, moxa_mac: '0:144:232:111:186:99', port: 4001},
        {serial_num: 1200339, slave_addr: 1, moxa_mac: '0:144:232:111:186:20', port: 4002},
      ];
      it('Should change fixture 6052331s slave addr to 2', async function() {
        await comm.pushToFixtureMap({serial_num: 6052331, slave_addr: 2, moxa_mac: '0:144:232:111:186:99', port: 4001})
        assert.deepEqual(comm.findFixture(6052331), [2, '192.168.1.21', 4001]);
      });
      it('Should return [1, 192.168.1.20, 4002]', function() {
        assert.deepEqual(comm.findFixture(1200339), [1, '192.168.1.20', 4002]);
      });
      it('Should return false', function() {
        assert.deepEqual(comm.findFixture(109564), [null, 'not found', 0]);
      });
    });
    describe('#waitForMsg()', function() {
      comm.responseList = [
        {sequence_num: 13, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]},
        {sequence_num: 10, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]}
      ];
      let response = undefined, idx = 0;
      it('Should return the response with sequence number 13.', async function() {
        [response, idx] = await comm.waitForMsg(13);
        assert.deepEqual(response.sequence_num, 13);
      });
      it('Should return undefined after timeout.', async function() {
        [response, idx] = await comm.waitForMsg(9);
        assert.deepEqual(response, undefined);
      });
    });
    describe('#storeResponses(), #waitForMsg() and #compareCRC()', function() {
      let msg = Buffer.from([1,0,0,127,0x0B,0x00,0x00,0x00,0x00,0x03,0x00, 76, 255])
      let rinfo = {address: '192.168.1.21', port: 5050};
      let response = 0, idx = 0;
      it('Should return the response with sequence number 11.', async function() {
        comm.storeResponses(msg, rinfo);
        [response, idx] = await comm.waitForMsg(11);
        logger.debug(`waited for: ${idx}`);
        assert.deepEqual(response.sequence_num, 11);
      });
      it('Should return true for correct CRC.', function() {
        assert.equal(compute.compareCRC(response.msg), true);
      });
    });
    describe('#updateFixtureMap()', async function() {
      let comm2 = new proto.RS485protocol('192.168.1.111');
      comm2.takenSlaves = [19,21,22,23,24,26,27,28,29,30,31,32,33,34,50,51,52,53,54,55];
      comm2.responseList = [
          {sequence_num: 2, serial_num: 149987, slave_addr: 1, function_code: 3, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]},
          //{sequence_num: 4, serial_num: 980182, slave_addr: 23, function_code: 3, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]},
          {sequence_num: 32, serial_num: 976901, slave_addr: 13, function_code: 3, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]},
          {sequence_num: 25, serial_num: 286100, slave_addr: 56, function_code: 3, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]},
          {sequence_num: 57, serial_num: 286100, slave_addr: 56, function_code: 4, msg: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]}
      ];
      it('Should return the number of stored fixtures', async function() {
        comm2.comissioningPORT = 4001;
        let added = await comm2.updateFixtureMap();
        assert.equal(added,3);
    });
    });
    describe('#cleanFixtureMap()', async function() {
      it('Should return 2 removed fixtures.', async function() {
        //console.log(`fixture map to be cleaned:`)
        //console.log(comm.fixtureMap);
        let removed = await comm.cleanFixtureMap('0:144:232:111:186:20', 4001);
        console.log(`REMOVEEEDDDD: ${removed}`);
        //console.log(comm.fixtureMap);
        assert.equal(removed,2);
      });
    });
  });
});
