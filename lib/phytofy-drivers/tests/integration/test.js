const discovery = require('../../lib/moxa-discovery.js');
const rs485 = require('../../lib/rs485-protocol.js');
const utils = require('../../src/moxa-discovery_utils.js');
const defines = require('../../src/defines.js');
const logger = require('../../src/logger.js');
const compute = require('../../src/compute.js');

async function testCommissioning(){
  //moxaList = await discovery.discoverMoxas();

  //logger.verbose(`[TEST] listing discovered moxas:`)
  //console.log(moxaList);

  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.102',
    ports: [ 4001 ],
    mac: '0:144:232:116:37:8' },
  { ip: '192.168.1.101',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  comm.fixtureMap = [
    { serial_num: 1003,
      slave_addr: 6,
      moxa_mac: '0:144:232:111:186:241',
      port: 4001 },
    { serial_num: 1004,
      slave_addr: 8,
      moxa_mac: '0:144:232:111:186:241',
      port: 4001 },
    { serial_num: 1006,
      slave_addr: 12,
      moxa_mac: '0:144:232:111:186:241',
      port: 4001 }
  ]

  let addedFixtures = await comm.commissionFixtures('0:144:232:111:186:241', [4001]);
  addedFixtures = await comm.commissionFixtures('0:144:232:116:37:8', [4001]);
  console.log(comm.fixtureMap);

};

async function uncommission(serialNum, port, mac){
  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.101',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:166' },
  { ip: '192.168.1.101',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  await comm.openConnection(port, mac);

  let ack = await comm.setSlaveAddress(serialNum, 255, 1);
  console.log(ack);

  await comm.closeConnection();
};

async function singleShotCommands(serialNum, port, mac, slave=1){

  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.102',
    ports: [ 4001 ],
    mac: '0:144:232:116:37:8' },
  { ip: '192.168.1.101',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  // comm.fixtureMap = [
  //   {serial_num: serialNum, slave_addr: slave, moxa_mac: mac, port: port}
  // ];
  comm.fixtureMap = [
  { serial_num: 1003,
    slave_addr: 1,
    moxa_mac: '0:144:232:111:186:241',
    port: 4001 },
  { serial_num: 1004,
    slave_addr: 2,
    moxa_mac: '0:144:232:111:186:241',
    port: 4001 },
  { serial_num: 204003,
    slave_addr: 2,
    moxa_mac: '0:144:232:116:37:8',
    port: 4001 }
  ];

  let con = await comm.openConnection(port, mac);
  //logger.debug(`connection: ${con}`);
  //let resp0 = await comm.setModuleCalibration(serialNum, 0, [0.15, 0.85, 8.65, 0.051, 0.95, 46.43, 0.33, 0.67, 17.71, 0.11, 0.89, 43.55, 0.73, 0.93, 19.14, 0.038, 0.96, 38.82])
  //let resp0 = await comm.setIlluminanceConfiguration(serialNum, [1.62, 1.54, 1.52, 1.5, 1.53, 1.54]);
  //console.log(resp0);

  //let resp = await comm.getFixtureInfo(serialNum);
  let resp = await comm.getLEDstate(serialNum, 'percentage');
  //let resp = await comm.getIlluminanceConfiguration(serialNum);
  //let resp = await comm.getModuleCalibration(serialNum, 1);
  console.log(resp);

  //let resp10 = await comm.resumeScheduling(serialNum);
  //let resp10 = await comm.stopScheduling(serialNum);
  //console.log(resp10);

  //let resp2 = await comm.getScheduleState(serialNum);
  //let resp2 = await comm.getSchedule(serialNum, 33);
  //console.log(resp2);

  //let resp8 = await comm.setSchedule(serialNum, 33, 1535646403,1535646643, [0,5,10,10,0,0]);
  //console.log(resp8);
  //let resp9 = await comm.deleteAllSchedules(serialNum);
  //let resp9 = await comm.deleteSchedule(serialNum, 101);
  //console.log(resp9);
  //let resp3 = await comm.getScheduleCount(serialNum);
  //console.log(resp3);

  /*let epoch = 1535636070;
  logger.verbose(`sending epoch: ${epoch}`);
  let resp5 = await comm.setTimeReference(serialNum, epoch);
  console.log(resp5);*/

  //let resp4 = await comm.getTimeReference(serialNum);
  //console.log(resp4);

  //let resp6 = await comm.getModuleTemperature(serialNum);
  //console.log(resp6);

  // for(let i = 0; i<4; i++){
  //   if(i == 3){
  //     cmd = [0, 0, 0, 0, 0, 0]
  //   }
  //   else if (i == 2) {
  //     cmd = [0, 5, 5, 0, 0, 0]
  //   }else if (i == 1) {
  //     cmd = [0, 0, 5, 0, 0, 5]
  //   }else if (i == 0) {
  //     cmd = [0, 2, 0, 10, 0, 5]
  //   }else{
  //     cmd = [0, 0, 0, 0, 0, 0]
  //   }
  //
  //   await comm.setLEDrt(1004,  cmd);
  //   await comm.setLEDrt(1003, cmd);
  //   await comm.goToSleep(500);
  // }

  let resp7 = await comm.setLEDrt(serialNum, [0, 0, 0, 0, 0, 0], 'irradiance');
  //let resp7 = await comm.setLEDrt(serialNum, [0, 10, 0, 0, 0, 0], 'percentage');
  console.log(resp7);
  //let resp11 = await comm.getSlaveAddress(serialNum);
  //let resp11 = await comm.setGroupId(serialNum, 101);
  //let resp11 = await comm.getGroupId(serialNum);
  //console.log(resp11);

  await comm.closeConnection();

};

async function netTests(port, mac){
  const comm = new rs485.RS485protocol('192.168.1.119');

  comm.moxaList = [
    { ip: '192.168.1.101',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:166' },
  { ip: '192.168.1.101',
    ports: [ 4001 ],
    mac: '0:144:232:111:186:241' }
  ];

  let c = await comm.openConnection(port, mac);
  //let ret = await comm.openConnection(port, mac);

  await comm.closeConnection();
  //let resp11 = await comm.getGroupId(1004);
  //console.log(resp11);

};
//uncommission(1004, 4001, '0:144:232:111:186:241');
//uncommission(1005, 4001, '0:144:232:111:186:166');

//ctestCommissioning();

//singleShotCommands(1004, 4001, '0:144:232:111:186:241');
singleShotCommands(204003, 4001, '0:144:232:116:37:8');

//singleShotCommands(1003, 4001, '0:144:232:111:186:241');

//netTests(4001, '0:144:232:111:186:241');
