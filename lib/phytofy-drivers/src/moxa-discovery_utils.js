/*

      returns true if the message from moxa server passes all the checks
      Takes msg as a stream of chars as argument

*/
const parseCheck = (stream) => {
  //check length
  if (stream.length != 24) return false;
  //check function code
  if (stream[0] != 0x81) return false;
  //check moxa manufacturer unique ID (first 3 bytes of moxa macaddress)
  if (stream[14] != 0 || stream[15] != 0x90 || stream[16] != 0xE8) return false;
  return true;
};

/*
    returns true if the address is already stored in msgQueue.
*/
const isRepeated = (address, moxaList) => {
  return moxaList.some (function (item){
    return item.ip == address;
  });
};

/*

      returns number of ports in moxa server, -1 if no valid value encountered
      Takes msg as a stream of chars as argument

*/
const getNumPorts = (stream) => {
  let num = 1;
  let ports = [
    {val: 1, nr: 1},
    {val: 2, nr: 2},
    {val: 4, nr: 4},
    {val: 7, nr: 8},
    {val: 8, nr: 16}
  ];
  let loworder = stream[13] & 15;

  ports.forEach( function(item){
    if (item.val == loworder) num = item.nr;
  });
  return num;
};

module.exports = {
  parseCheck,
  isRepeated,
  getNumPorts
};
