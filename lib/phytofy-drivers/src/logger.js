const {createLogger, format, transports}= require('winston');

const customFormat = format.printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const logger = createLogger({
  format: format.combine(
      format.label({label: 'phytofy-drivers'}),
      format.timestamp(),
      format.colorize(),
      customFormat
  ),
  transports: [
    new transports.Console({level: 'info'})
  ]
});

module.exports = logger;
