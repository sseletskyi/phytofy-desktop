const dgram = require('dgram');
const utils = require('../src/moxa-discovery_utils.js');
const defines = require('../src/defines.js');
const logger = require('../src/logger.js');
const compute = require('../src/compute.js');

/*
  Broadcast promise function
  55001 is the moxa port assigned for discovery

*/
function timedBroadcast(outPORT = 4800, sock, period, timeout, broadcastIp){
  return new Promise( (resolve, reject) => {
    //let sock = dgram.createSocket('udp4');
    let limit = timeout/period, counter = 0; //5 times
    //broadcast [0x01, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00]
    //in little endyan: [0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01]
    let broadcastMsg = Buffer.from(defines.MOXA_BROADCAST_MSG);

    //Broadcast message is sent every broadcastInterval miliseconds
    let broadcastInterval = setInterval( () => {
      sock.send(broadcastMsg, outPORT, broadcastIp, (err) => {
        if (err != null){
          logger.error(`[Moxa Discovery] Broadcast failed with error: ${err}. Stoping broadcast`);
          clearInterval(broadcastInterval);
          return resolve(-1);
        }else {
          logger.debug(`[Moxa Discovery] broadcasting: ${broadcastMsg} | count:${counter}`);
        }
      });
      counter ++;
      if (counter >= limit) {
        clearInterval(broadcastInterval);
        return resolve(1);
      }
    }, period);
  });
};

/*
      Moxa discovery function
      returns an object array of the type:
      moxaList= [

      ]

*/

async function discoverMoxas(broadcastIp=null){
  if(broadcastIp == null){
    broadcastIp = '255.255.255.255';
  }
  //let listenerSock = dgram.createSocket({type:'udp4', reuseAddr: true});
  let sock = dgram.createSocket('udp4');
  let moxaList = [];

  sock.on('close', () => {
    logger.verbose('[Moxa Discovery] closing socket.');
  });

  sock.on('listening', () => {
    sock.setBroadcast(true);
    logger.verbose('[Moxa Discovery] socket is ready');
  });

  sock.on('message', (msg, rinfo) => {
    let stream = [... msg];//.reverse(); //msg Buffer to array (byte-order is little endyan)
    logger.debug(`[Moxa Discovery] got: ${stream}, from ${rinfo.address}:${rinfo.port}`);

    if (utils.parseCheck(stream) && !utils.isRepeated(rinfo.address, moxaList) ){
      let numPorts = utils.getNumPorts(stream);
      if (numPorts != -1){
        moxaList.push({ip:rinfo.address, ports: [...Array(numPorts).keys()].map(i => defines.MOXA_START_PORT+i), mac: [stream[14], stream[15], stream[16], stream[17], stream[18], stream[19]].join(":") });
      }
      else {
        logger.verbose(`[Moxa Discovery] Number of ports is wrongly configured - moxa: ${rinfo.address}`);
      }
    }
  });

  sock.bind();

  await timedBroadcast(4800, sock, defines.BROADCAST_PERIOD, defines.BROADCAST_TIMEOUT, broadcastIp);

  sock.close();
  //might need to wait a bit more before closing listening socket

  return moxaList;
};


module.exports = {
  discoverMoxas
};
