import React from "react";
import styled from "styled-components";

export default styled.p`
  color: var(--red, #e53012);
  margin-top: 10px;
  padding-left: 5px;
`;
