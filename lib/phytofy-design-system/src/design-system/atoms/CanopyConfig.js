import React, { Component } from "react";
import styled from "styled-components";

const CanopyConfig = styled.img`
  width: 100%;
  height: auto;
`;

export default class extends Component {
  render() {
    return <CanopyConfig src={this.props.config} />;
  }
}
