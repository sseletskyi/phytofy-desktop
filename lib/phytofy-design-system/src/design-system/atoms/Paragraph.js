import React, { Component } from "react";
import styled, { css } from "styled-components";

const Paragraph = styled.p`
  display: block;
  font-size: 1rem;
  color: var(--textColor, #424242);

  & + button {
    margin-top: var(--spacing, 30px);
  }

  ${props =>
    props.bold &&
    css`
      font-weight: var(--bold, 700);
    `}
`;

export default class extends Component {
  render() {
    return <Paragraph bold={this.props.bold}>{this.props.children}</Paragraph>;
  }
}
