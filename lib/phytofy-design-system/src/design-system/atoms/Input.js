import React, { Component } from "react";
import styled, { css } from "styled-components";

const Input = styled.input`
  border: 0;
  border-bottom: 1px solid var(--sidebarPrimaryColor, #c5c6c8);
  width: 100%;
  padding-bottom: 10px;
  font-size: 1rem;
  color: var(--textColor, #424242);

  &::-webkit-search-decoration {
    display: none;
  }

  &::-webkit-search-cancel-button {
    display: none;
  }

  &::placeholder {
    color: #bdbdbd;
  }

  &:invalid {
    border-bottom: 1px solid var(--red, #E53012);
    color: var(--red, #E53012);
  }

  &[type=search] {
    background: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'><path fill='#c5c6c8' d='M20.667 18.667h-1.053l-0.373-0.36c1.6-1.867 2.427-4.413 1.973-7.12-0.627-3.707-3.72-6.667-7.453-7.12-5.64-0.693-10.387 4.053-9.693 9.693 0.453 3.733 3.413 6.827 7.12 7.453 2.707 0.453 5.253-0.373 7.12-1.973l0.36 0.373v1.053l5.667 5.667c0.547 0.547 1.44 0.547 1.987 0v0c0.547-0.547 0.547-1.44 0-1.987l-5.653-5.68zM12.667 18.667c-3.32 0-6-2.68-6-6s2.68-6 6-6 6 2.68 6 6-2.68 6-6 6z'></path></svg>") no-repeat top right;
    background-size: 24px 24px;
    padding-right: 30px;
  }

  ${props => props.type === "time" && css`
    font-family: 'Open Sans', sans-serif;
    font-size: 1rem;
  `}

  ${props => props.type === "file" && css`
    border-bottom: 0;
    padding: 5px 0;

    &::-webkit-file-upload-button {
      visibility: hidden;
    }

    &:before {
      content: 'Browse';
      background-color: var(--sidebarPrimaryColor, #C5C6C8);
      border-radius: 4px;
      padding: 5px 10px;
      color: var(--backgroundColor, #FFFFFF);
      appearance: none;
      cursor: pointer;
      transition-duration: .2s;
      outline: none;
      white-space: nowrap;
      -webkit-user-select: none;
    }

    &:hover::before {
      background-color: var(--darkGrey, #A7A8AA);
    }

    &:active {
      outline: 0;
    }

    :active::before {
      background-color: var(--sidebarPrimaryColor, #C5C6C8);
    }
  `}
`;

export default class extends Component {
  constructor(props) {
    super(props);
    // this.state = {value: this.props.value};
    // this.handleChange = this.handleChange.bind(this);
  }

  // handleChange(event) {
  //   this.setState({value: event.target.value});
  // }

  render() {
    return (
      <Input
        type={this.props.type}
        name={this.props.name}
        placeholder={this.props.placeholder}
        value={this.props.value}
        min={this.props.min}
        max={this.props.max}
        onChange={this.props.onChange} />
    );
  }
}
