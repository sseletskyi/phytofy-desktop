import React, { Component } from "react";
import styled, { css } from "styled-components";

const Text = styled.span`
  font-size: 1rem;
  color: var(--textColor, #424242);

  ${props =>
    props.bold &&
    css`
      font-weight: var(--bold, 700);
    `}

  ${props =>
    props.italic &&
    css`
      font-style: italic;
    `}
`;

export default class extends Component {
  render() {
    return (
      <Text bold={this.props.bold} italic={this.props.italic}>
        {this.props.children}
      </Text>
    );
  }
}
