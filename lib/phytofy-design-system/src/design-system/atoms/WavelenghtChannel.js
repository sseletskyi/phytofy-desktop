import React from "react";
import styled, { css } from "styled-components";

const WavelenghtChannel = styled.li`
  display: flex;
  justify-content: center;
  align-items: center;
  list-style: none;
  border-width: 2px;
  border-style: solid;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  font-size: .75rem;
  font-weight: var(--bold, 700);
  color: var(--white, #FFFFFF);

  ${props =>
    props.name === "UV" &&
    css`
      background-color: var(--channelUV, #6a1b9a);
      border-color: var(--channelUV, #6a1b9a);
    `}

  ${props =>
    props.name === "B" &&
    css`
      background-color: var(--channelBlue, #61aff1);
      border-color: var(--channelBlue, #61aff1);
    `}

  ${props =>
    props.name === "G" &&
    css`
      background-color: var(--channelGreen, #09e425);
      border-color: var(--channelGreen, #09e425);
    `}

  ${props =>
    props.name === "HR" &&
    css`
      background-color: var(--channelHRed, #c62828);
      border-color: var(--channelHRed, #c62828);
    `}

  ${props =>
    props.name === "FR" &&
    css`
      background-color: var(--channelFRed, #ac2a18);
      border-color: var(--channelFRed, #ac2a18);
    `}

  ${props =>
    props.name === "W" &&
    css`
      background-color: var(--channelWhite, #a7a8aa);
      border-color: var(--channelWhite, #a7a8aa);
    `}

  ${props =>
    props.state === false &&
    css`
      background: transparent;
      border-color: var(--topbarBorder, #eceded);
      color: var(--topbarBorder, #eceded);
    `}
`;

function Channel(props) {
  return (
    <WavelenghtChannel name={props.name} state={props.state}>
      {props.name}
    </WavelenghtChannel>
  );
}

export default Channel;
