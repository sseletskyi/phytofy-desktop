import React from "react";
import styled from "styled-components";

import Button from "../atoms/Button";

const ActionButtonWrapper = styled.div`
  button {
    display: flex;
    flex-direction: row;
    align-items: center;
    background: transparent;
    border: 0;
    min-width: 30px;
    height: 30px;
    padding: 0;
    appearance: none;
    cursor: pointer;

    svg {
      width: auto;
      height: 30px;
      fill: var(--secondaryColor, #87888a);
      transition-duration: 0.2s;
    }

    span {
      margin-left: 5px;
      color: var(--secondaryColor, #87888a);
      white-space: nowrap;
    }
  }

  &:not(:last-child) {
    margin-right: 10px;
  }

  &:hover {
    svg {
      fill: var(--darkGrey, #a7a8aa);
    }

    span {
      color: var(--darkGrey, #a7a8aa);
    }
  }
`;

function ActionButton(props) {
  return (
    <ActionButtonWrapper>
      <Button
        icon={props.icon}
        name={props.name}
        onClick={props.onClick}
        tooltip={props.tooltip}
        flow={props.flow}
      />
    </ActionButtonWrapper>
  );
}

export default ActionButton;
