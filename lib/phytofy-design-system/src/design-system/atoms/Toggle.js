import React, { Component } from "react";
import styled from "styled-components";

import Label from "./Label";

const Toggle = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap;

  > label a {
    transition-duration: 0.2s;

    &:hover {
      color: var(--primaryColor, #ff6600);
      cursor: pointer;
    }
  }

  > div label {
    margin: 0 0.5rem 0;
    color: var(--textColor, #424242);
    cursor: pointer;
  }

  input[type="radio"] {
    position: relative;
    display: inline-block;
    margin: 0;
    width: 24px;
    height: 100%;
    opacity: 0;
    z-index: 1;
    cursor: pointer;
  }

  .switcher {
    display: block;
    position: absolute;
    top: -3px;
    left: 0;
    right: 100%;
    width: 29px;
    height: 29px;
    border-radius: 50%;
    background-color: var(--primaryColor, #ff6600);
    transition: all 0.1s ease-out;
    z-index: 2;
  }

  .background {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
    border-radius: 25px;
    background-color: var(--backgroundPrimaryMenu, #f5f6f8);
    transition: all 0.1s ease-out;
  }

  #right:checked ~ .switcher {
    right: 0;
    left: calc(50% - 2px);
  }

  #right:checked ~ .background {
    background-color: var(--textColor, #424242);
  }
`;

const Wrapper = styled.span`
  position: relative;
  display: inline-block;
  border-radius: 25px;
  border: 1px solid var(--topbarBorder, #eceded);
  width: 50px;
  height: 25px;
  vertical-align: middle;
`;

export default class extends Component {
  render() {
    return (
      <Toggle>
        <Label label={this.props.label} />

        <div>
          <label htmlFor="left">{this.props.leftLabel}</label>
          <Wrapper onClick={this.props.onClick}>
            <input type="radio" name={this.props.name} id="left" />
            <input
              type="radio"
              name={this.props.name}
              id="right"
              checked={this.props.rightChecked}
            />
            <span aria-hidden="true" className="background" />
            <span aria-hidden="true" className="switcher" />
          </Wrapper>
          <label htmlFor="right">{this.props.rightLabel}</label>
        </div>
      </Toggle>
    );
  }
}
