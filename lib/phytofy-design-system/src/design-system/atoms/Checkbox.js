import React from 'react';
import styled from 'styled-components';

const CheckboxContainer = styled.label `
  display: block;
  position: relative;
  padding-left: 0;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &:hover input ~ span {
    border-color: var(--secondaryColor, #E0E0E1);
  }
`

const CheckboxLabel = styled.span `
  padding-left: 35px;
`

const CheckboxInput = styled.input `
  position: absolute;
  opacity: 0;
  cursor: pointer;

  &:checked ~ span {
    background-color: var(--secondaryColor, #E0E0E1);
    border-color: var(--secondaryColor, #E0E0E1);

    &:after {
      display: block;
    }
  }
`

const Checkmark = styled.span `
  position: absolute;
  top: 0;
  left: 0;
  width: 20px;
  height: 20px;
  border: 3px solid #E0E0E1;
  border-radius: 4px;
  transition-duration: .2s;

  &:after {
    content: "";
    position: absolute;
    display: none;
    left: 5px;
    width: 5px;
    height: 12px;
    border: solid #FFFFFF;
		border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`

function Checkbox(props) {
  return (
    <CheckboxContainer>
      <CheckboxLabel>{props.checkboxLabel}</CheckboxLabel>
      <CheckboxInput type="checkbox" name={props.name} checked={props.checked} onChange={props.onChange} />
      <Checkmark></Checkmark>
    </CheckboxContainer>
  );
}

export default Checkbox;
