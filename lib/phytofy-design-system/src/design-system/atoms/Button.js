import React, { Component } from "react";
import styled, { css } from "styled-components";

import Icon from "./Icons";

const Button = styled.button.attrs({
  tooltip: props => props.tooltip,
  flow: props => props.flow
})`

  border: 0;
  border-radius: 4px;
  height: 36px;
  padding: 0 26px;
  color: var(--backgroundColor, #FFFFFF);
  appearance: none;
  cursor: pointer;
  transition-duration: .2s;

  ${props =>
    props.state === "success" &&
    css`
      background-color: var(--green, #009d3d);

      &:hover {
        background-color: var(--darkGreen, #006d0e);
      }
    `}

  ${props =>
    props.state === "cancel" &&
    css`
      background-color: var(--sidebarPrimaryColor, #c5c6c8);

      &:hover {
        background-color: var(--darkGrey, #a7a8aa);
      }
    `}

  ${props =>
    props.state === "danger" &&
    css`
      background-color: var(--red, #e53012);

      &:hover {
        background-color: #d84315;
      }
    `}

  &:disabled {
    background-color: var(--backgroundPrimaryColor, #FAFAFA);
    color: var(--darkGrey, #A7A8AA);
  }

  svg {
    width: auto;
    height: 26px;
  }
`;

const Text = styled.span`
  font-size: 1rem;
`;

export default class extends Component {
  render() {
    const icon = this.props.icon;
    const name = this.props.name;

    return (
      <Button
        disabled={this.props.disabled}
        state={this.props.state}
        type="button"
        onClick={this.props.onClick}
        tooltip={this.props.tooltip}
        flow={this.props.flow}
      >
        {icon ? <Icon icon={this.props.icon} fill={this.props.fill} /> : ""}
        {name ? <Text>{this.props.name}</Text> : ""}
      </Button>
    );
  }
}
