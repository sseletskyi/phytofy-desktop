import React, { Component } from "react";
import { FlexibleXYPlot, HeatmapSeries, Hint } from "react-vis";
import styled from "styled-components";

const Canopy = styled.div`
  position: relative;
  width: max-content;
  max-width: 100%;
  margin: 0 auto;

  .tray {
    z-index: 1;

    .canopy-bed {
      display: flex;
      max-width: 100%;

      svg {
        > g {
          rect {
            &.canopy {
              fill: #e3e4e5;
            }
          }

          > g {
            rect {
              fill: var(--primaryColor, #ff6600);
            }
          }
        }
      }
    }
  }
`;

const IrradianceMap = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 2;

  .rv-hint {
    background-color: rgba(0, 0, 0, 0.5);
    border-radius: 4px;
    padding: 10px;
    color: var(--backgroundColor, #ffffff);
  }
`;

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null
    };
  }

  _forgetValue = () => {
    this.setState({
      value: null
    });
  };

  _rememberValue = value => {
    this.setState({ value });
  };

  render() {
    let trayLayout;
    const { value } = this.state;

    switch (this.props.type) {
      case "1v":
        var fixture1x = this.props.length / 2 - this.props.fixtureWidth / 2;
        var fixture1y = this.props.width / 2 - this.props.fixtureLength / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture1x + " " + fixture1y + ")"}
            />
          </g>
        );
        break;

      case "1h":
        var fixture1x = this.props.length / 2 - this.props.fixtureLength / 2;
        var fixture1y = this.props.width / 2 - this.props.fixtureWidth / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture1x + " " + fixture1y + ")"}
            />
          </g>
        );
        break;

      case "2v":
        var fixture1x =
          this.props.length / 2 -
          this.props.fixtureWidth -
          this.props.spacing / 2;
        var fixture2x = this.props.length / 2 + this.props.spacing / 2;
        var fixtureY = this.props.width / 2 - this.props.fixtureLength / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture1x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture2x + " " + fixtureY + ")"}
            />
          </g>
        );
        break;

      case "2h":
        var fixture1x =
          this.props.length / 2 -
          this.props.fixtureLength -
          this.props.spacing / 2;
        var fixture2x = this.props.length / 2 + this.props.spacing / 2;
        var fixtureY = this.props.width / 2 - this.props.fixtureWidth / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture1x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture2x + " " + fixtureY + ")"}
            />
          </g>
        );
        break;

      case "3v":
        var fixture1x =
          this.props.length / 2 -
          this.props.fixtureWidth -
          this.props.fixtureWidth / 2 -
          this.props.spacing;
        var fixture2x = this.props.length / 2 - this.props.fixtureWidth / 2;
        var fixture3x =
          this.props.length / 2 +
          this.props.fixtureWidth / 2 +
          this.props.spacing / 1;
        var fixtureY = this.props.width / 2 - this.props.fixtureLength / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture1x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture2x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture3x + " " + fixtureY + ")"}
            />
          </g>
        );
        break;

      case "2x2h":
        var fixture1x =
          this.props.length / 2 -
          this.props.fixtureLength -
          this.props.spacing / 2;
        var fixture2x = this.props.length / 2 + this.props.spacing / 2;
        var fixtureYtop =
          this.props.width / 2 -
          this.props.fixtureWidth -
          this.props.spacing / 2;
        var fixtureYbottom = this.props.width / 2 + this.props.spacing / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture1x + " " + fixtureYtop + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture2x + " " + fixtureYtop + ")"}
            />

            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture1x + " " + fixtureYbottom + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureLength}
              height={this.props.fixtureWidth}
              transform={"translate(" + fixture2x + " " + fixtureYbottom + ")"}
            />
          </g>
        );
        break;

      case "4v":
        var fixture1x =
          this.props.length / 2 -
          this.props.fixtureWidth * 2 -
          this.props.spacing * 1.5;
        var fixture2x =
          this.props.length / 2 -
          this.props.fixtureWidth -
          this.props.spacing / 2;
        var fixture3x = this.props.length / 2 + this.props.spacing / 2;
        var fixture4x =
          this.props.length / 2 +
          this.props.fixtureWidth * 1 +
          this.props.spacing * 1.5;
        var fixtureY = this.props.width / 2 - this.props.fixtureLength / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture1x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture2x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture3x + " " + fixtureY + ")"}
            />
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture4x + " " + fixtureY + ")"}
            />
          </g>
        );
        break;

      default:
        var fixture1x = this.props.length / 2 - this.props.fixtureWidth / 2;
        var fixture1y = this.props.width / 2 - this.props.fixtureLength / 2;

        trayLayout = (
          <g>
            <rect
              x="0"
              y="0"
              width={this.props.fixtureWidth}
              height={this.props.fixtureLength}
              transform={"translate(" + fixture1x + " " + fixture1y + ")"}
            />
          </g>
        );
    }

    return (
      <Canopy
        type={this.props.type}
        length={this.props.length}
        width={this.props.width}
        spacing={this.props.spacing}
        fixtureLength={this.props.fixtureLength}
        fixtureWidth={this.props.fixtureWidth}
      >
        <IrradianceMap>
          <FlexibleXYPlot margin={{ top: 0, left: 0, right: 0, bottom: 0 }}>
            {!this.props.isHidden && (
              <HeatmapSeries
                colorType="literal"
                data={
                  this.props.data == null
                    ? [{ x: 1, y: 1, color: "#000000", opacity: 0.8 }]
                    : this.props.data
                }
                onValueMouseOver={this._rememberValue}
                onValueMouseOut={this._forgetValue}
              />
            )}

            {value ? (
              <Hint value={value}>
                {[value.irradianceValue, " μmol/m", <sup>2</sup>, "/s"]}
              </Hint>
            ) : null}
          </FlexibleXYPlot>
        </IrradianceMap>
        <div className="tray">
          <div className="canopy-bed">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width={this.props.length}
              height="auto"
              viewBox={"0 0 " + this.props.length + " " + this.props.width}
            >
              <g>
                <rect
                  className="canopy"
                  width={this.props.length}
                  height={this.props.width}
                />
                {trayLayout}
              </g>
            </svg>
          </div>
        </div>
      </Canopy>
    );
  }
}
