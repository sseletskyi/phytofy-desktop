import React, { Component } from "react";
import styled from "styled-components";

const Label = styled.label`
  display: block;
  font-size: 0.75rem;
  color: var(--sidebarPrimaryColor, #c5c6c8);

  &:has(+ input:invalid) {
    color: var(--red, #e53012);
  }
`;

export default class extends Component {
  render() {
    return <Label>{this.props.label}</Label>;
  }
}
