import React, { Component } from "react";
import styled, { css } from "styled-components";

import Text from "../atoms/Text";
import LoadingBar from "../molecules/LoadingBar";

const Commissioning = styled.div`
  background-color: rgba(255, 255, 255, 0.85);
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  z-index: 999;

  @supports (-webkit-backdrop-filter: none) or (backdrop-filter: none) {
    background-color: transparent;
    -webkit-backdrop-filter: blur(8px);
  }

  span {
    margin-bottom: 10px;
    font-weight: var(--bold, 700);
    color: var(--primaryColor, #ff6600);
    text-transform: uppercase;
  }
`;

export default class extends Component {
  render() {
    return (
      <Commissioning>
        <Text>{this.props.text}</Text>
        <LoadingBar progress={this.props.progress} />
      </Commissioning>
    );
  }
}
