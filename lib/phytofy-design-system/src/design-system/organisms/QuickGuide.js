import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import Title from "../atoms/Title";
import image from "../../images/quick-guide.svg";

const QuickGuide = styled.div`
  display: grid;
  grid-template-columns: 500px 1fr;
  grid-gap: var(--spacing, 30px);
  height: calc(100vh - 120px);

  h1 {
    margin-bottom: var(--spacing, 30px);
  }
`;

const QuickGuideImage = styled.div`
  position: relative;

  img {
    position: absolute;
    top: 0;
    left: 0;
    max-height: calc(100vh - 120px);
  }
`;

const Glossary = styled.div`
  ul {
    list-style: none;

    li {
      &:first-child {
        color: #c70063;
      }

      &:nth-child(2) {
        color: #0097bc;
      }

      &:nth-child(3) {
        color: #553989;
      }

      &:not(:last-child) {
        margin-bottom: 5px;
      }
    }
  }
`;

const Instructions = styled.div`
  margin-top: var(--spacing, 30px);

  ol {
    list-style: none;
    counter-reset: counterList;

    li {
      counter-increment: counterList;
      max-width: 250px;
      height: 30px;
      padding-left: 40px;

      &:before {
        position: absolute;
        margin-left: -40px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 2px solid #8ab92a;
        border-radius: 100%;
        width: 30px;
        height: 30px;
        font-weight: var(--bold, 700);
        color: #8ab92a;
        content: counter(counterList);
        transition-duration: 0.5s;
      }

      &:not(:last-child) {
        margin-bottom: 5px;
      }

      &:hover {
        &:before {
          border-color: var(--green, #009d3d);
          background-color: var(--green, #009d3d);
          color: var(--backgroundColor, #ffffff);
        }

        > a {
          background-color: var(--green, #009d3d);
        }
      }

      > a {
        display: flex;
        align-items: center;
        border-radius: 4px;
        background-color: #8ab92a;
        height: 30px;
        padding: 10px;
        text-decoration: none;
        color: var(--backgroundColor, #ffffff);
        transition-duration: 0.5s;
      }
    }
  }

  ul {
    display: none;
    margin-top: var(--spacing, 30px);
    list-style: none;

    a {
      color: hsl(24, 100%, 50%);
      transition-duration: 0.5s;

      &:hover {
        color: hsl(24, 100%, 40%);
      }
    }
  }
`;

export default class extends Component {
  render() {
    const glossaryList = this.props.glossary.map(value => (
      <li key={value.key}>{value.value}</li>
    ));

    const instructionsList = this.props.instructions.map(value => (
      <li>
        <Link to={value.to}>{value.value}</Link>
      </li>
    ));

    const externalLinks = this.props.links.map(value => (
      <li>
        <a href={value.url}>{value.value}</a>
      </li>
    ));

    return (
      <QuickGuide>
        <div>
          <Glossary>
            <Title title="Glossary" />
            <ul>{glossaryList}</ul>
          </Glossary>
          <Instructions>
            <Title title="Instructions" />
            <ol>{instructionsList}</ol>
            <ul>{externalLinks}</ul>
          </Instructions>
        </div>
        <QuickGuideImage>
          <img src={image} alt="Glossary image example" />
        </QuickGuideImage>
      </QuickGuide>
    );
  }
}
