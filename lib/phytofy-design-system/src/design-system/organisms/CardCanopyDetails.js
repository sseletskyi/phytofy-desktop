import React, { Component } from "react";
import styled, { css } from "styled-components";

import Tag from "../atoms/Tag";

const CardCanopyDetails = styled.div`
  display: flex;
  flex-direction: column;
  background-color: var(--backgroundColor, #ffffff);
  border-width: 4px 1px 1px 1px;
  border: 1px solid var(--topbarBorder, #eceded);
  border-top: 4px solid var(--primaryColor, #ff6600);
  border-radius: 4px;

  ${props =>
    props.state === "danger" &&
    css`
      border-top: 4px solid var(--red, #e53012);

      h1 {
        color: var(--red, #e53012) !important;
      }
    `};
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: nowrap;
  margin-bottom: var(--spacing, 30px);
  padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

  h1 {
    color: var(--primaryColor, #ff6600);
  }

  button {
    background: transparent;
    border: 0;
    color: var(--textColor, #424242);
    transition-duration: 0.2s;
    cursor: pointer;

    &:hover {
      color: var(--primaryColor, #ff6600);
    }
  }
`;

const Body = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: var(--spacing, 30px);
  height: 100%;
  position: relative;
  padding: 0 var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);

  @media all and (min-width: 970px) {
    grid-template-columns: 50% 50%;
  }

  h1 {
    margin: var(--spacing, 30px) 0;
    color: var(--primaryColor, #ff6600);
  }

  table tbody tr td:last-child {
    width: 60px;
  }
`;

export default class extends Component {
  render() {
    return (
      <CardCanopyDetails state={this.props.state}>
        <Header>
          <h1>{this.props.title}</h1>
          <button onClick={this.props.action}>
            <Tag tag="Edit" />
          </button>
        </Header>
        <Body>{this.props.children}</Body>
      </CardCanopyDetails>
    );
  }
}
