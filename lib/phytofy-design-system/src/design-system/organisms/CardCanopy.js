import React, { Component } from "react";
import { ResponsiveStream } from "@nivo/stream";
import { linearGradientDef } from "@nivo/core";
import styled from "styled-components";

import CanopyConfig from "../atoms/CanopyConfig";
import CardValue from "../molecules/CardValue";

const CardCanopy = styled.div`
  background-color: var(--backgroundColor, #ffffff);
  border-width: 4px 1px 1px 1px;
  border: 1px solid var(--topbarBorder, #eceded);
  border-top: 4px solid var(--primaryColor, #ff6600);
  border-radius: 4px;

  &:hover {
    cursor: pointer;
  }
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  margin-bottom: var(--spacing, 30px);
  padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

  h1 {
    color: var(--primaryColor, #ff6600);
  }
`;

const Body = styled.div`
  padding: 0 var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);
`;

const Configuration = styled.div`
  display: grid;
  grid-template-columns: 154px minmax(154px, 1fr);
  grid-gap: var(--spacing, 30px);
  border-bottom: 1px solid var(--backgroundPrimaryMenu, #f5f6f8);
  margin-bottom: 20px;
  padding-bottom: 20px;

  > div {
    background-color: #e3e4e5;
    height: 80px;
  }

  img {
    width: auto;
    height: 80px;
  }
`;

const Legends = styled.div`
  border-bottom: 1px solid var(--backgroundPrimaryMenu, #f5f6f8);
  margin-bottom: 20px;
  padding-bottom: 20px;
  text-align: center;
  font-weight: var(--bold, 700);
`;

const Values = styled.div`
  margin-top: 20px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 1px;
  background-color: var(--backgroundPrimaryMenu, #f5f6f8);
`;

export default class extends Component {
  render() {
    const cardValues = this.props.values.map(value => (
      <CardValue
        key={value.key}
        icon={value.icon}
        value={value.value}
        unit={value.unit}
        legend={value.legend}
        tooltip={value.tooltip}
        flow={value.flow}
      />
    ));

    const data = this.props.data;

    return (
      <CardCanopy onClick={this.props.onClick}>
        <Header>
          <h1>{this.props.title}</h1>
        </Header>
        <Body>
          <Configuration>
            <div>
              <CanopyConfig config={this.props.configImage} />
            </div>
            <div>
              <ResponsiveStream
                data={data}
                keys={["spectrum", "hidden"]}
                height={80}
                margin={{
                  top: 10,
                  right: 0,
                  bottom: 0,
                  left: 0
                }}
                axisTop={null}
                axisRight={null}
                axisBottom={null}
                axisLeft={null}
                enableGridX={false}
                curve="natural"
                offsetType="none"
                fillOpacity={1}
                isInteractive={false}
                enableStackTooltip={false}
                defs={[
                  linearGradientDef("spectrumGradient", [
                    { offset: 0, color: "rgb(64, 0, 64" },
                    { offset: 16, color: "rgb(255, 0, 255)" },
                    { offset: 36, color: "rgb(0, 0, 255)" },
                    { offset: 38, color: "rgb(0, 255, 255)" },
                    { offset: 43, color: "rgb(0, 255, 0)" },
                    { offset: 56, color: "rgb(255, 255, 0)" },
                    { offset: 69, color: "rgb(255, 0, 0)" },
                    { offset: 96, color: "rgb(255, 0, 0)" },
                    { offset: 100, color: "rgb(64, 0, 0)" }
                  ])
                ]}
                fill={[{ match: { id: "spectrum" }, id: "spectrumGradient" }]}
                animate={true}
                motionStiffness={90}
                motionDamping={15}
              />
            </div>
          </Configuration>

          <Legends>
            {this.props.configDesc} — {this.props.recipeName}
          </Legends>

          <Values>{cardValues}</Values>
        </Body>
      </CardCanopy>
    );
  }
}
