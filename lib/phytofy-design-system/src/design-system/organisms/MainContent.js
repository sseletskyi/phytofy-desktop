import styled, { css } from 'styled-components';

const MainContent = styled.div`
	background-color: var(--backgroundColor, #FFFFFF);
	grid-row: 1 / 2;
	grid-column: 2 / 4;
	min-height: 100vh;
	overflow-y: auto;
	z-index: 4;
	transition: margin 0.4s;

	${ props => props.isSecondaryMenuOpen && css`
		margin-left: var(--subMenuWidth, 200px);
	` }

	${ props => props.isThirdMenuOpen && css`
		margin-left: calc(var(--subMenuWidth, 200px) * 2);
	` }
`
export default MainContent;
