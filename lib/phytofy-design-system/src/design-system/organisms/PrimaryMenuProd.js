import React, { Component } from "react";
import styled from "styled-components";

import MenuLink from "../molecules/MenuLinkProd";

const PrimaryMenu = styled.div`
  --menuWidth: 80px;
  --menuLinkHeight: 40px;

  position: absolute;
  margin-top: 60px;
  background-color: var(--backgroundPrimaryMenu, #f5f6f8);
  grid-row: 2 / 3;
  grid-column: 1 / 2;
  width: var(--menuWidth, 80px);
  height: calc(100vh - 60px);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: var(--spacing, 30px) 0 0 0;
  z-index: 6;
  transition-duration: 0.4s;

  &::-webkit-scrollbar {
    display: none;
  }

  &::-webkit-scrollbar {
    display: none;
  }

  ul {
    margin-bottom: var(--spacing, 30px);
  }
`;

const BottomMenu = styled.ul`
  margin: 0;
`;

export default class extends Component {
  render() {
    const menuLink = this.props.values.map(value => (
      <MenuLink
        key={value.key}
        to={value.location}
        icon={value.icon}
        tooltip={value.tooltip}
        isActive={this.props.checkIsActive(value.location)}
      />
    ));

    return (
      <PrimaryMenu isOpen={this.props.isOpen}>
        <ul>{menuLink}</ul>
        <BottomMenu>{this.props.children}</BottomMenu>
      </PrimaryMenu>
    );
  }
}
