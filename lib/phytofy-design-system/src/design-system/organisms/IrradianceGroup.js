import React, { Component } from "react";
import styled, { css } from "styled-components";

const IrradianceGroup = styled.div`
  margin-bottom: var(--spacing, 30px);
  display: grid;
  grid-template-columns: repeat(1, minmax(500px, 1fr));
  grid-template-rows: 1fr 1fr;
  grid-gap: var(--spacing, 30px);

  @media all and (min-width: 1280px) {
    grid-template-columns: repeat(2, minmax(500px, 1fr));
    grid-template-rows: 1fr;
  }

  ${props =>
    props.irradiance === false &&
    css`
      > div:first-child {
        > div:first-child {
          > div:nth-child(2) {
            > div input:last-child {
              display: none;
            }
          }

          > div > div:last-child {
            display: none;
          }
        }

        > div:last-child {
          > div > div {
            margin-top: 20px;

            > input:last-child {
              display: none;
            }
          }
        }
      }
    `}
`;

export default class extends Component {
  render() {
    return (
      <IrradianceGroup irradiance={this.props.irradiance}>
        {this.props.children}
      </IrradianceGroup>
    );
  }
}
