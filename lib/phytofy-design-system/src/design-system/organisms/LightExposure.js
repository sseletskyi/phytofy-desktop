import React, { Component } from "react";
import styled from "styled-components";

const LightExposure = styled.div`
  margin: var(--spacing, 30px) 0;
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-gap: var(--spacing, 30px);

  > div {
    max-width: 300px;
  }

  @media all and (min-width: 800px) {
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: 1fr;
  }
`;

export default class extends Component {
  render() {
    return <LightExposure>{this.props.children}</LightExposure>;
  }
}
