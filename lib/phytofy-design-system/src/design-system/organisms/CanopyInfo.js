import React, { Component } from "react";
import styled from "styled-components";

const CanopyInfo = styled.div`
  margin-bottom: var(--spacing, 30px);
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
  grid-auto-rows: minmax(min-content, max-content);
  grid-gap: 15px;
  padding-bottom: var(--spacing, 30px);
`;

export default class extends Component {
  render() {
    return <CanopyInfo>{this.props.children}</CanopyInfo>;
  }
}
