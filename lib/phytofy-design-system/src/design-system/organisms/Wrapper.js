import React, { Component } from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  --menuWidth: 80px;
  --topBarHeight: 60px;
  --rightSidebarWidth: 300px;

  display: grid;
  grid-template-rows: var(--topBarHeight, 60px) auto;
  grid-template-columns: var(--menuWidth, 80px) auto var(
      --rightSidebarWidth,
      300px
    );
  width: 100vw;
  height: 100vh;
  overflow: hidden;
`;

export default class MenuLink extends Component {
  render() {
    return <Wrapper>{this.props.children}</Wrapper>;
  }
}
