import styled from "styled-components";

const RightSidebarContent = styled.div`
  padding: calc(var(--topBarHeight, 60px) + var(--spacing, 30px))
    var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);
`;

export default RightSidebarContent;
