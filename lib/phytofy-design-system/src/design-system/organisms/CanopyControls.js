import React, { Component } from "react";
import styled from "styled-components";

const CanopyControls = styled.div`
  margin-top: var(--spacing, 30px);
  display: flex;
  flex-direction: row;
  justify-content: center;

  > div {
    display: flex;
    justify-content: center;
    width: 50%;

    > label {
      width: 100%;
      margin-bottom: 15px;
      text-align: center;
    }
  }
`;

export default class extends Component {
  render() {
    return <CanopyControls>{this.props.children}</CanopyControls>;
  }
}
