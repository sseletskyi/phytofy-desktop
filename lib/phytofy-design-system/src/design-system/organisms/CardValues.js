import React, { Component } from "react";
import styled from "styled-components";

// import CanopyConfig from '../atoms/CanopyConfig';
import CardValue from "../molecules/CardValue";

const CardValues = styled.div`
  background-color: var(--backgroundColor, #ffffff);
  border-width: 4px 1px 1px 1px;
  border: 1px solid var(--topbarBorder, #eceded);
  border-top: 4px solid var(--primaryColor, #ff6600);
  border-radius: 4px;
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  margin-bottom: var(--spacing, 30px);
  padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

  h1 {
    color: var(--primaryColor, #ff6600);
  }
`;

const Body = styled.div`
  padding: 0 var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);
`;

const Values = styled.div`
  margin-top: 20px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-row-gap: 1px;
  grid-column-gap: 1px;
  background-color: var(--backgroundPrimaryMenu, #f5f6f8);
`;

export default class extends Component {
  render() {
    const cardValues = this.props.values.map(value => (
      <CardValue
        key={value.key}
        icon={value.icon}
        fill={value.fill}
        value={value.value}
        unit={value.unit}
        legend={value.legend}
        tooltip={value.tooltip}
        flow={value.flow}
      />
    ));

    return (
      <CardValues>
        <Header>
          <h1>{this.props.title}</h1>
        </Header>
        <Body>
          <Values>{cardValues}</Values>
        </Body>
      </CardValues>
    );
  }
}
