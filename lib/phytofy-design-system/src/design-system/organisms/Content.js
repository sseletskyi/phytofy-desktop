import styled, { css } from 'styled-components';

const Content = styled.div`
  padding: calc(60px + var(--spacing, 30px)) var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);

  ${props => props.cardList && css`
    display: grid;
    grid-gap: var(--spacing, 30px);
    grid-template-columns: repeat(auto-fill, minmax(400px, 1fr));
  `}

  ${props => props.details && css`
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: var(--spacing, 30px);

    @media all and (min-width: 970px) {
      grid-template-columns:  repeat(2, minmax(400px, 1fr));

      > div:first-child {
        grid-column: 1 / 3;
        grid-row: 1 / 2;
      }

      > div:nth-child(2) {
          grid-column: 1 / 2;
          grid-row: 2 / 3;
      }

      > div:nth-child(3) {
          grid-column: 2 / 3;
          grid-row: 2 / 3;
      }

      > div:last-child {
          grid-column: 1 / 3;
          grid-row: 3 / 4;
      }
    }
  `}
`

export default Content;
