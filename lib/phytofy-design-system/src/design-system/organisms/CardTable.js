import React, { Component } from "react";
import styled from "styled-components";

import CardActionButton from "../molecules/CardActionButtons";
import CardActionNavigation from "../molecules/CardActionNavigation";
import FormGroup from "../molecules/FormGroup";

const CardTable = styled.div`
  background-color: var(--backgroundColor, #ffffff);
  border-width: 4px 1px 1px 1px;
  border: 1px solid var(--topbarBorder, #eceded);
  border-top: 4px solid var(--primaryColor, #ff6600);
  border-radius: 4px;
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-between;
  margin-bottom: var(--spacing, 30px);
  padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

  > div {
    display: flex;
    flex-direction: row;
    align-items: center;

    h1 {
      color: var(--primaryColor, #ff6600);
    }
  }
`;

const Actions = styled.div`
  justify-content: flex-end;
  color: var(--secondaryColor, #87888a);
`;

const Body = styled.div`
  padding: 0;
`;

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = { goto: props.from };
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.from != prevProps.from) {
      this.setState({ goto: this.props.from });
    }
  }

  handleKeyPress(event) {
    if (event.key === "Enter") {
      let page = parseInt(event.target.value);
      if (!isNaN(page)) this.props.pageGoTo(parseInt(event.target.value));
    }
  }

  handleChange(event) {
    this.setState({ goto: event.target.value });
  }

  render() {
    let header;

    switch (this.props.header) {
      case true:
        header = (
          <Header>
            <div>
              <h1>{this.props.title}</h1>
            </div>
            <Actions>
              <FormGroup
                format="minimal"
                type="select"
                options={this.props.options}
              />
              <CardActionNavigation
                from={this.props.from}
                to={this.props.to}
                of={this.props.of}
                pageBack={this.props.pageBack}
                pageNext={this.props.pageNext}
              />
              <CardActionButton values={this.props.actions} />
            </Actions>
          </Header>
        );
        break;

      default:
        header = "";
    }

    return (
      <CardTable>
        {header}
        <Body>{this.props.children}</Body>
      </CardTable>
    );
  }
}
