import React, { Component } from "react";
import styled, { css } from "styled-components";

import RightSidebarTopbar from "../organisms/RightSidebarTopbar";
import RightSidebarContent from "../organisms/RightSidebarContent";

const RightSidebar = styled.div`
  position: absolute;
  width: 0;
  height: 100vh;
  color: var(--sidebarPrimaryColor, #c5c6c8);
  background-color: transparent;
  overflow: hidden;
  z-index: 7;
  right: 0;
  transition: transform 0.4s, background-color 0.3s;
  transition-delay: background-color 2s;

  > div {
    right: calc(-100% - 300px);
    transition: right 0.4s ease;
  }

  img {
    margin-bottom: 30px;
  }

  ${props =>
    props.isOpen &&
    css`
      display: initial;
      width: 100vw;
      background-color: rgba(66, 66, 66, 0.9);
      transform: translateX(0);

      > div {
        right: 0;
      }
    `}

  ${props =>
    props.type === "xl" &&
    css`
      > div {
        width: 100%;

        > div {
          width: 100%;

          > div {
            width: 100%;
          }
        }
      }
    `}
`;

const RightSidebarContainer = styled.div`
  position: absolute;
  right: 0;
  background-color: var(--backgroundColor, #ffffff);
  width: 300px;
  height: 100%;
  box-shadow: -2px 0 6px rgba(0, 0, 0, 0.05);
  overflow-y: scroll;
`;

export default class extends Component {
  render() {
    return (
      <RightSidebar type={this.props.type} isOpen={this.props.isOpen}>
        <RightSidebarContainer>
          <RightSidebarTopbar
            goBack={this.props.goBack}
            label={this.props.label}
            title={this.props.title}
            closeRightSidebar={this.props.closeRightSidebar}
          />
          <RightSidebarContent>{this.props.children}</RightSidebarContent>
        </RightSidebarContainer>
      </RightSidebar>
    );
  }
}
