import React, { Component } from "react";
import styled from "styled-components";

import Logo from "../atoms/Logos";

const Topbar = styled.div`
  left: 0;
  grid-row: 1 / 2;
  grid-column: 1 / 4;
  position: fixed;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid var(--topbarBorder, #eceded);
  background-color: var(--backgroundColor, #ffffff);
  width: 100%;
  height: var(--topBarHeight, 60px);
  z-index: 1000;

  ul {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    width: 100%;
    height: 100%;

    li {
      --tabWidth: 150px;

      list-style: none;
      border-left-width: 1px;
      border-style: solid;
      border-color: var(--topbarBorder, #eceded);
      width: var(--tabWidth, 150px);

      &.active {
        a {
          color: var(--primaryColor, #ff6600) !important;
        }

        &:before {
          position: absolute;
          bottom: 0;
          width: var(--tabWidth, 150px);
          height: 5px;
          background-color: var(--primaryColor, #ff6600);
          content: "";
        }
      }

      &:last-child {
        border-right-width: 1px;
      }

      a {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        font-weight: var(--bold, 700);
        color: var(--secondaryColor, #87888a);
        text-decoration: none;
        cursor: pointer;
      }
    }
  }
`;

const LogoContainer = styled.div`
  height: 20px;
  padding: 0 var(--spacing, 30px);
  box-sizing: border-box;
`;

export default class extends Component {
  render() {
    return (
      <Topbar>
        <LogoContainer>
          <Logo logo="osramLogo" />
        </LogoContainer>

        {this.props.children}
      </Topbar>
    );
  }
}
