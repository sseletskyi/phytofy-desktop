import React, { Component } from "react";
import styled, { css } from "styled-components";

const Card = styled.div`
  display: flex;
  flex-direction: column;
  background-color: var(--backgroundColor, #ffffff);
  border-width: 4px 1px 1px 1px;
  border: 1px solid var(--topbarBorder, #eceded);
  border-top: 4px solid var(--primaryColor, #ff6600);
  border-radius: 4px;

  ${props =>
    props.state === "danger" &&
    css`
      border-top: 4px solid var(--red, #e53012);

      h1 {
        color: var(--red, #e53012) !important;
      }
    `};
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  margin-bottom: var(--spacing, 30px);
  padding: var(--spacing, 30px) var(--spacing, 30px) 0 var(--spacing, 30px);

  h1 {
    color: var(--primaryColor, #ff6600);
  }
`;

const Body = styled.div`
  height: 100%;
  position: relative;
  padding: 0 var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px);

  h1 {
    margin: var(--spacing, 30px) 0;
    color: var(--primaryColor, #ff6600);
  }

  button:not(:last-child) {
    margin-right: 10px;
  }
`;

export default class extends Component {
  render() {
    return (
      <Card state={this.props.state}>
        <Header>
          <h1>{this.props.title}</h1>
        </Header>
        <Body>{this.props.children}</Body>
      </Card>
    );
  }
}
