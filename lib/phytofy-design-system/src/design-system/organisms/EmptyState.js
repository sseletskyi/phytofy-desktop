import React, { Component } from "react";
import styled from "styled-components";

import Button from "../atoms/Button";
import emptyStateTable from "../../images/emtpyState-table.svg";
import emptyStateCard from "../../images/emtpyState-card.svg";

const EmptyState = styled.div`
  text-align: center;

  img {
    margin-bottom: 50px;
    width: 60%;
    max-width: 700px;
    height: auto;
  }

  div {
    max-width: 500px;
    margin: 0 auto;

    > span {
      display: block;
      font-size: 1.5rem;
      color: #e0e0e1;
    }

    button {
      margin-top: 30px;
    }
  }
`;

const Image = styled.img``;

export default class extends Component {
  render() {
    let image;

    switch (this.props.type) {
      case "table":
        image = <Image src={emptyStateTable} />;
        break;
      case "card":
        image = <Image src={emptyStateCard} />;
        break;
      default:
        image = <Image src={emptyStateTable} />;
    }

    return (
      <EmptyState type={this.props.type}>
        {image}
        <div>
          <span>{this.props.message}</span>
          <Button
            state="success"
            name={this.props.action}
            onClick={this.props.onClick}
          />
        </div>
      </EmptyState>
    );
  }
}
