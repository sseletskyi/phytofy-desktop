import React, { Component } from "react";

import Content from "../organisms/Content";
import ContentHeader from "../organisms/ContentHeader";
import CardActionGoTo from "../molecules/CardActionGoTo";
import CardActionNavigation from "../molecules/CardActionNavigation";
import CardActionButton from "../molecules/CardActionButtons";
import CardTable from "../organisms/CardTable";
import Table from "../organisms/Table";
import TableHeaderCell from "../molecules/TableHeaderCell";
import TableRow from "../molecules/TableRow";
import TableRowOptions from "../molecules/TableRowOptions";
import TableData from "../molecules/TableData";
import Icon from "../atoms/Icons";
import FormGroup from "../molecules/FormGroup";
import Tag from "../atoms/Tag";

/* Card Action Buttons */

const actionButtons = [
  {
    icon: "add",
    onclick: ids => {
      console.log("click");
    }
  }
];

/* Filter */

const canopyConfig = [
  { value: "1v", name: "1V" },
  { value: "1h", name: "1H" },
  { value: "2v", name: "2V" },
  { value: "2h", name: "2H" },
  { value: "3v", name: "3V" },
  { value: "2x2h", name: "2x2H" },
  { value: "3v", name: "4V" }
];

/* Table Content */

const header = [
  <TableHeaderCell selectable>
    <FormGroup type="checkbox" value="selectAll" />
  </TableHeaderCell>,
  <TableHeaderCell orderAsc>Recipe</TableHeaderCell>,
  <TableHeaderCell>Canopy</TableHeaderCell>
];

class Recipes extends Component {
  constructor(props) {
    super(props);
    this.state = { goto: props.from };
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.from !== prevProps.from) {
      this.setState({ goto: this.props.from });
    }
  }

  handleKeyPress(event) {
    if (event.key === "Enter") {
      let page = parseInt(event.target.value, 10);
      if (!isNaN(page)) this.props.pageGoTo(parseInt(event.target.value, 10));
    }
  }

  handleChange(event) {
    this.setState({ goto: event.target.value });
  }

  render() {
    return (
      <div>
        <Content>
          <ContentHeader>
            <div>
              <CardActionGoTo
                goto="1"
                onKeyPress={this.handleKeyPress}
                onChange={this.handleChange}
              />
              <CardActionNavigation
                from="1"
                to="10"
                of="100"
                pageBack={this.props.pageBack}
                pageNext={this.props.pageNext}
              />
              <CardActionButton values={actionButtons} />
            </div>
            <div>
              <FormGroup
                type="search"
                label="Search"
                placeholder="Lorem Ipsum"
                name="Search Fixture"
                value={this.state.text}
                error=""
                onChange={this.handleInputChange}
              />
              <FormGroup
                type="select"
                label="Select Name"
                options={canopyConfig}
                error=""
              />
            </div>
          </ContentHeader>

          <CardTable
            title="Recipes"
            actions={actionButtons}
            goto="1"
            from="1"
            to="10"
            of="100"
          >
            <Table header={header}>
              <TableRow>
                <TableData selectable>
                  {/* <Checkbox /> */}
                  <FormGroup type="checkbox" value="Id-1" />
                </TableData>
                <TableData>Recipe #01</TableData>
                <TableData>
                  <Tag tag="Canopy #01" />
                  <Tag tag="Canopy #09" />
                  <Tag tag="Canopy #10" />
                </TableData>
                <TableRowOptions>
                  <button>
                    <Icon icon="edit" tooltip="Edit" />
                  </button>
                  <button>
                    <Icon icon="duplicate" tooltip="Duplicate" />
                  </button>
                  <button>
                    <Icon icon="delete" tooltip="Delete" />
                  </button>
                </TableRowOptions>
              </TableRow>
            </Table>
          </CardTable>
        </Content>
      </div>
    );
  }
}

export default Recipes;
