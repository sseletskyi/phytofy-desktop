import React, { Component } from "react";
import Content from "../organisms/Content";
import ProductInfo from "../molecules/ProductInfo";

const info = [
  { key: 1, value: "Version 0.8.2 (0.8.2)" },
  { key: 2, value: "© 2019, OSRAM GmbH. All rights reserved." },
  { key: 3, value: "www.osram.com/horticulture" },
  { key: 4, value: "horticulture@osram.de" }
];

export default class extends Component {
  render() {
    return (
      <div>
        <Content>
          <ProductInfo title={["PHYTOFY", <sup>®</sup>, " RL"]} values={info} />
        </Content>
      </div>
    );
  }
}
