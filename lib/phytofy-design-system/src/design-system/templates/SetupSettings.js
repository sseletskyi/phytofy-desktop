import React, { Component } from "react";

import CheckboxGroup from "../molecules/CheckboxGroup";
import FormGroup from "../molecules/FormGroup";
import Content from "../organisms/Content";
import Card from "../organisms/Card";
import Paragraph from "../atoms/Paragraph";
import Button from "../atoms/Button";

const dropdownOptions = [
  { value: "1", name: "Option 1" },
  { value: "2", name: "Option 2" },
  { value: "3", name: "Option 3" },
  { value: "4", name: "Option 4" }
];

class Setup extends Component {
  render() {
    return (
      <div>
        <Content cardList>
          <Card title="Units of Measure">
            <FormGroup type="select" label="Units" options={dropdownOptions} />
            <FormGroup
              type="select"
              label="Irradiance"
              options={dropdownOptions}
            />
            <FormGroup
              type="select"
              label="Irradiance Map Colors"
              options={dropdownOptions}
            />
            <FormGroup
              type="select"
              label="Results per Page"
              options={dropdownOptions}
            />
            <Button
              state="success"
              name="Save"
              onClick="console.log('click')"
            />
          </Card>

          <Card title="Export">
            <CheckboxGroup>
              <FormGroup type="checkbox" checkboxLabel="Canopy Profiles" />
              <FormGroup type="checkbox" checkboxLabel="Recipes" />
            </CheckboxGroup>
            <Button
              state="success"
              name="Download"
              onClick="console.log('click')"
            />
          </Card>

          <Card title="Backup/Restore">
            <FormGroup type="file" label="Maximum file size is 1GB" />
            <Button
              state="success"
              name="Upload"
              onClick="console.log('upload')"
            />
            <Button
              state="success"
              name="Download"
              onClick="console.log('download')"
            />
          </Card>

          <Card title="Moxa & Ports Settings">
            <FormGroup
              type="text"
              label="Communication Port"
              placeholder="0000"
              value=""
            />
            <FormGroup
              type="text"
              label="Subnet Mask"
              placeholder="255.255.255.240"
              value=""
            />
            <Button
              state="success"
              name="Save"
              onClick="console.log('click')"
            />
          </Card>

          <Card state="danger" title="Reset">
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit vivamus
              pharetra sed elit eget sollicitudin.
            </Paragraph>
            <Button
              state="danger"
              name="Reset"
              onClick="console.log('click')"
            />
          </Card>
        </Content>
      </div>
    );
  }
}

export default Setup;
