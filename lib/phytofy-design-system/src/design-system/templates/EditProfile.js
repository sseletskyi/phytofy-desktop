import React, { Component } from "react";

import Button from "../atoms/Button";
import CanopyConfig from "../atoms/CanopyConfig";
import Form from "../organisms/Form";
import FormGroup from "../molecules/FormGroup";
import FormActions from "../molecules/FormActions";
// import ConnectedFixturesList from "../molecules/ConnectedFixturesList";
// import FixtureItem from "../atoms/FixtureItem";

import Config2V from "../../images/2v.svg";

/* Canopy Configuration on Sidebar */

const canopyConfig = [
  { value: "1v", name: "1V" },
  { value: "1h", name: "1H" },
  { value: "2v", name: "2V" },
  { value: "2h", name: "2H" },
  { value: "3v", name: "3V" },
  { value: "2x2h", name: "2x2H" },
  { value: "3v", name: "4V" }
];

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = { isRightSidebarClosed: true, isSecondaryMenuClosed: true };
  }

  render() {
    return (
      <div>
        <CanopyConfig config={Config2V} />

        <Form>
          {/* <FormGroup
            type="text"
            label="Input Name"
            placeholder="example text"
            name="text"
            value={this.state.text}
            error=""
            onChange={this.handleInputChange} /> */}

          <FormGroup
            type="select"
            label="Select Name"
            options={canopyConfig}
            error=""
          />

          <FormGroup
            type="text"
            label="Name"
            value={this.state.text}
            error=""
            onChange={this.handleInputChange}
          />

          <FormGroup
            type="number"
            label="Length (mm)"
            value={this.state.text}
            min="0"
            max="10"
            error=""
            onChange={this.handleInputChange}
          />

          <FormGroup
            type="number"
            label="Width (mm)"
            value={this.state.text}
            error=""
            onChange={this.handleInputChange}
          />

          {/* <ConnectedFixturesList label="Connected Fixtures">
            <FixtureItem value="OSR074F2B29-H9" />
            <FixtureItem value="OSR074F2B29-H6" />
            <FixtureItem value="" addText="Connected fixture" />
          </ConnectedFixturesList> */}

          <FormActions>
            <Button
              state="cancel"
              name="Cancel"
              onClick={this.props.closeRightSidebar}
            />
            <Button
              state="success"
              name="Submit"
              onClick="console.log('click')"
            />
          </FormActions>
        </Form>
      </div>
    );
  }
}

export default EditProfile;
