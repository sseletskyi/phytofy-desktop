import React, { Component } from "react";
import Content from "../organisms/Content";
import Terms from "../molecules/Terms";
import Paragraph from "../atoms/Paragraph";
import Text from "../atoms/Text";

export default class extends Component {
  render() {
    return (
      <div>
        <Content>
          <Terms>
            <Paragraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Velit
              euismod in pellentesque massa placerat duis ultricies lacus. Lacus
              vestibulum sed arcu non odio euismod. Faucibus scelerisque
              eleifend donec pretium vulputate sapien nec. Et malesuada fames ac
              turpis egestas maecenas.{" "}
              <Text bold italic>
                Vitae sapien
              </Text>{" "}
              pellentesque habitant morbi tristique senectus. Tellus rutrum
              tellus pellentesque eu tincidunt tortor. Molestie nunc non blandit
              massa enim nec dui nunc mattis. Faucibus a pellentesque sit amet.
              Ante in nibh mauris cursus mattis molestie. Porttitor leo a diam
              sollicitudin tempor id eu. Enim ut tellus elementum sagittis
              vitae. Volutpat ac tincidunt vitae semper quis lectus. Amet
              consectetur adipiscing elit duis tristique. Nisl suscipit
              adipiscing bibendum est ultricies integer quis auctor. Enim nunc
              faucibus a pellentesque sit amet. Enim nulla aliquet porttitor
              lacus luctus accumsan tortor posuere ac. Nunc pulvinar sapien et
              ligula ullamcorper malesuada proin. Leo a diam sollicitudin tempor
              id eu nisl nunc. Elit sed vulputate mi sit amet mauris commodo
              quis imperdiet."
            </Paragraph>
            <Paragraph bold>
              Vitae aliquet nec ullamcorper sit amet. Dignissim enim sit amet
              venenatis urna cursus. Ac felis donec et odio pellentesque. Morbi
              tristique senectus et netus et malesuada fames ac. Sed turpis
              tincidunt id aliquet risus. Vel turpis nunc eget lorem dolor sed
              viverra ipsum nunc. Fringilla phasellus faucibus scelerisque
              eleifend donec pretium vulputate sapien. Tristique senectus et
              netus et. Volutpat est velit egestas dui id ornare arcu. Facilisis
              leo vel fringilla est ullamcorper eget nulla. Libero justo laoreet
              sit amet cursus sit."
            </Paragraph>
            <Paragraph>
              Vitae tortor condimentum lacinia quis vel. Id ornare arcu odio ut.
              Pellentesque habitant morbi tristique senectus et netus et. Enim
              sed faucibus turpis in eu mi bibendum neque egestas. In
              pellentesque massa placerat duis ultricies lacus. Senectus et
              netus et malesuada fames ac turpis egestas. Id diam vel quam
              elementum pulvinar etiam non quam. Pharetra diam sit amet nisl
              suscipit adipiscing bibendum. Sagittis aliquam malesuada bibendum
              arcu vitae. Quis enim lobortis scelerisque fermentum dui. Pretium
              lectus quam id leo in vitae turpis massa sed. Enim sit amet
              venenatis urna cursus eget nunc scelerisque viverra. Turpis cursus
              in hac habitasse platea dictumst quisque sagittis purus. Tristique
              nulla aliquet enim tortor. Turpis egestas integer eget aliquet.
              Nibh praesent tristique magna sit."
            </Paragraph>
            <Paragraph>
              Ac turpis egestas integer eget. Porttitor eget dolor morbi non.
              Augue lacus viverra vitae congue eu consequat ac felis donec. Nisi
              porta lorem mollis aliquam ut porttitor. Felis donec et odio
              pellentesque. Venenatis lectus magna fringilla urna. Vitae tempus
              quam pellentesque nec nam aliquam sem et tortor. Dignissim diam
              quis enim lobortis scelerisque fermentum dui faucibus in. Nibh
              venenatis cras sed felis eget velit aliquet sagittis. Urna id
              volutpat lacus laoreet non curabitur gravida arcu. Arcu felis
              bibendum ut tristique et egestas quis ipsum."
            </Paragraph>
            <Paragraph>
              Integer vitae justo eget magna fermentum iaculis. Pellentesque
              dignissim enim sit amet venenatis urna cursus. Etiam erat velit
              scelerisque in dictum non consectetur. Aenean et tortor at risus
              viverra adipiscing at in. Pellentesque habitant morbi tristique
              senectus et netus et malesuada fames. In dictum non consectetur a
              erat. Cras sed felis eget velit aliquet sagittis. Euismod quis
              viverra nibh cras pulvinar. Dolor sit amet consectetur adipiscing
              elit duis tristique sollicitudin. Fermentum odio eu feugiat
              pretium nibh ipsum consequat nisl. Risus feugiat in ante metus
              dictum at tempor commodo ullamcorper. Ultricies lacus sed turpis
              tincidunt id aliquet risus. Ultricies mi quis hendrerit dolor
              magna eget. Quis vel eros donec ac odio tempor orci. Id eu nisl
              nunc mi. At lectus urna duis convallis convallis. Amet nisl
              suscipit adipiscing bibendum. Ullamcorper morbi tincidunt ornare
              massa eget. Varius sit amet mattis vulputate enim nulla aliquet
              porttitor lacus. Fermentum iaculis eu non diam phasellus
              vestibulum lorem sed."
            </Paragraph>
            <Paragraph>
              Velit aliquet sagittis id consectetur purus ut. Morbi quis commodo
              odio aenean sed. Adipiscing tristique risus nec feugiat in
              fermentum posuere. Etiam dignissim diam quis enim lobortis
              scelerisque fermentum. Pulvinar elementum integer enim neque
              volutpat ac tincidunt vitae semper. Ultrices vitae auctor eu augue
              ut lectus arcu bibendum. Et odio pellentesque diam volutpat
              commodo sed egestas egestas. Purus faucibus ornare suspendisse sed
              nisi lacus sed. Amet massa vitae tortor condimentum lacinia quis
              vel eros. Diam maecenas ultricies mi eget mauris pharetra. Mattis
              nunc sed blandit libero volutpat. Facilisi etiam dignissim diam
              quis enim lobortis scelerisque. Quisque sagittis purus sit amet
              volutpat consequat mauris nunc congue. Eleifend mi in nulla
              posuere. Consectetur adipiscing elit duis tristique sollicitudin
              nibh sit amet commodo. Vitae proin sagittis nisl rhoncus mattis
              rhoncus urna neque. Consectetur libero id faucibus nisl tincidunt
              eget nullam non nisi. Turpis egestas maecenas pharetra convallis
              posuere morbi leo urna molestie. Integer feugiat scelerisque
              varius morbi enim. Gravida neque convallis a cras semper auctor
              neque vitae tempus."
            </Paragraph>
            <Paragraph>
              Elementum facilisis leo vel fringilla est ullamcorper eget.
              Dictumst quisque sagittis purus sit amet volutpat. Augue interdum
              velit euismod in. Magna fermentum iaculis eu non diam phasellus
              vestibulum lorem. At augue eget arcu dictum. Morbi tempus iaculis
              urna id volutpat lacus. Sit amet massa vitae tortor condimentum
              lacinia quis vel. At lectus urna duis convallis convallis tellus
              id interdum velit. Ipsum nunc aliquet bibendum enim facilisis
              gravida neque. Orci porta non pulvinar neque laoreet suspendisse
              interdum. Congue mauris rhoncus aenean vel elit scelerisque mauris
              pellentesque pulvinar."
            </Paragraph>
            <Paragraph>
              Phasellus egestas tellus rutrum tellus pellentesque eu. Non enim
              praesent elementum facilisis leo vel. Commodo ullamcorper a lacus
              vestibulum sed arcu. Consectetur libero id faucibus nisl. Lorem
              ipsum dolor sit amet consectetur adipiscing elit duis tristique.
              Accumsan sit amet nulla facilisi morbi tempus iaculis urna id. Ac
              auctor augue mauris augue neque gravida in fermentum. Suspendisse
              ultrices gravida dictum fusce ut placerat orci nulla. Cursus
              turpis massa tincidunt dui ut ornare lectus sit amet. Quis
              imperdiet massa tincidunt nunc. Sit amet justo donec enim. Nulla
              facilisi etiam dignissim diam quis enim lobortis scelerisque
              fermentum. Sit amet purus gravida quis blandit turpis cursus in
              hac."
            </Paragraph>
            <Paragraph>
              Lorem dolor sed viverra ipsum nunc aliquet bibendum enim
              facilisis. Nam at lectus urna duis convallis. Neque ornare aenean
              euismod elementum nisi quis eleifend quam adipiscing. Sed augue
              lacus viverra vitae. Bibendum arcu vitae elementum curabitur.
              Egestas sed sed risus pretium quam. Malesuada pellentesque elit
              eget gravida cum sociis natoque penatibus. Non odio euismod
              lacinia at quis risus sed. Id eu nisl nunc mi ipsum faucibus.
              Vitae et leo duis ut diam. Aenean pharetra magna ac placerat
              vestibulum lectus. Mollis aliquam ut porttitor leo. Nec sagittis
              aliquam malesuada bibendum. Volutpat diam ut venenatis tellus in."
            </Paragraph>
            <Paragraph>
              Gravida rutrum quisque non tellus orci ac. Nisl tincidunt eget
              nullam non nisi est sit amet. Nulla malesuada pellentesque elit
              eget gravida cum. Id ornare arcu odio ut sem nulla. Sit amet
              consectetur adipiscing elit duis tristique sollicitudin nibh sit.
              Blandit massa enim nec dui nunc mattis enim ut. Quis lectus nulla
              at volutpat diam ut venenatis tellus in. Interdum consectetur
              libero id faucibus nisl tincidunt eget nullam non. Faucibus purus
              in massa tempor nec. Purus in massa tempor nec feugiat nisl
              pretium fusce. Vitae suscipit tellus mauris a. Enim tortor at
              auctor urna. Nascetur ridiculus mus mauris vitae ultricies leo
              integer malesuada. Facilisis sed odio morbi quis commodo odio
              aenean. Nunc pulvinar sapien et ligula ullamcorper malesuada proin
              libero nunc. Augue ut lectus arcu bibendum at varius vel pharetra.
              Urna porttitor rhoncus dolor purus non enim praesent elementum
              facilisis."
            </Paragraph>
          </Terms>
        </Content>
      </div>
    );
  }
}
