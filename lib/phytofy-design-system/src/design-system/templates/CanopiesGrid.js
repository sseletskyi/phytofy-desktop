import React, { Component } from "react";

import Content from "../organisms/Content";
import CardCanopy from "../organisms/CardCanopy";
import Config1H from "../../images/1h.svg";

const values = [
  {
    key: 1,
    icon: "fixture",
    value: "100",
    unit: "%",
    legend: "μmol/m<sup>2</sup>/s",
    tooltip: "Example"
  },
  {
    key: 2,
    icon: "time",
    value: "2",
    unit: "",
    legend: "Days Left",
    tooltip: "Example"
  },
  {
    key: 3,
    icon: "irradiance",
    value: "100",
    unit: "%",
    legend: "μmol/m<sup>2</sup>/d<sup>-1</sup>",
    tooltip: "Example"
  }
];

const data = [
  { spectrum: 18, hidden: 0 },
  { spectrum: 92, hidden: 0 },
  { spectrum: 39, hidden: 0 },
  { spectrum: 55, hidden: 0 },
  { spectrum: 142, hidden: 0 },
  { spectrum: 121, hidden: 0 },
  { spectrum: 135, hidden: 0 },
  { spectrum: 147, hidden: 0 },
  { spectrum: 100, hidden: 200 }
];

class CanopiesGridList extends Component {
  render() {
    function handleClick(e) {
      e.preventDefault();
      window.location = "/overview/demo";
    }

    return (
      <div>
        <Content cardList>
          <CardCanopy
            title="Canopy #01"
            configImage={Config1H}
            configDesc="Lorem Ipsum (1H)"
            values={values}
            recipeName="Recipe #1"
            data={data}
            onClick={handleClick}
          />
        </Content>
      </div>
    );
  }
}

export default CanopiesGridList;
