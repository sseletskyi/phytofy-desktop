import React, { Component } from "react";
import html2canvas from "html2canvas";

import Content from "../organisms/Content";
import CardCanopyDetails from "../organisms/CardCanopyDetails";
import CardValues from "../organisms/CardValues";
import CardTable from "../organisms/CardTable";
import Canopy from "../atoms/Canopy";
import Toggle from "../atoms/Toggle";
import CanopyControls from "../organisms/CanopyControls";
import CanopyInfo from "../organisms/CanopyInfo";
import Table from "../organisms/Table";
import TableHeaderCell from "../molecules/TableHeaderCell";
import TableRow from "../molecules/TableRow";
import TableRowOptions from "../molecules/TableRowOptions";
import TableData from "../molecules/TableData";
import Icon from "../atoms/Icons";
import FormGroup from "../molecules/FormGroup";
import Tag from "../atoms/Tag";

/* Card Values */

const colorChannels = [
  {
    key: 1,
    icon: "fixture",
    fill: "var(--channelUV, rgb(106, 27, 154))",
    value: "00",
    unit: "%",
    legend: "Ultraviolet"
  },
  {
    key: 2,
    icon: "fixture",
    fill: "var(--channelGreen, rgb(9, 228, 37))",
    value: "00",
    unit: "%",
    legend: "Green"
  },
  {
    key: 3,
    icon: "fixture",
    fill: "var(--channelBlue, rgb(97, 175, 241))",
    value: "00",
    unit: "%",
    legend: "Blue"
  },
  {
    key: 4,
    icon: "fixture",
    fill: "var(--channelHyperRed, rgb(198, 40, 40))",
    value: "00",
    unit: "%",
    legend: "Hyper-Red"
  },
  {
    key: 5,
    icon: "fixture",
    fill: "var(--channelFarRed, rgb(172, 42, 24))",
    value: "00",
    unit: "%",
    legend: "Far-Red"
  },
  {
    key: 6,
    icon: "fixture",
    fill: "var(--channelWarmWhite, rgb(167, 168, 170))",
    value: "00",
    unit: "%",
    legend: "White"
  }
];

const maxIrradiance = [
  {
    key: 1,
    icon: "fixture",
    fill: "var(--channelUV, rgb(106, 27, 154))",
    value: "00",
    legend: ["μmol/m<sup>2</sup>/s"],
    // legend: "Ultraviolet"
    // legend: ["μmol/m", <sup>2</sup>, "/s"],
    tooltip: "Ultraviolet"
  },
  {
    key: 2,
    icon: "fixture",
    fill: "var(--channelGreen, rgb(9, 228, 37))",
    value: "00",
    // unit: "%",
    // legend: "Blue"
    legend: ["μmol/m<sup>2</sup>/s"],
    tooltip: "Green"
  },
  {
    key: 3,
    icon: "fixture",
    fill: "var(--channelBlue, rgb(97, 175, 241))",
    value: "00",
    // unit: "%",
    // legend: "Green"
    legend: ["μmol/m<sup>2</sup>/s"],
    tooltip: "Blue"
  },
  {
    key: 4,
    icon: "fixture",
    fill: "var(--channelHyperRed, rgb(198, 40, 40))",
    value: "00",
    // unit: "%",
    // legend: "Hyper-Red"
    legend: ["μmol/m<sup>2</sup>/s"],
    tooltip: "Hyper-Red"
  },
  {
    key: 5,
    icon: "fixture",
    fill: "var(--channelFarRed, rgb(172, 42, 24))",
    value: "00",
    // unit: "%",
    // legend: "Far-Red"
    legend: ["μmol/m<sup>2</sup>/s"],
    tooltip: "Far-Red"
  },
  {
    key: 6,
    icon: "fixture",
    fill: "var(--channelWarmWhite, rgb(167, 168, 170))",
    value: "00",
    // unit: "%",
    // legend: "White"
    legend: ["μmol/m<sup>2</sup>/s"],
    tooltip: "White"
  }
];

/* Irradiance Map */

const irradianceData = [
  { key: 1, x: 1, y: 1, color: "red", opacity: 0.8, irradianceValue: 0 },
  { key: 2, x: 2, y: 1, color: "red", opacity: 0.8, irradianceValue: 0 },
  { key: 3, x: 3, y: 1, color: "yellow", opacity: 0.8, irradianceValue: 0 },
  { key: 4, x: 4, y: 1, color: "red", opacity: 0.8, irradianceValue: 0 },
  { key: 5, x: 5, y: 1, color: "orange", opacity: 0.8, irradianceValue: 0 },
  { key: 6, x: 6, y: 1, color: "blue", opacity: 0.8, irradianceValue: 0 },
  { key: 7, x: 1, y: 2, color: "red", opacity: 0.8, irradianceValue: 0 },
  { key: 8, x: 2, y: 2, color: "blue", opacity: 0.8, irradianceValue: 0 },
  { key: 9, x: 3, y: 2, color: "yellow", opacity: 0.8, irradianceValue: 0 },
  { key: 10, x: 4, y: 2, color: "yellow", opacity: 0.8, irradianceValue: 0 },
  { key: 11, x: 5, y: 2, color: "orange", opacity: 0.8, irradianceValue: 0 },
  { key: 12, x: 6, y: 2, color: "green", opacity: 0.8, irradianceValue: 0 },
  { key: 13, x: 1, y: 3, color: "green", opacity: 0.8, irradianceValue: 0 },
  { key: 14, x: 2, y: 3, color: "orange", opacity: 0.8, irradianceValue: 0 },
  { key: 15, x: 3, y: 3, color: "red", opacity: 0.8, irradianceValue: 0 },
  { key: 16, x: 4, y: 3, color: "red", opacity: 0.8, irradianceValue: 0 },
  { key: 17, x: 5, y: 3, color: "blue", opacity: 0.8, irradianceValue: 0 },
  { key: 18, x: 6, y: 3, color: "yellow", opacity: 0.8, irradianceValue: 0 }
];

/* Card Action Buttons */

const actionButtons = [
  {
    key: 1,
    icon: "add",
    name: "Add Schedule"
    // onclick: ids => {console.log("click");}
  }
];

const dropdownOptions = [
  { key: 1, value: "past", name: "Past" },
  { key: 2, value: "ongoing", name: "Ongoing" }
];

/* Table Content */

const header = [
  <TableHeaderCell selectable>
    <FormGroup type="checkbox" value="selectAll" />
  </TableHeaderCell>,
  <TableHeaderCell orderDesc>Start Date</TableHeaderCell>,
  <TableHeaderCell>End Date</TableHeaderCell>,
  <TableHeaderCell>Recipe</TableHeaderCell>
];

class CanopyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      objects: [],
      mapVis: true,
      rightChecked: false
    };
  }

  showIrradiance(event) {
    if (event.target.id === "right") {
      this.setState({
        ...this.state,
        rightChecked: true,
        mapVis: false
      });
    } else {
      this.setState({
        ...this.state,
        rightChecked: false,
        mapVis: true
      });
    }
  }

  saveImage() {
    var input = document.getElementById("canvas");
    html2canvas(input).then(canvas => {
      let imgData = canvas
        .toDataURL("image/jpg")
        .replace("image/jpg", "octet-stream");
      this.downloadURL(imgData);
      console.log("export");
    });
  }

  downloadURL(imgData) {
    var a = document.createElement("a");
    a.href = imgData.replace("image/jpg", "octet-stream");
    a.download = "Irradiance-Map.jpg";
    a.click();
  }

  render() {
    function handleClick(e) {
      e.preventDefault();
      console.log("edit click");
    }

    return (
      <Content details>
        <CardCanopyDetails title="Canopy Map" action={handleClick}>
          <div>
            <div id="canvas">
              <Canopy
                type="2x2h"
                length="2438"
                width="1219"
                spacing="200"
                fixtureLength="673"
                fixtureWidth="298"
                data={irradianceData}
                isHidden={this.state.mapVis}
              />
            </div>

            <CanopyControls>
              <Toggle
                name="showIrradiance"
                label={
                  this.state.rightChecked === true
                    ? [
                        "Irradiance Map — ",
                        <a onClick={() => this.saveImage()}>Download</a>
                      ]
                    : "Irradiance Map"
                }
                leftLabel="Hide"
                rightLabel="Show"
                rightChecked={this.state.rightChecked}
                onClick={this.showIrradiance.bind(this)}
              />
            </CanopyControls>
          </div>

          <div>
            <CanopyInfo>
              <FormGroup type="detail" label="Name" value="Canopy #01" />
              <FormGroup type="detail" label="Layout" value="1v" />
              <FormGroup type="detail" label="Lenght" value="2438 mm" />
              <FormGroup type="detail" label="Width" value="1219 mm" />
              <FormGroup type="detail" label="Height" value="000 mm" />
              <FormGroup type="detail" label="△L" value="200 mm" />
            </CanopyInfo>
          </div>
        </CardCanopyDetails>

        <CardValues title="Color Channels" values={colorChannels} />
        <CardValues title="Maximum Irradiance" values={maxIrradiance} />

        <CardTable
          header={true}
          title="Schedules"
          options={dropdownOptions}
          actions={actionButtons}
          from="1"
          to="10"
          of="100"
        >
          <Table header={header}>
            <TableRow>
              <TableData selectable>
                <FormGroup type="checkbox" value="Id-1" />
              </TableData>
              <TableData>2018-10-00</TableData>
              <TableData>2018-10-00</TableData>
              <TableData>
                <Tag tag="Recipe #1" />
              </TableData>
              <TableRowOptions>
                <button>
                  <Icon icon="edit" />
                </button>
                <button>
                  <Icon icon="duplicate" />
                </button>
                <button>
                  <Icon icon="delete" />
                </button>
              </TableRowOptions>
            </TableRow>

            <TableRow>
              <TableData selectable>
                <FormGroup type="checkbox" value="Id-2" />
              </TableData>
              <TableData>2018-10-00</TableData>
              <TableData>2018-10-00</TableData>
              <TableData>
                <Tag tag="Recipe #2" />
              </TableData>
              <TableRowOptions>
                <button>
                  <Icon icon="edit" />
                </button>
                <button>
                  <Icon icon="duplicate" />
                </button>
                <button>
                  <Icon icon="delete" />
                </button>
              </TableRowOptions>
            </TableRow>
          </Table>
        </CardTable>
      </Content>
    );
  }
}

export default CanopyDetails;
