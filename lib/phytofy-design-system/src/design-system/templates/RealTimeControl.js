import React, { Component } from "react";
import { ResponsiveStream } from "@nivo/stream";
import { linearGradientDef } from "@nivo/core";

import IrradianceGroup from "../organisms/IrradianceGroup";
import WavelenghtControlGroup from "../organisms/WavelenghtControlGroup";
import ChannelContainer from "../molecules/ChannelContainer";
import TotalContainer from "../molecules/TotalContainer";
import ChartContainer from "../molecules/ChartContainer";
import VerticalSlider from "../atoms/VerticalSlider";

const data = [
  { spectrum: 18, hidden: 0 },
  { spectrum: 92, hidden: 0 },
  { spectrum: 39, hidden: 0 },
  { spectrum: 55, hidden: 0 },
  { spectrum: 142, hidden: 0 },
  { spectrum: 121, hidden: 0 },
  { spectrum: 135, hidden: 0 },
  { spectrum: 147, hidden: 0 },
  { spectrum: 100, hidden: 200 }
];

class RealTimeControl extends Component {
  render() {
    return (
      <div>
        <IrradianceGroup irradiance={false}>
          <WavelenghtControlGroup>
            <ChannelContainer
              title="Channels"
              unit={["μmol/m", <sup>2</sup>, "/s"]}
            >
              <VerticalSlider
                channel="uv"
                min={0}
                max={400}
                defaultValue={31}
                percentage="00"
              />
              <VerticalSlider
                channel="blue"
                min={0}
                max={400}
                defaultValue={173}
                percentage="00"
              />
              <VerticalSlider
                channel="green"
                min={0}
                max={400}
                defaultValue={73}
                percentage="00"
              />
              <VerticalSlider
                channel="hyperRed"
                min={0}
                max={400}
                defaultValue={198}
                percentage="00"
              />
              <VerticalSlider
                channel="farRed"
                min={0}
                max={400}
                defaultValue={93}
                percentage="00"
              />
              <VerticalSlider
                channel="warmWhite"
                min={0}
                max={400}
                defaultValue={193}
                percentage="00"
              />
            </ChannelContainer>
            <TotalContainer title="Total">
              <VerticalSlider
                channel="total"
                min={0}
                max="300"
                defaultValue={200}
                unit="%"
              />
            </TotalContainer>
          </WavelenghtControlGroup>

          <ChartContainer title="Wavelength" x="nm" xMin={300} xMax={800}>
            <ResponsiveStream
              data={data}
              keys={["spectrum", "hidden"]}
              height={300}
              margin={{
                top: 30,
                right: 20,
                bottom: 50,
                left: 20
              }}
              axisTop={null}
              axisRight={null}
              axisBottom={{
                orient: "bottom",
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                type: "linear",
                tickValues: 2
              }}
              axisLeft={null}
              enableGridX={false}
              curve="natural"
              offsetType="none"
              fillOpacity={1}
              isInteractive={false}
              enableStackTooltip={false}
              defs={[
                linearGradientDef("spectrumGradient", [
                  { offset: 0, color: "rgb(64, 0, 64" },
                  { offset: 16, color: "rgb(255, 0, 255)" },
                  { offset: 36, color: "rgb(0, 0, 255)" },
                  { offset: 38, color: "rgb(0, 255, 255)" },
                  { offset: 43, color: "rgb(0, 255, 0)" },
                  { offset: 56, color: "rgb(255, 255, 0)" },
                  { offset: 69, color: "rgb(255, 0, 0)" },
                  { offset: 96, color: "rgb(255, 0, 0)" },
                  { offset: 100, color: "rgb(64, 0, 0)" }
                ])
              ]}
              fill={[{ match: { id: "spectrum" }, id: "spectrumGradient" }]}
              animate={false}
              motionStiffness={90}
              motionDamping={15}
            />
          </ChartContainer>
        </IrradianceGroup>
      </div>
    );
  }
}

export default RealTimeControl;
