import React, { Component } from "react";
import Content from "../organisms/Content";
import LicensesWrapper from "../organisms/LicensesWrapper";
import LicenseItem from "../molecules/LicenseItem";
import licenses from "../../utils/licenses";

export default class extends Component {
  state = {
    licenses: licenses
  };

  render() {
    return (
      <Content>
        <LicensesWrapper>
          {Object.keys(this.state.licenses).map(key => (
            <LicenseItem
              key={licenses[key].name}
              id={licenses[key].name}
              name={licenses[key].name}
              license={licenses[key].licenseText}
            />
          ))}
        </LicensesWrapper>
      </Content>
    );
  }
}
