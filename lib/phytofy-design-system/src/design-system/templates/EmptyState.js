import React, { Component } from "react";
import Content from "../organisms/Content";
import EmptyState from "../organisms/EmptyState";

class EmptyStateTemplate extends Component {
  render() {
    return (
      <div>
        <Content>
          <EmptyState
            type="table"
            message="Cras lobortis felis id diam convallis vulputate morbi rutrum et lorem sed venenatis."
            action="Go to XYZ"
            onClick=""
          />
        </Content>
      </div>
    );
  }
}

export default EmptyStateTemplate;
