import React, { Component } from 'react'

import Topbar from '../organisms/Topbar'
import Content from '../organisms/Content'
import CardTable from '../organisms/CardTable'
import Table from '../organisms/Table'
import TableHeaderCell from '../molecules/TableHeaderCell'
import TableRow from '../molecules/TableRow'
import TableRowOptions from '../molecules/TableRowOptions'
import TableData from '../molecules/TableData'
import Button from '../atoms/Button'
import Checkbox from '../atoms/Checkbox'
import Tag from '../atoms/Tag'

/* Tabs */

const tabList = [
    { name: 'Grid', location: '/' },
    { name: 'List', location: '/canopies-list' },
    { name: 'Details', location: '/canopies/details' },
];

/* Card Action Buttons */

const actionButtons = [
    { icon: 'add', onclick: (ids) => { console.log("click") } },
];

/* Table Content */

const header = [
    <TableHeaderCell selectable><Checkbox /></TableHeaderCell>,
    <TableHeaderCell orderAsc>Recipe</TableHeaderCell>,
    <TableHeaderCell>Canopy</TableHeaderCell>,
];

class CanopiesList extends Component {
    render() {
        return (
            <div>
                <Topbar values={tabList} />

                <Content>
                    <CardTable title="Canopies List" actions={actionButtons} goto="1" from="1" to="10" of="100" >
                        <Table header={header}>
                            <TableRow>
                                <TableData selectable>
                                    <Checkbox />
                                </TableData>
                                <TableData>Recipe #01</TableData>
                                <TableData>
                                    <Tag tag="Canopy #01" />
                                    <Tag tag="Canopy #09" />
                                    <Tag tag="Canopy #10" />
                                </TableData>
                                <TableRowOptions>
                                    <Button icon="edit" tooltip="Example" />
                                    <Button icon="duplicate" tooltip="Example" />
                                    <Button icon="delete" tooltip="Example" />
                                </TableRowOptions>
                            </TableRow>
                        </Table>
                    </CardTable>
                </Content>
            </div>
        )
    }
}

export default CanopiesList;
