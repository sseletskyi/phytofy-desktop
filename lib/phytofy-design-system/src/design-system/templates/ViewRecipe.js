import React, { Component } from "react";
import Timeline from "react-visjs-timeline";
import TimeField from "react-simple-timefield";
import { ResponsiveStream } from "@nivo/stream";
import { linearGradientDef } from "@nivo/core";
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody
} from "react-accessible-accordion";
import Icon from "../atoms/Icons";
import Recipe from "../organisms/Recipe";
import LightExposure from "../organisms/LightExposure";
import LightExposureItem from "../molecules/LightExposureItem";
import FormGroup from "../molecules/FormGroup";
import ItemTimeContainer from "../molecules/ItemTimeContainer";
import IrradianceGroup from "../organisms/IrradianceGroup";
import WavelenghtControlGroup from "../organisms/WavelenghtControlGroup";
import ChannelContainer from "../molecules/ChannelContainer";
import TotalContainer from "../molecules/TotalContainer";
import ChartContainer from "../molecules/ChartContainer";
import VerticalSlider from "../atoms/VerticalSlider";
import Button from "../atoms/Button";
import "../../css/Accordion.css";

/* Timeline Options */

const options = {
  width: "100%",
  height: "79px",
  showMajorLabels: false,
  showMinorLabels: true,
  showCurrentTime: false,
  min: "2018-11-2 00:00:00",
  max: "2018-11-2 23:59:59",
  start: "2018-11-2 00:00:00",
  end: "2018-11-2 23:59:59",
  zoomMin: 300000,
  stack: false,
  format: {
    minorLabels: {
      minute: "HH:mm",
      hour: "HH:mm"
    }
  }
};

/* Timeline Items */

const items = [
  { id: 1, start: "2018-07-25 10:00:00", end: "2018-07-25 10:59:00" },
  { id: 2, start: "2018-07-25 11:00:00", end: "2018-07-25 11:59:00" },
  { id: 3, start: "2018-07-25 12:30:00", end: "2018-07-25 13:30:00" },
  { id: 4, start: "2018-07-25 17:00:00", end: "2018-07-25 19:00:00" }
];

const data = [
  { spectrum: 18, hidden: 0 },
  { spectrum: 92, hidden: 0 },
  { spectrum: 39, hidden: 0 },
  { spectrum: 55, hidden: 0 },
  { spectrum: 142, hidden: 0 },
  { spectrum: 121, hidden: 0 },
  { spectrum: 135, hidden: 0 },
  { spectrum: 147, hidden: 0 },
  { spectrum: 100, hidden: 200 }
];

const TimelineItems = () => (
  <Accordion accordion={false}>
    <AccordionItem>
      <AccordionItemTitle>
        <span>Add period</span>
        <div class="accordion__arrow">
          <Icon icon="arrowDown" fill="#87888A" />
        </div>
      </AccordionItemTitle>
      <AccordionItemBody>
        <ItemTimeContainer title="Time">
          <TimeField
            input={<FormGroup type="text" label="Start" />}
            colon=":"
          />
          <TimeField input={<FormGroup type="text" label="End" />} colon=":" />
        </ItemTimeContainer>

        <IrradianceGroup>
          <WavelenghtControlGroup>
            <ChannelContainer
              title="Channels"
              unit={["μmol/m", <sup>2</sup>, "/s"]}
            >
              <VerticalSlider
                channel="uv"
                min={0}
                max={400}
                defaultValue={31}
                percentage="00"
              />
              <VerticalSlider
                channel="blue"
                min={0}
                max={400}
                defaultValue={173}
                percentage="00"
              />
              <VerticalSlider
                channel="green"
                min={0}
                max={400}
                defaultValue={73}
                percentage="00"
              />
              <VerticalSlider
                channel="hyperRed"
                min={0}
                max={400}
                defaultValue={198}
                percentage="00"
              />
              <VerticalSlider
                channel="farRed"
                min={0}
                max={400}
                defaultValue={93}
                percentage="00"
              />
              <VerticalSlider
                channel="warmWhite"
                min={0}
                max={400}
                defaultValue={193}
                percentage="00"
              />
            </ChannelContainer>
            <TotalContainer title="Total">
              <VerticalSlider
                channel="total"
                min={0}
                max="300"
                defaultValue={200}
                unit="%"
              />
            </TotalContainer>
          </WavelenghtControlGroup>

          <ChartContainer title="Wavelenght" x="nm" xMin={300} xMax={800}>
            <ResponsiveStream
              data={data}
              keys={["spectrum", "hidden"]}
              height={300}
              margin={{
                top: 30,
                right: 20,
                bottom: 50,
                left: 20
              }}
              axisTop={null}
              axisRight={null}
              axisBottom={{
                orient: "bottom",
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                type: "linear",
                tickValues: 2
              }}
              axisLeft={null}
              enableGridX={false}
              curve="natural"
              offsetType="none"
              fillOpacity={1}
              isInteractive={false}
              enableStackTooltip={false}
              defs={[
                linearGradientDef("spectrumGradient", [
                  { offset: 0, color: "rgb(64, 0, 64" },
                  { offset: 16, color: "rgb(255, 0, 255)" },
                  { offset: 36, color: "rgb(0, 0, 255)" },
                  { offset: 38, color: "rgb(0, 255, 255)" },
                  { offset: 43, color: "rgb(0, 255, 0)" },
                  { offset: 56, color: "rgb(255, 255, 0)" },
                  { offset: 69, color: "rgb(255, 0, 0)" },
                  { offset: 96, color: "rgb(255, 0, 0)" },
                  { offset: 100, color: "rgb(64, 0, 0)" }
                ])
              ]}
              fill={[{ match: { id: "spectrum" }, id: "spectrumGradient" }]}
              animate={true}
              motionStiffness={90}
              motionDamping={15}
            />
          </ChartContainer>
        </IrradianceGroup>

        <Button state="success" name="Save" />
        <Button state="danger" name="Delete" />
      </AccordionItemBody>
    </AccordionItem>

    <AccordionItem>
      <AccordionItemTitle>
        <span>Add period</span>
        <div class="accordion__arrow">
          <Icon icon="arrowDown" fill="#87888A" />
        </div>
      </AccordionItemTitle>
      <AccordionItemBody>
        <p>
          Has closed eyes but still sees you have a lot of grump in yourself
          because you can't forget to be grumpy and not be like king grumpy cat
          yet you have cat to be kitten me right meow licks paws chill on the
          couch table stare out the window flop over.
        </p>
      </AccordionItemBody>
    </AccordionItem>

    <AccordionItem>
      <AccordionItemTitle>
        <span>Add period</span>
        <div class="accordion__arrow">
          <Icon icon="arrowDown" fill="#87888A" />
        </div>
      </AccordionItemTitle>
      <AccordionItemBody>
        <p>
          Has closed eyes but still sees you have a lot of grump in yourself
          because you can't forget to be grumpy and not be like king grumpy cat
          yet you have cat to be kitten me right meow licks paws chill on the
          couch table stare out the window flop over.
        </p>
      </AccordionItemBody>
    </AccordionItem>
  </Accordion>
);

class ViewRecipe extends Component {
  render() {
    return (
      <Recipe>
        <Timeline options={options} items={items} />
        <LightExposure>
          <LightExposureItem title="Photoperiod" value={7} unit="hours" />
        </LightExposure>
        <TimelineItems />
      </Recipe>
    );
  }
}

export default ViewRecipe;
