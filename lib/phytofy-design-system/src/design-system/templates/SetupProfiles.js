import React, { Component } from "react";

import Content from "../organisms/Content";
import ContentHeader from "../organisms/ContentHeader";
import CardActionGoTo from "../molecules/CardActionGoTo";
import CardActionNavigation from "../molecules/CardActionNavigation";
import CardActionButton from "../molecules/CardActionButtons";
import FormGroup from "../molecules/FormGroup";
import CardTable from "../organisms/CardTable";
import Table from "../organisms/Table";
import TableHeaderCell from "../molecules/TableHeaderCell";
import TableRow from "../molecules/TableRow";
import TableRowOptions from "../molecules/TableRowOptions";
import TableData from "../molecules/TableData";
import Icon from "../atoms/Icons";
import Checkbox from "../atoms/Checkbox";

/* Card Action Buttons */

const actionButtons = [
  {
    icon: "add",
    onclick: ids => {
      console.log("click");
    }
  }
];

/* Filter */

const canopyConfig = [
  { value: "1v", name: "1V" },
  { value: "1h", name: "1H" },
  { value: "2v", name: "2V" },
  { value: "2h", name: "2H" },
  { value: "3v", name: "3V" },
  { value: "2x2h", name: "2x2H" },
  { value: "3v", name: "4V" }
];

/* Table Content */

const header = [
  <TableHeaderCell selectable>
    <Checkbox />
  </TableHeaderCell>,
  <TableHeaderCell orderAsc>Name</TableHeaderCell>,
  <TableHeaderCell config>Config</TableHeaderCell>,
  <TableHeaderCell measure>Lenght</TableHeaderCell>,
  <TableHeaderCell measure>Width</TableHeaderCell>,
  <TableHeaderCell measure>Height</TableHeaderCell>,
  <TableHeaderCell measure>△L</TableHeaderCell>,
  <TableHeaderCell status>Status</TableHeaderCell>
];

class SetupProfiles extends Component {
  constructor(props) {
    super(props);
    this.state = { goto: props.from };
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.from !== prevProps.from) {
      this.setState({ goto: this.props.from });
    }
  }

  handleKeyPress(event) {
    if (event.key === "Enter") {
      let page = parseInt(event.target.value, 10);
      if (!isNaN(page)) this.props.pageGoTo(parseInt(event.target.value, 10));
    }
  }

  handleChange(event) {
    this.setState({ goto: event.target.value });
  }

  render() {
    return (
      <div>
        <Content>
          <ContentHeader>
            <div>
              <CardActionGoTo
                goto="1"
                onKeyPress={this.handleKeyPress}
                onChange={this.handleChange}
              />
              <CardActionNavigation
                from="1"
                to="10"
                of="100"
                pageBack={this.props.pageBack}
                pageNext={this.props.pageNext}
              />
              <CardActionButton values={actionButtons} />
            </div>
            <div>
              <FormGroup
                type="search"
                label="Search"
                placeholder="Lorem Ipsum"
                name="Search Fixture"
                value={this.state.text}
                error=""
                onChange={this.handleInputChange}
              />
              <FormGroup
                type="select"
                label="Select Name"
                options={canopyConfig}
                error=""
              />
            </div>
          </ContentHeader>

          <CardTable
            title="Canopy Profiles"
            actions={actionButtons}
            goto="1"
            from="1"
            to="10"
            of="100"
          >
            <Table header={header}>
              <TableRow>
                <TableData selectable>
                  <Checkbox />
                </TableData>
                <TableData>Profile #01</TableData>
                <TableData config>2V</TableData>
                <TableData measure>000 cm</TableData>
                <TableData measure>000 cm</TableData>
                <TableData measure>000 cm</TableData>
                <TableData measure>00 cm</TableData>
                <TableData status>
                  <Icon fill="#009D3D" icon="checkedCircle" />
                </TableData>
                <TableRowOptions>
                  <button>
                    <Icon icon="edit" />
                  </button>
                  <button>
                    <Icon icon="duplicate" />
                  </button>
                  <button>
                    <Icon icon="delete" />
                  </button>
                </TableRowOptions>
              </TableRow>
            </Table>
          </CardTable>
        </Content>
      </div>
    );
  }
}

export default SetupProfiles;
