import React, { Component } from "react";
import styled from "styled-components";

const LicenseItem = styled.div`
  max-width: 800px;

  &:not(:last-child) {
    margin-bottom: 5px;
  }

  input {
    display: none;
  }

  label {
    position: relative;
    font-weight: var(--bold, 700);
    z-index: 20;
    cursor: pointer;
  }

  input:checked ~ pre {
    margin-bottom: 20px;
    border: 1px solid var(--sidebarPrimaryColor, #c5c6c8);
    padding: 10px;
  }

  pre {
    position: relative;
    margin-top: 5px;
    border-radius: 4px;
    background-color: var(--backgroundSecondaryMenu, #fafafa);
    height: 0px;
    padding: 0;
    font-weight: var(--light, 300);
    overflow: hidden;
    z-index: 10;
  }

  input:checked ~ pre {
    height: auto;
  }
`;

export default class extends Component {
  render() {
    return (
      <LicenseItem>
        <input id={this.props.id} name={this.props.name} type="checkbox" />
        <label htmlFor={this.props.id}>{this.props.name}</label>
        <pre>{this.props.license}</pre>
      </LicenseItem>
    );
  }
}
