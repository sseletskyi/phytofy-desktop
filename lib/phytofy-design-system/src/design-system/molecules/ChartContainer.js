import React, { Component } from "react";
import styled from "styled-components";

const ChartContainer = styled.div`
  position: relative;
  border: 1px solid var(--topbarBorder, #eceded);
  border-radius: 4px;

  > span {
    position: absolute;
    margin: -11px 0 0 var(--spacing, 30px);
    background-color: var(--backgroundColor, #ffffff);
    padding: 0 10px;
    color: var(--topbarBorder, #eceded);
  }
`;

const Chart = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: var(--spacing, 30px) var(--spacing, 30px) var(--spacing, 30px)
    var(--spacing, 30px);
`;

const LabelX = styled.div`
  position: absolute;
  margin-top: -60px;
  width: 100%;
  padding-left: 20px;
  text-align: center;
  color: var(--textColor, #424242);
`;

const LabelY = styled.div`
  position: absolute;
  margin: 330px 0 0 30px;
  width: 300px;
  padding-left: 25px;
  text-align: center;
  color: var(--textColor, #424242);
  transform: rotate(-90deg);
  transform-origin: top left;
`;

const Xmin = styled.div`
  position: absolute;
  margin-top: -75px;
  margin-left: 77px;
  width: min-content;
  font-size: 11px;
  color: rgb(51, 51, 51);
`;

const Xmax = styled(Xmin)`
  right: 47px;
`;

export default class extends Component {
  render() {
    return (
      <ChartContainer>
        <span>{this.props.title}</span>
        <LabelY>{this.props.y}</LabelY>
        <Chart>{this.props.children}</Chart>
        <Xmin>{this.props.xMin}</Xmin>
        <Xmax>{this.props.xMax}</Xmax>
        <LabelX>{this.props.x}</LabelX>
      </ChartContainer>
    );
  }
}
