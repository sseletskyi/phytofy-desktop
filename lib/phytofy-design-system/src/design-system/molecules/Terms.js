import React, { Component } from "react";
import styled from "styled-components";

const Terms = styled.div`
  margin: 0 auto;
  max-width: 800px;
  font-size: 1rem;
  font-weight: var(--light, 300);
  line-height: 150%;

  p {
    &:not(:last-child) {
      margin-bottom: 10px;
    }
  }
`;

export default class extends Component {
  render() {
    return <Terms>{this.props.children}</Terms>;
  }
}
