import styled from 'styled-components';

export default styled.div `
    opacity: 0;
    position: absolute;
    right: 0;
    display: flex;
    flex-direction: row;
    align-items: center;
    height: var(--rowHeight, 50px);
    padding: 0 var(--spacing, 30px) 0 calc(var(--spacing, 30px) * 2);
    box-shadow: -30px 0 30px rgba(0, 0, 0, .05);
    transition-duration: .2s;
`
