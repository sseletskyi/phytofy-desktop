import React, { Component } from "react";
import styled from "styled-components";

const TotalContainer = styled.div`
  width: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid var(--topbarBorder, #eceded);
  border-radius: 4px;

  > span {
    position: absolute;
    align-self: flex-start;
    margin: -11px 0 0;
    background-color: var(--backgroundColor, #ffffff);
    padding: 0 10px;
    color: var(--topbarBorder, #eceded);
  }

  > div {
    padding: var(--spacing, 30px);
  }
`;

export default class extends Component {
  render() {
    return (
      <TotalContainer>
        <span>{this.props.title}</span>

        <div>{this.props.children}</div>
      </TotalContainer>
    );
  }
}
