import styled, { css } from 'styled-components';

const TableHeaderCell = styled.th `
    padding: 0 calc(var(--spacing, 30px) / 3);
    vertical-align: middle;

    &:first-child {
        padding-left: var(--spacing, 30px);
    }

    &:before {
        position: absolute;
        margin: -1px 0 0 calc(-5px - var(--orderIcon,18px));
        width: var(--orderIcon, 18px);
        height: var(--orderIcon, 18px);
        content: "";
    }

    ${props => props.orderDesc && css`
        &:before {
            background: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'><path fill='rgb(135,136,138)' d='M14.668 6.667v14.894l-6.507-6.507c-0.243-0.241-0.577-0.39-0.947-0.39s-0.704 0.149-0.947 0.39l0-0c-0.241 0.24-0.389 0.573-0.389 0.94s0.149 0.699 0.389 0.94v0l8.788 8.788c0.24 0.241 0.573 0.389 0.94 0.389s0.699-0.149 0.94-0.389v0l8.788-8.788c0.241-0.24 0.389-0.573 0.389-0.94s-0.149-0.699-0.389-0.94v0c-0.24-0.241-0.573-0.389-0.94-0.389s-0.699 0.149-0.94 0.389v0l-6.507 6.507v-14.894c-0.002-0.737-0.6-1.333-1.337-1.333-0.001 0-0.001 0-0.002 0h0c-0.734 0.004-1.328 0.599-1.33 1.333v0z'></path></svg>") no-repeat;
        }
    `}

    ${props => props.orderAsc && css`
        &:before {
            background: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32'><path fill='rgb(135,136,138)' d='M17.335 25.332v-14.892l6.507 6.507c0.243 0.241 0.577 0.39 0.947 0.39s0.704-0.149 0.947-0.39l-0 0c0.241-0.24 0.389-0.573 0.389-0.94s-0.149-0.699-0.389-0.94v0l-8.788-8.788c-0.24-0.241-0.573-0.39-0.94-0.39s-0.7 0.149-0.94 0.39l-0 0-8.798 8.773c-0.241 0.24-0.389 0.573-0.389 0.94s0.149 0.699 0.389 0.94v0c0.24 0.241 0.573 0.389 0.94 0.389s0.699-0.149 0.94-0.389v0l6.521-6.492v14.894c0.002 0.736 0.598 1.331 1.333 1.333h0c0.736-0.002 1.332-0.599 1.333-1.335v-0z'></path></svg>") no-repeat;
        }
    `}

    ${props => props.selectable && css`
        width: var(--selectable, 80px);
        text-align: center;
    `}

    ${props => props.config && css`
        min-width: var(--config, 100px);
        text-align: center;
    `}

    ${props => props.measure && css`
        min-width: var(--measure, 100px);
        text-align: center;
    `}

    ${props => props.status && css`
        width: var(--status, 100px);
        text-align: center;
    `}
`

export default TableHeaderCell;
