import styled from "styled-components";

export default styled.tr`
  position: relative;
  height: var(--rowHeight, 50px);
  text-align: left;

  button {
    background: transparent;
    border: 0;
    width: 26px;
    height: 26px;
    padding: 0;
    appearance: none;
    cursor: pointer;
    transition-duration: 0.2s;

    svg {
      margin: 0;
      width: auto;
      height: 26px;
      fill: var(--textColor, #424242);
    }

    &:not(:last-child) {
      margin-right: 15px;
      ${'' /* fill: var(--textColor, #424242); */}

      ${'' /* svg {
        width: auto;
        height: 26px;
      } */}
    }
  }
`;
