import styled from "styled-components";

const FormActions = styled.div`
  margin-top: 50px;
  display: flex;
  flex-direction: row;

  button {
    width: 50%;
    padding: 0;

    &:first-child {
      margin-right: 5px;
    }

    &:last-child {
      margin-left: 5px;
    }
  }
`;

export default FormActions;
