import React, { Component } from "react";
import styled from "styled-components";

const LightExposureItem = styled.div`
  color: var(--textColor, #424242);

  > span {
    font-weight: var(--bold, 700);
  }
`;

export default class extends Component {
  render() {
    return (
      <LightExposureItem>
        <span>{this.props.title}:</span> {this.props.value} {this.props.unit}
      </LightExposureItem>
    );
  }
}
