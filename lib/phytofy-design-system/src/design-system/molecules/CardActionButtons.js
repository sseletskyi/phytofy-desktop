import React, { Component } from "react";
import styled from "styled-components";

import ActionButton from "../atoms/ActionButton";

const ActionButtons = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: var(--spacing, 30px);
`;

export default class CardActionButtons extends Component {
  render() {
    let values = this.props.values ? this.props.values : [];

    const actionButtons = values.map(value => (
      <ActionButton
        key={value.key}
        icon={value.icon}
        name={value.name}
        tooltip={value.tooltip}
        flow={value.flow}
        onClick={value.onClick}
      />
    ));

    return <ActionButtons>{actionButtons}</ActionButtons>;
  }
}
