import styled from 'styled-components';

const CheckboxGroup = styled.div`
	margin-bottom: var(--spacing, 30px);
	
	> div {
		&:not(:last-child) {
			margin-bottom: 10px;
		}
	}
`

export default CheckboxGroup;
