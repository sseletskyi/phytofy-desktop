import React, { Component } from "react";
import styled, { css } from "styled-components";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const TopbarTab = styled.li`
  --tabWidth: 150px;

  list-style: none;
  border-left-width: 1px;
  border-style: solid;
  border-color: var(--topbarBorder, #eceded);
  width: var(--tabWidth, 150px);

  ${props =>
    props.isActive &&
    css`
      a {
        color: var(--primaryColor, #ff6600) !important;
      }

      &:before {
        position: absolute;
        bottom: 0;
        width: var(--tabWidth, 150px);
        height: 5px;
        background-color: var(--primaryColor, #ff6600);
        content: "";
      }
    `}

  &:last-child {
    border-right-width: 1px;
  }

  a {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: var(--bold, 700);
    color: var(--secondaryColor, #87888a);
    text-decoration: none;
    cursor: pointer;
  }
`;

export default class extends Component {
  render() {
    return (
      <TopbarTab isActive={this.props.isActive}>
        <Link to={this.props.to} isActive={this.props.isActive}>
          <span>{this.props.name}</span>
        </Link>
      </TopbarTab>
    );
  }
}
