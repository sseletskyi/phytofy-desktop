import React, { Component } from "react";
import styled, { css } from "styled-components";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Icon from "../atoms/Icons";

const LineItem = styled.li.attrs({
  tooltip: props => props.tooltip,
  flow: props => props.flow
})`
  display: flex;
  cursor: pointer;

  &:not(:last-child) {
    margin-bottom: 20px;
  }

  ${props =>
    props.isActive &&
    css`
      &:before {
        position: absolute;
        left: 0;
        width: 5px;
        height: var(--menuLinkHeight, 40px);
        background-color: var(--primaryColor, #ff6600);
        content: "";
      }

      a svg {
        fill: var(--primaryColor, #ff6600) !important;
      }
    `}

  a {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: var(--menuLinkHeight, 40px);
    font-weight: var(--bold, 700);
    color: var(--secondaryColor, #87888a);
    text-decoration: none;

    &[tooltip] {
      font-weight: var(--regular, 400);
    }

    &[tooltip][flow^="right"]::before {
      right: calc(0em - -5px);
    }

    &[tooltip][flow^="right"]::after {
      left: calc(100% - 5px);
    }

    > div {
      margin-top: 2px;
    }

    svg {
      height: var(--menuLinkHeight, 40px);
      width: auto;
      fill: var(--secondaryColor, #87888a);
    }
  }
`;

export default class MenuLink extends Component {
  render() {
    return (
      <LineItem isActive={this.props.isActive} onClick={this.props.onClick}>
        <Link
          to={this.props.to}
          isActive={this.props.isActive}
          tooltip={this.props.tooltip}
          flow="right"
        >
          <Icon icon={this.props.icon} />
        </Link>
      </LineItem>
    );
  }
}
