import React, { Component } from "react";
import styled from "styled-components";

const ProductInfo = styled.div`
  h1 {
    margin-bottom: calc(var(--spacing, 30px) / 2);
    font-size: 2rem;
    font-weight: var(--bold, 700);
  }

  ul {
    list-style: none;
    font-weight: var(--light, 300);

    li {
      &:not(:last-child) {
        margin-bottom: 2px;
      }
    }
  }
`;

export default class extends Component {
  render() {
    const info = this.props.values.map(value => (
      <li key={value.key}>{value.value}</li>
    ));

    return (
      <ProductInfo>
        <h1>{this.props.title}</h1>
        <ul>{info}</ul>
      </ProductInfo>
    );
  }
}
