import React, { Component } from "react";
import styled from "styled-components";

import Icon from "../atoms/Icons";

const ValueContainer = styled.div.attrs({
  tooltip: props => props.tooltip,
  flow: props => props.flow })`

  background-color: var(--backgroundColor, #ffffff);
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px 0;

  svg {
    width: 40px;
    height: 40px;
    fill: var(--primaryColor, #ff6600);
  }
`;

const Value = styled.span`
  font-size: 2rem;
  font-weight: var(--bold, 700);
`;

export default class CardValue extends Component {
  render() {
    return (
      <ValueContainer tooltip={this.props.tooltip} flow={this.props.flow}>
        <Icon icon={this.props.icon} fill={this.props.fill} />
        <div>
          <Value>{this.props.value}</Value>
          <span>{this.props.unit}</span>
        </div>
        <span dangerouslySetInnerHTML={{ __html: this.props.legend }} />
      </ValueContainer>
    );
  }
}
