const menu = [
  { key: 1, icon: "fixture", tooltip: "Overview", location: "/overview" },
  { key: 2, icon: "aspectRatio", tooltip: "Layouts", location: "/layouts" },
  { key: 3, icon: "groups", tooltip: "Groups", location: "/groups" },
  { key: 4, icon: "time", tooltip: "Day Recipes", location: "/recipes" },
  { key: 5, icon: "schedule", tooltip: "Schedules", location: "/schedules" },
  {
    key: 6,
    icon: "setup",
    tooltip: "Configuration",
    location: "/configurations"
  }
];

export default menu;
