import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Routes from "./Routes";
import Topbar from "./design-system/organisms/Topbar";
import PrimaryMenu from "./design-system/organisms/PrimaryMenu";
import RightSidebar from "./design-system/organisms/RightSidebar";
import Wrapper from "./design-system/organisms/Wrapper";
import MainContent from "./design-system/organisms/MainContent";
import MenuLink from "./design-system/molecules/MenuLink";
import menu from "./utils/menu";
import "./css/App.css";

import Commissioning from "./design-system/organisms/Commissioning";
// // import RealTimeControl from "./design-system/templates/RealTimeControl";
// import EditProfile from "./design-system/templates/EditProfile";
import ViewRecipe from "./design-system/templates/ViewRecipe";
// import FixtureDetails from "./design-system/templates/FixtureDetails";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRightSidebarClosed: true
    };

    this.openRightSidebar = this.openRightSidebar.bind(this);
    this.closeRightSidebar = this.closeRightSidebar.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  openRightSidebar() {
    this.setState({ isRightSidebarClosed: false });
  }
  closeRightSidebar() {
    this.setState({ isRightSidebarClosed: true });
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === "checkbox" ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <Router>
        <Wrapper>
          <Commissioning text="Commissioning refresh in progress" />

          <PrimaryMenu values={menu}>
            <MenuLink to="/guide" icon="help" tooltip="Quick Guide" />
            <MenuLink to="/about" icon="info" tooltip="About" />
          </PrimaryMenu>

          <MainContent isRightSidebarOpen={!this.state.isRightSidebarClosed}>
            <Topbar>
              {Routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={route.topbar}
                />
              ))}
            </Topbar>

            {Routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}

            <button onClick={this.openRightSidebar}>Sidebar</button>
          </MainContent>

          {!this.state.isRightSidebarClosed}
          <RightSidebar
            type="xl"
            label="Label"
            title="Example"
            isOpen={!this.state.isRightSidebarClosed}
            // isOpen={true}
            closeRightSidebar={this.closeRightSidebar}
          >
            {/* <EditProfile /> */}
            {/* <ConnectedFixture /> */}
            {/* <FixtureDetails /> */}
            <ViewRecipe />
            {/* <RealTimeControl /> */}
          </RightSidebar>
        </Wrapper>
      </Router>
    );
  }
}

export default App;
