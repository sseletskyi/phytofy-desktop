import React from "react";
import { Route, Link, Redirect } from "react-router-dom";

import CanopiesGrid from "./design-system/templates/CanopiesGrid";
import CanopyDetails from "./design-system/templates/CanopyDetails";
import Recipes from "./design-system/templates/Recipes";
import Schedules from "./design-system/templates/Schedules";
import SetupCanopies from "./design-system/templates/SetupCanopies";
import SetupProfiles from "./design-system/templates/SetupProfiles";
import SetupSettings from "./design-system/templates/SetupSettings";
import QuickGuide from "./design-system/templates/QuickGuide";
import ProductInfo from "./design-system/templates/ProductInfo";
import Credits from "./design-system/templates/Credits";
import Terms from "./design-system/templates/Terms";
import EmptyState from "./design-system/templates/EmptyState";

const Routes = [
  {
    exact: true,
    path: "/",
    topbar: () => "",
    main: () => <Redirect to={`/overview`} />
  },
  {
    path: "/overview",
    topbar: () => "",
    main: ({ match }) => (
      <div>
        <Route exact path={match.path} render={() => <CanopiesGrid />} />
        <Route
          path={`${match.path}/demo`}
          children={props => (props.match ? <CanopyDetails /> : "")}
        />
      </div>
    )
  },
  {
    path: "/layouts",
    topbar: () => "",
    main: () => <SetupProfiles />
  },
  {
    path: "/groups",
    topbar: () => "",
    main: () => <SetupCanopies />
  },
  {
    path: "/recipes",
    topbar: () => "",
    main: () => <Recipes />
  },
  {
    path: "/schedules",
    topbar: () => "",
    main: () => <Schedules />
  },
  {
    path: "/guide",
    topbar: () => "",
    main: () => <QuickGuide />
  },
  {
    path: "/configurations",
    topbar: ({ match, location }) => (
      <ul>
        <li
          className={
            location.pathname === `${match.path}/adapters` ? "active" : ""
          }
        >
          <Link to={`${match.url}/adapters`}>Adapters</Link>
        </li>
        <li
          className={
            location.pathname === `${match.path}/fixtures` ? "active" : ""
          }
        >
          <Link to={`${match.url}/fixtures`}>Fixtures</Link>
        </li>
        <li
          className={
            location.pathname === `${match.path}/settings` ? "active" : ""
          }
        >
          <Link to={`${match.url}/settings`}>Settings</Link>
        </li>
      </ul>
    ),
    main: ({ match }) => (
      <div>
        <Route
          exact
          path={match.path}
          render={() => <Redirect to={`${match.path}/adapters`} />}
        />
        <Route
          path={`${match.path}/adapters`}
          children={props => (props.match ? <EmptyState /> : "")}
        />
        <Route
          path={`${match.path}/fixtures`}
          children={props => (props.match ? <EmptyState /> : "")}
        />
        <Route
          path={`${match.path}/settings`}
          children={props => (props.match ? <SetupSettings /> : "")}
        />
      </div>
    )
  },
  {
    path: "/about",
    topbar: ({ match, location }) => (
      <ul>
        <li
          className={
            location.pathname === `${match.path}/product` ? "active" : ""
          }
        >
          <Link to={`${match.url}/product`}>Product Info</Link>
        </li>
        <li
          className={
            location.pathname === `${match.path}/credits` ? "active" : ""
          }
        >
          <Link to={`${match.url}/credits`}>Credits</Link>
        </li>
        <li
          className={
            location.pathname === `${match.path}/terms` ? "active" : ""
          }
        >
          <Link to={`${match.url}/terms`}>Terms of Service</Link>
        </li>
      </ul>
    ),
    main: ({ match }) => (
      <div>
        <Route
          exact
          path={match.path}
          render={() => <Redirect to={`${match.path}/product`} />}
        />
        <Route
          path={`${match.path}/product`}
          children={props => (props.match ? <ProductInfo /> : "")}
        />
        <Route
          path={`${match.path}/credits`}
          children={props => (props.match ? <Credits /> : "")}
        />
        <Route
          path={`${match.path}/terms`}
          children={props => (props.match ? <Terms /> : "")}
        />
      </div>
    )
  }
];

export default Routes;
