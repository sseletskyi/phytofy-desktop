let logger = require('./logger.js');
let utils = require('./utils.js');


async function calcIrradianceMap_1(W, L, fixtureMap){
  //use W and L in inches
  //initialize canopy map nested array:
  let canopyMap = new Array(2*L).fill(0); //first create rows
  for(let l = 0; l < 2*L; l++){
    canopyMap[l] = new Array(2*W).fill(0);
  }
  let luf = 0;
  let irradInside = 0, irradOutside = 0, max = 0;

  let fmLength = fixtureMap.map.length, fmWidth = fixtureMap.map[0].length;
  let fmCenterL = (fmLength-1)/2, fmCenterW = (fmWidth-1)/2;
  let canopyIdxW = 0, canopyIdxL = 0;
  logger.debug(`center L: ${fmCenterL} | W: ${fmCenterW}`);
  //logger.debug(`left W:${canopyIdxW} -- L ${canopyIdxL}`);
  logger.debug(`${fixtureMap.tag} fixture map width: ${fmWidth} | length: ${fmLength}`);
  for(let m = 0; m < fmWidth; m++){
    for(let n = 0; n < fmLength; n++){
      //canopyCenter = (W,L)
      canopyIdxW = W - fmCenterW + m;
      canopyIdxL = L - fmCenterL + n;
      //logger.debug(`(${canopyIdxW},${canopyIdxL})`);
      if(canopyIdxW < 0 || canopyIdxL < 0 || canopyIdxW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxW]) max = canopyMap[canopyIdxL][canopyIdxW];
      }
    }
  }
  logger.debug(`canopy map width: ${canopyMap[0].length} length: ${canopyMap.length}`);
  luf = irradInside/(irradInside + irradOutside);
  return [canopyMap, luf, max, fixtureMap.tag];
};

async function calcIrradianceMap_2(W, L, fixtureMap, distance, config){
  //use W and L in inches
  //initialize canopy map nested array:
  let canopyMap = new Array(2*L).fill(0); //first create rows
  for(let l = 0; l < 2*L; l++){
    canopyMap[l] = new Array(2*W).fill(0);
  }
  let irradInside = 0, irradOutside = 0, max = 0;
  let luf = {};
  let offset = null;

  if( config == '2v' || config == 'v') offset = distance + 12;
  else if ( config == '2h' || config == 'h') offset = distance + 27;
  else{
    logger.error(`[calcIrradianceMap_2] wrong ${config} config!`);
    return -1
  }

  let fmLength = fixtureMap.map.length, fmWidth = fixtureMap.map[0].length;
  let fmCenterL = (fmLength-1)/2, fmCenterW = (fmWidth-1)/2;
  let canopyIdxLeftW = 0, canopyIdxRightW = 0, canopyIdxL = 0;

  logger.debug(`${fixtureMap.tag} fixture map width: ${fmWidth} | length: ${fmLength}`);
  for(let m = 0; m < fmWidth; m++){
    for(let n = 0; n < fmLength; n++){
      //canopyCenter = (W,L)
      canopyIdxLeftW = W - fmCenterW + m - offset;
      canopyIdxRightW = W - fmCenterW + m + offset;
      canopyIdxL = L - fmCenterL + n;
      //logger.debug(`(${canopyIdxW},${canopyIdxL})`);
      if(canopyIdxLeftW < 0 || canopyIdxL < 0 || canopyIdxLeftW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxLeftW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxLeftW]) max = canopyMap[canopyIdxL][canopyIdxLeftW];
      }
      if(canopyIdxRightW < 0 || canopyIdxL < 0 || canopyIdxRightW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxRightW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxRightW]) max = canopyMap[canopyIdxL][canopyIdxRightW];
      }
    }
  }

  logger.debug(`canopy map width: ${canopyMap[0].length} length: ${canopyMap.length}`);
  luf = irradInside/(irradInside + irradOutside);
  return [canopyMap, luf, max, fixtureMap.tag];
};

async function calcIrradianceMap_3V(W, L, fixtureMap, distance){
  //use W and L in inches
  //initialize canopy map nested array:
  let canopyMap = new Array(2*L).fill(0); //first create rows
  for(let l = 0; l < 2*L; l++){
    canopyMap[l] = new Array(2*W).fill(0);
  }
  let irradInside = 0, irradOutside = 0, max = 0;
  let luf = {};
  let offset = 2 * distance + 12 + 12;

  let fmLength = fixtureMap.map.length, fmWidth = fixtureMap.map[0].length;
  let fmCenterL = (fmLength-1)/2, fmCenterW = (fmWidth-1)/2;
  let canopyIdxLeftW = 0, canopyIdxRightW = 0, canopyIdxCenterW = 0,  canopyIdxL = 0;

  logger.debug(`${fixtureMap.tag} fixture map center: (${fmCenterL},${fmCenterW}), offset: ${offset}`);
  for(let m = 0; m < fmWidth; m++){
    for(let n = 0; n < fmLength; n++){
      canopyIdxLeftW = W - fmCenterW + m - offset;
      canopyIdxRightW = W - fmCenterW + m + offset;
      canopyIdxCenterW = W - fmCenterW + m;
      canopyIdxL = L - fmCenterL + n;
      //logger.debug(`left W:${canopyIdxLeftW}, right W:${canopyIdxRightW} center W:${canopyIdxCenterW} -- L ${canopyIdxL}`);

      if(canopyIdxLeftW < 0 || canopyIdxL < 0 || canopyIdxLeftW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxLeftW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxLeftW]) max = canopyMap[canopyIdxL][canopyIdxLeftW];
      }
      if(canopyIdxRightW < 0 || canopyIdxL < 0 || canopyIdxRightW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxRightW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxRightW]) max = canopyMap[canopyIdxL][canopyIdxRightW];
      }
      if(canopyIdxCenterW < 0 || canopyIdxL < 0 || canopyIdxCenterW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxCenterW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxCenterW]) max = canopyMap[canopyIdxL][canopyIdxCenterW];
      }
    }
  }
  logger.debug(`canopy map width: ${canopyMap[0].length} length: ${canopyMap.length}`);
  luf = irradInside/(irradInside + irradOutside);
  return [canopyMap, luf, max, fixtureMap.tag];
};

async function calcIrradianceMap_4V(W, L, fixtureMap, distance){
  //use W and L in inches
  //initialize canopy map nested array:
  let canopyMap = new Array(2*L).fill(0); //first create rows
  for(let l = 0; l < 2*L; l++){
    canopyMap[l] = new Array(2*W).fill(0);
  }
  let irradInside = 0, irradOutside = 0, max = 0;
  let luf = {};
  let innerOffset = distance + 12;
  let outerOffset = 3*innerOffset;

  let fmLength = fixtureMap.map.length, fmWidth = fixtureMap.map[0].length;
  let fmCenterL = (fmLength-1)/2, fmCenterW = (fmWidth-1)/2;
  let canopyIdxInnerLeftW = 0, canopyIdxInnerRightW = 0, canopyIdxOuterLeftW  = 0, canopyIdxOuterRightW = 0,  canopyIdxL = 0;

  logger.debug(`${fixtureMap.tag} fixture map center: (${fmCenterL},${fmCenterW})`);
  for(let m = 0; m < fmWidth; m++){
    for(let n = 0; n < fmLength; n++){
      canopyIdxInnerLeftW = W - fmCenterW + m - innerOffset;
      canopyIdxInnerRightW = W - fmCenterW + m + innerOffset;
      canopyIdxOuterLeftW = W - fmCenterW + m - outerOffset;
      canopyIdxOuterRightW = W - fmCenterW + m + outerOffset;
      canopyIdxL = L - fmCenterL + n;
      //logger.debug(`left W:${canopyIdxLeftW}, right W:${canopyIdxRightW} center W:${canopyIdxCenterW} -- L ${canopyIdxL}`);

      if(canopyIdxInnerLeftW < 0 || canopyIdxL < 0 || canopyIdxInnerLeftW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxInnerLeftW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxInnerLeftW]) max = canopyMap[canopyIdxL][canopyIdxInnerLeftW];
      }
      if(canopyIdxInnerRightW < 0 || canopyIdxL < 0 || canopyIdxInnerRightW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxInnerRightW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxInnerRightW]) max = canopyMap[canopyIdxL][canopyIdxInnerRightW];
      }
      if(canopyIdxOuterLeftW < 0 || canopyIdxL < 0 || canopyIdxOuterLeftW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxOuterLeftW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxOuterLeftW]) max = canopyMap[canopyIdxL][canopyIdxOuterLeftW];
      }
      if(canopyIdxOuterRightW < 0 || canopyIdxL < 0 || canopyIdxOuterRightW > 2*W-1 || canopyIdxL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxL][canopyIdxOuterRightW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxL][canopyIdxOuterRightW]) max = canopyMap[canopyIdxL][canopyIdxOuterRightW];
      }
    }
  }
  logger.debug(`canopy map width: ${canopyMap[0].length} length: ${canopyMap.length}`);
  luf = irradInside/(irradInside + irradOutside);
  return [canopyMap, luf, max, fixtureMap.tag];
};

async function calcIrradianceMap_2X2H(W, L, fixtureMap, distance){
  //use W and L in inches
  //initialize canopy map nested array:
  let canopyMap = new Array(2*L).fill(0); //first create rows
  for(let l = 0; l < 2*L; l++){
    canopyMap[l] = new Array(2*W).fill(0);
  }
  let irradInside = 0, irradOutside = 0, max = 0;
  let luf = {};
  let offsetW = distance + 27;
  let offsetL = distance + 12;

  let fmLength = fixtureMap.map.length, fmWidth = fixtureMap.map[0].length;
  let fmCenterL = (fmLength-1)/2, fmCenterW = (fmWidth-1)/2;
  let canopyIdxLeftW = 0, canopyIdxRightW = 0, canopyIdxCenterW = 0,  canopyIdxUpperL = 0, canopyIdxLowerL = 0;

  logger.debug(`${fixtureMap.tag} fixture map center: (${fmCenterL},${fmCenterW})`);
  for(let m = 0; m < fmWidth; m++){
    for(let n = 0; n < fmLength; n++){
      canopyIdxLeftW = W - fmCenterW + m - offsetW;
      canopyIdxRightW = W - fmCenterW + m + offsetW;
      canopyIdxUpperL = L - fmCenterL + n - offsetL;
      canopyIdxLowerL = L - fmCenterL + n + offsetL;
      //logger.debug(`left W:${canopyIdxLeftW}, right W:${canopyIdxRightW} center W:${canopyIdxCenterW} -- L ${canopyIdxL}`);

      if(canopyIdxLeftW < 0 || canopyIdxUpperL < 0 || canopyIdxLeftW > 2*W-1 || canopyIdxUpperL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxUpperL][canopyIdxLeftW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxUpperL][canopyIdxLeftW]) max = canopyMap[canopyIdxUpperL][canopyIdxLeftW];
      }
      if(canopyIdxRightW < 0 || canopyIdxUpperL < 0 || canopyIdxRightW > 2*W-1 || canopyIdxUpperL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxUpperL][canopyIdxRightW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxUpperL][canopyIdxRightW]) max = canopyMap[canopyIdxUpperL][canopyIdxRightW];
      }
      if(canopyIdxLeftW < 0 || canopyIdxLowerL < 0 || canopyIdxLeftW > 2*W-1 || canopyIdxLowerL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxLowerL][canopyIdxLeftW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxLowerL][canopyIdxLeftW]) max = canopyMap[canopyIdxLowerL][canopyIdxLeftW];
      }
      if(canopyIdxRightW < 0 || canopyIdxLowerL < 0 || canopyIdxRightW > 2*W-1 || canopyIdxLowerL > 2*L-1){
        irradOutside += fixtureMap.map[n][m];
      }else{
        canopyMap[canopyIdxLowerL][canopyIdxRightW] += fixtureMap.map[n][m];
        irradInside += fixtureMap.map[n][m];
        if( max < canopyMap[canopyIdxLowerL][canopyIdxRightW]) max = canopyMap[canopyIdxLowerL][canopyIdxRightW];
      }
    }
  }
  logger.debug(`canopy map width: ${canopyMap[0].length} length: ${canopyMap.length}`);
  luf = irradInside/(irradInside + irradOutside);
  return [canopyMap, luf, max, fixtureMap.tag];
};

module.exports = {
  calcIrradianceMap_1,
  calcIrradianceMap_2,
  calcIrradianceMap_3V,
  calcIrradianceMap_4V,
  calcIrradianceMap_2X2H
};
