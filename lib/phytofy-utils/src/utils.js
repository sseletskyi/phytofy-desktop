let fs = require('fs');
let csv = require('csv-parse');
let logger = require('./logger.js');
let glob = require('glob');

async function getSphereColorFactor(height){
  //use find to get value for specific height or height range...
  //height in inches
  if(height < 9.5){
    return {"uv": 1.14359063, "blue": 1.03492594, "green": 1.02482, "hyperred": 1.01637, "farred": 1.03429, "white": 1.03383};
  }else if(height < 11.5){
    return {"uv": 1.11029635, "blue": 1.02769486, "green": 1.0187, "hyperred": 1.0124, "farred": 1.02726, "white": 1.0263};
  }else if(height < 13.5){
    return {"uv": 1.07526648, "blue": 1.0194635, "green": 1.01252, "hyperred": 1.00836, "farred": 1.0192, "white": 1.01815};
  }else if(height < 15.5){
    return {"uv": 1.03850103, "blue": 1.01023188, "green": 1.00629, "hyperred": 1.00422, "farred": 1.01012, "white": 1.00938};
  }else if(height < 17.5){
    return {"uv": 1.0, "blue": 1.0, "green": 1.0, "hyperred": 1.0, "farred": 1.0, "white": 1.0};
  }else if(height < 19.5){
    return {"uv": 0.95976338, "blue": 0.98876785, "green": 0.99365, "hyperred": 0.99488, "farred": 0.98886, "white": 0.99};
  }else if(height < 21.5){
    return {"uv": 0.91779118, "blue": 0.97653543, "green": 0.98725, "hyperred": 0.9913, "farred": 0.97668, "white": 0.97938};
  }else{
    return {"uv": 0.87408339, "blue": 0.96330275, "green": 0.98078, "hyperred": 0.98681, "farred": 0.96348, "white": 0.96814};
  }
};

async function listFiles(dir){
  let gpath = dir;
  gpath += '/*.csv';
  let lizt = [];

  return new Promise((resolve, reject) => {
    glob(gpath, function(er, files) {
      files.forEach(function(path) {
        let slashed = path.split("/");
        lizt.push(slashed.slice(-1)[0]);
      });
      return resolve(lizt);
    });
  });
};

async function csv2multiArray(filename){
  let multiArray = [];
  let i = 0;
  return new Promise((resolve, reject) => {
    fs.createReadStream(filename)
      .pipe(csv({delimiter: ','}))
      .on('data', function(data){
        multiArray[i] = new Array();
        data.forEach((element, index) => {
          multiArray[i].push(parseFloat(element));
        });
        i++;
      })
      .on('end', function() {
        return resolve(multiArray);
      });
  });
};

async function loadFixtureMapsTest(dir, config='1V', channels=['uv', 'blue', 'green', 'hr', 'fr', 'white']){
  let orientation = null;
  if( config == '1v'|| config == '2v'|| config == '3v'|| config == '3' || config == 'v'){
    orientation = 'V';
  }else if(config == '1h'|| config == '2h'|| config == '2x2h'|| config == '2x2' || config == 'h'){
    orientation = 'H'
  }else{
    logger.error(`[loadFixtureMaps] ${config} config is unknown!`)
    return -1
  }
  let fm = [];
  channels.forEach((channel) => {
    fm.push({map: [0], tag: channel});
  });
  let lizt = await listFiles(dir);

  for(let channel of fm) {
    let name = lizt.find((element) => {
      let tag = element.split('_');
      if(tag[0] == channel.tag && tag[1] == orientation ) return element;
    });
    if(name == undefined){
      logger.error(`[loadFixtureMaps] ${channel.tag} map file not found!`);
      channel.map = [-1];
    }else{
      let filename = dir+'/'+name;
      channel.map = await csv2multiArray(filename);
    }
  };
  return fm;

};

async function loadFixtureMaps(dir, height, config='1V', channels=['uv', 'blue', 'green', 'hyperred', 'farred', 'white']){
  //height must be in inches
  let orientation = null;
  if( config == '1v'|| config == '2v'|| config == '3v'|| config == '3' || config == 'v' || config == '4' || config == '4v'){
    orientation = 'vertical.csv';
  }else if(config == '1h'|| config == '2h'|| config == '2x2h'|| config == '2x2' || config == 'h'){
    orientation = 'horizontal.csv'
  }else{
    logger.error(`[loadFixtureMaps] ${config} config is unknown!`)
    return -1
  }
  let d = mapHeight(height);

  let fm = [];
  channels.forEach((channel) => {
    fm.push({map: [0], tag: channel});
  });
  let lizt = await listFiles(dir);

  for(let channel of fm) {
    let name = lizt.find((element) => {
      let tag = element.split('_');
      if(tag[0] == channel.tag && tag[1] == d && tag[3] == orientation ) return element;
    });
    if(name == undefined){
      logger.error(`[loadFixtureMaps] ${channel.tag} map file not found!`);
      channel.map = [-1];
    }else{
      let filename = dir+'/'+name;
      channel.map = await csv2multiArray(filename);
    }
  };
  return fm;

};

function mapHeight(height){
  if(height < 9.5){
    return '8inch';
  }else if(height < 11.5){
    return '10inch';
  }else if(height < 13.5){
    return '12inch';
  }else if(height < 15.5){
    return '14inch';
  }else if(height < 17.5){
    return '16inch';
  }else if(height < 19.5){
    return '18inch';
  }else if(height < 21.5){
    return '20inch';
  }else{
    return '22inch';
  }
}

function mapColor(value, scale='rgb'){
  if(scale =='grayscale'){
    if(value < 0.1){
      return "#000000";
    }
    if(value < 0.10){
      return "#212121";
    }else if(value < 0.20){
      return "#424242";
    }else if(value < 0.30){
      return "#616161";
    }else if(value < 0.40){
      return "#757575";
    }else if(value < 0.50){
      return "#9E9E9E";
    }else if(value < 0.60){
      return "#BDBDBD";
    }else if(value < 0.70){
      return "#E0E0E0";
    }else if(value < 0.80){
      return "#EEEEEE";
    }else if(value < 0.90){
      return "#F5F5F5";
    }else{
      return "#FAFAFA";
    }
  }else{
    if( value < 0.05){
      return "#303F9F";
    }else if(value < 0.10){
      return "#1E88E5";
    }else if( value < 0.20){
      return "#4FC3F7";
    }else if( value < 0.30){
      return "#69F0AE";
    }else if( value < 0.40){
      return "#FFF176";
    }else if( value < 0.50){
      return "#FFCA28";
    }else if( value < 0.60){
      return "#FFA726";
    }else if( value < 0.75){
      return "#EF6C00";
    }else if( value < 0.85){
      return "#F44336";
    }else{
      return "#C62828";
    }
  }
}

module.exports = {
  getSphereColorFactor,
  loadFixtureMaps,
  listFiles,
  csv2multiArray,
  mapColor
};
