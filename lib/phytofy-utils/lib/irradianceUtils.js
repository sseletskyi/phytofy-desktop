const alg = require('../src/map-algorithms.js');
const utils = require('../src/utils.js');
const logger = require('../src/logger.js');

async function irradianceMap(W, L, config, height, channels=['uv', 'blue', 'green', 'hyperred', 'farred', 'white'], distance=0, fixtureDir='testFixtureMaps'){
  let fm = await utils.loadFixtureMaps(fixtureDir, distance, config, channels);
  let luf = {}, canopyMap = {}, maximum = {};
  let lufValue = 0, max = 0, channel = 0;
  const nrChannels = channels.length;

  for( let c = 0; c < nrChannels; c++){
    if(config == '1v' || config == '1h'){
      [canopyMap[fm[c].tag], lufValue, max, channel] = await alg.calcIrradianceMap_1(W,L, fm[c]);
      luf[channel] = lufValue;
      maximum[channel] = max;
    }else if(config == '2v' || config == '2h'){
      [canopyMap[fm[c].tag], lufValue, max, channel] = await alg.calcIrradianceMap_2(W,L, fm[c], distance, config);
      luf[channel] = lufValue;
      maximum[channel] = max;
    }else if(config == '3v' || config == '3'){
      [canopyMap[fm[c].tag], lufValue, max, channel] = await alg.calcIrradianceMap_3V(W,L, fm[c], distance);
      luf[channel] = lufValue;
      maximum[channel] = max;
    }else if(config == '4v' || config == '4'){
      [canopyMap[fm[c].tag], lufValue, max, channel] = await alg.calcIrradianceMap_4V(W,L, fm[c], distance);
      luf[channel] = lufValue;
      maximum[channel] = max;
    }else if(config == '2x2h' || config == '2x2'){
      [canopyMap[fm[c].tag], lufValue, max, channel] = await alg.calcIrradianceMap_2X2H(W,L, fm[c], distance);
      luf[channel] = lufValue;
      maximum[channel] = max;
    }else{
      logger.error(`[irradiance Map] configuration ${config} is not implemented!`);
    }
  }

  return {'luf': luf, 'canopyMap': canopyMap, 'maximumValues': maximum};
};

async function sumAndFormat(canopyMap, maximum, period, channels, raw=false, scale='rgb'){
  //sum channels and format data for heatmap...

  let summed = canopyMap[channels[0]].slice();
  let max = 0;
  let colour = ""
  let data = []
  let sum = 0;


  for(let m = 0; m < summed[0].length; m++){
    for(let n = 0; n < summed.length; n++){
      for(let chan of channels){
        sum += period[chan]*(canopyMap[chan][n][m]/maximum[chan]);
      }
      if(max < sum) max = sum;
      summed[n][m] = sum;
      sum = 0;
    }
  }

  if(raw){
    for(let m = 0; m < summed[0].length; m++){
      for(let n = 0; n < summed.length; n++){
          colour = utils.mapColor(summed[n][m]/max, scale);
          data.push({x: m, y: n, color: colour, irradianceValue: summed[n][m].toFixed(2)});
      }
    }
  }else{
    for(let m = 0; m < summed[0].length; m++){
      for(let n = 0; n < summed.length; n++){
          summed[n][m] /= max;
          colour = utils.mapColor(summed[n][m], scale);
          data.push({x: m, y: n, color: colour, irradianceValue: summed[n][m].toFixed(2)});
      }
    }
  }

  return data;
}

async function computeEnviron(config, luf, W, L, height){
  //height must be in inches
  let numModules = 1;
  if( config == '1v' || config == '1h'){
    numModules = 2;
  }else if( config == '2v' || config == '2h'){
    numModules = 4;
  }else if( config == '3' || config == '3v'){
    numModules = 6;
  }else if( config == '4' || config == '4v' || config == '2x2h'){
    numModules = 8;
  }
  let environ = {"uv": 0, "blue": 0, "green": 0, "hyperred": 0, "farred": 0, "white": 0};
  let retArr = []
  let sfc = await utils.getSphereColorFactor(height);
  let bed = (W*L)*(0.0254*0.0254);
  //console.log(numModules, sfc, bed);

  for(let key in environ){
    environ[key] = sfc[key]*(numModules)*(luf[key]/bed);
    retArr.push(Number(environ[key].toFixed(2)));
  }
  return retArr;
};

module.exports = {
  irradianceMap,
  computeEnviron,
  sumAndFormat
};
