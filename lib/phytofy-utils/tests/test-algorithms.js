let utils = require('../src/utils.js');
let alg = require('../src/map-algorithms.js');
let computeEnviron = require('../lib/irradianceUtils.js').computeEnviron;
let irradianceMap = require('../lib/irradianceUtils.js').irradianceMap;
let sumAndFormat = require('../lib/irradianceUtils.js').sumAndFormat;

async function test1(config){
  let channelArr = ['uv', 'blue', 'green', 'hr', 'fr', 'white'];
  let luf = {}, canopyMap = {}, irradInside;
  let fm = await utils.loadFixtureMaps('../src/FixtureMaps', config, channelArr);

  for( let c = 0; c < channelArr.length; c++){
    [canopyMap[fm[c].tag], value, channel] = await alg.calcIrradianceMap_1(50,30, fm[c], true);
    luf[channel] = value;
  }

  console.log(canopyMap['blue'])
  //let fdata = await utils.formatData(canopyMap['blue']);
  //console.log(luf)
  //console.log(fdata)
  let environ = await computeEnviron(1, luf, 60, 30);
  //console.log(environ);
}

async function test2(config){
  let channelArr = ['uv', 'blue', 'green', 'hr', 'fr', 'white'];
  let luf = {}, canopyMap = {};
  let fm = await utils.loadFixtureMaps('../src/FixtureMaps', config, channelArr);

  for( let c = 0; c < channelArr.length; c++){
    [canopyMap[fm[c].tag], value, channel] = await alg.calcIrradianceMap_2(60,30, fm[c], 7, config);
    luf[channel] = value;
  }

  console.log(luf)
  let environ = await computeEnviron(2, luf, 60, 30);
  console.log(environ);
}

async function test3(){
  let channelArr = ['uv', 'blue', 'green', 'hr', 'fr', 'white'];
  let luf = {}, canopyMap = {};
  let fm = await utils.loadFixtureMaps('../src/FixtureMaps', 'V', channelArr);

  for( let c = 0; c < channelArr.length; c++){
    [canopyMap[fm[c].tag], value, channel] = await alg.calcIrradianceMap_3V(100,30, fm[c], 6);
    luf[channel] = value;
  }

  console.log(luf)
  let environ = await computeEnviron(3, luf, 100, 30);
  console.log(environ)
}

async function test4(){
  let channelArr = ['uv', 'blue', 'green', 'hr', 'fr', 'white'];
  let luf = {}, canopyMap = {};
  let fm = await utils.loadFixtureMaps('../src/FixtureMaps', 'V', channelArr);

  for( let c = 0; c < channelArr.length; c++){
    [canopyMap[fm[c].tag], value, channel] = await alg.calcIrradianceMap_4V(110,40, fm[c], 6);
    luf[channel] = value;
  }

  console.log(luf)
  let environ = await computeEnviron(3, luf, 110, 40);
  console.log(environ)
}

async function test2X2(){
  let channelArr = ['uv', 'blue', 'green', 'hr', 'fr', 'white'];
  let luf = {}, canopyMap = {};
  let fm = await utils.loadFixtureMaps('../src/FixtureMaps', 'H', channelArr);

  for( let c = 0; c < channelArr.length; c++){
    [canopyMap[fm[c].tag], value, channel] = await alg.calcIrradianceMap_2X2H(110,40, fm[c], 6);
    luf[channel] = value;
  }

  console.log(luf)
  let environ = await computeEnviron(3, luf, 110, 40);
  console.log(environ)
}

async function test(){
  let channelArr = ['uv','blue', 'green', 'farred', 'hyperred', 'white'];
  let luf = {}, canopyMap = {};
  let retObj = null;
  let w = 85, l = 30, height = 19, distance=6;
  let config = '2v';

  retObj = await irradianceMap(l,w,config, height, channelArr, distance, '../src/FixtureMaps');
  luf = retObj['luf'];

  //canopyMap = await sumAndFormat(retObj['canopyMap'], retObj['maximumValues'], {uv: 34, blue: 56, green: 64} ,channelArr);

  //console.log(canopyMap)
  //console.log(luf)
  let environ = await computeEnviron(config, luf, w, l, height);
  console.log(environ)
}

//test1('H');
//test2('2H');
//test2('2V');
//test3();
//test4();
//test2X2();
test();
