const utils = require('../src/utils.js');
const assert = require('assert');

describe('utils tests', function(){
  let testDir = '../src/testFixtureMaps';
  let dir = '../src/FixtureMaps'
  describe('#listFiles()', function() {
    it('should return the expected list of files', async function() {
      let matchLizt = [
          'blue_H_sample_fm.csv',
          'blue_V_sample_fm.csv',
          'fr_H_sample_fm.csv',
          'fr_V_sample_fm.csv',
          'green_H_sample_fm.csv',
          'green_V_sample_fm.csv',
          'hr_H_sample_fm.csv',
          'hr_V_sample_fm.csv',
          'unittest.csv',
          'uv_H_sample_fm.csv',
          'uv_V_sample_fm.csv',
          'white_H_sample_fm.csv',
          'white_V_sample_fm.csv'
        ];
        let lizt = await utils.listFiles(testDir);
      assert.deepEqual(lizt, matchLizt);
    });
  });
  describe('#loadFixtureMaps()', function() {
    it('should return the blue channel vertical fixture map 8 inch height', async function() {
      let fm = await utils.loadFixtureMaps(dir, 8, '1v', ['blue']);
      assert.equal(fm[0].tag, 'blue');
    });
  })
  describe('#csv2multiArray()', function() {
    let testMap = new Array(5); //first create rows
    for(let l = 0; l < 5; l++){
      testMap[l] = new Array(10);
      testMap[l].fill(l);
    }
    it('should return the correct NxM array', async function(){
      let map = await utils.csv2multiArray(testDir+'/unittest.csv');
      assert.deepEqual(testMap, map);
    });
  });
});
