FROM ubuntu:bionic-20190204

ADD . /project
WORKDIR /project
RUN /bin/sh /project/ci.prepare.sh && /bin/sh /project/ci.build.sh

CMD ["/bin/sh", "-c", "cp /project/release/*.exe /project/release/*.AppImage /release/"]
