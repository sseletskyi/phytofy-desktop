# Osram Phytofy RL SW Documentation

## Overview

The Osram Phytofy RL Software is built using **Electron**, a framework for building native desktop applications using web technolgies. You can find more about electron [here] (https://electronjs.org/).

The application uses **React** and **Redux**. [React](https://reactjs.org/) is a JavaScript library for building reactive UIs with minimum overhead. [Redux] (https://redux.js.org/) is a centralized and reliable state container for JavaScript apps.

*Some notes on nomenclature, all canopy/canopies are interchangeable with group/groups and profile/profiles are interchangeable with layout/layouts.*

## The app tree directory

(*click to expand*)

* <details> <summary>actions</summary> 
	* contains the actions files for each data object (canopies/groups, profiles/layouts, recipes, schedules), as well as sidebar and helper actions. 
	* Group/Canopy related action are divided between *canopies.js* and *canopiesSetup.js* The former refers to the overview and details pages. The latter refers to the Group listing page (CanopiesSetupPage.js, aswell as realTime related functions. They also point to different object on the application state, the canopies object and the canopiesSetup object.
	* Devices actions include actions related to both moxas and fixture objects.
	* Irradiance Maps for showing on the details page are computed on the schedules actions, when retrieving the schedules for the selected group.
	* See more about how actions work [here](stateHandling.md).

* <details> <summary>reducers</summary> 
	* Contains the reducers for each action file. Reducers tell how each action changes the application state. To see how the application handles state go [here](stateHandling.md).	
	
* <details> <summary>containers</summary> 
	Containers whose names ends in *Page*, contain actual pages that get rendered. Containers that end in form are the base containers for create or edit forms, which end in *Create* or *Edit*.
	
	Containers that interface directly with the application's state, have the *mapStateToProps* function that makes state objects available to the container via it's props. For example:
	
		function mapStateToProps(state) {
  			return {
  				objects: state.schedules.objects,
  			}
  		}
  		
 In this case, you can access *this.props.objects* to access the schedule objects on the application. Next, a list of all containers, what they are for and their dependencies:
 
	* **AddCanopyToSchedule** - sidebar component to add a group to a schedule.
	* **AddFixtureToCanopy** - sidebar component to add a fixture to a canopy.
	* **App** - Main component that renders the main navigation components.
	* **CanopiesSetupPage** - corresponds to the *Groups* page on the software. Makes use of ListContainerDataOptions container.
	* **CanopyDetailsPage** - corresponds to the details page when clicking on a group card on overview.
	* **CanopyForm** - the base component for *CanopyFormCreate* and *CanopyFormEdit*, for creating and editing groups.
	* **CanopyFormCreate** - the component form to create groups, gets rendered on the right sidebar.
	* **CanopyFormEdit** - the component form to edit groups, gets rendered on the right sidebar.
	* **CanopyProfileForm** - the base component for *CanopyProfileFormCreate* and *CanopyProfileFormEdit*, for creating and editing layouts.
	* **CanopyProfileFormCreate** - the component form to create layouts, gets rendered on the right sidebar.
	* **CanopyProfileFormEdit** - the component form to create layouts, gets rendered on the right sidebar.
	* **CreditsPage** - renders the credits page on the about section.
	* **DevicesPage** - this is the *Adapters* page on the software, it makes use of the ListContainer.
	* **FiltersContainer** - this container is used on list containers or on pages with lists, like schedules page or day recipes page.
	* **FixturesPage** - this is the *Light Fixtures* page on the sfotware, it makes use of ListContainerDataOptions.
	* **FormContainer** - this is the base container for forms, it is used by *CanopyForm*, *CanopyProfileForm*, *RecipeForm*, *ScheduleForm* and *ScheduleFormDetails*.
	* **GridContainer** - this is the container for each group card that appears on Overview.
	* **helpers** - helper functions used on containers.
	* **HomePage** - this renders the overview page, it makes use of *GridContainer*.
	* **ListContainer** - the most simple of the list containers.
	* **ListContainerDataOptions** - adds invisible rows to the list containers, which appear when hovering - no overlapping of icons.
	* **ListContainerRowOptions** - adds row options to the list, row options are options that appear over a row when hovered upon.
	* **ProductInfoPage** - page with the product info.
	* **ProfilesPage** - corresponds to the *Layouts* page on the application. Makes use of *ListContainerRowOptions*.
	* **QuickGuidePage** - corresponds to the quick guide page on the application. Equal to what the homepage renders when there are no groups created.
	* **RealTimeContainer** - the container for real-time control, makes use of LightController and LightControllerFixtures (from the design system) for controlling the light channel scrollbars. Is rendered on the right sidebar, with maximum width.
	* **RecipeForm** - the base component for *RecipeFormCreate* and *RecipeFormEdit*, for creating and editing recipes. Makes use of *RecipeItemContainer* and *Timeline*.
	* **RecipeFormCreate** - the form container to create recipes, gets rendered on the right sidebar, with maximum width.
	* **RecipeFormEdit** - the form container to edit recipes, gets rendered on the right sidebar, with maximum width.
	* **RecipeItemContainer** - this container is used by *RecipeForm*, it holds most of the visible components of the recipe create/edit forms, including the LightController (for controlling the light channel scrollbars) as well as the accordion, for expanding/collapsing periods.
	* **RecipesPage** - corresponds to the *Day Recipes* page on the application. Makes use of ListContainerRowOptions*.
	* **ScheduleForm** - the base component for *ScheduleFormCreate* and *ScheduleFormEdit*, for creating and editing schedules.
	* **ScheduleFormCreate** - the form component to create schedules, gets rendered on the right sidebar.
	* **ScheduleFormEdit** - the form component to edit schedules, gets rendered on the right sidebar.
	* **ScheduleFormDetails** - the base component for *ScheduleFormAddToCanopy* to add schedules to a group on details page.
	* **ScheduleFormAddToCanopy** - the form component to add schedules to a group on details page, gets rendered on the right sidebar.
	* **SchedulesPage** -  corresponds to the *Schedules* page on the application, makes use of *ListContainerDataOptions*.
	* **SettingsPage** - corresponds to the *Settings* page on the application.
	* **TermsPage** - corresponds to the *Terms of Service* page on the application, on the about section.
	* **TopbarContainer** - this component is used to render the topbar secondary menu, appears on the configuration and about sections.

* <details> <summary>models</summary> 
	* Each file corresponds to an object on the database (each object actually corresponds to a databse file). The database used is [nedb](https://github.com/louischatriot/nedb). 
	* Model files also include validation rules as well as import and export functions as well as any other function related to databse handling, such as deleting schedules from fixtures or discovering moxas and commissioning fixtures (which includes communicating with the drivers).	

* <details> <summary>utils</summary> 
	* **FormValidator** - the main logic for basic form validation, according to the rules set in each model file (those that have forms associated with them, like profiles, canopies, recipes and schedules).
	* **notifications** - notification style configurations for the notifications package, *react-notification-system-redux*.

* <details> <summary>main.dev.js</summary>
	* this file contains the main proccess. Changes to the logger transport (see [electron-log](https://www.npmjs.com/package/electron-log)) must be made here. This is also where the app's windows are manages.

* <details> <summary>menu.js</summary>
	* this file builds the application's menu window on top of main.dev.
	* Uncomment the line at:
	
			buildMenu() {
    		//this.mainWindow.openDevTools();
	
		To enable developer console on the build. This can be useful to 		debug the application after building.
		
* <details> <summary>routes.js</summary>
	* this file contains the routing information for navigating on the application. It maps paths to the *Pages* containers.

* <details> <summary>static</summary>
	* contains the .csv files with the various fixture maps for irradiance map calculations.
	* On the base directory's package.json you must add "static/" under the files array under the build object.
## Libs

For the application to function the following three libraries need be included under the lib directory:

* **phytofy-design-system** - includes most of the base components used on the containers. It also contains the LightController and LightControllerFixture containers, which have the logic for handling the light channel scrollbars and spectrum charts.

* **phytofy-drivers** - implements the communication with the fixtures via RS485 via the moxas devices. It implements the discover moxas and fixture commissioning algorithms.

* **phytofy-utils** - implements some auxiliary computations, like computing the irradiance maps, summing active irradiance maps (depending on active light channels), normalization and formatting. Also has functions to compute environment variables for maximum irradiance calibration.


## Application state handling

Read about how state handling, actions and reducers work [here](stateHandling.md)

## Phytofy Drivers

This library handles communication between the client software and the phytofy fixtures through TCP sockets. It also includes a moxa device discovery algorithm.

### Tree Directory

Under the **lib directory** you can find the high level functions to be imported by the dashboard pertaining to communication with the phytofy fixtures.

* <details> <summary>moxa-discovery.js</summary>
	* Implements the moxa discovery algorithm, exports function *discoverMoxas*

* <details> <summary>rs485-protocol.js</summary>
	* Implements the rs485 protocol to communicate with the phytofy fixtures through TCP sockets, serialization is handled by the moxa devices. Exports the class RS485protocol.
	
Under the **src directory** you can gind the auxiliary functions and constants for the communication classes.

* <details> <summary>compute.js</summary>
	* Implements CRC correction, type conversions and other loose auxiliary functions for the communication.

* <details> <summary>defines.js</summary>
	* Exports constants related to communications: 
		* BROADCAST_PERIOD, default 200 (this value is currently only used for moxa discovery. fixture commissioning broadcast period is hardcoded as 500ms).
		* BROADCAST_TIMEOUT, defaultm 1000ms
		* MOXA_BROADCAST_MSG - as per documentation
		* MOXA_START_PORT, default 4001

* <details> <summary>logger.js</summary>
	* Winston logging, mainly used for debugging during development. RS485protocol class also uses electron log to log issues in production.

* <details> <summary>moxa-discovery_utils.js</summary>
	* Implements auxiliary functions for moxa discovery algorithm.

Under the **tests directory** you can find unit, functional and integration tests for this package library.

### Running the tests

#### Unit tests

Unit tests are on the base directory of **/tests**. They use the mocha testing framework and can be run using:

		mocha test-moxaDiscovery.js
		mocha test_rs485protocol.js
		
Alternatively the package.json for phytofy-drivesr can be updated to run all unit tests using *npm run test* command. 
#### Functional tests

These are some simple functional tests for moxa moxaDiscovery and the rs485protocol tests to be run using [ncat ](https://nmap.org/ncat/), a very useful and flexible network utility from the makers of nmap. At this level of maturity of the drivers these tests are, however, less relevant.

#### Integration tests

The **/integration/test.js** file contains some ad hoc tests to test communication and commissioning with real phytofy fixtures.

## Phytofy Utils

This library implements the irradiance map algorithms and computes the environment variables for maximum irradiance calibration of the phytofy fixtures.

### Tree Directory

Under the **lib directory** you can find the high level functions to be imported by the dashboard.

* <details> <summary>irradianceUtils.js</summary>
	* **irradianceMap** -  this function computes the irradiance maps for a given group dimensions and configuration for the selected channels. On the dashboard it is normally used for all channels. Summation over channels is made on sumAndFormat.
	* **sumAndFormat** - this function sums the various calculated irradiance maps for different channels according to a period array with irradiance values for each channel. It also formats data for display: can show normalized or actual irradiance values on tooltip, use rgb or gray scale for coloring. 
	* **computeEnviron** - this function computes environment variables for calibration of phytofy fixtures' maximum irradiance according to Osram's documentation.

Under the **src** directory you can find fixture maps csv files, auxiliary functions and the actual algorithms for each kind of group configuration.

* <details> <summary>FixtureMaps</summary>
	* This directory includes the production fixture maps csv files provided by Osram.

* <details> <summary>testFixtureMaps</summary>
	* This directory includes the testing fixture maps csv files provided by Osram.

* <details> <summary>logger.js</summary>
	* Winston logging, mainly used for debugging during development. 

* <details> <summary>map-algorithms.js</summary>
	* Implements and exports the actual algorithms for the various group configurations.

* <details> <summary>utils.js</summary>
	* Exports auxiliary functions for mapping values to color, loading the correct fixture map csv files and getting the correct sphere color factors.

Under the **tests** directory you can find unit tests for the util functions and some ad hoc tests for the algorithms. 

### Running the tests

The Unit tests use the mocha testing framework and can be run using:

		mocha test-utils.js
		
Alternatively the package.json for phytofy-utils can be updated to run all unit tests using *npm run test* command. 