# Application State Handling

## Overview

All data objects, like groups, layouts, schedules, recipes, settings as well as other relevant variables for the flow of the application. With redux, the application state cannot be edited directly, reducing any chance of misnhandling the state. For that we use actions and reducers.

For more on redux check the tutorials at redux's [homepage](https://redux.js.org/).

## Actions

Actions send data payloads from the application to the store, in the form of objects. This is done using dispatch. For Example: 
		
		export const getIrradianceMap = (canopyId, channelArr, periodValues) => {
  			return async (dispatch, getState) => {
  				...
				dispatch({
        			type: "SCHEDULES_GET_IRRADIANCE_MAP",
        			map
    			});

    	
## Reducers

On the other hand, reducers specify how each action changes the application state. For the action on the previous example:

	case 'SCHEDULES_GET_IRRADIANCE_MAP':
          return {
              ...state,
              irradianceMap: action.map
    }
    
This results in a simple update of the irradianceMap object of the schedules object on the application's state, *state.schedules.irradianceMap*. 

## Store

The Store brings actions and reducers together and it contains the application state in a single *state* object. Via the store you can:

* access the state with *getState()*
* update the state with *dispatch(action)*

On container files we call *bindActionCreators(actionCreators, dispatch)* so you can directly call dispatch and getState inside actions as you can see on the action example above. For more on this check the [documentation](https://redux.js.org/api/bindactioncreators).