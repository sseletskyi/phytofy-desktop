#!/bin/sh

set -e

DEBIAN_FRONTEND=noninteractive

apt-get update -yq
apt-get install -yq gpg-agent apt-transport-https software-properties-common curl unzip build-essential

curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get update -yq
apt-get install -yq nodejs

WINE_VERSION=winehq-staging

dpkg --add-architecture i386
apt-get update -qy
apt-get install --no-install-recommends -qfy apt-transport-https software-properties-common curl unzip rename
curl -OL https://dl.winehq.org/wine-builds/winehq.key
apt-key add winehq.key
add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'
apt-get update -qy
apt-get install --no-install-recommends -qfy $WINE_VERSION
apt-get clean
