# Phytofy Dashboard

## Documentation

You can find the documentation under the /doc directory, [here](./docs/index.md)

## Commands
The package.json file contains all the commands available under *scripts*.

Tu run in dev environment run the command: npm run dev

### Building

Before building the application install the node dependencies with: npm install

Also, the following libs need to be installed in the lib directory:

phytofy-design-system, phytody-drivers and phytofy-utils.

To build the application run:

*for all distributions*:

  npm run package-all

*for each distribution individually*:

  npm run package-linux

  npm run package-mac

  npm run package-win

## Troubleshooting

In the file `menu.js`, there are two functions: `buildMenu()` and `setupDevelopmentEnvironment()`, the first takes care of production build,
the second is the dev environment. In both, there is a line `this.mainWindow.openDevTools();`, which is commented in the `buildMenu()`,
one can uncomment it and build your source, and it will open the devTools on start up to see the runtime errors, crashes, etc.
