const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

let Fixture;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Fixture = new Datastore({ filename: `${userDataPath}/fixtures.db`, autoload: true });

  return Fixture;
}

export default Fixture ? Fixture : createDatastore();
