const electron = require('electron');
const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

import FormValidator from '../utils/FormValidator';
import Fixture from './Fixture';

import Profile from '../models/Profile';

let Canopy;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Canopy = new Datastore({ filename: `${userDataPath}/canopies.db`, autoload: true });
  try {
    Canopy.ensureIndex({ fieldName: 'name', unique: true });
  } catch (err) {
    console.error(err);
  }

  return Canopy;
}

export default Canopy ? Canopy : createDatastore();

const rules = [
  {
    field: 'name',
    method: validator.isEmpty,
    validWhen: false,
    message: 'Name is required'
  },
  {
    field: 'name',
    method: async (value) => {
      let count = await Canopy.count({name: value});
      return count == 0;
    },
    validWhen: true,
    ignoreEqualsPrev: true,
    message: 'A group with this name already exists'
  },
  {
    field: 'name',
    method: validator.isLength,
    args: [{min: 0, max: 30}],
    validWhen: true,
    message: 'Name exceeds the maximum length of 30'
  },
  {
    field: 'name',
    method: async (value) => {
      return /^[A-Za-z\d\s]+$/.test(value)
    },
    validWhen: true,
    message: 'Name contains invalid characters'
  },
];

export const CanopyFormValidator = new FormValidator(rules);

export const getFixturesFromCanopy = async (canopy) => {
  return await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
}

export const exportData = async () => {
  const Json2csvParser = require('json2csv').Parser;
  let table = []
  let aux = {}
  let objects = await Canopy.cfind({}).exec();
  let fields = ['name', 'layout name', 'layout config', 'fixtures'];

  for(let obj of objects){
    //aux._id = obj._id;
    aux.name = obj.name;
    aux['layout name'] = obj._profile.name;
    aux['layout config'] = obj._profile.config;
    aux.fixtures = obj.fixtures;
    table.push(aux);
    aux = {};
  }

  const parser = new Json2csvParser({fields});
  const csv = parser.parse(table);

  return csv;
}

export const importData = async (data) => {
  let canopyObjs = [];

  for(let row of data){
    let rfixtures;
    if(row.fixtures == "") rfixtures = [];
    else{
      try{
        rfixtures = JSON.parse(row.fixtures);
      }catch(err){
        rfixtures = [];
      }
    }
    canopyObjs.push({
      //_id: row._id,
      name: row.name,
      fixtures: rfixtures,
      fInfo: {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0},
      _profile:{
        name : row['layout name'],
      }
    });
  }

  let canopyNames = canopyObjs.map((obj) => {
    return obj.name;
  });

  let profileNames = canopyObjs.map((obj) => {
    return obj._profile.name;
  });

  let existingCanopies = await Canopy.cfind({name: {$in: canopyNames}}).exec();
  let existingCanopyNames = existingCanopies.map((obj) => {
    return obj.name;
  })

  let existingProfiles = await Profile.cfind({name: {$in: profileNames}}).exec();
  let existingProfileNames = existingProfiles.map((obj) => {
    return obj.name;
  })

  let createdCanopies = [];
  let errorCount = 0;

  console.log(canopyObjs);
  for(let obj of canopyObjs) {
    let duplicated = null;
    if(existingCanopyNames.indexOf(obj.name) > -1) {
      duplicated = await Canopy.findOne({name: obj.name+'-imported'});
      obj.name = obj.name +"-imported";
    }
    if(existingProfileNames.indexOf(obj._profile.name) > -1){
      let profile = await Profile.findOne({name: obj._profile.name});
      obj.profileId = profile._id;
      obj._profile = profile;
      let takenFixtures = await Canopy.cfind({fixtures: {$in: obj.fixtures}}).exec();
      
      if(takenFixtures.length == 0 && duplicated == null){
        await Canopy.insert(obj);
        createdCanopies.push(obj);
        await Profile.update({_id: profile._id}, {'$inc': {nCanopies: 1}});
      }else{
        errorCount++;
        if(duplicated == null){
          log.error(`[Import] cannot import group ${obj.name} - fixtures already taken`);
        }else{
          log.error(`[Import] cannot import group ${obj.name} - name already taken! (with -imported appended also taken)`);
        }
      }
    }else{
      //if profile does not exist, insert profile and create canopy
      errorCount++;
      log.error(`[Import] cannot import group ${obj.name} with non-existent layout ${obj._profile.name}`);
    }
  };

  return {created: createdCanopies, errorCount: errorCount};
}

async function compareProfiles(profi1, profi2){
  if( profi1.config != profi2.config) return false;
  if( profi1.width != profi2.width) return false;
  if( profi1.length != profi2.length) return false;
  if( profi1.deltaWidth != profi2.deltaWidth) return false;
  if( profi1.deltaLength != profi2.deltaLength) return false;
  return true;
}
