const electron = require('electron');
const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';
import moment from 'moment';

import FormValidator from '../utils/FormValidator';
import { runCommandOnFixtures} from '../actions/helpers';
import Canopy from '../models/Canopy';
import Recipe from '../models/Recipe';
import Fixture from '../models/Fixture'

let Schedule;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Schedule = new Datastore({ filename: `${userDataPath}/schedules.db`, autoload: true });

  return Schedule;
}

const rules = [
  {
    field: 'start_date',
    method: validator.isEmpty,
    validWhen: false,
    message: 'Start date is required'
  },
  {
    field: 'end_date',
    method: validator.isEmpty,
    validWhen: false,
    message: 'End date is required'
  },
  {
    field: 'end_date',
    method: async (value, state) => {
      if(state.end_date) return value >= state.start_date
      return true;
    },
    validWhen: true,
    message: 'Invalid date'
  },
  {
    field: 'canopies',
    method: async (canopyIds, state) => {
      let schedulesFromCanopy;
      let valid = true;
      for(let canopyId of canopyIds){
        try {
          schedulesFromCanopy = await Schedule.cfind({canopies: canopyId}).exec();
          //schedulesFromCanopy = schedulesFromCanopy.sort(function(a,b) {return moment(a.end_date) < moment(b.end_date) ? 1 : ((moment(b.end_date) < moment(a.end_date)) ? -1 : 0)} );
          console.log(schedulesFromCanopy);
          let startDate = new Date(state.start_date);
          let endDate = new Date(state.end_date);
          valid = await validateSchedules(schedulesFromCanopy, startDate.getTime(), endDate.getTime(), state._id);
        } catch(err) {
          console.log(err);
        }
      }
      return valid;
    },
    validWhen: true,
    message: 'There is already an overlapping schedule on at least one group.'

  }
]

export default Schedule ? Schedule : createDatastore();

export const ScheduleFormValidator = new FormValidator(rules);

export const addScheduleToFixtures = async (schedule, fixtures, comm, dispatch) => {
  //console.log(schedule);
  let curr = moment(schedule.start_date);
  let end = moment(schedule.end_date);
  //console.log(`start: ${curr}, end: ${end}`);
  let ack = -1;

  let scheduleFixtures = schedule.fixtures ? {...schedule.fixtures} : {};

  for(var i=0; i<schedule._recipe.periods.length; i++){
      let period = schedule._recipe.periods[i];
      let periodStartHours = parseInt(period.start.split(":")[0]);
      let periodStartMinutes = parseInt(period.start.split(":")[1]);

      let recipeStartDate = curr.hours(periodStartHours).minutes(periodStartMinutes).unix();
      //console.log(recipeStartDate);

      let periodEndHours = parseInt(period.end.split(":")[0]);
      let periodEndMinutes = parseInt(period.end.split(":")[1]);

      let recipeEndDate = end.hours(periodEndHours).minutes(periodEndMinutes).unix();
      //console.log(recipeEndDate);

      await runCommandOnFixtures(fixtures, comm, dispatch, async (fixture) => {
        let scheduleId = Math.floor(Math.random()*4294967295);

        // let scheduleId = `${currentTimestampStr.substring(currentTimestampStrLength-4,currentTimestampStrLength)}${fixture.serial_num}${i}`;
        if(!Object.keys(scheduleFixtures).includes(fixture.name)) {
          scheduleFixtures[fixture.serial_num] = [scheduleId];
        } else {
          scheduleFixtures[fixture.serial_num].push(scheduleId);
        }
        ack = await comm.setTimeReference(fixture.serial_num, moment().unix());
        if(ack == 0) log.error(`[Add Schedule to Fixture: ${fixture.serial_num}] Error setting time reference `);
        console.log(`Schedule ID: ${scheduleId} | fixture: ${fixture.serial_num}`);
        ack = await comm.setSchedule(fixture.serial_num, scheduleId, recipeStartDate, recipeEndDate,
            [
                period.uv,
                period.blue,
                period.green,
                period.hyperRed,
                period.farRed,
                period.white
            ])
        if(ack == 0 || ack.length > 1) log.error(`[Add Schedule to Fixture: ${fixture.serial_num}] Error saving schedule to fixture`);
      })
  }

  console.log(scheduleFixtures);
  await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
  if( ack == 1){
      //
      //log.info(`[Add Schedule to Fixture] Success!`)
      return 1;
  }else {
    return 0;
  }

}

export const addScheduleToFixturesOld = async (schedule, fixtures, comm, dispatch) => {
  let curr = moment(schedule.start_date);
  let end = moment(schedule.end_date).add(1, 'd');
  let ack = -1;

  let scheduleFixtures = schedule.fixtures ? {...schedule.fixtures} : {};

  while(curr < end) {
    for(var i=0; i<schedule._recipe.periods.length; i++){
        let period = schedule._recipe.periods[i];
        let periodStartHours = parseInt(period.start.split(":")[0]);
        let periodStartMinutes = parseInt(period.start.split(":")[1]);

        let recipeStartDate = curr.hours(periodStartHours).minutes(periodStartMinutes).unix();

        let periodEndHours = parseInt(period.end.split(":")[0]);
        let periodEndMinutes = parseInt(period.end.split(":")[1]);

        let recipeEndDate = curr.hours(periodEndHours).minutes(periodEndMinutes).unix();
        await runCommandOnFixtures(fixtures, comm, dispatch, async (fixture) => {
          let scheduleId = Math.floor(Math.random()*4294967295);

          // let scheduleId = `${currentTimestampStr.substring(currentTimestampStrLength-4,currentTimestampStrLength)}${fixture.serial_num}${i}`;
          if(!Object.keys(scheduleFixtures).includes(fixture.name)) {
            scheduleFixtures[fixture.serial_num] = [scheduleId];
          } else {
            scheduleFixtures[fixture.serial_num].push(scheduleId);
          }
          ack = await comm.setTimeReference(fixture.serial_num, moment().unix());
          if(ack == 0) log.error(`[Add Schedule to Fixture: ${fixture.serial_num}] Error setting time reference `);
          ack = await comm.setSchedule(fixture.serial_num, scheduleId, recipeStartDate, recipeEndDate,
              [
                  period.uv,
                  period.blue,
                  period.green,
                  period.hyperRed,
                  period.farRed,
                  period.white
              ])
          if(ack == 0) log.error(`[Add Schedule to Fixture: ${fixture.serial_num}] Error saving schedule to fixture`);
        })
    }

    curr = curr.add(1, 'd');
  }

  console.log(scheduleFixtures);

  // if(scheduleFixtures instanceof Array) await Schedule.update({_id: schedule._id}, {$addToSet: {fixtures: {$each: scheduleFixtures}}});
  // else await Schedule.update({_id: schedule._id}, {$addToSet: {fixtures: scheduleFixtures}});
  await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
  if( ack == 1){
      //
      //log.info(`[Add Schedule to Fixture] Success!`)
      return 1;
  }else {
    return 0;
  }
}

export const removeScheduleFromFixtures = async (schedule, fixtures, comm, dispatch) => {
  let scheduleFixtures = schedule.fixtures ? {...schedule.fixtures} : {};
  let ack = -1;
  console.log(scheduleFixtures);
  await runCommandOnFixtures(fixtures, comm, dispatch, async (fixture) => {
    if(!schedule.fixtures[fixture.serial_num]) return;

    for(let i=0; i<schedule.fixtures[fixture.serial_num].length; i++) {
      let scheduleId = schedule.fixtures[fixture.serial_num][i];
      ack = await comm.deleteSchedule(fixture.serial_num, scheduleId);
      if(ack == 0) log.error(`[Remove schedule from Fixture: ${fixture.serial_num}] Error removing schedule`);
    }

    //let {[fixture.serial_num]: omit, ...scheduleFixtures} = newScheduleFixtures;
    scheduleFixtures[fixture.serial_num].length = 0;
    //scheduleFixtures = newScheduleFixtures;
  });

  await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
  console.log(scheduleFixtures);
  if(ack == 1){
      //await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
      log.info(`[Remove schedule from Fixture] Success!`);
  }
}

export const removeScheduleFromCanopy = async (schedule, canopyId, fixtures, comm, dispatch) => {
  let scheduleFixtures = schedule.fixtures ? {...schedule.fixtures} : {};
  let ack = -1;
  let errorCount = 0;

  await runCommandOnFixtures(fixtures, comm, dispatch, async (fixture) => {
    if(!schedule.fixtures[fixture.serial_num]) return;
    else console.log(schedule.fixtures[fixture.serial_num]);
    let errors = 0;

    for(let i=0; i<schedule.fixtures[fixture.serial_num].length; i++) {
      let scheduleId = schedule.fixtures[fixture.serial_num][i];
      console.log(scheduleId);
      ack = await comm.deleteSchedule(fixture.serial_num, scheduleId);
      console.log(`GOT ACK: ${ack}`);
      if(ack == 0 || ack.length > 1){
        log.error(`[Remove schedule from Fixture: ${fixture.serial_num}] Error removing schedule`);
        errorCount += 1;
        errors += 1;
      }
    }

    //let {[fixture.serial_num]: omit, ...newScheduleFixtures} = scheduleFixtures;
    //scheduleFixtures = newScheduleFixtures;
    if(errors == 0) scheduleFixtures[fixture.serial_num].length = 0;
  });

  if(ack == 1){
    console.log(`UPDATING SCHEDULES ?!`);
    await Schedule.update({_id: schedule._id}, {$set: {fixtures: scheduleFixtures}});
    let idx = schedule.canopies.indexOf(canopyId);
    await Schedule.update({_id: schedule._id}, {$set: {canopies: [...schedule.canopies.slice(0, idx), ...schedule.canopies.slice(idx+1)]}});
    log.info(`[Remove schedule from Fixture] Success!`);
  }
  return errorCount;
}

export const exportData = async () => {
  const Json2csvParser = require('json2csv').Parser;

  let objects = await Schedule.cfind({}).exec();
  for(let obj of objects){
    obj.canopyNames = '';
    for(let i = 0; i < obj.canopies.length; i++){
      if(i != 0) obj.canopyNames += '/';
      let canopy = await Canopy.findOne({_id: obj.canopies[i]});
      if(canopy != null){
        obj.canopyNames += canopy.name;
        console.log(obj.canopyNames);
      }
    }
  }

  let fields = [
    //{value: '_id', label: 'id'},
    {value: 'start_date', label: 'start date'},
    {value: 'end_date', label: 'end date'},
    //{value: 'recipeId', label: 'recipe id'},
    {value: '_recipe.name', label: 'recipe name'},
    {value: 'canopyNames', label: 'groups'},
    // {value: '_recipe.periods.start', label: 'period start'},
    // {value: '_recipe.periods.end', label: 'period end'},
    // {value: '_recipe.periods.uv', label: 'uv'},
    // {value: '_recipe.periods.blue', label: 'blue'},
    // {value: '_recipe.periods.green', label: 'green'},
    // {value: '_recipe.periods.hyperRed', label: 'hyper Red'},
    // {value: '_recipe.periods.farRed', label: 'far Red'},
    // {value: '_recipe.periods.white', label: 'white'},
    // {value: '_recipe.periods.total', label: 'total'}
  ];

  const parser = new Json2csvParser({fields});
  const csv = parser.parse(objects);

  return csv;
  console.log(csv);
}

export const importData = async (data) => {
  let scheduleObjs = [];

  for(let row of data){
    scheduleObjs.push({
      //_id: row._id,
      name: "",
      canopyNames: row.groups.split('/'),
      start_date: row['start date'],
      end_date: row['end date'],
      _recipe:{
        name: row['recipe name'],
      }
    });
  }

  let recipeNames = scheduleObjs.map((obj) => {
    return obj._recipe.name;
  })

  let existingRecipes = await Recipe.cfind({name: {$in: recipeNames}}).exec();

  let existingRecipeNames = existingRecipes.map((obj) => {
    return obj.name;
  })

  let createdSchedules = [];
  let errorCount = 0;

  for(let obj of scheduleObjs){
    console.log(obj);
    if(existingRecipeNames.indexOf(obj._recipe.name) > -1){
      let recipe = await Recipe.findOne({name: obj._recipe.name});
      obj.recipeId = recipe._id;
      obj._recipe = recipe;
      obj.canopies = [];
      let canopies = await Canopy.cfind({name: {$in: obj.canopyNames}}).exec();
      console.log(canopies);
      console.log(obj.canopyNames);
      if(canopies.length == 0 && obj.canopyNames[0]==""){
        obj = await Schedule.insert(obj);
      }else if(canopies.length == obj.canopyNames.length){
        for(let canopy of canopies){
          let schedulesFromCanopy = await Schedule.cfind({canopies: canopy._id}).exec();
          let startDate = new Date(obj.start_date);
          let endDate = new Date(obj.end_date);
          let valid = await validateSchedules(schedulesFromCanopy, startDate.getTime(), endDate.getTime(), "");
          if(valid){
            obj.canopies.push(canopy._id);
          }else{
            log.error(`Cant add ${canopy.name} to schedule because it has overlapping schedules`);
            errorCount++;
          }
        }
        obj = await Schedule.insert(obj);
        createdSchedules.push(obj);
        log.info(`[Import] imported schedule with success!`);
      }else{
        log.error(`[Import] Cannot import schedule - at least one of the Groups does not exist`);
        errorCount++;
      }
    }else{
      log.error(`[Import] Cannot import schedule - recipe does not exist`);
      errorCount++;
    }
  }

  //await Schedule.insert(objectsToCreate);
  //only add to createdSchedules those that need schedules to load
  return {created: createdSchedules, errorCount: errorCount};
}

export const validateSchedules = async (schedules, startDate, endDate, schedId) => {
  let valid = true;
  for(let schedule of schedules){
    if(schedId != schedule._id){
      let testStart = new Date(schedule.start_date);
      testStart = testStart.getTime();
      let testEnd = new Date(schedule.end_date);
      testEnd = testEnd.getTime();
      console.log(`testing: ${startDate}/${endDate} against ${testStart}/${testEnd}`);
      if(startDate <= testStart && testStart <= endDate){
        //schedule starts in new schedule
        console.log('schedule starts in new schedule');
        valid = false;
      }else if(startDate <= testEnd && testEnd <= endDate){
        //schedule ends in new schedule
        console.log('schedule ends in new schedule');
        valid = false;
      }else if(testStart < startDate && endDate < testEnd){
        //new scheudle is in schedule
        console.log('new scheudle is in schedule');
        valid = false;
      }
    }
  }
  return valid;
}
