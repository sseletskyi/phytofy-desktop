const electron = require('electron');
//const log = require('electron').remote.require('electron-log');
const log = require('electron-log');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

import FormValidator from '../utils/FormValidator';

let Recipe;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Recipe = new Datastore({ filename: `${userDataPath}/recipes.db`, autoload: true });

  return Recipe;
}

const rules = [
    {
      field: 'name',
      method: validator.isEmpty,
      validWhen: false,
      message: 'Name is required'
    },
    {
        field: 'name',
        method: async (value) => {
          let count = await Recipe.count({name: value});
          return count == 0;
        },
        validWhen: true,
        ignoreEqualsPrev: true,
        message: 'A recipe with this name already exists'
      },
      {
        field: 'name',
        method: validator.isLength,
        args: [{min: 0, max: 30}],
        validWhen: true,
        message: 'Name exceeds the maximum length of 30'
      },
      {
        field: 'name',
        method: async (value) => {
          return /^[A-Za-z\d\s]+$/.test(value)
        },
        validWhen: true,
        message: 'Name contains invalid characters'
      },
      {
        field: 'periods',
        method: async (periods) =>{
          return validatePeriods(periods);
        },
        validWhen: true,
        message: 'There are overlapping periods'
      }
]

export default Recipe ? Recipe : createDatastore();

export const RecipeFormValidator = new FormValidator(rules);

export const exportData = async () => {
  const Json2csvParser = require('json2csv').Parser;

  let table = []
  let aux = {}
  let objects = await Recipe.cfind({}).exec();
  // let fields = ['_id', 'name', 'start', 'end', 'uv', 'blue', 'green', 'hyperRed', 'farRed', 'white', 'total',
  //               'irradianceUVA', 'irradianceBlue', 'irradianceGreen', 'irradianceHyperRed', 'irradianceFarRed', 'irradianceWhite', 'totalIrradiance'];

  let fields = [
    {value: 'name', label: 'name'},
    {value: 'periods.start', label: 'start'},
    {value: 'periods.end', label: 'end'},
    {value: 'periods.uv', label: 'uv'},
    {value: 'periods.blue', label: 'blue'},
    {value: 'periods.green', label: 'green'},
    {value: 'periods.hyperRed', label: 'hyper Red'},
    {value: 'periods.farRed', label: 'far Red'},
    {value: 'periods.white', label: 'white'},
    {value: 'periods.total', label: 'total'}
  ];

  const parser = new Json2csvParser({fields, unwind: ['periods'], unwindBlank: true});
  const csv = parser.parse(objects);

  return csv;
}

export const importData = async (data) => {
  let recipeObjs = [];
  let tmpPeriods = [];
  let tmpObj = {};
  let init = 0, errors=0;

  //map data back to Recipe object
  for(let row of data){
    if(row.name != "" && init == 0){
      tmpObj.name = row.name;
      //tmpObj._id = row.id;
      let tmpPeriodObj = {};
      tmpPeriodObj.start = row.start;
      tmpPeriodObj.end = row.end;
      tmpPeriodObj.uv = Number(row.uv);
      tmpPeriodObj.irradianceUVA = Math.round(tmpPeriodObj.uv*50.0/100)
      tmpPeriodObj.blue = Number(row.blue);
      tmpPeriodObj.irradianceBlue = Math.round(tmpPeriodObj.blue*250.0/100)
      tmpPeriodObj.green = Number(row.green);
      tmpPeriodObj.irradianceGreen = Math.round(tmpPeriodObj.green*100.0/100)
      tmpPeriodObj.hyperRed = Number(row['hyper Red']);
      tmpPeriodObj.irradianceHyperRed = Math.round(tmpPeriodObj.hyperRed*250.0/100)
      tmpPeriodObj.farRed = Number(row['far Red']);
      tmpPeriodObj.irradianceFarRed = Math.round(tmpPeriodObj.farRed*100.0/100)
      tmpPeriodObj.white = Number(row.white);
      tmpPeriodObj.irradianceWhite = Math.round(tmpPeriodObj.white*250.0/100)
      tmpPeriodObj.total = Number(row.total);
      tmpPeriodObj.totalIrradiance = tmpPeriodObj.irradianceUVA + tmpPeriodObj.irradianceBlue +
                                  + tmpPeriodObj.irradianceGreen + tmpPeriodObj.irradianceHyperRed +
                                  + tmpPeriodObj.irradianceFarRed + tmpPeriodObj.irradianceWhite;
      tmpPeriods.push(tmpPeriodObj);
      console.log(tmpPeriods);
      tmpPeriodObj = {};
      init++;
    }else if(row.name != ""){
      tmpObj.periods = tmpPeriods;
      recipeObjs.push(tmpObj);
      tmpObj = {};
      tmpPeriods = [];
      tmpObj.name = row.name;

      let tmpPeriodObj = {};
      tmpPeriodObj.start = row.start;
      tmpPeriodObj.end = row.end;
      tmpPeriodObj.uv = Number(row.uv);
      tmpPeriodObj.irradianceUVA = Math.round(tmpPeriodObj.uv*50.0/100)
      tmpPeriodObj.blue = Number(row.blue);
      tmpPeriodObj.irradianceBlue = Math.round(tmpPeriodObj.blue*250.0/100)
      tmpPeriodObj.green = Number(row.green);
      tmpPeriodObj.irradianceGreen = Math.round(tmpPeriodObj.green*100.0/100)
      tmpPeriodObj.hyperRed = Number(row['hyper Red']);
      tmpPeriodObj.irradianceHyperRed = Math.round(tmpPeriodObj.hyperRed*250.0/100)
      tmpPeriodObj.farRed = Number(row['far Red']);
      tmpPeriodObj.irradianceFarRed = Math.round(tmpPeriodObj.farRed*100.0/100)
      tmpPeriodObj.white = Number(row.white);
      tmpPeriodObj.irradianceWhite = Math.round(tmpPeriodObj.white*250.0/100)
      tmpPeriodObj.total = Number(row.total);
      tmpPeriodObj.totalIrradiance = tmpPeriodObj.irradianceUVA + tmpPeriodObj.irradianceBlue +
                                  + tmpPeriodObj.irradianceGreen + tmpPeriodObj.irradianceHyperRed +
                                  + tmpPeriodObj.irradianceFarRed + tmpPeriodObj.irradianceWhite;
      tmpPeriods.push(tmpPeriodObj);
      tmpPeriodObj = {};
    }else{
      let tmpPeriodObj = {};
      tmpPeriodObj.start = row.start;
      tmpPeriodObj.end = row.end;
      tmpPeriodObj.uv = Number(row.uv);
      tmpPeriodObj.irradianceUVA = Math.round(tmpPeriodObj.uv*50.0/100)
      tmpPeriodObj.blue = Number(row.blue);
      tmpPeriodObj.irradianceBlue = Math.round(tmpPeriodObj.blue*250.0/100)
      tmpPeriodObj.green = Number(row.green);
      tmpPeriodObj.irradianceGreen = Math.round(tmpPeriodObj.green*100.0/100)
      tmpPeriodObj.hyperRed = Number(row['hyper Red']);
      tmpPeriodObj.irradianceHyperRed = Math.round(tmpPeriodObj.hyperRed*250.0/100)
      tmpPeriodObj.farRed = Number(row['far Red']);
      tmpPeriodObj.irradianceFarRed = Math.round(tmpPeriodObj.farRed*100.0/100)
      tmpPeriodObj.white = Number(row.white);
      tmpPeriodObj.irradianceWhite = Math.round(tmpPeriodObj.white*250.0/100)
      tmpPeriodObj.total = Number(row.total);
      tmpPeriodObj.totalIrradiance = tmpPeriodObj.irradianceUVA + tmpPeriodObj.irradianceBlue +
                                  + tmpPeriodObj.irradianceGreen + tmpPeriodObj.irradianceHyperRed +
                                  + tmpPeriodObj.irradianceFarRed + tmpPeriodObj.irradianceWhite;
      tmpPeriods.push(tmpPeriodObj);
    }
  }
  //get last recipe
  tmpObj.periods = tmpPeriods;
  recipeObjs.push(tmpObj);

  console.log(recipeObjs);

  let names = recipeObjs.map((obj) => {
    return obj.name;
  })

  let existingObjects = await Recipe.cfind({name: {$in: names}}).exec();


  let existingNames = existingObjects.map((obj) => {
    return obj.name;
  })

  let objectsToCreate = [];

  for(let obj of recipeObjs){
    let duplicated = null;
    if(existingNames.indexOf(obj.name) > -1) {
      duplicated = await Recipe.findOne({name: obj.name+'-imported'});
      obj.name = obj.name +"-imported";
      //await Recipe.insert(object);
    }
    if(duplicated == null){
      obj.photoperiod = await getPhotoperiod(obj.periods);
      let valid = await validatePeriods(obj.periods);
      if(valid){
        objectsToCreate.push(obj);
      }
      else{
        log.error(`[import Recipes csv] cant import ${obj.name}, there are overlapping periods`);
        errors += 1;
      }
    }else{
      log.error(`[import Recipes csv] cant import, there is already a recipe named ${obj.name}`);
      errors += 1;
    }
  }

  await Recipe.insert(objectsToCreate);

  return errors;
}

export const importJSON = async (recipeObj) => {

  let repeatedObject = await Recipe.findOne({name: recipeObj.name});
  if(repeatedObject != null){
    let repeatedObject2 = await Recipe.findOne({name: recipeObj.name+'-imported'});
    if(repeatedObject2 != null) return -1;
    recipeObj.name = recipeObj.name+'-imported';
  }
  for(let i = 0; i < recipeObj.periods.length; i++){
    recipeObj.periods[i].irradianceUVA = Math.round(recipeObj.periods[i].uv*50.0/100);
    recipeObj.periods[i].irradianceBlue = Math.round(recipeObj.periods[i].blue*250.0/100);
    recipeObj.periods[i].irradianceGreen = Math.round(recipeObj.periods[i].green*100.0/100);
    recipeObj.periods[i].irradianceHyperRed = Math.round(recipeObj.periods[i].hyperRed*250.0/100);
    recipeObj.periods[i].irradianceFarRed = Math.round(recipeObj.periods[i].farRed*100.0/100);
    recipeObj.periods[i].irradianceWhite = Math.round(recipeObj.periods[i].white*250.0/100);
    recipeObj.periods[i].totalIrradiance = recipeObj.periods[i].irradianceUVA + recipeObj.periods[i].irradianceBlue +
                                + recipeObj.periods[i].irradianceGreen + recipeObj.periods[i].irradianceHyperRed +
                                + recipeObj.periods[i].irradianceFarRed + recipeObj.periods[i].irradianceWhite;
  }
  recipeObj.photoperiod = await getPhotoperiod(recipeObj.periods);
  let valid = await validatePeriods(recipeObj.periods);
  if(!valid) return -2;
  await Recipe.insert(recipeObj);
  return 1;
}

export const getPhotoperiod =  async (periods) => {

  let photoperiod = 0;
  if(periods.length > 0){
    for(let period of periods){
      let end = period.end.split(':')
      let start = period.start.split(':')
      let hours = parseInt(end[0])-parseInt(start[0]);
      let minutes = (parseInt(end[1])-parseInt(start[1]))/60;
      photoperiod += (hours+minutes);
    }
  }
  return photoperiod.toFixed(2);
}

async function validatePeriods(periods){
  let valid = true;
  if(periods.length == 1) return true;

  for(let i = 0; i < periods.length; i++){
    let testAgainst = periods.slice();
    testAgainst.splice(i, 1);
    let testEnd = periods[i].end.split(':');
    let testStart = periods[i].start.split(':');
    let testEndMinutes = parseInt(testEnd[0])*60+parseInt(testEnd[1]);
    let testStartMinutes = parseInt(testStart[0])*60+parseInt(testStart[1]);

    for(let k = 0; k < testAgainst.length; k++){
      let eachEnd = testAgainst[k].end.split(':');
      let eachStart = testAgainst[k].start.split(':');
      let endMinutes = parseInt(eachEnd[0])*60+parseInt(eachEnd[1]);
      let startMinutes = parseInt(eachStart[0])*60+parseInt(eachStart[1]);
      valid = await compareTimes(testStartMinutes, testEndMinutes, startMinutes, endMinutes);
      if(!valid) return false
    }
  }
  return valid;
}

export function compareTimes(start1, end1, start2, end2){
  //console.log(`period 1 -  start:${start1}|end:${end1}`);
  //console.log(`period 2 -  start:${start2}|end:${end2}`);
  if(start1 < start2 && end1 <= start2){
    //console.log(`Period 1 before period 2!`);
    return true;
  }else if(start1 >= end2 && end1 > end2){
    //console.log(`Period 1 after period 2!`);
    return true;
  }else{
    //console.log(`Invalid timeframes!`);
    return false;
  }
}
