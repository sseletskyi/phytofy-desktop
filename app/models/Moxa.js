const electron = require('electron');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

let Moxa;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Moxa = new Datastore({ filename: `${userDataPath}/moxas.db`, autoload: true });
  return Moxa;
}

export default Moxa ? Moxa : createDatastore();
