const electron = require('electron');
const log = require('electron-log');

import Promise from 'bluebird';
import Datastore from 'nedb-promise';
import validator from 'validator';

import FormValidator from '../utils/FormValidator';
import i18n from '../../i18n';

import { getLengthConverted, getLengthToStore } from './Settings';

let Profile;

const createDatastore = () => {
  const userDataPath = (electron.app || electron.remote.app).getPath('userData');
  Profile = new Datastore({ filename: `${userDataPath}/profiles.db`, autoload: true });
  try {
    Profile.ensureIndex({ fieldName: 'name', unique: true });
  } catch (err) {
    console.error(err);
  }

  return Profile;
}

export default Profile ? Profile : createDatastore();

export const canopyConfigs = {
  '1v': `1 ${i18n.t('vertical')}`,
  '1h': `1 ${i18n.t('horizontal')}`,
  '2v': `2 ${i18n.t('vertical')}`,
  '2h': `2 ${i18n.t('horizontal')}`,
  '3v': `3 ${i18n.t('vertical')}`,
  '2x2h': `2x2 ${i18n.t('horizontal')}`,
  '4v': `4 ${i18n.t('vertical')}`
}

export const canopyConfigsNFixtures = {
  '1v': 1,
  '1h': 1,
  '2v': 2,
  '2h': 2,
  '3v': 3,
  '2x2h': 4,
  '4v': 4,
}

export const profileLengthBounds = {
  '1v':{
    'mm': 299,
    'in': 11.8
  },
  '1h':{
    'mm': 667,
    'in': 26.3
  },
  '2v':{
    'mm': 648,
    'in': 25.5
  },
  '2h':{
    'mm': 1384,
    'in': 54.5
  },
  '3v': {
    'mm': 997,
    'in': 39.25
  },
  '4v':{
    'mm': 1346,
    'in': 53
  },
  '2x2h':{
    'mm': 1384,
    'in': 54.5
  }
}

export const profileWidthBounds = {
  '1v':{
    'mm': 667,
    'in': 26.30
  },
  '1h':{
    'mm': 299,
    'in': 11.8
  },
  '2v':{
    'mm': 667,
    'in': 26.3
  },
  '2h':{
    'mm': 299,
    'in': 11.8
  },
  '3v': {
    'mm': 667,
    'in': 26.3
  },
  '4v':{
    'mm': 667,
    'in': 26.3
  },
  '2x2h':{
    'mm': 648,
    'in': 25.5
  }
}

export const minimumHeight = {'mm': 200, 'in': 7.9};
export const minimumDelta = {'mm': 50, 'in': 2};

export const configsWithDeltaL = ['2v', '2h', '3v', '2x2h', '4v'];
export const configsWithDeltaW = ['2x2h'];

export const getCanopyConfigsForSelect = () => {
  return Object.keys(canopyConfigs).map((value) => {
    return {value: value, name: canopyConfigs[value]}
  })
}

export const getImageForConfig = (config) => {
  if(config){
    return require(`phytofy-design-system/src/images/${config}.svg`);
  } else {
    return null;
  }

}

export const convertLengthsToRead = (obj, unit) => {
  if(unit == 'mm'){
    return obj;
  }

  let newObj = {...obj};

  newObj.length = getLengthConverted(newObj.length, unit);
  newObj.width = getLengthConverted(newObj.width, unit);
  newObj.height = getLengthConverted(newObj.height, unit);
  newObj.deltaLength = getLengthConverted(newObj.deltaLength, unit);
  newObj.deltaWidth = getLengthConverted(newObj.deltaWidth, unit);

  return newObj;
}

export const convertLengthsToStore = (obj, unit) => {
  if(unit == 'mm'){
    return obj;
  }

  let newObj = {...obj};

  newObj.length = getLengthToStore(newObj.length, unit);
  newObj.width = getLengthToStore(newObj.width, unit);
  newObj.height = getLengthToStore(newObj.height, unit);
  newObj.deltaLength = getLengthToStore(newObj.deltaLength, unit);
  newObj.deltaWidth = getLengthToStore(newObj.deltaWidth, unit);

  return newObj;
}

const rules = [
  {
    field: 'name',
    method: validator.isEmpty,
    validWhen: false,
    message: 'Name is required'
  },
  {
    field: 'name',
    method: async (value) => {
      let count = await Profile.count({name: value});
      return count == 0;
    },
    validWhen: true,
    ignoreEqualsPrev: true,
    message: 'A Profile with this name already exists'
  },
  {
    field: 'name',
    method: validator.isLength,
    args: [{min: 0, max: 30}],
    validWhen: true,
    message: 'Name exceeds the maximum length of 30'
  },
  {
    field: 'name',
    method: async (value) => {
      return /^[A-Za-z\d\s]+$/.test(value)
    },
    validWhen: true,
    message: 'Name contains invalid characters'
  },
  {
    field: 'length',
    method: validator.isNumeric,
    args: [{min: 0}],  // an array of additional arguments
    validWhen: true,
    message: 'Length must be a positive integer'
  },
  {
    field: 'length',
    method: validator.isLength,
    args: [{min: 0, max: 6}],
    validWhen: true,
    message: 'Length exceeds the maximum length of 6 numbers'
  },
  {
    field: 'width',
    method: validator.isNumeric,
    args: [{min: 0}],  // an array of additional arguments
    validWhen: true,
    message: 'Width must be a positive integer'
  },
  {
    field: 'width',
    method: validator.isLength,
    args: [{min: 0, max: 6}],
    validWhen: true,
    message: 'Width exceeds the maximum length of 6 numbers'
  },
  {
    field: 'height',
    method: validator.isNumeric,
    args: [{min: 0}],  // an array of additional arguments
    validWhen: true,
    message: 'Height must be a positive integer'
  },
  {
    field: 'height',
    method: validator.isLength,
    args: [{min: 0, max: 6}],
    validWhen: true,
    message: 'Height exceeds the maximum length of 6 numbers'
  },
];

export const getValidationRules = (obj) => {
  let valRules = [...rules];
  if(configsWithDeltaL.find((el) => { return el == obj.config; })){
    valRules.push({
      field: 'deltaLength',
      method: validator.isNumeric,
      args: [{min: 0}],  // an array of additional arguments
      validWhen: true,
      message: '△ Length must be a positive integer'
    });
    valRules.push({
      field: 'deltaLength',
      method: validator.isLength,
      args: [{min: 0, max: 6}],
      validWhen: true,
      message: '△ Length exceeds the maximum length of 6 numbers'
    });
    valRules.push({
      field: 'deltaLength',
      method: async (value) => {
        console.log(obj.unit);
        return parseInt(value) >= minimumDelta[obj.unit]
      },
      validWhen: true,
      message: `△ Length is below minimum size: ${minimumDelta[obj.unit]}`
    });
  }

  if(configsWithDeltaW.find((el) => { return el == obj.config; })){
    valRules.push({
      field: 'deltaWidth',
      method: validator.isNumeric,
      args: [{min: 0}],  // an array of additional arguments
      validWhen: true,
      message: '△ Width must be a positive integer'
    });
    valRules.push({
      field: 'deltaWidth',
      method: validator.isLength,
      args: [{min: 0, max: 6}],
      validWhen: true,
      message: '△ Width exceeds the maximum length of 6 numbers'
    });
    valRules.push({
      field: 'deltaWidth',
      method: async (value) => {
        return parseInt(value) >= minimumDelta[obj.unit]
      },
      validWhen: true,
      message: `△ Width is below minimum size: ${minimumDelta[obj.unit]}`
    });
  }

  valRules.push({
    field: 'length',
    method: async (value) => {
      return parseInt(value) >= profileLengthBounds[obj.config][obj.unit]
    },
    validWhen: true,
    message: `Length is below minimum size: ${profileLengthBounds[obj.config][obj.unit]}.`
  });

  valRules.push({
    field: 'width',
    method: async (value) => {
      return parseInt(value) >= profileWidthBounds[obj.config][obj.unit]
    },
    validWhen: true,
    message: `Width is below minimum size: ${profileWidthBounds[obj.config][obj.unit]}`
  });

  valRules.push({
    field: 'height',
    method: async (value) => {
      return parseInt(value) >= minimumHeight[obj.unit]
    },
    validWhen: true,
    message: `Height is below minimum size: ${minimumHeight[obj.unit]}`
  });

  return valRules;
}

export const getProfileAttrFromId = (_id, attr, profiles) => {
  let filtered = profiles.filter((profile) => {
    return profile._id == _id;
  })

  if(filtered.length > 0){
    return filtered[0][attr];
  }

  return "";
}

export const exportData = async () => {
  const Json2csvParser = require('json2csv').Parser;

  let objects = await Profile.cfind({}).exec();
  let fields = ['name', 'config', 'length', 'width', 'height', 'deltaWidth', 'deltaLength', 'unit'];

  const parser = new Json2csvParser({fields});
  const csv = parser.parse(objects);

  return csv;
}

export const importData = async (data) => {
  let names = data.map((obj) => {
    return obj.name;
  })

  let existingObjects = await Profile.cfind({name: {$in: names}}).exec();

  let existingNames = existingObjects.map((obj) => {
    return obj.name;
  })

  let objectsToCreate = [];
  let errors = 0;

  data.forEach(async (obj) => {
    let duplicated = null;
    obj.length = parseInt(obj.length);
    obj.width = parseInt(obj.width);
    obj.deltaLength = parseInt(obj.deltaLength) ? parseInt(obj.deltaLength)  : "";
    obj.deltaWidth = parseInt(obj.deltaWidth) ? parseInt(obj.deltaWidth) : "";
    obj.height = parseInt(obj.height);

    if(existingNames.indexOf(obj.name) > -1) {
      duplicated = await Profile.findOne({name: obj.name+'-imported'});
      obj.name = obj.name +"-imported";
      obj.nCanopies = 0;
      //delete obj._id;
      //await Profile.insert(obj);
    }else{
      obj.nCanopies = 0;
    }
    let profileOkay = await validateProfile(obj);
    if(duplicated == null && profileOkay){
      objectsToCreate.push(obj);
    }else{
      errors += 1;
    }
  });

  await Profile.insert(objectsToCreate);

  return errors;
}

const validateProfile = async (profile) => {
  if(profile.length < profileLengthBounds[profile.config][profile.unit]){
    log.error(`[Import] cannot import layout ${profile.name} - length is below minimum ${profileLengthBounds[profile.config][profile.unit]}`);
    return false;
  }
  if(profile.width < profileWidthBounds[profile.config][profile.unit]){
    log.error(`[Import] cannot import layout ${profile.name} - width is below minimum ${profileWidthBounds[profile.config][profile.unit]}`);
    return false;
  }
  if(profile.height < minimumHeight[profile.unit]){
    log.error(`[Import] cannot import layout ${profile.name} - height is below minimum ${minimumHeight[profile.unit]}`);
    return false;
  }
  return true;
}
