import React,{Component} from 'react';

import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormGroupList from 'phytofy-design-system/src/design-system/molecules/FormGroupList';
import ListItem from 'phytofy-design-system/src/design-system/atoms/ListItem';
import ErrorMessage from 'phytofy-design-system/src/design-system/atoms/ErrorMessage';

import i18n from '../../i18n';

import FormContainer from './FormContainer';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';


const ScheduleForm = class extends Component {
    constructor(props){
        super();
        this.props = props;

        this.state = {...this.props.obj}

        this.getData = this.getData.bind(this);
        this.recipeChangeHandler = this.recipeChangeHandler.bind(this);
    }

    getData() {
        return {...this._form.getData(), canopies: this.state.canopies};
    }

    recipeChangeHandler(event) {
        this.setState({recipeId: event.target.value});
    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj});
        }
    }

    render() {
        const getRecipesForSelect = (recipes) => {
            return recipes.map((recipe) => {
                return {name: recipe.name, value: recipe._id}
            })
        }

        const getCanopiesFromIds = (canopyIds) => {
            return this.props.canopies.filter((canopy) => {
                return canopyIds.indexOf(canopy._id) != -1;
            })
        }

        const removeCanopy = (canopyId) => {
            let canopies = this.state.canopies;
            if(canopies.indexOf(canopyId) != -1) {
                canopies.splice(canopies.indexOf(canopyId), 1);
            }

            this.setState({canopies});
        }

        return (
            <div>
                <FormContainer obj={this.props.obj} validation={this.props.validation} actions={this.props.actions} ref={(ref) => this._form = ref}>
                    <FormGroup type="date" label={i18n.t("start_date")} name="start_date" placeholder="" value={this.state.start_date} onChange={this.handleInputChange} error={this.props.validation.start_date && this.props.validation.start_date.message}/>
                    <FormGroup type="date" label={i18n.t("end_date")} name="end_date" placeholder="" value={this.state.end_date} onChange={this.handleInputChange} error={this.props.validation.end_date && this.props.validation.end_date.message}/>
                    <FormGroup type="select" label={i18n.t("New Day Recipe")} name="recipeId" options={getRecipesForSelect(this.props.recipes)} value={this.state.recipeId} onChange={this.recipeChangeHandler} />
                    <FormGroupList label={i18n.t("groups")}>
                        <ListItem type="add" value="Add new" onClick={() => this.props.addCanopy(this.getData())} />
                        {getCanopiesFromIds(this.state.canopies).map((canopy) => {
                            return <ListItem value={canopy.name} onClick={() => removeCanopy(canopy._id)}/>
                        })}
                        { this.props.validation.canopies && <ErrorMessage>{ this.props.validation.canopies.message }</ErrorMessage> }
                    </FormGroupList>
                </FormContainer>
            </div>
        )
    }
}

export default ScheduleForm;
