// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import ContentHeader from 'phytofy-design-system/src/design-system/organisms/ContentHeader';
import CardActionButtons from 'phytofy-design-system/src/design-system/molecules/CardActionButtons';
import CardActionGoTo from 'phytofy-design-system/src/design-system/molecules/CardActionGoTo';
import CardActionNavigation from 'phytofy-design-system/src/design-system/molecules/CardActionNavigation';

import Table from 'phytofy-design-system/src/design-system/organisms/Table';
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData';
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow';
// import Card from 'phytofy-design-system/src/design-system/organisms/Card';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions';
import Checkbox from 'phytofy-design-system/src/design-system/atoms/Checkbox';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';

import { getTableHeaderCellFromObj } from './helpers';
import { delay } from '../actions/helpers';
//import ListContainer from './ListContainer';
import ListContainer from './ListContainerDataOptions';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as DevicesActions from '../actions/devices';
import * as CanopiesActions from '../actions/canopiesSetup';


const FixturesContainer = class extends Component {
  constructor(props){
    super(props);
    this.firstMount = true;
    this.updateFixtures = this.updateFixtures.bind(this);
    this.state = { objects: [], view: 'grid', goto: props.from, search: ''};
    this.handleGotoKeyPress = this.handleGotoKeyPress.bind(this);
    this.handleGotoChange = this.handleGotoChange.bind(this);
  }

  handleGotoKeyPress(event) {
    if (event.key === 'Enter') {
        let page = parseInt(event.target.value);
        if(!isNaN(page)) this.props.recipesActions.goto(parseInt(event.target.value));
    }
  }

  handleGotoChange(event) {
      this.setState({goto: event.target.value});
  }

  componentDidMount(){
     if(this.firstMount) this.updateFixtures();
  }

  async updateFixtures(){
    this.firstMount = false;
    await this.props.devicesActions.getMoxas(this.props.history);
    await this.props.devicesActions.commissionFixtures();
    //await delay(50);
    //this.props.devicesActions.getFixtures();
  }

  render() {
    const tableAttributes = {
      serial_num: {name: "Serial Number", configs: []},
      firmware: {name: "Firmware", configs: ['measure', 'nosort']},
      hardware: {name: "Hardware", configs: ['measure', 'nosort']},
      port: {name: "Port", configs: ['measure'] },
      temperature: {name: "Temperature", configs: ['measure', 'nosort']},
      status: {name: "Status", configs: ['status', 'nosort'], transform: (row) => {
        if(row.status == 0){
          return <Icon fill="#009D3D" icon="checkedCircle" tooltip="Fixture is Responsive" />
        } else if(row.status == 1){
          return <Icon fill="#009D3D" icon="uncheckedCircle" tooltip="Waiting..." />
        } else {
          return <Icon fill="#FF6600" icon="warning" tooltip="Fixture is unresponsive" />
        }
      }}
    }

    let actionButtons = [
      { icon: 'refresh', onClick: (ids) => { this.props.devicesActions.commissionFixtures() } },
    ];

    const dataOptions = [
      {optionMethod: this.props.canopiesActions.realtimeFixture, iconName: 'recipes', tooltip: "To real-time control"},
      {optionMethod: this.props.devicesActions.removeFixture, iconName: 'delete', tooltip: "Remove fixture not in use anymore"}
    ];

    return (
      <div>
        <TopbarContainer tabList={[
            { name: 'Light Fixtures', location: '/setup/fixtures', isActive: true },
            { name: 'Adapters', location: '/setup/devices', isActive: false },
            { name: 'Settings', location: '/setup/settings', isActive: false },
        ]}/>
        {/* <FiltersContainer filters={[]} /> */}
        <Content>
        <ContentHeader>
              <div>
                  <CardActionGoTo goto={this.state.goto} onKeyPress={this.handleGotoKeyPress} onChange={this.handleGotoChange} />
                  <CardActionNavigation from={this.props.from} to={this.props.to} of={this.props.count} pageBack={this.props.devicesActions.pageBack} pageNext={this.props.devicesActions.pageNext}/>
                  <CardActionButtons values={actionButtons} />
              </div>
          </ContentHeader>
        {this.props.moxas.length > 0 &&
        <ListContainer title="Fixtures" actions={{...this.props.devicesActions}} {...this.props} tableAttributes={tableAttributes}
            actionButtons={actionButtons} dataOptions={dataOptions} objects={this.props.fixtures} selected={[]} />
        }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    moxas: state.devices.moxas,
    fixtures: state.devices.fixtures,
    sortAttribute: state.devices.sortAttribute ? state.devices.sortAttribute : null,
    sortType: state.devices.sortType ? state.devices.sortType : 'orderAsc',
    count: state.devices.count,
    from: state.devices.from,
    to: state.devices.to,
    hasPrev: state.devices.hasPrev,
    hasNext: state.devices.hasNext
  };
}

function mapDispatchToProps(dispatch) {
  return {
    devicesActions: {...bindActionCreators(DevicesActions, dispatch)},
    canopiesActions: {...bindActionCreators(CanopiesActions, dispatch)}
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FixturesContainer));
