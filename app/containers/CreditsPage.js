import React, { Component } from "react";
import Content from "phytofy-design-system/src/design-system/organisms/Content";
import LicensesWrapper from "phytofy-design-system/src/design-system/organisms/LicensesWrapper";
import LicenseItem from "phytofy-design-system/src/design-system/molecules/LicenseItem";
import licenses from "phytofy-design-system/src/utils/licenses";

import TopbarContainer from './TopbarContainer';

export default class extends Component {
  state = {
    licenses: licenses
  };

  render() {
    return (
      <div>
        <TopbarContainer tabList={[
            { name: 'Product Info', location: '/about/product', isActive: false },
            { name: 'Credits', location: '/about/credits', isActive: true },
            { name: 'Terms of Service', location: '/about/terms', isActive: false }
        ]}/>

        <Content>
          <LicensesWrapper>
            {Object.keys(this.state.licenses).map(key => (
              <LicenseItem
                key={licenses[key].name}
                id={licenses[key].name}
                name={licenses[key].name}
                license={licenses[key].licenseText}
              />
            ))}
          </LicensesWrapper>
        </Content>
      </div>
    );
  }
}
