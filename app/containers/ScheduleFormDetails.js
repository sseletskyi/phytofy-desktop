import React,{Component} from 'react';

import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormGroupList from 'phytofy-design-system/src/design-system/molecules/FormGroupList';
import ListItem from 'phytofy-design-system/src/design-system/atoms/ListItem';


import i18n from '../../i18n';

import FormContainer from './FormContainer';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';


const ScheduleFormDetails = class extends Component {
    constructor(props){
        super();
        this.props = props;

        this.state = {...this.props.obj, scheduleId: this.props.obj._id}
        console.log(this.state);
        this.getData = this.getData.bind(this);
        this.scheduleChangeHandler = this.scheduleChangeHandler.bind(this);
    }

    getData() {
        console.log(this.props.schedules.filter((obj) => {
            return obj._id === this.state.scheduleId;
        })[0]);
        return this.props.schedules.filter((obj) => {
            return obj._id === this.state.scheduleId;
        })[0];
        //return {...this._form.getData()};
    }

    scheduleChangeHandler(event) {
        this.setState({scheduleId: event.target.value});
    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj, scheduleId: this.props.obj._id});
        }
    }

    render() {
        const getSchedulesForSelect = (schedules) => {
            return schedules.map((schedule) => {
                let start = schedule.start_date.split('-')
                let end = schedule.end_date.split('-')
                return {name: start[1]+"-"+start[2]+" to "+end[1]+"-"+end[2]+" -- "+schedule._recipe.name, value: schedule._id}
            })
        }

        return (
            <div>
                <FormContainer obj={this.props.obj} actions={this.props.actions} ref={(ref) => this._form = ref}>
                    <FormGroup type="select" label={"Schedule"} name="_id" options={getSchedulesForSelect(this.props.schedules)} value={this.state.scheduleId} onChange={this.scheduleChangeHandler} />
                    <div />
                </FormContainer>
            </div>
        )
    }
}

export default ScheduleFormDetails;
