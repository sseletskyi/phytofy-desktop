import React, { Component } from "react";
import Content from "phytofy-design-system/src/design-system/organisms/Content";
import Terms from "phytofy-design-system/src/design-system/molecules/Terms";
import Paragraph from "phytofy-design-system/src/design-system/atoms/Paragraph";
import Text from "phytofy-design-system/src/design-system/atoms/Text";

import TopbarContainer from './TopbarContainer';

export default class extends Component {
  render() {
    return (
      <div>
        <TopbarContainer tabList={[
            { name: 'Product Info', location: '/about/product', isActive: false },
            { name: 'Credits', location: '/about/credits', isActive: false },
            { name: 'Terms of Service', location: '/about/terms', isActive: true }
        ]}/>

        <Content>
          <Terms>
            <Paragraph>
              <Text italic>(Please scroll down for USA & Canada version)</Text>
            </Paragraph>
            <Paragraph bold>
                End User License Agreement for Phytofy® RL Control Software to use with
                Phytofy® RL LED lighting system (“EULA”)
            </Paragraph>
            <Paragraph bold>
                (EU Countries)
            </Paragraph>
            <Paragraph>
              This End User License Agreement (<Text bold>“EULA”</Text>) is a legal agreement between you (as either individual or a
              single entity, “You”) and OSRAM GmbH, Marcel-Breuer-Str. 6, 80807 Munich, Germany (“OSRAM”) that
              governs the use of the Phytofy®  RL Control Software and any updates and/or upgrades thereto
              provided by OSRAM (“Software”). You should carefully read the EULA and tick the “Accept” button
              if you fully accept and agree to all of the provisions of this EULA. If you do not accept all of
              the terms of this EULA, you have no right to use the Software. By signing this EULA, or using the
              Software, you confirm that you have read the EULA and consent to be bound by all of its terms and conditions.
            </Paragraph>
            <Paragraph bold>
                Preamble
            </Paragraph>
            <Paragraph>
              The Software runs on Windows<sup>1</sup>/Apple<sup>2</sup>/Linux<sup>3</sup> PCs and operates
              the Phytofy® RL LED lighting system and is suitable to support the control of such device. The
              Phytofy® RL LED lighting system may require updating of the Software loaded onto such PC, which can
              also be performed “over the air”. The Software contains software modules owned by OSRAM
              and is licensed or, as applicable, sublicensed to You by OSRAM at the terms and conditions of this EULA.
            </Paragraph>
            <Paragraph>
              The Software contains software modules owned by OSRAM and is licensed or, as applicable,
              sublicensed to You by OSRAM at the terms and conditions of this EULA.
            </Paragraph>
            <Paragraph bold>
                I. License Grant and License Restrictions
            </Paragraph>
            <Paragraph>
              Subject to the terms and conditions of this EULA, You are hereby granted the non-exclusive,
              non-transferrable, worldwide right to use the Software in connection with Phytofy® RL products
              other than those explicitly stipulated herein, You are not granted any rights to the Software.
            </Paragraph>
            <Paragraph>
              In no event may You (i) use the Software in conjunction with any product other than OSRAM Phytofy®
              RL LED light system component; (ii) distribute, license or sell the Software as a stand-alone
              product; (iii) decrypt, disassemble, reverse assemble or reverse compile the Software, except
              to the extent that such restrictions are prohibited by applicable law and/or (iv) take any
              actions that would cause the Software and/or any portion thereof to become subject to Open
              License Terms that impose any limitation, restriction or condition on the right or ability to
              use or distribute the Software and/or any portion thereof. In addition to your liability to
              OSRAM under this You agree to be liable for any breach of the terms of this Agreement by you or
              Your affiliates and shall bear all costs, risks, and liabilities incurred as a result of such
              breach.
            </Paragraph>
            <Paragraph bold>
                II. Intellectual Property
            </Paragraph>
            <Paragraph>
              The Software is the property of OSRAM and/or its licensors and protected by copyright
              and trademark laws and other intellectual property rights.
            </Paragraph>
            <Paragraph>
              Details regarding Open Source Software components included in this software can be found
              in the About/Credits section of this Software.
            </Paragraph>
            <Paragraph>
              You shall maintain in the Software and any related documentation, if any, all notices
              and legends (including copyright and trademark notices) included.
            </Paragraph>
            <Paragraph>
              Other than explicitly stated herein, nothing contained in the above shall be construed
              as granting, by implication or otherwise, any license or right to use any patent,
              trademark or any other intellectual property right of OSRAM GmbH and/or its licensors
              nor does it grant any license or rights to use any copyrights or other rights related
              to the materials described above except as explicitly stated.
            </Paragraph>
            <Paragraph bold>
                III. Consideration
            </Paragraph>
            <Paragraph>
              The Software is provided free of charge and comes together with a purchase of a Phytofy® RL LED light system.
            </Paragraph>
            <Paragraph bold>
                IV. Warranty
            </Paragraph>
            <Paragraph>
              OSRAM warrants that the Software will be up to date, complete and free from material errors.
              In the event that a copy of the Software fails to meet the above warranties (“Defect”), OSRAM
              will use commercially reasonable efforts to have the respective copy corrected.
            </Paragraph>
            <Paragraph>
              This warranty shall not apply to copies of the Software not provided by OSRAM. Warranty
              claims are also excluded (a) in case of insignificant Defects or (b) if You or any other
              third party modifies the Software.
            </Paragraph>
            <Paragraph bold>
                V. Liability
            </Paragraph>
            <Paragraph>
              OSRAM’s liability under this EULA shall not be limited as far as the damage has been
              caused by intent and/or gross negligence. It shall also not be limited for all claims
              arising from the Product Liability Act (Produkthaftungsgesetz).
            </Paragraph>
            <Paragraph>
              Should OSRAM negligently breach an essential or cardinal obligation, OSRAM’s liability
              is limited to the amount of the typically arising and foreseeable damages.
            </Paragraph>
            <Paragraph>
              In all other cases than those stipulated above or except as otherwise explicitly
              stipulated in this EULA, claims for damages against OSRAM shall be excluded. This
              exclusion also includes the personal liability of employees, legal representatives and
              vicarious agents.
            </Paragraph>
            <Paragraph bold>
                VI. Confidentiality
            </Paragraph>
            <Paragraph>
              The Software is confidential information and contains valuable proprietary and trade
              secret information of OSRAM. You shall not disclose to any third party other than Your
              affiliates and/or subcontractors involved in the execution of this EULA, or use in any
              manner not expressly permitted herein, the Software. You agree to take all reasonable
              measures to protect the Software and prevent unauthorized disclosure thereof, which
              measures shall be at least as stringent as those measures You take to protect Your own
              confidential information of like kind.  Without in any way limiting the foregoing, You shall
              restrict access to the Software to Your own and Your affiliates’ employees and individual
              third party subcontractors working on site who have executed a written agreement sufficient
               to protect the Software in accordance with the provisions of this EULA and who have a
               “need-to-know” to exercise the license rights granted herein.
            </Paragraph>
            <Paragraph bold>
                VII. Export Control
            </Paragraph>
            <Paragraph>
              This EULA involves products, software and/or technical data that may, e.g. due to their
              nature, or intended use or final destination, be controlled under export control
              regulations and thus be subject to authorisation. You are obliged to strictly conform
              with all applicable export regulations, in particular with those of the EU, individual EU
              member states, the USA and Canada. You represent and warrant that you will not export
              or reexport the Software to anyone listed on relevant sanctions lists and that you are
              not listed on any such list. You further agree that you will not use the Software in
              connection with:
              <ul>
                <li>chemical, biological or nuclear weapons or other nuclear explosive devices or
                missiles capable of delivering such weapons;</li>
                <li>any military end use;</li>
                <li>the construction, operation or installation of a nuclear facility; </li>
                <li>any violation of human rights as defined by the Charter of Fundamental Rights
                of the European Union.</li>
              </ul>
            </Paragraph>
            <Paragraph bold>
                VIII. Term
            </Paragraph>
            <Paragraph>
              Both OSRAM and You may terminate this EULA, in addition to any other remedies it
              may have, if the other party is in default of a material obligation under this EULA and
              fails to cure such default within thirty (30) days following its receipt of written
              notice of the default. Within thirty (30) days following termination of this EULA,
              whether terminated by You or OSRAM, You must destroy the Software or else return it to
              OSRAM. Notwithstanding the foregoing, each party may terminate this EULA for material
              cause.
            </Paragraph>
            <Paragraph bold>
                IX. Assignment
            </Paragraph>
            <Paragraph>
                Neither the rights nor the obligations of this EULA may be assigned or transferred in
                any manner, except with the prior written consent of the other party and except as part of
                a transfer of all or of a substantial part of the activities to which the subject matter of this EULA pertains whether by sale, merger or consolidation provided, however, that OSRAM may assign any and all of its rights and obligations without Your prior written to an OSRAM Affiliate. In case of such a transfer the respective arty shall take care that the transferee, assignee or successor will comply with this EULA
            </Paragraph>
            <Paragraph bold>
                X. Dispute Resolution and Applicable Law
            </Paragraph>
            <Paragraph>
              The place of jurisdiction shall be Munich if You are a merchant in terms of the German
              Commercial Code (Handelsgesetzbuch).
            </Paragraph>
            <Paragraph>
              This EULA shall be governed by German law. The application of the German Uniform Code
              on the Formation of Contracts for the International Sale of Movable Goods of 17 July
              1973 and the United Nations Convention on Contracts for the International Sales of
              Goods (CISG) of 11 April 1980 is excluded.
            </Paragraph>
            <Paragraph bold>
                End User License Agreement for Phytofy® RL Control Software to use with
                Phytofy® RL LED lighting system (“EULA”)
            </Paragraph>
            <Paragraph bold>
                (EU Countries)
            </Paragraph>
            <Paragraph bold>
                Effective Date: February 15, 2019
            </Paragraph>
            <Paragraph>
              This End User License (“License”) and the Data or Privacy Policy
              (https://www.osram.com/cb/services/privacy-policy/index.jsp & https://www.osram.us/cb/privacy-policy/index.jsp)
               govern your use of Phytofy®  RL Control Software and any updates and/or upgrades thereto offered by
               OSRAM SYLVANIA Inc. and its affiliates (“OSRAM”) in the U.S.A. and Canada (<Text bold>"Software”</Text>). The Data
               or Privacy Policy describes the collection and processing of information from users of the
               Product and Software by OSRAM. Your purchase of any OSRAM product associated with the
               Software (“Product”) is governed by the limited warranty and any terms of sale provided with
               the Product. The use of this Software is intended solely in combination with
               PHYTOFY® RL Hardware. To use this Software you must have the minimum legal age to be entitled
               to work with a professional horticultural system in your state.
            </Paragraph>
            <Paragraph>
              <Text bold>Consent.</Text>  By clicking "I accept" button or downloading and using the Software, you agree to be bound by this License and the Privacy or Data Policy, including any amendments, which will remain in effect as long as you use the Software. If you do NOT agree to all these terms, you should NOT use the Software or and uninstall the Software from your device. If you purchased the Product from an authorized retailer without an opportunity to review the Terms of Use and Data or Privacy Policy, and do not accept these terms, you may be eligible to return the Products subject to the retailer’s return policy. Your ability to download and use the Software may depend on your compliance with the terms and conditions of the Software itself. Data and messaging rates may apply when you download or use this Software.
            </Paragraph>
            <Paragraph>
              <Text bold>LICENSE GRANT AND RESTRICTIONS.</Text> OSRAM grants You a personal,
              revocable, non-exclusive, non-transferable, limited right to install and use the
              Software on a device owned or controlled by You ("Device"), and to access and use the Software
               on such Device strictly in accordance with this License.
            </Paragraph>
            <Paragraph>
              You may not, or allow others to: (a) decompile, reverse engineer, disassemble, attempt to derive the source code of, or decrypt the Software; (b) make any modification, translation or derivative work from the Software; (c) violate any applicable laws, rules or regulations in connection with Your use of the Software; (d) remove, alter or obscure any proprietary notice of OSRAM or its affiliates, partners, suppliers or Licensors of the Software; (e) use the Software for creating a product, service or software that is competitive with or in any way a substitute for any services, product or software offered by OSRAM; or (f) use any proprietary information, interfaces of other intellectual property of OSRAM in the design, development, manufacture, licensing or distribution of any Softwares, accessories or devices for use with the Software.
            </Paragraph>
            <Paragraph>
              <Text bold>INTELLECTUAL PROPERTY RIGHTS.</Text>  OSRAM and its licensors own all right, title, and interest to the intellectual property relating to and incorporated in the Software, Product and any modifications or additions to the Software or Product that may arise from any suggestions, recommendations or other feedback provided by You. You hereby assign all right, title, and interest in intellectual property resulting from suggestions, recommendations, or other feedback pertaining to the Software or Product to OSRAM and will confirm this in writing at OSRAM’s request.
            </Paragraph>
            <Paragraph>
              The OSRAM company name and the related logos, product and service names, design marks and slogans are trademarks, trade dress and service marks owned by and used under license from OSRAM and its Licensors (the "OSRAM Marks"). You are not authorized to use the OSRAM Marks in any advertising, publicity or in any other commercial manner without OSRAM’s express and prior written consent.
            </Paragraph>
            <Paragraph>
              OSRAM’s licensors are beneficiaries of certain rights and protections in this License and any applicable third-party license agreement, including open source software licenses, and may enforce those rights against You. OSRAM will disclose any third-party license agreements or intellectual property rights as required by the applicable license. Details regarding Open Source Software components included in this software can be found in the About/Credits section of this Software
            </Paragraph>
            <Paragraph>
              <Text bold>PERFORMANCE AND RESULTS.</Text> OSRAM does not promise any result or benefit from the use of the Software or Product. The Software may allow control of Product settings. If Your choice of settings causes the Product to operate outside of recommended limits you will void the warranty and potentially cause other harm, e.g. overheating, Product failure, etc. You are solely responsible for any damages resulting from excessive or abnormal use of the Product.
            </Paragraph>
            <Paragraph>
              <Text bold>NO WARRANTY.</Text> THE SOFTWARE IS PROVIDED "AS IS” AND "AS AVAILABLE," AND YOUR USE OF OR RELIANCE UPON THE SOFTWARE IS AT YOUR SOLE RISK AND DISCRETION. OSRAM AND ITS LICENSORS HEREBY DISCLAIM ANY WARRANTIES REGARDING THE SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
            </Paragraph>
            <Paragraph>
              <Text bold>LIMITATION OF LIABILITY.</Text>  OSRAM AND ITS LICENSORS SHALL NOT BE LIABLE FOR ACCIDENTS, PROPERTY DAMAGE, PERSONAL INJURY, DEATH, OR FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES ARISING OUT OF YOUR ACCESS OR USE OF THE SOFTWARE, WHETHER OR NOT THE DAMAGES WERE FORESEEABLE AND WHETHER OR NOT OSRAM WAS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.
            </Paragraph>
            <Paragraph>
              <Text bold>GOVERNING LAW.</Text>  This License will be governed by and construed in accordance with the laws of the Commonwealth of Massachusetts, as applied to agreements entered into and to be performed entirely within Massachusetts. Software of the United Nations Convention on Contracts for the International Sale of Goods is expressly excluded. If for any reason any provision of this License is made or found to be unenforceable, the remaining provisions shall continue in effect.
            </Paragraph>
            <Paragraph>
              <Text bold>EXPORT RESTRICTION.</Text>   The Software, Product and all related technical information are subject to U.S. export control jurisdiction and the export controls, trade restrictions, embargoes and sanctions established by the United States and other countries (“Export Laws”). You agree to comply with all applicable Export Laws and ensure that the Software and Product are not exported directly or indirectly to any prohibited person or country or otherwise in violation of Export Laws and are not used for any purpose prohibited by Export Laws.
            </Paragraph>
            <Paragraph>
              <Text bold>ENTIRE AGREEMENT.</Text>   This License including the documents incorporated herein by reference constitute the entire agreement with respect to Your use of the Software and supersedes all prior or contemporaneous understandings, communications, proposals or representations with respect to the Software.
            </Paragraph>
            <Paragraph>
              <Text bold>TERMINATION.</Text>  OSRAM may suspend or terminate your rights to use the Software without prior notice if it reasonably believes that you failed or intend to fail to comply with any of these provisions, or violate any applicable law, regulation, third-party agreement, or in the event of a required software patch or bug fix.
            </Paragraph>
            <Paragraph>
              (1) Windows® is a registered trademark of Microsoft Corporation in the U.S. and other
              countries. All Rights Reserved
            </Paragraph>
            <Paragraph>
              (2) Apple® is a trademark of Apple Inc., registered in the U.S. and other countries
            </Paragraph>
            <Paragraph>
              (3) Linux® is the registered trademark of Linus Torvalds in the U.S. and other
              countries
            </Paragraph>
          </Terms>
        </Content>
      </div>
    );
  }
}
