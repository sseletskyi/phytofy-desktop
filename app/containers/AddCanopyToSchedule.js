import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormGroupList from 'phytofy-design-system/src/design-system/molecules/FormGroupList';
import ListItem from 'phytofy-design-system/src/design-system/atoms/ListItem';
import Form from 'phytofy-design-system/src/design-system/organisms/Form';


import i18n from '../../i18n';

import * as SchedulesActions from '../actions/schedules';



const AddCanopyToSchedule = class extends Component {
    constructor(props){
        super();
        this.props = props;

        this.state = {...this.props, search: ''}

    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props});
        }
    }

    componentDidMount(){
      this.props.getAvailableCanopies();
    }

    render() {
        return (
            <Form>
                <FormGroup
                    type="search"
                    label="Group"
                    placeholder="Search Group"
                    name="Search Group"
                    value={this.state.search}
                    error=""
                    onChange={(event) => {this.setState({search: event.target.value}); this.props.getCanopies(event.target.value)}}
                />

                <FormGroupList label="Results">
                    {this.props.canopiesSearch.map((canopy) => {
                        return <ListItem value={canopy.name} onClick={() => this.props.submitAddCanopy(canopy._id)} />
                    })}
                </FormGroupList>
            </Form>
        )
    }
}


function mapStateToProps(state) {
    return {
        canopiesSearch: state.schedules.canopiesSearch
    };
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators(SchedulesActions, dispatch);
  }

  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCanopyToSchedule));
