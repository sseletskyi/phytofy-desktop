import React,{Component} from 'react';
import Timeline from 'react-visjs-timeline'
import { Accordion, AccordionItem, AccordionItemTitle, AccordionItemBody } from 'react-accessible-accordion'
import LightExposure from "phytofy-design-system/src/design-system/organisms/LightExposure";
import LightExposureItem from "phytofy-design-system/src/design-system/molecules/LightExposureItem";

import CanopyConfig from 'phytofy-design-system/src/design-system/atoms/CanopyConfig';
import ErrorMessage from 'phytofy-design-system/src/design-system/atoms/ErrorMessage';

import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';

import i18n from '../../i18n';

import FormContainer from './FormContainer';
import RecipeItemContainer from './RecipeItemContainer';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import moment from 'moment';

const maxUVA = 50.0;
const maxBlue = 250.0;
const maxGreen = 100.0;
const maxHyperRed = 250.0;
const maxFarRed = 100.0;
const maxWhite = 250.0;

const options = {
    width: '100%',
    height: '79px',
    showMajorLabels: false,
    showMinorLabels: true,
    showCurrentTime: false,
    min: "2018-07-25 00:00:00",
    max: "2018-07-25 23:59:59",
    start: "2018-07-25 00:00:00",
    end: "2018-07-25 23:59:59",
    zoomMin: 300000,
    stack: false,
    format: {
        minorLabels: {
          minute: "HH:mm",
          hour: "HH:mm"
        }
    }
}

/* Timeline Items */

const RecipeForm = class extends Component {
    constructor(props){
        super();
        this.props = props;

        let tmpPeriods = this.props.obj.periods ? this.props.obj.periods : [];
        let {periods, timelineItems} = this.sortPeriods(tmpPeriods);

        this.state = {...this.props.obj, timelineItems:timelineItems, activeItems: [], periods}

        this.sortPeriods = this.sortPeriods.bind(this);
        this.addPeriod = this.addPeriod.bind(this);
        this.editPeriod = this.editPeriod.bind(this);
        this.deletePeriod = this.deletePeriod.bind(this);
        this.timelineClickHandler = this.timelineClickHandler.bind(this);
        this.getPhotoperiod = this.getPhotoperiod.bind(this);

        this.sortPeriods(this.state.periods);
    }

    getData() {
        return {...this._form.getData(), periods: this.state.periods};
    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj});
            // this.sortPeriods(this.props.obj.periods);
        }
    }

    sortPeriods(periods) {
        let newPeriods = [...periods].sort(function(a,b) {return (a.start > b.start) ? 1 : ((b.start > a.start) ? -1 : 0);} );
        let timelineItems = newPeriods.map((obj, i) => {
            return {
                id: i+1,
                start: `2018-07-25 ${obj.start}:00`,
                end: `2018-07-25 ${obj.end}:00`
            }
        })

        return {periods: newPeriods, timelineItems, activeItems: []};
    }

    addPeriod(obj) {
        let newPeriods = [...this.state.periods, obj];
        this.setState(this.sortPeriods(newPeriods));
    }

    editPeriod(obj, i) {
        let newPeriods = [...this.state.periods];
        newPeriods.splice(i, 1);
        newPeriods.push(obj);
        this.setState(this.sortPeriods(newPeriods));
    }

    deletePeriod(i) {
        let newPeriods = [...this.state.periods];
        newPeriods.splice(i, 1);
        this.setState(this.sortPeriods(newPeriods));
    }

    timelineClickHandler(obj) {
        if(obj.item) {
            if(!this.state.activeItems.includes(obj.item-1)){
                this.setState({activeItems: [...this.state.activeItems, obj.item-1]})
                console.log(this.state.activeItems);
            }
        }
    }

    getPhotoperiod(){
      let periods = this.state.periods;
      let photoperiod = 0;
      if(periods.length > 0){
        for(let period of periods){
          let end = period.end.split(':')
          let start = period.start.split(':')
          let hours = parseInt(end[0])-parseInt(start[0]);
          let minutes = (parseInt(end[1])-parseInt(start[1]))/60;
          photoperiod += (hours+minutes);
        }
      }
      return photoperiod.toFixed(2);
    }

    getDailyLightIntegral(){
      let periods = this.state.periods;
      let dailyLightIntegral = 0;
      for(let period of periods){
        let mols = Math.round(period.uv*maxUVA/100) +
                  Math.round(period.blue*maxBlue/100) +
                  Math.round(period.green*maxGreen/100) +
                  Math.round(period.farRed*maxFarRed/100) +
                  Math.round(period.hyperRed*maxHyperRed/100) +
                  Math.round(period.white*maxWhite/100);
        let end = period.end.split(':')
        let start = period.start.split(':')
        let hours = (parseInt(end[0])-parseInt(start[0]))*360;
        let minutes = (parseInt(end[1])-parseInt(start[1]))*60;
        let photoperiodSeconds = hours+minutes;
        dailyLightIntegral += (mols/100000)*photoperiodSeconds;
      }
      return dailyLightIntegral.toFixed(2);
    }

    render() {
        //this.getPhotoperiod();
        console.log(this.state.timelineItems);
        return (
            <div>
                <FormContainer obj={this.props.obj} validation={this.props.validation} actions={this.props.actions} ref={(ref) => this._form = ref}>
                    <FormGroup type="text" label={i18n.t("name")} name="name" placeholder="New Day Recipe" value={this.state.name} onChange={this.handleInputChange} error={this.props.validation.name && this.props.validation.name.message}/>
                    <Timeline options={options} items={this.state.timelineItems} clickHandler={this.timelineClickHandler} />
                    <LightExposure>
                      <LightExposureItem title="Photoperiod" value={this.getPhotoperiod()} unit="hours" />
                    </LightExposure>
                    <Accordion onChange={(value) => {this.setState({activeItems: value})}} accordion={false} >
                        {this.state.periods.map((period, i) => {
                            const otherPeriods = [...this.state.periods];
                            otherPeriods.splice(i, 1);
                            return (<RecipeItemContainer uuid={i} periods={otherPeriods} isOpen={this.state.activeItems.includes(i)} key={i} i={i} deletePeriod={this.deletePeriod} action={this.editPeriod} obj={period} />)
                        })}
                        <RecipeItemContainer new={true} periods={this.state.periods} uuid={'newitem'} action={this.addPeriod} />
                    </Accordion>
                    <div>
                      { this.props.validation.periods && <ErrorMessage>{ this.props.validation.periods.message }</ErrorMessage> }
                    </div>
                </FormContainer>
            </div>
        )
    }
}

export default RecipeForm;
