import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import * as CanopiesActions from '../actions/canopiesSetup';
import CanopyForm from './CanopyForm';

const CanopyFormCreate = class extends Component {
    render() {
        const actions = [
            <Button state="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button state="success" name="Submit" onClick={() => this.props.submitEdit(this._form.getData())} />
        ]

        return (
            <CanopyForm actions={actions} validation={this.props.validation} profiles={this.props.profiles} fixtures={this.props.allFixtures} addFixture={this.props.addFixture}  obj={this.props.obj} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: state.canopiesSetup.canopyObj,
        validation: state.canopiesSetup.validation,
        profiles: state.profiles.objectsAll,
        allFixtures: state.devices.allFixtures
    };
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators(CanopiesActions, dispatch);
  }

  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CanopyFormCreate));
