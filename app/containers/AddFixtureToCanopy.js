import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormGroupList from 'phytofy-design-system/src/design-system/molecules/FormGroupList';
import ListItem from 'phytofy-design-system/src/design-system/atoms/ListItem';
import Form from 'phytofy-design-system/src/design-system/organisms/Form';


import i18n from '../../i18n';

import * as CanopiesActions from '../actions/canopiesSetup';



const AddFixtureToCanopy = class extends Component {
    constructor(props){
        super();
        this.props = props;

        this.state = {...this.props, search: ''}

    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props});
        }
    }
    
    componentDidMount(){
      this.props.getAllUnusedFixtures();
    }

    render() {
        return (
            <Form>
                <FormGroup
                    type="search"
                    label="Fixture"
                    placeholder="Search Fixture"
                    name="Search Fixture"
                    value={this.state.search}
                    error=""
                    onChange={(event) => {this.setState({search: event.target.value}); this.props.getFixtures(event.target.value)}}
                />

                <FormGroupList label="Results">
                    {this.props.fixturesSearch.filter((fixture) => {
                        return this.props.canopyObj.fixtures.indexOf(fixture.serial_num) === -1; 
                    }).map((fixture) => {
                        return <ListItem value={fixture.serial_num} onClick={() => this.props.submitAddFixture(fixture.serial_num)} />
                    })}
                </FormGroupList>
            </Form>
        )
    }
}


function mapStateToProps(state) {
    return {
        fixturesSearch: state.canopiesSetup.fixturesSearch,
        canopyObj: state.canopiesSetup.canopyObj,
    };
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators(CanopiesActions, dispatch);
  }

  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddFixtureToCanopy));
