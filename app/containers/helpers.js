import React from 'react';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';

export const getTableHeaderCellFromObj = (attribute, obj, sortAttribute, sortType, onClick) => {
    let configsMap = {};
    obj.configs.forEach((config => configsMap[config] = true))

    let sortConfig = (sortAttribute == attribute) ? sortType: null;
    if(sortConfig) configsMap[sortType] = true;

    return <TableHeaderCell onClick={onClick} {...configsMap}>{obj.name}</TableHeaderCell>
}

export const getSelectValues = (values) => {
    let result = []

    for(const [key, value] of Object.entries(values)) {
        result.push({name: value, value: key});
    }

    return result;
}