// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import JSZip from 'jszip';
import fs from 'fs';
const electron = require('electron');
const {shell} = require('electron');
const is = require('electron-is');

import Paragraph from 'phytofy-design-system/src/design-system/atoms/Paragraph';
import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Table from 'phytofy-design-system/src/design-system/organisms/Table';
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData';
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow';
// import Card from 'phytofy-design-system/src/design-system/organisms/Card';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions';
import Checkbox from 'phytofy-design-system/src/design-system/atoms/Checkbox';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import CheckboxGroup from 'phytofy-design-system/src/design-system/molecules/CheckboxGroup';
import Card from 'phytofy-design-system/src/design-system/organisms/Card';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import TopbarContainer from './TopbarContainer';

import { getTableHeaderCellFromObj, getSelectValues } from './helpers';

import Canopy from '../models/Canopy';
import Profile from '../models/Profile';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';
import { pageSizeOptions, irradianceUnitOptions, colorscaleOptions, lengthUnitOptions, getPageSize, getIrradianceColorscale, getIrradianceUnit, getLengthUnit, loggingOptions, syncOptions } from '../models/Settings';

import * as SettingsActions from '../actions/settings';

const CanopyContainer = class extends Component {

  constructor(props){
    super(props);

    this.state = {...props.settings, exportProfiles: false, exportRecipes: false};
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.setSettings = this.setSettings.bind(this);
    this.setLoggingMode = this.setLoggingMode.bind(this);
    this.isExportDisabled = this.isExportDisabled.bind(this);
    this.isImportDisabled = this.isImportDisabled.bind(this);
    this.isRestoreDisabled = this.isRestoreDisabled.bind(this);
    this.openLogsFolder = this.openLogsFolder.bind(this);
  }

  componentDidMount() {
    this.props.settingsActions.getSettings();
  }

  // componentDidUpdate(prevProps){
  //   if(this.props.settings != prevProps.settings){
  //       this.setState({...this.props.settings})
  //   }
  // }

  onChangeHandler = (event) => {
    const target = event.target;
    const name = target.name;
    let value = null
    switch(target.type) {
        case 'checkbox':
            value = target.checked;
            break;
        case 'number':
            if(!isNaN(parseFloat(target.value))) value = parseFloat(target.value);
            else value = target.value;
            break;
          case 'file':
            value = target.files;
            break;
        default:
            value = target.value;
    }
    this.setState({
      [name]: value
    });
  }

  isExportDisabled = () => {
    return !this.state.exportProfiles && !this.state.exportRecipes && !this.state.exportCanopies && !this.state.exportSchedules;
  }

  isImportDisabled = () => {
    if(this.state.importFiles == undefined) return true;
    else return false;
  }

  isRestoreDisabled = () => {
    if(this.state.restoreFile == undefined) return true;
    else return false;
  }

  setSettings = () => {
    this.props.settingsActions.setSettings(this.state.pageSize, this.state.lengthUnit, this.state.irradianceUnit, this.state.irradianceColorscale, this.state.schedResume);
  }

  setLoggingMode = () => {
    this.props.settingsActions.setLoggingMode(this.state.loggingMode);
  }

  openLogsFolder = () => {
    let logsPathWin = (electron.app || electron.remote.app).getPath('userData');
    let logsPathUnix = process.env.HOME;
    let logsPath = null;

    if(is.macOS()) logsPath = logsPathUnix + '/Library/Logs/OSRAM\ -\ Phytofy\ RL/log.log';
    else if(is.windows()) logsPath = logsPathWin + "\\logs\\log.log"
    else if(is.linux()) logsPath = logsPathUnix + '/.config/OSRAM\ -\ Phytofy\ RL/log.log';

    let ret = shell.showItemInFolder(logsPath);
    console.log(`SHELL RESULT: ${ret} for --> ${logsPath}`);
  }

  render() {

    //console.log(this.state.loggingMode);
    console.log(this.state.pageSize);
    //lolol

    return (
      <div>
        {/* <Topbar>
        </Topbar> */}
        <TopbarContainer tabList={[
            { name: 'Light Fixtures', location: '/setup/fixtures', isActive: false },
            { name: 'Adapters', location: '/setup/devices', isActive: false },
            { name: 'Settings', location: '/setup/settings', isActive: true },
        ]}/>
        <Content cardList>

          <Card title="Import CSV">
            <FormGroup type="file" name="importFiles" onChange={this.onChangeHandler} label="Maximum file size is 1GB" />
            <Paragraph>Import a .csv file. You can use an exported .csv file as template.</Paragraph>
            <Button state="success" name="Upload" disabled={this.isImportDisabled()} onClick={() => this.props.settingsActions.importData(this.state.importFiles)} />
          </Card>

          <Card title="Export CSV">
            <CheckboxGroup>
              <FormGroup type="checkbox" name="exportProfiles" onChange={this.onChangeHandler} checkboxLabel="Layouts" />
              <FormGroup type="checkbox" name="exportCanopies" onChange={this.onChangeHandler} checkboxLabel="Groups" />
              <FormGroup type="checkbox" name="exportRecipes" onChange={this.onChangeHandler} checkboxLabel="Day Recipes" />
              <FormGroup type="checkbox" name="exportSchedules" onChange={this.onChangeHandler} checkboxLabel="Schedules" />
            </CheckboxGroup>
            <Button state="success" name="Download" disabled={this.isExportDisabled()} onClick={() => this.props.settingsActions.exportData(this.state.exportProfiles, this.state.exportRecipes, this.state.exportCanopies, this.state.exportSchedules)} />
          </Card>

          <Card title="Backup/Restore">
            <FormGroup type="file" name="restoreFile" onChange={this.onChangeHandler} label="Maximum file size is 1GB" />
            <FormGroup type="checkbox" name="resetSchedRestore" onChange={this.onChangeHandler} checkboxLabel="Reset Schedules on Restore" />
            <Button state="success" name="Upload" disabled={this.isRestoreDisabled()} onClick={() => this.props.settingsActions.restore(this.state.restoreFile, this.state.resetSchedRestore)} />
            <Button state="success" name="Download" onClick={() => this.props.settingsActions.backup()} />
          </Card>

          <Card title="Preferences">
            <FormGroup type="select" name="lengthUnit" label="Units" options={getSelectValues(lengthUnitOptions)} value={this.state.lengthUnit} onChange={this.onChangeHandler} />
            <FormGroup type="select" name="irradianceUnit" label="Irradiance" options={getSelectValues(irradianceUnitOptions)} value={this.state.irradianceUnit} onChange={this.onChangeHandler} />
            <FormGroup type="select" name="irradianceColorscale" label="Irradiance Map Colors" options={getSelectValues(colorscaleOptions)} value={this.state.irradianceColorscale}  onChange={this.onChangeHandler} />
            <FormGroup type="select" name="pageSize" label="Results per Page" options={getSelectValues(pageSizeOptions)} value={this.state.pageSize}  onChange={this.onChangeHandler} />
            <FormGroup type="select" name="schedResume" label="Resume Scheduling" options={getSelectValues(syncOptions)} value={this.state.schedResume} onChange={this.onChangeHandler} />
            <Button state="success" name="Submit" onClick={this.setSettings} />
          </Card>

          <Card title="Logs">
            <Paragraph> Takes you to the folder containing the logs file.</Paragraph>
            <Paragraph>  Here you can check warning and error messages logged during operation.</Paragraph>
            <Button state="success" name="Log File" onClick={() => this.openLogsFolder()} />
            <div style={{ marginTop: 30 }} />
            <FormGroup type="select" name="loggingMode" label="Logging Mode" options={getSelectValues(loggingOptions)} value={this.state.loggingMode} onChange={this.onChangeHandler} />
            <Paragraph> Debug mode logs all comunication to and from fixtures.</Paragraph>
            <Paragraph> The application needs to be restarted in order for this configuration to be applied.</Paragraph>
            <div style={{ marginTop: 30 }} />
            <Button state="success" name="Submit" onClick={this.setLoggingMode} />
          </Card>

          <Card state="danger" title="Reset data">
            <Paragraph>Once you reset the data, there is no going back. Please be certain. You may want to use the "Backup" function before.</Paragraph>
            <Button state="danger" name="Reset" onClick={() => this.props.settingsActions.deleteData()} />
          </Card>

        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    settingsActions: {...bindActionCreators(SettingsActions, dispatch)},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CanopyContainer);
