import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import * as SchedulesActions from '../actions/schedules';
import ScheduleForm from './ScheduleForm';

const ScheduleFormCreate = class extends Component {
    
    render() {
        const actions = [
            <Button state="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button state="success" name="Submit" onClick={() => this.props.submitCreate(this._form.getData()) } />
        ]

        return (
            <ScheduleForm actions={actions} recipes={this.props.recipes} validation={this.props.validation} canopies={this.props.canopies} addCanopy={this.props.addCanopy} obj={this.props.obj} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: state.schedules.scheduleObj,
        validation: state.schedules.validation,
        recipes: state.schedules.recipes,
        canopies: state.schedules.canopies
    };
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators(SchedulesActions, dispatch);
  }

  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ScheduleFormCreate));
