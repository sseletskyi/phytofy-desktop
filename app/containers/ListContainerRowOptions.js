// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Table from 'phytofy-design-system/src/design-system/organisms/Table';
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData';
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow';
import CardTable from 'phytofy-design-system/src/design-system/organisms/CardTable';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions';
import Checkbox from 'phytofy-design-system/src/design-system/atoms/Checkbox';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';


import { getTableHeaderCellFromObj } from './helpers';

import { getPageSize } from '../models/Settings';


const ListContainer = class extends Component {

    getTableRows(data, tableAttributes, selectable, rowOptions) {
        return data.map((row) => {
            let tableRow = Object.keys(tableAttributes).map(attribute => {
                let attributeConfigs = {};
                tableAttributes[attribute].configs.forEach((config) => {
                    attributeConfigs[config] = true;
                })

                let value;
                if (tableAttributes[attribute].transform) {
                    value = tableAttributes[attribute].transform(row)
                } else {
                    value = row[attribute]
                }
                return <TableData {...attributeConfigs}>{value}</TableData>
            })

            return (
                <TableRow>
                    {selectable &&
                        <TableData selectable>
                            <Checkbox checked={this.props.selected.indexOf(row._id) != -1}
                                onChange={(event) => { event.target.checked ? this.props.actions.select(row._id) : this.props.actions.deselect(row._id) }}
                            />
                        </TableData>
                    }
                    {tableRow}
                    {rowOptions &&
                    (
                        <TableRowOptions>
                            {rowOptions.map(({optionMethod, iconName, tooltip, flow}) => {
                                return <button onClick={() => optionMethod(row)}>
                                    <Icon icon={iconName} tooltip={tooltip} flow={flow}/>
                                </button>
                            })}
                        </TableRowOptions>
                    )}
                </TableRow>
            )
        })
    }

    render() {

        let header = Object.keys(this.props.tableAttributes).map((key) => {
            let obj = this.props.tableAttributes[key];
            return getTableHeaderCellFromObj(key, obj, this.props.sortAttribute, this.props.sortType, () => {
                if(obj.configs.indexOf('nosort') > -1) return;
                this.props.actions.sort(key);
            });
        })

        if (this.props.selectable) {
            header.unshift(
                <TableHeaderCell selectable>
                    <Checkbox checked={this.props.selected.length != 0 && this.props.selected.length == this.props.objects.length}
                        onChange={(event) => { event.target.checked ? this.props.actions.selectAll() : this.props.actions.deselectAll() }}
                    />
                </TableHeaderCell>
            );
        }

        let actionButtons = [...this.props.actionButtons];
        if (this.props.selected.length > 0) {
            actionButtons = [ ...this.props.actionButtonsWhenSelected, ...actionButtons ];
        }
        return (
            <div>
                {this.props.objects != null &&
                    <CardTable {...this.props} header={this.props.header} options={this.props.options} title={this.props.title} actions={actionButtons} from={this.props.from} to={this.props.to} of={this.props.count} pageGoTo={this.props.actions.goto} pageBack={this.props.actions.pageBack} pageNext={this.props.actions.pageNext}>
                        <Table header={header} >
                            {this.getTableRows(this.props.objects, this.props.tableAttributes, this.props.selectable, this.props.rowOptions)}
                        </Table>
                    </CardTable>
                }
            </div>
        )
    }
}

export default ListContainer;
