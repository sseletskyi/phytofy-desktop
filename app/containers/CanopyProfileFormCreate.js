import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from 'phytofy-design-system/src/design-system/atoms/Button';

import * as ProfilesActions from '../actions/profiles';
import CanopyProfileForm from './CanopyProfileForm';

const CanopyProfileFormCreate = class extends Component {
    render() {
        const actions = [
            <Button state="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button state="success" name="Submit" onClick={() => this.props.submitCreate(this._form.getData())} />
        ]

        return (
            <CanopyProfileForm lengthUnit={this.props.settings.lengthUnit} actions={actions} validation={this.props.validation} obj={this.props.obj} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: {...state.profiles.profileObj, unit: state.settings.lengthUnit},
        validation: state.profiles.validation,
        settings: state.settings
    };
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators(ProfilesActions, dispatch);
  }

  export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CanopyProfileFormCreate));
