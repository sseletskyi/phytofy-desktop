// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import ContentHeader from 'phytofy-design-system/src/design-system/organisms/ContentHeader';
import CardActionGoTo from 'phytofy-design-system/src/design-system/molecules/CardActionGoTo';
import CardActionNavigation from 'phytofy-design-system/src/design-system/molecules/CardActionNavigation';
import CardActionButtons from 'phytofy-design-system/src/design-system/molecules/CardActionButtons';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainerRowOptions';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as RecipesActions from '../actions/recipes';


const RecipesContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [], view: 'grid', goto: props.from, search: ''};
    this.handleGotoKeyPress = this.handleGotoKeyPress.bind(this);
    this.handleGotoChange = this.handleGotoChange.bind(this);

  }

  handleGotoKeyPress(event) {
    if (event.key === 'Enter') {
        let page = parseInt(event.target.value);
        if(!isNaN(page)) this.props.recipesActions.goto(parseInt(event.target.value));
    }
  }

  handleGotoChange(event) {
      this.setState({goto: event.target.value});
  }

  componentDidUpdate(prevProps){
    if(this.props.from != prevProps.from){
        this.setState({goto: this.props.from})
    }
  }


  componentDidMount() {
    this.props.recipesActions.getObjects();
  }

  render() {
    let { t } = this.props;

    const tableAttributes = {
      name: {name: t('name'), configs: []},
    }

    let actionButtons = [
      {icon: 'arrowUpward', onClick: (ids) => { this.props.recipesActions.importJsonRecipe() }, tooltip: "Import recipe in JSON format"},
      {icon: 'add', onClick: (ids) => { this.props.recipesActions.create() }, name: "Add Day Recipe" }
    ];

    let actionButtonsWhenSelected = [
      { icon: 'delete', onClick: () => { this.props.recipesActions.deleteMultiple() }, tooltip: "Delete selected Recipes" },
      {icon: 'arrowDownward', onClick: () => { this.props.recipesActions.exportJsonRecipe() }, tooltip: "Export recipes in JSON format"}
    ];

    if (this.props.selected.length > 0) {
      actionButtons = [ ...actionButtonsWhenSelected, ...actionButtons ];
    }
    // let configFilter = [
    //   {value: -1, name: 'All'},
    //   ...getCanopyConfigsForSelect()
    // ]

    const filters = [
      // {label: t('orientation'), options: configFilter, attribute: 'config'}
    ]

    const rowOptions = [
      {optionMethod: this.props.recipesActions.edit, iconName: 'edit', tooltip: 'Edit Recipe'},
      {optionMethod: this.props.recipesActions.duplicate, iconName: 'duplicate', tooltip: 'Duplicate Recipe'},
      {optionMethod: this.props.recipesActions.deleteObj, iconName: 'delete', tooltip: 'Delete Recipe'}
    ];

    return (
      <div>
        <TopbarContainer />
        {/* <FiltersContainer search={(value) => {this.props.recipesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.recipesActions.addFilter} removeFilter={this.props.recipesActions.removeFilter} /> */}
        <Content>
          <ContentHeader>
              <div>
                  <CardActionGoTo goto={this.state.goto} onKeyPress={this.handleGotoKeyPress} onChange={this.handleGotoChange} />
                  <CardActionNavigation from={this.props.from} to={this.props.to} of={this.props.count} pageBack={this.props.recipesActions.pageBack} pageNext={this.props.recipesActions.pageNext}/>
                  <CardActionButtons values={actionButtons} />
              </div>
              <div>
                  <FormGroup
                      type="search"
                      label="Search"
                      placeholder="Search Day Recipe"
                      name="Search Recipe"
                      value={this.state.search}
                      error=""
                      onChange={(event) => {this.setState({search: event.target.value}); this.props.recipesActions.addFilter('name', new RegExp(event.target.value, 'i'))}}
                  />
              </div>
          </ContentHeader>
          {this.props.objects != null &&
            <ListContainer title={t('recipes')} actions={{...this.props.recipesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
              actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions} />
          }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.recipes.sortAttribute ? state.recipes.sortAttribute : null,
    sortType: state.recipes.sortType ? state.recipes.sortType : 'orderAsc',
    objects: state.recipes.objects,
    count: state.recipes.count,
    from: state.recipes.from,
    to: state.recipes.to,
    selected: state.recipes.selected,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    recipesActions: {...bindActionCreators(RecipesActions, dispatch)}

  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(RecipesContainer));
