// @flow
import * as React from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Notifications from 'react-notification-system-redux';

const log = require('electron-log');

import PrimaryMenu from 'phytofy-design-system/src/design-system/organisms/PrimaryMenuProd';
import MenuLink from "phytofy-design-system/src/design-system/molecules/MenuLinkProd";
// import SecondaryMenu from 'phytofy-design-system/src/design-system/organisms/SecondaryMenu';
// import ThirdMenu from 'phytofy-design-system/src/design-system/organisms/ThirdMenu';
import RightSidebar from 'phytofy-design-system/src/design-system/organisms/RightSidebar';
import Wrapper from 'phytofy-design-system/src/design-system/organisms/Wrapper';
import MainContent from 'phytofy-design-system/src/design-system/organisms/MainContent';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Commissioning from "phytofy-design-system/src/design-system/organisms/Commissioning";
import i18n from '../../i18n';
import { getNotificationsStyle } from '../utils/notifications';

//import {automaticSync, automaticSyncTimeRef} from '../models/Schedule';

import 'phytofy-design-system/src/css/App.css';
// import 'phytofy-design-system/src/css/Accordion.css';
// // import 'vis/dist/vis-timeline-graph2d.min.css';
// import 'vis/dist/vis-timeline-graph2d.min.css'
// import 'phytofy-design-system/src/css/Vis.css';

import CanopyProfileFormCreate from './CanopyProfileFormCreate';
import CanopyProfileFormEdit from './CanopyProfileFormEdit';
import CanopyFormCreate from './CanopyFormCreate';
import CanopyFormEdit from './CanopyFormEdit';
import RecipeFormCreate from './RecipeFormCreate';
import RecipeFormEdit from './RecipeFormEdit';
import ScheduleFormCreate from './ScheduleFormCreate';
import ScheduleFormEdit from './ScheduleFormEdit';
import ScheduleFormAddToCanopy from './ScheduleFormAddToCanopy'

import RealtimeContainer from './RealtimeContainer';
import AddCanopyToSchedule from './AddCanopyToSchedule';
import AddFixtureToCanopy from './AddFixtureToCanopy';

import * as SidebarActions from '../actions/sidebar';
import * as SettingsActions from '../actions/settings';
import * as DevicesActions from '../actions/devices';


type Props = {
  children: React.Node
};

const primaryMenu = [
  { icon: 'fixture', name: `${i18n.t('overview')}`, key: 1, location: '/', tooltip: 'Overview'},
  { icon: 'aspectRatio', name: `${i18n.t('layouts')}`, key: 1, location: '/profiles', tooltip: 'Layouts'},
  { icon: 'groups', name: `${i18n.t('groups')}`, key: 1, location: '/canopies', tooltip: 'Groups'},
  { icon: 'time', name: `${i18n.t('recipes')}`, key: 4, location: '/recipes', tooltip: 'Day Recipes' },
  { icon: 'schedule', name: `${i18n.t('schedules')}`, key: 5, location: '/schedules', tooltip: 'Schedules' },
  { icon: 'setup', name: `${i18n.t('setup')}`, key: 7, location: '/setup/fixtures', tooltip: 'Configuration' },
];

const secondaryMenu = [
  {name: `${i18n.t('canopies')}`, key: 1, location: '/setup/canopies'},
  {name: `${i18n.t('devices')}`, key:2, location: '/setup/devices'},
  {name: `${i18n.t('layouts')}`, key:3, location: '/setup/profiles'},
  {name: `${i18n.t('diagnostic')}`, key: 6, location: '/setup/diagnostic' },
  {name: `${i18n.t('settings')}`, key: 7, location: '/setup/settings' },
];

const App = class extends React.Component<Props> {
  props: Props;

  constructor(props) {
    super(props);
    this.state = {isSecondaryMenuOpen: false};
    this.openRightSidebar = this.openRightSidebar.bind(this);
    this.closeRightSidebar = this.closeRightSidebar.bind(this);
    this.openSecondaryMenu = this.openSecondaryMenu.bind(this);
    this.closeSecondaryMenu = this.closeSecondaryMenu.bind(this);
    this.renderRoute = this.renderRoute.bind(this);
    this.renderRouteDevices = this.renderRouteDevices.bind(this);
    this.checkIsActivePrimary = this.checkIsActivePrimary.bind(this);
    this.checkIsActive = this.checkIsActive.bind(this);
    // this.checkIsActiveSecondary = this.checkIsActiveSecondary.bind(this);
    // this.checkIsActiveThird = this.checkIsActiveThird.bind(this);
    // this.hasSecondaryMenu = this.hasSecondaryMenu.bind(this);
    // this.hasThirdMenu = this.hasThirdMenu.bind(this);
    this.updateDevices = this.updateDevices.bind(this);
    this.firstMount = true;
    this.renderCommissionPannel = this.renderCommissionPannel.bind(this);
  }

  componentDidMount(){
    //this.props.settingsActions.getSettings();
    // this.props.devicesActions.getMoxas(this.props.history);
    // this.props.devicesActions.getAllFixtures();
    // this.props.devicesActions.getFixtures();
    this.updateDevices();
  }

  async updateDevices(){
    await this.props.settingsActions.getSettings();
    await this.props.devicesActions.getMoxas(this.props.history);
    if(this.firstMount){
      this.firstMount = false;
      this.props.devicesActions.commissionFixtures();
      //this.props.devicesActions.getAllFixtures();
      console.log(this.props.resumeSched);
      if(this.props.resumeSched == 'enabled'){
        console.log('=== automatic sync schedules and time ref!');
        log.warn(`[Automatic Sync] Synching time reference and resuming schedules.`);
        this.props.devicesActions.automaticSync(this.props.devices.comm);
      }else{
        console.log('=== automatic sync time ref!');
        log.warn(`[Automatic Sync] Synching time reference.`);
        this.props.devicesActions.automaticSyncTimeRef(this.props.devices.comm);
      }
    }
    else{
      this.props.devicesActions.getAllFixtures();
    }
  }

  openRightSidebar() {
    // this.setState({
    //   isRightSidebarOpen: true
    // });
    this.props.sidebarActions.openSidebar()
  }

  closeRightSidebar() {
    // this.setState({
    //   isRightSidebarOpen: false
    // });
    this.props.sidebarActions.closeSidebar()
  }

  openSecondaryMenu() {
    this.setState({
      isSecondaryMenuOpen: true
    });
  }

  closeSecondaryMenu() {
    this.setState({
      isSecondaryMenuOpen: false
    });
  }

  renderRoute(newRoute) {
    this.props.history.replace(newRoute)
  }

  renderRouteDevices(newRoute) {
    this.props.history.replace(newRoute)
    //let macAddress = this.props.history.location.pathname.split('/')[3]
    //this.props.devicesActions.getFixtures(macAddress);
  }

  checkIsActivePrimary(location) {
    return this.props.history.location.pathname.split('/')[1] == location.split('/')[1]
  }

  checkIsActive(currLocation, value){
    return currLocation === value;
  }

  renderCommissionPannel(flag) {
    if(flag){
      return <Commissioning text="Commissioning refresh in progress" />
    }
  }
  // checkIsActiveSecondary(location) {
  //   return this.props.history.location.pathname.indexOf(location) > -1;
  // }

  // checkIsActiveThird(location) {
  //   return this.props.history.location.pathname == location
  // }

  // hasSecondaryMenu() {
  //   return this.props.history.location.pathname.split("/")[1] == 'setup';
  // }

  // hasThirdMenu() {
  //   return this.props.history.location.pathname.split("/").length > 2 && this.props.history.location.pathname.split("/")[2] == 'devices';
  // }

  render() {
    const sidebarComponents = {
      'CREATE_CANOPY_PROFILE_FORM': CanopyProfileFormCreate,
      'EDIT_CANOPY_PROFILE_FORM': CanopyProfileFormEdit,
      'CREATE_CANOPY_FORM': CanopyFormCreate,
      'EDIT_CANOPY_FORM': CanopyFormEdit,
      'CREATE_RECIPE_FORM': RecipeFormCreate,
      'EDIT_RECIPE_FORM': RecipeFormEdit,
      'CREATE_SCHEDULE_FORM': ScheduleFormCreate,
      'EDIT_SCHEDULE_FORM': ScheduleFormEdit,
      'ADD_SCHEDULE_TO_CANOPY_FORM': ScheduleFormAddToCanopy,
      'REALTIME_CONTROL_FORM': RealtimeContainer,
      'ADD_CANOPY_TO_SCHEDULE_FORM': AddCanopyToSchedule,
      'ADD_FIXTURE_TO_CANOPY_FORM': AddFixtureToCanopy,
    }

    let SidebarComponent;
    try {
      SidebarComponent = sidebarComponents[this.props.rightSidebarComponent]
    } catch(err){
      console.error("ERR");
    }

    const notificationsStyle = getNotificationsStyle();

    return (
      <Wrapper>
        {(this.props.commissioning != null) && this.renderCommissionPannel(this.props.commissioning)}
        <Notifications style={notificationsStyle} notifications={this.props.notifications} />
        <PrimaryMenu history={this.props.history} values={primaryMenu} renderRoute={this.renderRoute} checkIsActive={this.checkIsActivePrimary} >
          <MenuLink to="/guide" icon="help" tooltip="Quick Guide" isActive={this.checkIsActive(this.props.history.location.pathname, "/guide")}/>
          <MenuLink to="/about/product" icon="info" tooltip="About" isActive={this.checkIsActive(this.props.history.location.pathname, "/about/product") || this.checkIsActive(this.props.history.location.pathname, "/about/credits") || this.checkIsActive(this.props.history.location.pathname, "/about/terms")}/>
        </PrimaryMenu>
        {/* <SecondaryMenu title="Setup" values={secondaryMenu} isOpen={this.hasSecondaryMenu()} checkIsActive={this.checkIsActiveSecondary} renderRoute={this.renderRoute} />
        <ThirdMenu title="Devices" refresh={() => this.props.devicesActions.getMoxas(this.props.history)} values={this.props.devices.moxaDisplay} isOpen={this.hasThirdMenu()} renderRoute={this.renderRoute} checkIsActive={this.checkIsActiveThird} renderRoute={this.renderRouteDevices} /> */}

        <MainContent isRightSidebarOpen={this.props.isRightSidebarOpen}>
          { this.props.children }
        </MainContent>
        <RightSidebar goBack={() => this.props.sidebarActions.openSidebar(
            this.props.rightSidebarPrevState.component,
            this.props.rightSidebarPrevState.label,
            this.props.rightSidebarPrevState.title,
            this.props.rightSidebarPrevState.type
          )} type={this.props.rightSidebarType} label={this.props.rightSidebarLabel} title={this.props.rightSidebarTitle} isOpen={this.props.isRightSidebarOpen} closeRightSidebar={this.closeRightSidebar} >
          {SidebarComponent && <SidebarComponent />}
        </RightSidebar>
      </Wrapper>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    rightSidebarComponent: state.sidebar.component,
    rightSidebarLabel: state.sidebar.label,
    rightSidebarTitle: state.sidebar.title,
    rightSidebarType: state.sidebar.type,
    rightSidebarPrevState: state.sidebar.prevState,
    notifications: state.notifications,
    devices: state.devices,
    commissioning: state.devices.commissioning,
    resumeSched: state.settings.schedResume
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: bindActionCreators(SidebarActions, dispatch),
    settingsActions: bindActionCreators(SettingsActions, dispatch),
    devicesActions: bindActionCreators(DevicesActions, dispatch),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
