import React,{Component} from 'react';
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import FormGroupList from 'phytofy-design-system/src/design-system/molecules/FormGroupList';
import ListItem from 'phytofy-design-system/src/design-system/atoms/ListItem';


import FormContainer from './FormContainer';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';
import ScheduleFormDetails from './ScheduleFormDetails';

import * as SchedulesActions from '../actions/schedules';

const ScheduleFormAddToCanopy = class extends Component {
    constructor(props){
        super();
        this.props = props;
    }

    render() {

        const actions = [
            <Button state="cancel" name="Cancel" onClick={() => this.props.cancelForm() }/>,
            <Button state="success" name="Submit" onClick={(ev) => {ev.preventDefault(); this.props.submitAddToCanopy(this._form.getData()) }} />
        ]

        return (
            <ScheduleFormDetails actions={actions} schedules={this.props.schedules} obj={this.props.obj} ref={(ref) => this._form = ref}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        obj: state.schedules.availableScheds[0],
        schedules: state.schedules.availableScheds,
        validation: state.schedules.validation,
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(SchedulesActions, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ScheduleFormAddToCanopy));
