import React, { Component } from "react";
import Content from "phytofy-design-system/src/design-system/organisms/Content";
import ProductInfo from "phytofy-design-system/src/design-system/molecules/ProductInfo";
import TopbarContainer from './TopbarContainer';

const info = [
  { key: 1, value: "Version 1.0.1 (1.0.1)" },
  { key: 2, value: "© 2019, OSRAM GmbH. All rights reserved." },
  { key: 3, value: "www.osram.com/phytofy " },
  { key: 4, value: "phytofysupport@osram.com" }
];

export default class extends Component {

  render() {

    return (
      <div>
        <TopbarContainer tabList={[
            { name: 'Product Info', location: '/about/product', isActive: true },
            { name: 'Credits', location: '/about/credits', isActive: false },
            { name: 'Terms of Service', location: '/about/terms', isActive: false }
        ]}/>
        <Content>
          <ProductInfo title={["PHYTOFY", <sup>®</sup>, " RL"]} values={info} />
        </Content>
      </div>
    );
  }
}
