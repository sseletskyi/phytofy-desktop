// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import ContentHeader from 'phytofy-design-system/src/design-system/organisms/ContentHeader';
import CardActionButtons from 'phytofy-design-system/src/design-system/molecules/CardActionButtons';
import Table from 'phytofy-design-system/src/design-system/organisms/Table';
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData';
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow';
// import Card from 'phytofy-design-system/src/design-system/organisms/Card';
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell';
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions';
import Checkbox from 'phytofy-design-system/src/design-system/atoms/Checkbox';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';


import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as DevicesActions from '../actions/devices';


const DevicesContainer = class extends Component {
  constructor(props){
    super(props);
    this.getCurrentMoxaMac = this.getCurrentMoxaMac.bind(this);
  }

  componentWillMount() {
    if(!this.getCurrentMoxaMac() && this.props.moxas.length > 0) {
      this.props.history.replace(`/setup/devices/${this.props.moxas[0].mac}`)
    }
  }

  componentDidMount() {
    this.props.devicesActions.getFixtures();
  }

  getCurrentMoxaMac() {
    return this.props.history.location.pathname.split('/')[3];
  }

  render() {
    const tableAttributes = {
      name: {name: "Address", configs: ['nosort']},
      ports: {name: "# of Ports", configs: ['measure', 'nosort']},
      fixtures: {name: "# of Fixtures", configs: ['measure', 'nosort']}
    }

    let actionButtons = [
      { icon: 'refresh', onClick: () => { this.props.devicesActions.getMoxas(this.props.history) } },
    ];

    return (
      <div>
        <TopbarContainer tabList={[
            { name: 'Light Fixtures', location: '/setup/fixtures', isActive: false },
            { name: 'Adapters', location: '/setup/devices', isActive: true },
            { name: 'Settings', location: '/setup/settings', isActive: false },
        ]}/>
        {/* <FiltersContainer filters={[]} /> */}
        <Content>
        <ContentHeader>
          <div>
              <CardActionButtons values={actionButtons} />
          </div>
        </ContentHeader>
        {this.props.moxas.length > 0 &&
        <ListContainer title="Adapters" actions={{...this.props.devicesActions}} {...this.props} tableAttributes={tableAttributes}
            actionButtons={actionButtons} objects={this.props.moxaDisplay} selected={[]} />
        }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    moxas: state.devices.moxas,
    moxaDisplay: state.devices.moxaDisplay,
    fixtures: state.devices.fixtures,
    sortAttribute: state.devices.sortAttribute ? state.devices.sortAttribute : null,
    sortType: state.devices.sortType ? state.devices.sortType : 'orderAsc',
    count: state.devices.count,
    from: state.devices.from,
    to: state.devices.to,
    hasPrev: state.devices.hasPrev,
    hasNext: state.devices.hasNext,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    devicesActions: {...bindActionCreators(DevicesActions, dispatch)}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DevicesContainer));
