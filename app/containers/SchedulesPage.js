// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import ContentHeader from 'phytofy-design-system/src/design-system/organisms/ContentHeader';
import CardActionGoTo from 'phytofy-design-system/src/design-system/molecules/CardActionGoTo';
import CardActionNavigation from 'phytofy-design-system/src/design-system/molecules/CardActionNavigation';
import CardActionButtons from 'phytofy-design-system/src/design-system/molecules/CardActionButtons';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainerDataOptions';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as SchedulesActions from '../actions/schedules';


const SchedulesContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [], goto: props.from, search: ''};
    this.handleGotoKeyPress = this.handleGotoKeyPress.bind(this);
    this.handleGotoChange = this.handleGotoChange.bind(this);
    this.formatDate = this.formatDate.bind(this);
  }

  handleGotoKeyPress(event) {
    if (event.key === 'Enter') {
        let page = parseInt(event.target.value);
        if(!isNaN(page)) this.props.schedulesActions.goto(parseInt(event.target.value));
    }
  }

  handleGotoChange(event) {
      this.setState({goto: event.target.value});
  }


  componentDidUpdate(prevProps){
    if(this.props.from != prevProps.from){
        this.setState({goto: this.props.from})
    }
  }


  componentDidMount() {
    this.props.schedulesActions.getObjects();
  }

  formatDate(dateString) {
    let auxDate = dateString.split('-');
    auxDate = auxDate.reverse();
    return auxDate.join('-');
  }

  render() {
    let { t } = this.props;

    const tableAttributes = {
      start_date: {name: t('start_date'), configs: [], transform: (row) => `${this.formatDate(row.start_date)}`},
      end_date: {name: t('end_date'), configs: [], transform: (row) => `${this.formatDate(row.end_date)}`},
      '_recipe.name': {name: "Day Recipe", configs: [], transform: (row) => `${row._recipe.name}` },
      status: {name: "", configs: [], transform: (row) =>{
        if(row.status == 0) return <Icon fill="#FF6600" icon="warning" tooltip="Edit schedule and submit again to solve issue"/>
      }}
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.schedulesActions.create() }, name: "Add Schedule" },
    ];

    let actionButtonsWhenSelected = [{ icon: 'delete', onClick: () => { this.props.schedulesActions.deleteMultiple() }, tooltip: "Delete selected Schedules" }];

    if (this.props.selected.length > 0) {
      actionButtons = [ ...actionButtonsWhenSelected, ...actionButtons ];
    }
    // let configFilter = [
    //   {value: -1, name: 'All'},
    //   ...getCanopyConfigsForSelect()
    // ]

    const filters = [
      // {label: t('orientation'), options: configFilter, attribute: 'config'}
    ]

    const rowOptions = [
      {optionMethod: this.props.schedulesActions.edit, iconName: 'edit', tooltip: "Edit Schedule"},
      {optionMethod: this.props.schedulesActions.duplicate, iconName: 'duplicate', tooltip: "Duplicate Schedule"},
      {optionMethod: this.props.schedulesActions.deleteObj, iconName: 'delete', tooltip: "Delete Schedule"}
    ];

    //if(this.props.objects != null) console.log(this.props.objects) //debug

    return (
      <div>
        <TopbarContainer />
        {/* <FiltersContainer filters={filters} addFilter={this.props.schedulesActions.addFilter} removeFilter={this.props.schedulesActions.removeFilter} /> */}
        <Content>
          <ContentHeader>
              <div>
                  <CardActionGoTo goto={this.state.goto} onKeyPress={this.handleGotoKeyPress} onChange={this.handleGotoChange} />
                  <CardActionNavigation from={this.props.from} to={this.props.to} of={this.props.count} pageBack={this.props.schedulesActions.pageBack} pageNext={this.props.schedulesActions.pageNext}/>
                  <CardActionButtons values={actionButtons} />
              </div>
          </ContentHeader>
          {this.props.objects != null &&
            <ListContainer title={t('schedules')} actions={{...this.props.schedulesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
              actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} dataOptions={rowOptions} />
          }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.schedules.sortAttribute ? state.schedules.sortAttribute : null,
    sortType: state.schedules.sortType ? state.schedules.sortType : 'orderAsc',
    objects: state.schedules.objects,
    count: state.schedules.count,
    from: state.schedules.from,
    to: state.schedules.to,
    selected: state.schedules.selected,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    schedulesActions: {...bindActionCreators(SchedulesActions, dispatch)}

  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(SchedulesContainer));
