import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/LightController';
import LightController from '../../lib/phytofy-design-system/src/design-system/organisms/LightController';
import LightControllerFixtures from '../../lib/phytofy-design-system/src/design-system/organisms/LightControllerFixtures';

import * as CanopiesActions from '../actions/canopiesSetup';


class RealtimeContainer extends Component {
  render() {
    if(this.props.realTimeMode == 'canopy'){
      return <LightController fInfo={this.props.fixtureInfo} {...this.props.fixturesLedState} onAfterChange={this.props.canopiesActions.realtimeControlChange} />
    }else if(this.props.realTimeMode == 'fixture'){
      return <LightControllerFixtures fInfo={this.props.fixtureInfo} {...this.props.fixturesLedState} onAfterChange={this.props.canopiesActions.realtimeFixtureChange} />
    }
  }
}

function mapStateToProps(state) {
    return {
      fixturesLedState: state.canopiesSetup.fixturesLedState,
      fixtureInfo: state.canopiesSetup.fixtureInfo,
      realTimeMode: state.canopiesSetup.realTimeMode
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      canopiesActions: {...bindActionCreators(CanopiesActions, dispatch)},
    }
  }

  export default translate()(connect(mapStateToProps, mapDispatchToProps)(RealtimeContainer));
