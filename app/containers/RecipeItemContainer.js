import React, { Component } from 'react';
import TimePicker from 'rc-time-picker';
import TimeField from "react-simple-timefield";
import { Accordion, AccordionItem, AccordionItemTitle, AccordionItemBody } from 'react-accessible-accordion';
import moment from 'moment';

import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';
import ItemTimeContainer from 'phytofy-design-system/src/design-system/molecules/ItemTimeContainer';
import LightController from 'phytofy-design-system/src/design-system/organisms/LightControllerRecipes';
import Button from 'phytofy-design-system/src/design-system/atoms/Button';
import ErrorMessage from 'phytofy-design-system/src/design-system/atoms/ErrorMessage';

import 'rc-time-picker/assets/index.css';
import {compareTimes} from '../models/Recipe';

class RecipeItemContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {...this.props.obj, errors: {start: '', end: ''}, isOpen: this.props.isOpen};

        this.onClickHandler = this.onClickHandler.bind(this);
        this.validateSinglePeriod = this.validateSinglePeriod.bind(this);
    }

    componentDidUpdate(prevProps){
        if(this.props != prevProps){
            this.setState({...this.props.obj, isOpen: this.props.isOpen})
        }
    }

    validateSinglePeriod = (period, testAgainst) => {
        let valid = true;
        if(testAgainst.length == 0) return true;

        let testEnd= period.end.split(':');
        let testStart = period.start.split(':');
        let testEndMinutes = parseInt(testEnd[0])*60+parseInt(testEnd[1]);
        let testStartMinutes = parseInt(testStart[0])*60+parseInt(testStart[1]);

        for(let k = 0; k < testAgainst.length; k++){
            let eachEnd = testAgainst[k].end.split(':');
            let eachStart = testAgainst[k].start.split(':');
            let endMinutes = parseInt(eachEnd[0])*60+parseInt(eachEnd[1]);
            let startMinutes = parseInt(eachStart[0])*60+parseInt(eachStart[1]);
            valid = compareTimes(testStartMinutes, testEndMinutes, startMinutes, endMinutes);
            if(!valid) return false
        }
        return valid;
    }

    onClickHandler() {
        let errorStart = false;
        let errorEnd = false;

        if(!this.state.start) this.state.start = "00:00";
        if(!this.state.end) this.state.end = "00:00";

        if(this.state.start >= this.state.end) {
            errorStart = true;
        }

        if(this.state.start >= this.state.end) {
            errorEnd = true;
        }

        this.setState({
            errors: {
                start: errorStart ? 'Invalid time' : '',
                end: errorEnd ? 'Invalid time' : '',
            }
        })

        if (!this.validateSinglePeriod({
            start: this.state.start, end: this.state.end
        }, this.props.periods)) {
            this.setState({ globalError: 'There are overlapping periods.' });
            return;
        } else {
            this.setState({ globalError: null });
        }

        if(errorStart || errorEnd) return;

        this.props.action({...this.state, errors: {start: '', end: ''}}, this.props.i);

        if(this.props.new) {
            let keys = ['start', 'end', 'uv', 'blue', 'green', 'hyperRed', 'farRed', 'white']
            let newPeriodInitValues = {};
            keys.forEach((key) => {
                newPeriodInitValues[key] = "";
            })

            this.setState({...newPeriodInitValues, errors: {start: '', end: ''}});
        }

    }

    render() {
        let title = "Add period";
        if(this.props.obj && this.props.obj.start && this.props.obj.end) {
            title = `${this.props.obj.start} - ${this.props.obj.end}`
        }

        return (
            <AccordionItem uuid={this.props.uuid} expanded={this.props.isOpen}>
                <AccordionItemTitle>
                    <span>{title}</span>
                    <div class="accordion__arrow">
                        <Icon icon="arrowDown" fill="#87888A" />
                    </div>
                </AccordionItemTitle>
                <AccordionItemBody>
                    <ItemTimeContainer title="Time">
                      <TimeField
                        value={this.state.start}
                        onChange={(value) => this.setState({start: value})}
                        input={<FormGroup type="text" label="Start" error={this.state.errors.start}  />}
                        colon=":"
                      />
                      <TimeField
                        value={this.state.end}
                        onChange={(value) => this.setState({end: value})}
                        input={<FormGroup type="text" label="End" error={this.state.errors.end}  />}
                        colon=":"
                      />
                    </ItemTimeContainer>
                    <LightController {...this.state} onAfterChange={(values) => this.setState({...values})} />
                    { this.state.globalError && <ErrorMessage>{ this.state.globalError }</ErrorMessage> }<br/>
                    <Button state="success" name={this.props.new ? "Add" : "Apply"} onClick={this.onClickHandler} />
                    {!this.props.new && <Button state="danger" name={"Delete"} onClick={() => this.props.deletePeriod(this.props.i)} />}
                </AccordionItemBody>
            </AccordionItem>
        )
    }
}

export default RecipeItemContainer;
