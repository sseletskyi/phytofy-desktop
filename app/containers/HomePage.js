// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import QuickGuide from "phytofy-design-system/src/design-system/organisms/QuickGuide";
import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainer';
// import FiltersContainer from './FiltersContainer';
import GridContainer from './GridContainer';
import TopbarContainer from './TopbarContainer';


import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as CanopiesActions from '../actions/canopies';
import * as ProfilesActions from '../actions/profiles';
import * as SchedulesActions from '../actions/schedules';


const CanopyContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: ['empty'], view: 'grid'};
    this.renderRoute = this.renderRoute.bind(this);
  }

  componentDidMount() {
    this.props.schedulesActions.resetIrradMap();
    this.props.canopiesActions.resetObject();
    this.props.canopiesActions.getObjects();
    this.props.profilesActions.getObjectsAll();
  }

  renderRoute(newRoute) {
    this.props.history.replace(newRoute)
  }

  render() {
    //quick guide stuff
    const glossaryList = [
      { key: 1, value: "Fixture – Single light emiting device" },
      { key: 2, value: "Layout – Description of orientation and spacing of the fixtures in a group (used for irradiance calculations)" },
      { key: 3, value: "Group – Set of fixtures to be controlled together on a schedule" },
      { key: 4, value: "Day Recipe – A recipe which defines the spectral distributions of light to be applied during specified periods of a single day" },
      { key: 5, value: "Schedule – Allows to repeat a day recipe over multiple days for a certain group" }
    ];

    const instructionsList = [
      { key: 1, to: "/profiles", value: "Create Layout" },
      { key: 2, to: "/canopies", value: "Create Group" },
      { key: 3, to: "/recipes", value: "Create Day Recipe" },
      { key: 4, to: "/schedules", value: "Create Schedule" }
    ];

    const externalLinks = [
      {
        key: 1,
        url: "https://www.osram.com/pia/horticulture-products.jsp",
        value: "Follow this link to the instruction video"
      },
      {
        key: 2,
        url: "https://www.osram.com/pia/horticulture-products.jsp",
        value: "Follow this link to the user manual"
      }
    ];
    //overview
    const tableAttributes = {
      name: {name: "Name", configs: []},
      '_profile.name': {name: "Layout", configs: [], transform: (row) => `${row._profile.name} (${canopyConfigs[row._profile.config]})` }
    }

    const getProfilesForSelect = (profiles) => {
      return profiles.map((profile) => {
          return {name: `${profile.name} (${canopyConfigs[profile.config]})`, value: profile._id}
      })
    };

    let profileFilter = [
      {value: -1, name: 'All'},
      ...getProfilesForSelect(this.props.profiles)
    ];

    const filters = [
      {label: 'Layout', options: profileFilter, attribute: 'profileId'}
    ]

    const rowOptions = [
      {optionMethod: this.props.canopiesActions.edit, iconName: 'edit'},
      {optionMethod: this.props.canopiesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.canopiesActions.deleteObj, iconName: 'delete'}
    ];

    if(this.props.objects.length == 0 && this.props.count == 0){
      return (
        <div>
          <TopbarContainer />
          <Content>
            <QuickGuide
              glossary={glossaryList}
              instructions={instructionsList}
              links={externalLinks}
            />
          </Content>
        </div>
      )
    }else{
      return (
        <div>
          <TopbarContainer />
          {/* <FiltersContainer search={(value) => {this.props.canopiesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.canopiesActions.addFilter} queryAction={this.props.canopiesActions.getObjectsAll} removeFilter={this.props.canopiesActions.removeFilter} /> */}
          <GridContainer objects={this.props.objects} renderRoute={this.renderRoute}/>
        </div>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    objects: state.canopies.objects,
    count: state.canopies.count,
    profiles: state.profiles.objectsAll,
    settings: state.settings,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    canopiesActions : {...bindActionCreators(CanopiesActions, dispatch)},
    profilesActions: {...bindActionCreators(ProfilesActions, dispatch)},
    schedulesActions: {...bindActionCreators(SchedulesActions, dispatch)},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CanopyContainer);
