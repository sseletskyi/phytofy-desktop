// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

const log = require('electron-log');
//const log = require('electron').remote.require('electron-log');

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import Topbar from 'phytofy-design-system/src/design-system/organisms/Topbar'
import CardCanopyDetails from 'phytofy-design-system/src/design-system/organisms/CardCanopyDetails'
import CardValues from 'phytofy-design-system/src/design-system/organisms/CardValues';
import CardTable from 'phytofy-design-system/src/design-system/organisms/CardTable'
import Canopy from 'phytofy-design-system/src/design-system/atoms/Canopy'
import CanopyControls from "phytofy-design-system/src/design-system/organisms/CanopyControls";
import CanopyInfo from "phytofy-design-system/src/design-system/organisms/CanopyInfo";
import Table from 'phytofy-design-system/src/design-system/organisms/Table'
import TableHeaderCell from 'phytofy-design-system/src/design-system/molecules/TableHeaderCell'
import TableRow from 'phytofy-design-system/src/design-system/molecules/TableRow'
import TableRowOptions from 'phytofy-design-system/src/design-system/molecules/TableRowOptions'
import TableData from 'phytofy-design-system/src/design-system/molecules/TableData'
import Toggle from "phytofy-design-system/src/design-system/atoms/Toggle";
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons'
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup'
import Tag from 'phytofy-design-system/src/design-system/atoms/Tag'

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainerRowOptions';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as SchedulesActions from '../actions/schedules';
import * as CanopiesActions from '../actions/canopies';
import * as CanopiesSetup from '../actions/canopiesSetup';
import { irradianceUnitOptions, getIrradianceToRead } from '../models/Settings';

import html2canvas from "html2canvas"

const DetailsContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [] ,mapVis: true, rightChecked: false};
    this.renderRoute = this.renderRoute.bind(this);
    this.getMap = this.getMap.bind(this);
    this.getIrradCard = this.getIrradCard.bind(this);
    this.getMaxIrradCard = this.getMaxIrradCard.bind(this);
    this.showIrradiance = this.showIrradiance.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.saveImage = this.saveImage.bind(this);
    this.downloadURL = this.downloadURL.bind(this);
  }

  componentDidMount(){
    this.props.canopiesActions.getObject(this.props.match.params.id);
    this.props.schedulesActions.getObjectsFromCanopy(this.props.match.params.id);
  }

  componentDidUpdate(prevProps){
    if((this.props.irradMap != null || this.props.irradMap != undefined) &&
      prevProps.irradMap != this.props.irradMap){
      this.setState({
          ... this.state,
          mapVis: false,
          rightChecked: true
      });
    }
  }

  async updateMap(){
    await this.props.schedulesActions.getObjectsFromCanopy(this.props.match.params.id);
    if(this.props.irradMap != null || this.props.irradMap != undefined){
      console.log('SHOW IRRADIANCE MAP!');
      this.setState({
          ... this.state,
          mapVis: false,
          rightChecked: true
      });
    }
  }

  renderRoute(newRoute) {
    this.props.history.replace(newRoute)
  }

  showIrradiance(event) {
    if(event.target.id === "right") {
      console.log("show irradiance map");
      //this.state.mapVis = false;
      this.setState({
          ... this.state,
          mapVis: false,
          rightChecked: true
      })
    } else {
      console.log("hide irradiance map");
      //this.state.mapVis = true;
      this.setState({
          ... this.state,
          mapVis: true,
          rightChecked: false
      })
    }
  }

  saveImage() {
    let input = document.getElementById("canvas");
    html2canvas(input).then(canvas => {
      let imgData = canvas
        .toDataURL("image/jpg")
        .replace("image/jpg", "octet-stream");
      this.downloadURL(imgData);
      console.log("export");
    });
  }

  downloadURL(imgData) {
    let a = document.createElement("a");
    a.href = imgData.replace("image/jpg", "octet-stream");
    a.download = "Irradiance-Map.jpg";
    a.click();
  }

  formatDate(dateString) {
    let auxDate = dateString.split('-');
    auxDate = auxDate.reverse();
    return auxDate.join('-');
  }

  getMap(data, mapData){

    const configMap = {
      '1v': `1 vertical`,
      '1h': `1 horizontal`,
      '2v': `2 vertical`,
      '2h': `2 horizontal`,
      '3v': `3 vertical`,
      '2x2h': `2x2 horizontal`,
      '4v': `4 vertical`
    }
    return (
      <CardCanopyDetails title="Irradiance Map" action={() => {this.props.canopiesSetup.edit(data)}}>

      <div>
        <div id="canvas">
          <Canopy id="thisCanopy" type={data._profile.config} length={data._profile.length} width={data._profile.width}
            spacing={data._profile.deltaLength} fixtureLength="673" fixtureWidth="298"
            data={mapData} isHidden={this.state.mapVis} />
        </div>
      </div>

      <div>
        <CanopyInfo>
          <FormGroup type="detail" label="Name" value={data.name} />
          <FormGroup type="detail" label="Layout" value={configMap[data._profile.config]} />
          <FormGroup type="detail" label="Length" value={data._profile.length} />
          <FormGroup type="detail" label="Width" value={data._profile.width} />
          <FormGroup type="detail" label="Height" value={data._profile.height} />
          <FormGroup type="detail" label="△L" value={data._profile.deltaLength} />
        </CanopyInfo>

        <CanopyControls>
          <Toggle
            name="showIrradiance"
            label={this.state.rightChecked === true ? ["Irradiance Map — ", <a onClick={() => this.saveImage()}>Download</a>] : "Irradiance Map"}
            leftLabel="Hide"
            rightLabel="Show"
            rightChecked={this.state.rightChecked}
            onClick={this.showIrradiance.bind(this)} />
        </CanopyControls>
      </div>


      </CardCanopyDetails>
    )
  }

  getIrradCard(data){
    let colorChannels = [];
    let u = irradianceUnitOptions[this.props.settings.irradianceUnit];

    if( data.ledState == null){
      colorChannels = [
          { icon: 'fixture', fill: 'var(--channelUV, rgb(106, 27, 154))', value: '-',  legend: 'Ultraviolet' },
          { icon: 'fixture', fill: 'var(--channelGreen, rgb(9, 228, 37))', value: '-',  legend: 'Green' },
          { icon: 'fixture', fill: 'var(--channelBlue, rgb(97, 175, 241))', value: '-',  legend: 'Blue' },
          { icon: 'fixture', fill: 'var(--channelHyperRed, rgb(198, 40, 40))', value: '-',  legend: 'Hyper-Red' },
          { icon: 'fixture', fill: 'var(--channelFarRed, rgb(172, 42, 24))', value: '-', legend: 'Far-Red' },
          { icon: 'fixture', fill: 'var(--channelWarmWhite, rgb(167, 168, 170))', value: '-', legend: 'White' },
      ];
    }else{
      if (data.ledState.uv == -1){
        colorChannels = [
            { icon: 'fixture', fill: 'var(--channelUV, rgb(106, 27, 154))', value: '?', legend: 'Ultraviolet' },
            { icon: 'fixture', fill: 'var(--channelGreen, rgb(9, 228, 37))', value: '?', legend: 'Green' },
            { icon: 'fixture', fill: 'var(--channelBlue, rgb(97, 175, 241))', value: '?', legend: 'Blue' },
            { icon: 'fixture', fill: 'var(--channelHyperRed, rgb(198, 40, 40))', value: '?', legend: 'Hyper-Red' },
            { icon: 'fixture', fill: 'var(--channelFarRed, rgb(172, 42, 24))', value: '?', legend: 'Far-Red' },
            { icon: 'fixture', fill: 'var(--channelWarmWhite, rgb(167, 168, 170))', value: '?', legend: 'White' },
        ];
      }else{
        const ledState = {};
        if( data.fInfo != null && data.fInfo != undefined){
          ledState.uv = getIrradianceToRead(data.ledState.uv, this.props.settings.irradianceUnit, data.fInfo.uvMax);
          ledState.blue = getIrradianceToRead(data.ledState.blue, this.props.settings.irradianceUnit, data.fInfo.blueMax);
          ledState.green = getIrradianceToRead(data.ledState.green, this.props.settings.irradianceUnit, data.fInfo.greenMax);
          ledState.hyperRed = getIrradianceToRead(data.ledState.hyperRed, this.props.settings.irradianceUnit, data.fInfo.hyperRedMax);
          ledState.farRed = getIrradianceToRead(data.ledState.farRed, this.props.settings.irradianceUnit, data.fInfo.farRedMax);
          ledState.white = getIrradianceToRead(data.ledState.white, this.props.settings.irradianceUnit, data.fInfo.whiteMax);
        }else{
          log.error(`Group ${this.props.object.name} does not have max irradiance values! Using default..`);
          ledState.uv = getIrradianceToRead(data.ledState.uv, this.props.settings.irradianceUnit, 50);
          ledState.blue = getIrradianceToRead(data.ledState.blue, this.props.settings.irradianceUnit, 250);
          ledState.green = getIrradianceToRead(data.ledState.green, this.props.settings.irradianceUnit, 100);
          ledState.hyperRed = getIrradianceToRead(data.ledState.hyperRed, this.props.settings.irradianceUnit, 250);
          ledState.farRed = getIrradianceToRead(data.ledState.farRed, this.props.settings.irradianceUnit, 100);
          ledState.white = getIrradianceToRead(data.ledState.white, this.props.settings.irradianceUnit, 250);
        }
        colorChannels = [
            { icon: 'fixture', fill: 'var(--channelUV, rgb(106, 27, 154))', value: ledState.uv.toFixed(0), legend: 'Ultraviolet' },
            { icon: 'fixture', fill: 'var(--channelGreen, rgb(9, 228, 37))', value: ledState.green.toFixed(0), legend: 'Green' },
            { icon: 'fixture', fill: 'var(--channelBlue, rgb(97, 175, 241))', value: ledState.blue.toFixed(0), legend: 'Blue' },
            { icon: 'fixture', fill: 'var(--channelHyperRed, rgb(198, 40, 40))', value: ledState.hyperRed.toFixed(0), legend: 'Hyper-Red' },
            { icon: 'fixture', fill: 'var(--channelFarRed, rgb(172, 42, 24))', value: ledState.farRed.toFixed(0), legend: 'Far-Red' },
            { icon: 'fixture', fill: 'var(--channelWarmWhite, rgb(167, 168, 170))', value: ledState.white.toFixed(0), legend: 'White' },
          ];
      }
    }


    return (
        <CardValues title={"Color Channel Irradiance ("+u+")"} values={colorChannels} />
    )
  }

  getMaxIrradCard(data){
    let maxIrradiance = [];
    let u = irradianceUnitOptions['mol'];

    if( data.fInfo == null || data.fInfo == undefined){
      maxIrradiance = [
          { icon: 'fixture', fill: 'var(--channelUV, rgb(106, 27, 154))', value: '-', legend: 'Ultraviolet' },
          { icon: 'fixture', fill: 'var(--channelGreen, rgb(9, 228, 37))', value: '-', legend: 'Green' },
          { icon: 'fixture', fill: 'var(--channelBlue, rgb(97, 175, 241))', value: '-', legend: 'Blue' },
          { icon: 'fixture', fill: 'var(--channelHyperRed, rgb(198, 40, 40))', value: '-',  legend: 'Hyper-Red' },
          { icon: 'fixture', fill: 'var(--channelFarRed, rgb(172, 42, 24))', value: '-',  legend: 'Far-Red' },
          { icon: 'fixture', fill: 'var(--channelWarmWhite, rgb(167, 168, 170))', value: '-', legend: 'White' },
      ];
    }else{
      maxIrradiance = [
          { icon: 'fixture', fill: 'var(--channelUV, rgb(106, 27, 154))', value: Math.round(data.fInfo.uvMax), legend: 'Ultraviolet' },
          { icon: 'fixture', fill: 'var(--channelGreen, rgb(9, 228, 37))', value: Math.round(data.fInfo.greenMax), legend: 'Green' },
          { icon: 'fixture', fill: 'var(--channelBlue, rgb(97, 175, 241))', value: Math.round(data.fInfo.blueMax), legend: 'Blue' },
          { icon: 'fixture', fill: 'var(--channelHyperRed, rgb(198, 40, 40))', value: Math.round(data.fInfo.hyperRedMax), legend: 'Hyper-Red' },
          { icon: 'fixture', fill: 'var(--channelFarRed, rgb(172, 42, 24))', value: Math.round(data.fInfo.farRedMax), legend: 'Far-Red' },
          { icon: 'fixture', fill: 'var(--channelWarmWhite, rgb(167, 168, 170))', value: Math.round(data.fInfo.whiteMax), legend: 'White' },
      ];
    }

    return (
        <CardValues title={"Maximum Irradiance ("+u+") for the Layout"} values={maxIrradiance} />
    )
  }

  render() {
    let { t } = this.props;

    /* Card Action Buttons */

    const actionButtons = [
        { icon: 'add', onClick: (ids) => { this.props.schedulesActions.addScheduleToCanopy(this.props.objects) }, name: "Add Schedule" },
    ];

    const tableAttributes = {
      start_date: {name: t('start_date'), configs: [], transform: (row) => `${this.formatDate(row.start_date)}`},
      end_date: {name: t('end_date'), configs: [], transform: (row) => `${this.formatDate(row.end_date)}`},
      '_recipe.name': {name: "Day Recipe", configs: [], transform: (row) => `${row._recipe.name}` }
    }

    let actionButtonsWhenSelected = [{ icon: 'delete', onClick: () => { this.props.schedulesActions.deleteMultipleFromCanopy() }, tooltip: "Delete selected Schedules" }] //canopiesActions.deleteMultipleSchedules

    const dropdownOptions = [
        { value: "past", name: "Past" },
        { value: "ongoing", name: "Ongoing" },
    ];

    /* Table Content */

    const header = [
        <TableHeaderCell selectable>
            <FormGroup type="checkbox" value="selectAll" />
        </TableHeaderCell>,
        <TableHeaderCell orderDesc>Start Date</TableHeaderCell>,
        <TableHeaderCell>End Date</TableHeaderCell>,
        <TableHeaderCell>Recipe</TableHeaderCell>,
    ];

    const rowOptions = [
      //{optionMethod: this.props.schedulesActions.edit, iconName: 'edit'},
      //{optionMethod: this.props.schedulesActions.duplicate, iconName: 'duplicate'},
      {optionMethod: this.props.schedulesActions.deleteObjFromCanopy, iconName: 'delete', tooltip: "Delete Schedule"}
    ];

    return (
        <div>
          <TopbarContainer tabList={[
              { name: 'Back', location: '/', isActive: false },
          ]}/>

            <Content details>
                {(this.props.object != null) && this.getMap(this.props.object, this.props.irradMap)}

                {this.props.object != null && this.getIrradCard(this.props.object)}
                {this.props.object != null && this.getMaxIrradCard(this.props.object)}

                {this.props.objects != null &&
                  <ListContainer header={true} title={t('schedules')} options={[]} actions={{...this.props.schedulesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
                    actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions}/>
                }
            </Content>
        </div>
      )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.schedules.sortAttribute ? state.schedules.sortAttribute : null,
    sortType: state.schedules.sortType ? state.schedules.sortType : 'orderAsc',
    objects: state.schedules.objects,
    count: state.schedules.count,
    from: state.schedules.from,
    to: state.schedules.to,
    selected: state.schedules.selected,
    object: state.canopies.object,
    irradMap: state.schedules.irradianceMap,
    settings: state.settings,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    canopiesActions: {...bindActionCreators(CanopiesActions, dispatch)},
    schedulesActions: {...bindActionCreators(SchedulesActions, dispatch)},
    canopiesSetup: {...bindActionCreators(CanopiesSetup, dispatch)},
  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(DetailsContainer));
