// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'


import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import ContentHeader from 'phytofy-design-system/src/design-system/organisms/ContentHeader';
import Icon from 'phytofy-design-system/src/design-system/atoms/Icons';
import CardActionGoTo from 'phytofy-design-system/src/design-system/molecules/CardActionGoTo';
import CardActionNavigation from 'phytofy-design-system/src/design-system/molecules/CardActionNavigation';
import CardActionButtons from 'phytofy-design-system/src/design-system/molecules/CardActionButtons';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainerDataOptions';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';
import GridContainer from './GridContainer';

import Canopy from '../models/Canopy';
import { canopyConfigs, getCanopyConfigsForSelect, getProfileAttrFromId } from '../models/Profile';

import { getPageSize } from '../models/Settings';
import * as SidebarActions from '../actions/sidebar';
import * as CanopiesActions from '../actions/canopiesSetup';
import * as ProfilesActions from '../actions/profiles';


const CanopyContainer = class extends Component {
  constructor(props) {
    super(props);
    this.state = { objects: [], view: 'grid', goto: props.from, search: ''};
    this.handleGotoKeyPress = this.handleGotoKeyPress.bind(this);
    this.handleGotoChange = this.handleGotoChange.bind(this);
  }

  componentDidUpdate(prevProps){
      if(this.props.from != prevProps.from){
          this.setState({goto: this.props.from})
      }
  }

  handleGotoKeyPress(event) {
      if (event.key === 'Enter') {
          let page = parseInt(event.target.value);
          if(!isNaN(page)) this.props.canopiesActions.goto(parseInt(event.target.value));
      }
  }

  handleGotoChange(event) {
      this.setState({goto: event.target.value});
  }

  componentDidMount() {
    this.props.canopiesActions.getObjects();
    this.props.profilesActions.getObjectsAll();
  }

  render() {
    const tableAttributes = {
      name: {name: "Name", configs: []},
      '_profile.name': {name: "Layout", configs: [], transform: (row) => `${row._profile.name} (${canopyConfigs[row._profile.config]})` },
      scheduleState: {name: "State", configs: ['status', 'nosort'], transform: (row) => {
        if(row.scheduleState){
          return <Icon fill="#009D3D" icon="play" tooltip="Schedules running" />
        } else {
          return <Icon fill="#FF6600" icon="pause" tooltip="Schedules paused" />
        }
      }}
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.canopiesActions.create() }, name: 'Add Group' },
    ];

    let actionButtonsWhenSelected = [
      { icon: 'play', onClick: () => { this.props.canopiesActions.resumeScheduleOnMultiple() }, tooltip: 'Resume selected Schedule' },
      { icon: 'pause', onClick: () => { this.props.canopiesActions.stopScheduleOnMultiple() }, tooltip: 'Pause selected Schedule' },
      { icon: 'delete', onClick: () => { this.props.canopiesActions.deleteMultiple() }, tooltip: 'Delete selected Groups' },
    ];

    if (this.props.selected.length > 0) {
        actionButtons = [ ...actionButtonsWhenSelected, ...actionButtons ];
    }

    const getProfilesForSelect = (profiles) => {
      return profiles.map((profile) => {
          return {name: `${profile.name} (${canopyConfigs[profile.config]})`, value: profile._id}
      })
    };

    let profileFilter = [
      {value: -1, name: 'All'},
      ...getProfilesForSelect(this.props.profiles)
    ];

    const filters = [
      {label: 'Layout', options: profileFilter, attribute: 'profileId'}
    ]

    console.log()

    const dataOptions = [
      {optionMethod: this.props.canopiesActions.resumeScheduleOnCanopy, iconName: 'play', tooltip: "Play"},
      {optionMethod: this.props.canopiesActions.stopScheduleOnCanopy, iconName: 'pause', tooltip: "Pause"},
      {optionMethod: this.props.canopiesActions.realtimeControl, iconName: 'recipes', tooltip: "To real-time control"},
      {optionMethod: this.props.canopiesActions.edit, iconName: 'edit', tooltip: "Edit Group"},
      //{optionMethod: this.props.canopiesActions.duplicate, iconName: 'duplicate', tooltip: "Duplicate Group"},
      {optionMethod: this.props.canopiesActions.deleteObj, iconName: 'delete', tooltip: "Delete Group"}
    ];

    return (
      <div>
        <TopbarContainer />
        <Content>
        <ContentHeader>
            <div>
                <CardActionGoTo goto={this.state.goto} onKeyPress={this.handleGotoKeyPress} onChange={this.handleGotoChange} />
                <CardActionNavigation from={this.props.from} to={this.props.to} of={this.props.count} pageBack={this.props.canopiesActions.pageBack} pageNext={this.props.canopiesActions.pageNext}/>
                <CardActionButtons values={actionButtons} />
            </div>
            <div>
                <FormGroup
                    type="search"
                    label="Search"
                    placeholder="Search Group"
                    name="Search Fixture"
                    value={this.state.search}
                    error=""
                    onChange={(event) => {this.setState({search: event.target.value}); this.props.canopiesActions.addFilter('name', new RegExp(event.target.value, 'i'))}}
                />
                <FiltersContainer search={(value) => {this.props.canopiesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.canopiesActions.addFilter} removeFilter={this.props.canopiesActions.removeFilter} />
                {/* <FormGroup type="select" label="Layout" options={profileFilter} error="" /> */}
            </div>
        </ContentHeader>
        <ListContainer title="Groups" actions={{...this.props.canopiesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
            actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} dataOptions={dataOptions}/>
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.canopiesSetup.sortAttribute ? state.canopiesSetup.sortAttribute : null,
    sortType: state.canopiesSetup.sortType ? state.canopiesSetup.sortType : 'orderAsc',
    objects: state.canopiesSetup.objects,
    count: state.canopiesSetup.count,
    from: state.canopiesSetup.from,
    to: state.canopiesSetup.to,
    selected: state.canopiesSetup.selected,
    profiles: state.profiles.objectsAll
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    canopiesActions: {...bindActionCreators(CanopiesActions, dispatch)},
    profilesActions: {...bindActionCreators(ProfilesActions, dispatch)}
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CanopyContainer));
