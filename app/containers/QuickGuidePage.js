import React, { Component } from "react";
import Content from "phytofy-design-system/src/design-system/organisms/Content";
import QuickGuide from "phytofy-design-system/src/design-system/organisms/QuickGuide";
import TopbarContainer from './TopbarContainer';

const glossaryList = [
  { key: 1, value: "Fixture – Single light emiting device" },
  { key: 2, value: "Layout – Description of orientation and spacing of the fixtures in a group (used for irradiance calculations)" },
  { key: 3, value: "Group – Set of fixtures to be controlled together on a schedule" },
  { key: 4, value: "Day Recipe – A recipe which defines the spectral distributions of light to be applied during specified periods of a single day" },
  { key: 5, value: "Schedule – Allows to repeat a day recipe over multiple days for a certain group" }
];

const instructionsList = [
  { key: 1, to: "/profiles", value: "Create Layout" },
  { key: 2, to: "/canopies", value: "Create Group" },
  { key: 3, to: "/recipes", value: "Create Day Recipe" },
  { key: 4, to: "/schedules", value: "Create Schedule" }
];

const externalLinks = [
  {
    key: 1,
    url: "https://www.osram.com/pia/horticulture-products.jsp",
    value: "Follow this link to the instruction video"
  },
  {
    key: 2,
    url: "https://www.osram.com/pia/horticulture-products.jsp",
    value: "Follow this link to the user manual"
  }
];

export default class extends Component {
  render() {
    return (
      <div>
        <TopbarContainer />
        <Content>
          <QuickGuide
            glossary={glossaryList}
            instructions={instructionsList}
            links={externalLinks}
          />
        </Content>
      </div>
    );
  }
}
