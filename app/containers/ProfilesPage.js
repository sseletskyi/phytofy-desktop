// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';

import Content from 'phytofy-design-system/src/design-system/organisms/Content';
import ContentHeader from 'phytofy-design-system/src/design-system/organisms/ContentHeader';
import CardActionGoTo from 'phytofy-design-system/src/design-system/molecules/CardActionGoTo';
import CardActionNavigation from 'phytofy-design-system/src/design-system/molecules/CardActionNavigation';
import CardActionButtons from 'phytofy-design-system/src/design-system/molecules/CardActionButtons';
import FormGroup from 'phytofy-design-system/src/design-system/molecules/FormGroup';

import { getTableHeaderCellFromObj } from './helpers';
import ListContainer from './ListContainerRowOptions';
import FiltersContainer from './FiltersContainer';
import TopbarContainer from './TopbarContainer';
import Profile, { canopyConfigs, getCanopyConfigsForSelect } from '../models/Profile';
import { lengthUnitOptions, getLengthToRead } from '../models/Settings'
import * as SidebarActions from '../actions/sidebar';
import * as ProfilesActions from '../actions/profiles';


const ProfileContainer = class extends Component {

  constructor(props) {
    super(props);
    this.state = { objects: [], view: 'grid', goto: props.from, search: ''};
    this.handleGotoKeyPress = this.handleGotoKeyPress.bind(this);
    this.handleGotoChange = this.handleGotoChange.bind(this);

  }

  handleGotoKeyPress(event) {
    if (event.key === 'Enter') {
        let page = parseInt(event.target.value);
        if(!isNaN(page)) this.props.profilesActions.goto(parseInt(event.target.value));
    }
  }

  handleGotoChange(event) {
      this.setState({goto: event.target.value});
  }


  componentDidMount() {
    this.props.profilesActions.getObjects();
  }

  componentDidUpdate(prevProps){
    if(this.props.from != prevProps.from){
        this.setState({goto: this.props.from})
    }
  }

  render() {
    let { t } = this.props;

    const tableAttributes = {
      name: {name: t('name'), configs: []},
      config: {name: t('orientation'), configs: [], transform: (row) => {return canopyConfigs[row.config]}},
      length: {name: t('length'), configs: ['measure'], transform: (row) => {return `${getLengthToRead(row.length, this.props.settings.lengthUnit)}`}},
      width: {name: t('width'), configs: ['measure'], transform: (row) => {return `${getLengthToRead(row.width, this.props.settings.lengthUnit)}`}},
      height: {name: t('height'), configs: ['measure'], transform: (row) => {return `${getLengthToRead(row.height, this.props.settings.lengthUnit)}`}},
      deltaLength: {name: `△ ${t('length')}`, configs: ['measure'], transform: (row) => {return row.deltaLength ? `${getLengthToRead(row.deltaLength, this.props.settings.lengthUnit)}` : '-'}},
      deltaWidth: {name: `△ ${t('width')}`, configs: ['measure'], transform: (row) => {return row.deltaWidth ? `${getLengthToRead(row.deltaWidth, this.props.settings.lengthUnit)}` : '-'}},
      nCanopies: {name: t('groups'), configs: ['measure']},
    }

    let actionButtons = [
      { icon: 'add', onClick: (ids) => { this.props.profilesActions.create() }, name: "Add Layout" },
    ];

    let actionButtonsWhenSelected = [{ icon: 'delete', onClick: () => { this.props.profilesActions.deleteMultiple() }, tooltip: "Delete selected Layouts" }];

    if (this.props.selected.length > 0) {
      actionButtons = [ ...actionButtonsWhenSelected, ...actionButtons ];
    }

    let configFilter = [
      {value: -1, name: 'All'},
      ...getCanopyConfigsForSelect()
    ]

    const filters = [
      {label: t('orientation'), options: configFilter, attribute: 'config'}
    ]

    const rowOptions = [
      {optionMethod: this.props.profilesActions.edit, iconName: 'edit', tooltip: "Edit Layout"},
      {optionMethod: this.props.profilesActions.duplicate, iconName: 'duplicate', tooltip: "Duplicate Layout"},
      {optionMethod: this.props.profilesActions.deleteObj, iconName: 'delete', tooltip: "Delete Layout"}
    ];

    return (
      <div>
        <TopbarContainer />
        <Content>
          <ContentHeader>
              <div>
                  <CardActionGoTo goto={this.state.goto} onKeyPress={this.handleGotoKeyPress} onChange={this.handleGotoChange} />
                  <CardActionNavigation from={this.props.from} to={this.props.to} of={this.props.count} pageBack={this.props.profilesActions.pageBack} pageNext={this.props.profilesActions.pageNext}/>
                  <CardActionButtons values={actionButtons} />
              </div>
              <div>
                  <FormGroup
                      type="search"
                      label="Search"
                      placeholder="Search Layout"
                      name="Search Layou"
                      value={this.state.search}
                      error=""
                      onChange={(event) => {this.setState({search: event.target.value}); this.props.profilesActions.addFilter('name', new RegExp(event.target.value, 'i'))}}
                  />
                  <FiltersContainer search={(value) => {this.props.profilesActions.addFilter('name', new RegExp(value, 'i'))}} filters={filters} addFilter={this.props.profilesActions.addFilter} removeFilter={this.props.profilesActions.removeFilter} />
              </div>
          </ContentHeader>
          {this.props.objects != null &&
            <ListContainer title={t('layouts')} actions={{...this.props.profilesActions}} {...this.props} selectable={true} tableAttributes={tableAttributes}
              actionButtons={actionButtons} actionButtonsWhenSelected={actionButtonsWhenSelected} rowOptions={rowOptions} />
          }
        </Content>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isRightSidebarOpen: state.sidebar.isOpen,
    sortAttribute: state.profiles.sortAttribute ? state.profiles.sortAttribute : null,
    sortType: state.profiles.sortType ? state.profiles.sortType : 'orderAsc',
    objects: state.profiles.objects,
    count: state.profiles.count,
    from: state.profiles.from,
    to: state.profiles.to,
    selected: state.profiles.selected,
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    sidebarActions: {...bindActionCreators(SidebarActions, dispatch)},
    profilesActions: {...bindActionCreators(ProfilesActions, dispatch)}
  }
}

export default translate()(connect(mapStateToProps, mapDispatchToProps)(ProfileContainer));
