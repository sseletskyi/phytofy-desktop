import React, { Component } from 'react';
import { withRouter } from 'react-router'


import Topbar from 'phytofy-design-system/src/design-system/organisms/TopbarProd';


const TopbarContainer = class extends Component {

    render() {
        return <Topbar values={this.props.tabList ? this.props.tabList : []} onClick={(location) => this.props.history.replace(location)} />
    }
}

export default withRouter(TopbarContainer);
