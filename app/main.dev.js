/* eslint global-require: 0, flowtype-errors/show-errors: 0 */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `npm run build` or `npm run build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import { app, BrowserWindow } from 'electron';
import MenuBuilder from './menu';

const log = require('electron-log');
const electron = require('electron');
const is = require('electron-is');

import Settings, {getLoggingMode, getSyncTimeFlag, getResumeSchedFlag} from './models/Settings';

let mainWindow = null;

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

if (
  process.env.NODE_ENV === 'development' ||
  process.env.DEBUG_PROD === 'true'
) {
  require('electron-debug')();
  const path = require('path');
  const p = path.join(__dirname, '..', 'app', 'node_modules');
  require('module').globalPaths.push(p);
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

  return Promise.all(
    extensions.map(name => installer.default(installer[name], forceDownload))
  ).catch(console.log);
};

/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});


app.on('ready', async () => {
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.DEBUG_PROD === 'true'
  ) {
    await installExtensions();
  }

  mainWindow = new BrowserWindow({
    show: false,
    //width: 1280,
    //height: 800
  });

  mainWindow.setMenu(null);

  mainWindow.maximize();
  //mainWindow.show();

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  // @TODO: Use 'ready-to-show' event
  //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
  mainWindow.webContents.on('did-finish-load', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    mainWindow.showInactive();
    mainWindow.setMenu(null);
    mainWindow.maximize();
    // mainWindow.focus();
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();

  //config log path for windows
  if(is.windows()){
    let logsPathWin = (electron.app || electron.remote.app).getPath('userData');
    log.transports.file.file = logsPathWin + "\\logs\\log.log";
    //log.warn(`New log directory in windows!`);
  }

  let loggingMode = await getLoggingMode();
  console.log(`LOGGING MODE SET TO: ${loggingMode}`);
  log.warn(`Debug logging mode is set to: ${loggingMode}.`);
  if(loggingMode=='debug'){
    log.transports.file.level = 'info';
  }

  const id = electron.powerSaveBlocker.start('prevent-app-suspension');

});
