/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import CanopiesSetupPage from './containers/CanopiesSetupPage';
import DevicesPage from './containers/DevicesPage';
import FixturesPage from './containers/FixturesPage';
import ProfilesPage from './containers/ProfilesPage';
import RecipesPage from './containers/RecipesPage';
import SchedulesPage from './containers/SchedulesPage';
import SettingsPage from './containers/SettingsPage';
import CanopyDetailsPage from './containers/CanopyDetailsPage';
import ProductInfoPage from './containers/ProductInfoPage';
import CreditsPage from './containers/CreditsPage';
import TermsPage from './containers/TermsPage';
import QuickGuidePage from './containers/QuickGuidePage';

export default (props) => (
  <App history={props.history}>
    <Switch>
      <Route path="/setup/devices" component={DevicesPage} />
      <Route path="/setup/fixtures" component={FixturesPage} />
      <Route path="/profiles" component={ProfilesPage} />
      <Route path="/recipes" component={RecipesPage} />
      <Route path="/schedules" component={SchedulesPage} />
      <Route path="/setup/settings" component={SettingsPage} />
      <Route path="/canopies" component={CanopiesSetupPage} />
      <Route path="/details/:id" component={CanopyDetailsPage} />
      <Route path="/about/product" component={ProductInfoPage} />
      <Route path="/about/credits" component={CreditsPage} />
      <Route path="/about/terms" component={TermsPage} />
      <Route path="/guide" component={QuickGuidePage} />
      <Route path="/" component={HomePage} />
    </Switch>
  </App>
);
