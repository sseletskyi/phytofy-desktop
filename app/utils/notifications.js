const notificationsDefaultColors = {
    error: '#E53012',
    success: '#009D3D',
    warning: '#FF6600',
    bg: '#FFFFFF',
    bgDismiss: '#ECEDED',
    icnDismiss: '#FFFFFF',
    textColor: '#424242',
    grey: '#C5C6C8'
}

export const getNotificationsStyle = () => {return {
    NotificationItem: {
      DefaultStyle: {
        backgroundColor: notificationsDefaultColors.bg,
        fontSize: '14px',
        padding: '20px'
      },
      success: {
        borderTop: `2px solid ${notificationsDefaultColors.success}`,
        backgroundColor: notificationsDefaultColors.bg,
        color: notificationsDefaultColors.textColor,
        boxShadow: '0 0 6px rgba(0, 0, 0, .1)',
      },

      warning: {
        borderTop: `2px solid ${notificationsDefaultColors.warning}`,
        backgroundColor: notificationsDefaultColors.bg,
        color: notificationsDefaultColors.textColor,
        boxShadow: '0 0 6px rgba(0, 0, 0, .1)',
      },

      error: {
        borderTop: `2px solid ${notificationsDefaultColors.error}`,
        backgroundColor: notificationsDefaultColors.bg,
        color: notificationsDefaultColors.textColor,
        boxShadow: '0 0 6px rgba(0, 0, 0, .1)',
      }
    },
    Title: {
      DefaultStyle: {
      },
      success: {
        color: notificationsDefaultColors.success,
      },
      warning: {
        color: notificationsDefaultColors.warning,
      },
      error: {
        color: notificationsDefaultColors.error,
      }
    },
    Dismiss: {
      DefaultStyle: {
        top: '10px',
        right: '10px'
      },
      success: {
        color: notificationsDefaultColors.icnDismiss,
        backgroundColor: notificationsDefaultColors.bgDismiss,
      },
      warning: {
        color: notificationsDefaultColors.icnDismiss,
        backgroundColor: notificationsDefaultColors.bgDismiss,
      },
      error: {
        color: notificationsDefaultColors.icnDismiss,
        backgroundColor: notificationsDefaultColors.bgDismiss,
      }
    },
    Action: {
      DefaultStyle: {
        background: notificationsDefaultColors.grey,
        margin: '15px 0 0 0',
        borderRadius: '4px'
      },
      success: {
        color: '#FFFFFF',
        backgroundColor: notificationsDefaultColors.success,
      },
      warning: {
        color: '#FFFFFF',
        backgroundColor: notificationsDefaultColors.warning,
      },
      error: {
        color: '#FFFFFF',
        backgroundColor: notificationsDefaultColors.error,
      }
    }
}}