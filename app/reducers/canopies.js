import { listReducer } from './helpers';

const defaultState = { from: 1, objects: [], objectsAll: [], sortAttribute: 'name', sortType: 'orderAsc', hasPrev: false, hasNext: false, selected: [], filters: {}, profiles: [] };

export default function canopies(state = defaultState, action) {
    let newState = listReducer('CANOPIES', state, action);

    if(newState) return newState;

    switch (action.type) {
      case "CANOPIES_GET_ONE":
        return {
            ...state,
            object: action.object
        }
      case "CANOPIES_RESET_OBJ":
        return{
          ...state,
          object: null
        }
      default:
        return state;
    }

}
