// @flow
export default function sidebar(state = {isOpen: false, type: 'normal', prevState: {}}, action) {
  switch (action.type) {
    case "OPEN_SIDEBAR":
      return {...state, type: action.sidebarType, isOpen: true, component: action.component, label: action.label, title: action.title, prevState: action.prevState};
    case "CLOSE_SIDEBAR":
      return {...state, isOpen: false};
    default:
      return state;
  }
}
