import { listReducer } from './helpers';

const defaultState = { from: 1, objects: [], objectsAll: [], sortAttribute: 'name', sortType: 'orderAsc', hasPrev: false, hasNext: false, selected: [], filters: {} };

export default function recipes(state = defaultState, action) {
    let newState = listReducer('RECIPES', state, action);

    if(newState) return newState;

    switch (action.type) {
        case "RECIPES_CREATE":
            return {
                ...state,
                recipeObj: action.recipeObj,
                validation: action.validation
            }
        case "RECIPES_SUBMIT_CREATE":
            return {
                ...state,
                recipeObj: action.recipeObj,
                validation: action.validation,
                selected: []
            }
        case "RECIPES_EDIT":
            return {
                ...state,
                recipeObj: action.recipeObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        case "RECIPES_SUBMIT_EDIT":
            return {
                ...state,
                recipeObj: action.recipeObj,
                editObjId: action.editObjId,
                validation: action.validation,
                selected: []
            }
        case "RECIPES_CANCEL_FORM":
            return {
                ...state,
                recipeObj: action.recipeObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        default:
            return state;
    }
}
