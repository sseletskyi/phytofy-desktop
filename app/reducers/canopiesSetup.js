import { listReducer } from './helpers';

const defaultState = { from: 1, objects: [], objectsAll: [], sortAttribute: 'name', sortType: 'orderAsc', hasPrev: false, hasNext: false, selected: [], filters: {}, profiles: [] };

export default function canopiesSetup(state = defaultState, action) {
    let newState = listReducer('CANOPIES_SETUP', state, action);

    if(newState) return newState;

    switch (action.type) {
        case "CANOPIES_GET_ONE":
          return {
              ...state,
              object: action.object
            }
        case "CANOPIES_SETUP_CREATE":
            return {
                ...state,
                canopyObj: action.canopyObj,
                validation: action.validation
            }
        case "CANOPIES_SETUP_SUBMIT_CREATE":
            return {
                ...state,
                canopyObj: action.canopyObj,
                validation: action.validation,
                selected: []
            }
        case "CANOPIES_SETUP_EDIT":
            return {
                ...state,
                canopyObj: action.canopyObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        case "CANOPIES_SETUP_SUBMIT_EDIT":
            return {
                ...state,
                canopyObj: action.canopyObj,
                editObjId: action.editObjId,
                validation: action.validation,
                selected: []
            }
        case "CANOPIES_SETUP_CANCEL_FORM":
            return {
                ...state,
                canopyObj: action.canopyObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        case "CANOPIES_SETUP_REALTIME":
            return {
                ...state,
                canopyObj: action.canopyObj,
                fixtureObj: action.fixtureObj,
                fixturesLedState: action.fixturesLedState,
                fixtureInfo: action.fInfo,
                realTimeMode: action.realTimeMode
            }
        case "CANOPIES_ADD_FIXTURE":
            return {
                ...state,
                canopyObj: action.canopyObj,
                fixturesSearch: action.fixturesSearch
            }
        case "CANOPIES_GET_FIXTURES":
            return {
                ...state,
                fixturesSearch: action.fixturesSearch
            }
        case "CANOPIES_SUBMIT_ADD_FIXTURE":
            return {
                ...state,
                canopyObj: action.canopyObj
            }
        default:
            return state;
    }
}
