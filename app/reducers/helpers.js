export const listReducer = (baseName, state, action) => {
    switch (action.type) {
        case `${baseName}_GET`:
            return {
                ...state,
                objects: action.objects,
                count: action.count,
                from: action.from,
                to: action.to,
                hasNext: action.hasNext,
                hasPrev: action.hasPrev,
                selected: []
        }
        case `${baseName}_GET_ALL`:
            return {
                ...state,
                objectsAll: action.objects,
        }
        case `${baseName}_SORT`:
            return {
                ...state, 
                sortAttribute: action.sortAttribute,
                sortType: action.sortType, 
                selected: []
            }; 
        case `${baseName}_PAGE_BACK`:
            return {
                ...state,
                count: action.count,
                from: action.from,
                to: action.to,
                hasNext: action.hasNext,
                hasPrev: action.hasPrev,
                selected: []
            }
        case `${baseName}_PAGE_NEXT`:
            return {
                ...state,
                count: action.count,
                from: action.from,
                to: action.to,
                hasNext: action.hasNext,
                hasPrev: action.hasPrev,
                selected: []
            }
        case `${baseName}_PAGE_GOTO`:
            return {
                ...state,
                count: action.count,
                from: action.from,
                to: action.to,
                hasNext: action.hasNext,
                hasPrev: action.hasPrev,
                selected: []
            }
        case `${baseName}_ADD_FILTER`:
            return {
                ...state,
                filters: action.filters,
                from: action.from
            }
        case `${baseName}_REMOVE_FILTER`:
            return {
                ...state,
                filters: action.filters,
                from: action.from
            }
        case `${baseName}_SELECT`:
            return {
                ...state,
                selected: action.selected
            }
        case `${baseName}_DESELECT`:
            return {
                ...state,
                selected: action.selected
            }
        case `${baseName}_SELECT_ALL`:
            return {
                ...state,
                selected: action.selected
            }
        case `${baseName}_DESELECT_ALL`:
            return {
                ...state,
                selected: action.selected
            }
        default:
            return null;
    }
}