import { listReducer } from './helpers';

const defaultState = { from: 1, scheduleObj: {'start_date': '', 'end_date': ''}, objects: [], objectsAll: [], canopiesSearch: [], sortAttribute: 'name', sortType: 'orderAsc', hasPrev: false, hasNext: false, selected: [], filters: {} };

export default function schedules(state = defaultState, action) {
    let newState = listReducer('SCHEDULES', state, action);

    if(newState) return newState;

    switch (action.type) {
        case "SCHEDULES_GET_FROM_CANOPY":
          return {
              ...state,
              objects: action.objects,
              channelArr: action.channelArr,
              count: action.count,
              from: action.from,
              to: action.to,
              hasNext: action.hasNext,
              hasPrev: action.hasPrev,
              selected: []
          }
        case 'SCHEDULES_ADD_TO_CANOPY':
          return {
              ...state,
              availableScheds: action.availableScheds,
          }
        case 'SCHEDULES_GET_IRRADIANCE_MAP':
          return {
              ...state,
              irradianceMap: action.map
          }
        case 'SCHEDULES_RESET_IRRAD_MAP':
          return {
            ...state,
            irradianceMap: action.irradMap
          }
        case "SCHEDULES_CREATE":
            return {
                ...state,
                scheduleObj: action.scheduleObj,
                validation: action.validation,
                recipes: action.recipes,
                canopies: action.canopies
            }
        case "SCHEDULES_SUBMIT_CREATE":
            return {
                ...state,
                scheduleObj: action.scheduleObj,
                validation: action.validation,
                selected: []
            }
        case "SCHEDULES_EDIT":
            return {
                ...state,
                scheduleObj: action.scheduleObj,
                editObjId: action.editObjId,
                validation: action.validation,
                recipes: action.recipes,
                canopies: action.canopies
            }
        case "SCHEDULES_SUBMIT_EDIT":
            return {
                ...state,
                scheduleObj: action.scheduleObj,
                editObjId: action.editObjId,
                validation: action.validation,
                selected: []
            }
        case "SCHEDULES_CANCEL_FORM":
            return {
                ...state,
                scheduleObj: action.scheduleObj,
                editObjId: action.editObjId,
                validation: action.validation
            }
        case "SCHEDULES_ADD_CANOPY":
            return {
                ...state,
                scheduleObj: action.scheduleObj,
                canopiesSearch: action.canopiesSearch
            }
        case "SCHEDULES_GET_CANOPIES":
            return {
                ...state,
                canopiesSearch: action.canopiesSearch
            }
        case "SCHEDULES_SUBMIT_ADD_TO_CANOPY":
            return {
                ...state,
                objects: [...state.objects, action.schedule]
            }
        default:
            return state;
    }
}
