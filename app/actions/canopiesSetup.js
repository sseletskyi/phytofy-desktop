// @flow
const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')
import path from 'path';
import validator from 'validator';
import Notifications from 'react-notification-system-redux';
import moment from 'moment';

import Profile from '../models/Profile';
import Canopy, { CanopyFormValidator, getFixturesFromCanopy} from '../models/Canopy';
import Fixture from '../models/Fixture';
import Schedule, { addScheduleToFixtures, removeScheduleFromFixtures } from '../models/Schedule';

import { getPageSize, getLengthConvertedToInches } from '../models/Settings';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
  makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext,
  makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction,
  makeGetObjectsAllAction, makePageGoTo, runCommandOnFixtures, runMultipleCommands, delay } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';

import { irradianceMap, computeEnviron } from 'phytofy-utils/lib/irradianceUtils'

const defaultCanopy = {name: ""};

const queryCanopies = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let canopies;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();

  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1;
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    canopies = await Canopy.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return canopies;
}

const queryCountCanopies = async(filters = {}) => {
  try {
    return Canopy.ccount(filters).exec();
  } catch(err){
    console.error(err);
    return 0;
  }

}

export const getObjects = () => {
  return async (dispatch, getState) => {
    let { sortAttribute, sortType, from, filters } = getState().canopiesSetup;
    if (from == 0) from = 1;
    let objects = await queryCanopies(sortAttribute, sortType, from, filters);
    for(let i=0; i<objects.length; i++){
        let canopy = objects[i];
        let fixtures = await getFixturesFromCanopy(canopy);

        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          objects[i].scheduleState = (await getState().devices.comm.getScheduleState(fixture.serial_num)).state;
        });
    }

    let count = await countQuery(filters, Canopy);
    dispatch({
        type: "CANOPIES_SETUP_GET",
        objects,
        count,
        ...await getPaginationInfo(from, count)
    })
  }
}


export const getObjectsAll = makeGetObjectsAllAction('CANOPIES_SETUP_GET_ALL', 'canopiesSetup', Canopy, queryCanopies);
export const sort = makeSortAction('CANOPIES_SETUP_SORT', 'canopiesSetup', getObjects);
export const addFilter = makeAddFilterAction('CANOPIES_SETUP_ADD_FILTER', 'canopiesSetup', getObjects);
export const removeFilter = makeRemoveFilterAction('CANOPIES_SETUP_REMOVE_FILTER', 'canopiesSetup', getObjects);
export const pageBack = makePageBack('CANOPIES_SETUP_PAGE_BACK', 'canopiesSetup', getObjects, Canopy);
export const pageNext = makePageNext('CANOPIES_SETUP_PAGE_NEXT', 'canopiesSetup', getObjects, Canopy);
export const goto = makePageGoTo('CANOPIES_SETUP_PAGE_GOTO', 'canopiesSetup', getObjects, Canopy);
export const select = makeSelectObject('CANOPIES_SETUP_SELECT', 'canopiesSetup');
export const deselect = makeDeselectObject('CANOPIES_SETUP_DESELECT', 'canopiesSetup');
export const selectAll = makeSelectAll('CANOPIES_SETUP_SELECT_ALL', 'canopiesSetup');
export const deselectAll = makeDeselectAll('CANOPIES_SETUP_DESELECT_ALL');

export const addFixture = (obj) => {
  return async (dispatch, getState) => {
      await dispatch({
          type: 'CANOPIES_ADD_FIXTURE',
          fixturesSearch: [],
          canopyObj: obj,
      })

      dispatch(openSidebar('ADD_FIXTURE_TO_CANOPY_FORM'));
  }
}

export const getFixtures = (search) => {
  return async (dispatch, getState) => {
      let canopies = await Canopy.cfind({}).exec();
      let usedFixtures = [];
      canopies.forEach((canopy) => {
        usedFixtures = usedFixtures.concat(canopy.fixtures);
      });


      let fixtures = await Fixture.cfind({serial_num: {$nin: usedFixtures}, name: new RegExp(search, 'i')}).sort({'serial_num': 1}).exec();

      await dispatch({
          type: 'CANOPIES_GET_FIXTURES',
          fixturesSearch: fixtures
      })
  }
}

export const getAllUnusedFixtures = () => {
  return async (dispatch, getState) => {
      console.log(`LOOK AT MEEEEE`);
      let canopies = await Canopy.cfind({}).exec();
      let usedFixtures = [];
      canopies.forEach((canopy) => {
        usedFixtures = usedFixtures.concat(canopy.fixtures);
      });


      let fixtures = await Fixture.cfind({serial_num: {$nin: usedFixtures}}).exec();

      await dispatch({
          type: 'CANOPIES_GET_FIXTURES',
          fixturesSearch: fixtures
      })
  }
}

export const submitAddFixture = (fixture) => {
  return async (dispatch, getState) => {
      let canopyObj = getState().canopiesSetup.canopyObj;
      canopyObj.fixtures.push(fixture);


      await dispatch({
          type: 'CANOPIES_EDIT_FIXTURE',
          canopyObj
      })

      let prevState = getState().sidebar.prevState;

      dispatch(openSidebar(prevState.component, prevState.label, prevState.title));
  }
}

export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let canopyObj = obj ? obj : {...defaultCanopy};
    let profiles = []
    try {
        profiles = await Profile.cfind({}).sort({'name': 1}).exec();
    } catch(err){
        console.error(err);
    }

    if(profiles.length == 0){
      dispatch(Notifications.error({
        title: `No layouts found!`,
        message: `Please create a layout before trying to add a new group.`,
        autoDismiss: 10
      }));
      return;
    }

    if(!canopyObj.profileId){
      canopyObj.profileId = profiles[0]._id
    }

    await dispatch({
      type: 'CANOPIES_SETUP_CREATE',
      canopyObj: canopyObj,
      validation: {}
    })
    dispatch(openSidebar('CREATE_CANOPY_FORM', 'Create', 'New Group'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    let validations = await CanopyFormValidator.validate(obj);
    let canopy = null;

    if(validations.isValid){
      try {
        await Profile.update({_id: obj.profileId}, {'$inc': {nCanopies: 1}});
        obj._profile = await Profile.findOne({_id: obj.profileId})
        obj.fInfo = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0}
        canopy = await Canopy.insert(obj);
      } catch(err){
        console.error(err);
      }
      obj = {...defaultCanopy};
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Group created',
        message: `Group ${obj.name} created with success.`,
        autoDismiss: 10
      }));
    }
    /*console.log("CANOPYYYYYY");
    if(canopy != null){
      console.log(canopy);
      let ret = await commissionCanopy(canopy);
    }
    dispatch(getObjects())*/

    dispatch(commissionCanopy(canopy))

    dispatch({
      type: 'CANOPIES_SETUP_SUBMIT_CREATE',
      canopyObj: obj,
      validation: validations
    })
  }
}

export const commissionCanopy = (canopy) => {
  return async (dispatch, getState) => {

    //compute environment variabls
    let env = {}, retObj = null;
    let channelArr = ['uv', 'blue', 'green', 'hyperred', 'farred', 'white'];
    let w = getLengthConvertedToInches(canopy._profile.width, getState().settings.lengthUnit);
    let l = getLengthConvertedToInches(canopy._profile.length, getState().settings.lengthUnit);
    let delta = getLengthConvertedToInches(canopy._profile.deltaLength, getState().settings.lengthUnit);
    let height = getLengthConvertedToInches(canopy._profile.height, getState().settings.lengthUnit);
    const relPath = path.join(require('electron').remote.app.getAppPath(),'static');
    const absPath = "/Users/ricardo/Documents/GitStation/phytofy-dashboard/lib/phytofy-utils/src/FixtureMaps"

    retObj = await irradianceMap(l,w, canopy._profile.config, height, channelArr, delta, relPath);
    env = await computeEnviron(canopy._profile.config, retObj.luf, l, w, height);
    const fixtures = await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
    //send FC24(illuminance configuration) and FC9 getInfo to get maximum irradiance
    try {
      let fixturesInfo = [];
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        console.log(env);
        let ack = await getState().devices.comm.resumeScheduling(fixture.serial_num);
        let resp = await getState().devices.comm.setIlluminanceConfiguration(fixture.serial_num, env);
        if(resp[0] == 1){
          log.info(`[CommissionCanopy] Set illuminance configuration success.`);
          let info = await getState().devices.comm.getFixtureInfo(fixture.serial_num);
          console.log(info);
          if( info['blueMax'] == -1 || isNaN(info['blueMax'])){
            info = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0};
            log.error(`[CommissionCanopy] Error getting maximum irradiance from fixture: ${fixture.serial_num}.`);
          }else if( info['blueMax'] == 0){
            log.error(`[CommissionCanopy] fixture ${fixture.serial_num} returned 0 max. irradiance - probably missing factory calibration.`);
            info = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0};
          }
          fixturesInfo.push(info);
        }else{
          log.error(`[CommissionCanopy] Illuminance configuration unacklowedged.`);
        }
      });
      //choose minimum max irradiances for each channel:
      canopy.fInfo = fixturesInfo[0];
      fixturesInfo.forEach((element) => {
        if(element['uvMax'] < canopy.fInfo['uvMax']) canopy.fInfo['uvMax'] = element['uvMax'];
        if(element['blueMax'] < canopy.fInfo['blueMax']) canopy.fInfo['blueMax'] = element['blueMax'];
        if(element['greenMax'] < canopy.fInfo['greenMax']) canopy.fInfo['greenMax'] = element['greenMax'];
        if(element['hyperRedMax'] < canopy.fInfo['hyperRedMax']) canopy.fInfo['hyperRedMax'] = element['hyperRedMax'];
        if(element['farRedMax'] < canopy.fInfo['farRedMax']) canopy.fInfo['farRedMax'] = element['farRedMax'];
        if(element['whiteMax'] < canopy.fInfo['whiteMax']) canopy.fInfo['whiteMax'] = element['whiteMax'];
      });
      console.log(canopy.fInfo);

    } catch(err){
      console.error(err);
    }
    let ret = await Canopy.update({_id: canopy._id}, canopy);
    console.log(ret);

    dispatch(getObjects())
  }

}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;

    dispatch(create(newObj));
  }
}

export const edit = (obj) => {
  return async (dispatch, getState) => {
    let canopyId = obj._id;

    await dispatch({
      type: 'CANOPIES_SETUP_EDIT',
      canopyObj: obj,
      editObjId: canopyId,
      validation: {}
    })

    dispatch(openSidebar('EDIT_CANOPY_FORM', 'Edit', `${obj.name}`));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {
    let original = (await Canopy.cfind({_id: getState().canopiesSetup.editObjId}).exec())[0];
    console.log(getState().canopiesSetup.editObjId, obj, original);
    let scheduleState = obj.scheduleState;
    let validations = await CanopyFormValidator.validate(obj, original);
    if(validations.isValid){
      try {
        let profi = await Profile.findOne({_id: getState().canopiesSetup.canopyObj.profileId});
        if((profi.nCanopies -1) >= 0){
          await Profile.update({_id: getState().canopiesSetup.canopyObj.profileId}, {'$inc': {nCanopies: -1}});
        }else{
          Profile.update({_id: getState().canopiesSetup.canopyObj.profileId}, {'$set': {nCanopies: 0}});
        }
        await Profile.update({_id: obj.profileId}, {'$inc': {nCanopies: 1}});
        obj._profile = await Profile.findOne({_id: obj.profileId})
        await Canopy.update({_id: getState().canopiesSetup.editObjId}, obj);
      } catch(err){
        console.error(err);
      }
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Group edited',
        message: `Group ${obj.name} edited with success.`,
        autoDismiss: 10
      }));

      let schedules = await Schedule.cfind({canopies: obj._id}).exec();

      for(let i=0; i<obj.fixtures.length; i++) {
        let fixtureId = obj.fixtures[i];
        if(original.fixtures.includes(fixtureId)) {
          continue;
        }

        let fixture = await Fixture.findOne({serial_num: fixtureId});

        if(!fixture) continue;

        for(let j=0; j<schedules.length; j++){
          let schedule = schedules[j];
          await addScheduleToFixtures(schedule, [fixture], getState().devices.comm, dispatch);
          if(scheduleState){
            await runCommandOnFixtures([fixture], getState().devices.comm, dispatch, async (fixture) => {
              let ack = await getState().devices.comm.resumeScheduling(fixture.serial_num);
              if(!ack){
                log.error(`[Edit group] error resuming scheduling for fixture: ${fixture.serial_num}`);
              }
            });
          }else{
            await runCommandOnFixtures([fixture], getState().devices.comm, dispatch, async (fixture) => {
              let ack = await getState().devices.comm.stopScheduling(fixture.serial_num);
              if(!ack){
                log.error(`[Edit group] error stopping scheduling for fixture: ${fixture.serial_num}`);
              }
            });
          }
        }

      }

      for(let i=0; i<original.fixtures.length; i++) {
        let fixtureId = original.fixtures[i];

        if(obj.fixtures.includes(fixtureId)) {
          continue;
        }

        let fixture = await Fixture.findOne({serial_num: fixtureId});
        console.log(fixture);
        if(!fixture) continue;

        for(let j=0; j<schedules.length; j++){
          let schedule = schedules[j];
          await removeScheduleFromFixtures(schedule, [fixture], getState().devices.comm, dispatch);
        }

      }
    }

    //dispatch(getObjects())
    dispatch(commissionCanopy(obj))

    dispatch({
      type: 'CANOPIES_SETUP_SUBMIT_EDIT',
      canopyObj: obj,
      editObjId: validations.isValid ? null : getState().canopiesSetup.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultCanopy};
    dispatch({
      type: 'CANCEL_CANOPY_FORM',
      canopyObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      dispatch(Notifications.success({
        title: `Deleting ${obj.name}`,
        message: `Communicating with fixtures please wait...`,
        autoDismiss: 10
      }));
      let schedules = await Schedule.cfind({canopies: obj._id}).exec();
      let fixtures = await getFixturesFromCanopy(obj);
      for(let j=0; j<schedules.length; j++){
        let schedule = schedules[j];
        await removeScheduleFromFixtures(schedule, fixtures, getState().devices.comm, dispatch);
      }
      let profi = await Profile.findOne({_id: obj.profileId});
      if((profi.nCanopies -1) >= 0){
        await Profile.update({_id: obj.profileId}, {'$inc': {nCanopies: -1}});
      }else{
        Profile.update({_id: obj.profileId}, {'$set': {nCanopies: 0}});
      }
      await Canopy.remove({_id: obj._id})
    } catch(err){
      console.error(err);
    }

    dispatch(Notifications.success({
      title: `Deleted ${obj.name}`,
      message: `Deleted ${obj.name} with success`,
      autoDismiss: 10
    }));
    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    dispatch(Notifications.warning({
      title: `Delete ${obj.name}`,
      message: `Are you sure you want to delete ${obj.name}?`,
      autoDismiss: 10,
      action: {
        label: 'Yes',
        callback: () => {
          dispatch(confirmDelete(obj))
        }
      }
    }));
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(Notifications.success({
        title: 'Deleting groups',
        message: `Communicating with fixtures please wait...`,
        autoDismiss: 10
      }));
      let canopies = await Canopy.cfind({_id: {$in: getState().canopiesSetup.selected}}).exec()
      let profileIds = {};
      for(var i=0; i<canopies.length; i++){
        let canopy = canopies[i];
        let schedules = await Schedule.cfind({canopies: canopy._id}).exec();
        let fixtures = await getFixturesFromCanopy(canopy);

        for(let j=0; j<schedules.length; j++){
          let schedule = schedules[j];
          await removeScheduleFromFixtures(schedule, fixtures, getState().devices.comm, dispatch);
        }

        if(Object.keys(profileIds).indexOf(canopy.profileId) == -1){
          profileIds[canopy.profileId] = 0;
        }
        profileIds[canopy.profileId] += 1;
      }

      let keyArr = Object.keys(profileIds);
      console.log(keyArr);
      for(let k = 0; k < keyArr.length; k++){
        let key = keyArr[k];
        console.log(`profile: ${key}`);
        let profi = await Profile.findOne({_id: key});
        if((profi.nCanopies -profileIds[key]) >= 0){
          Profile.update({_id: key}, {$inc: {nCanopies: -profileIds[key]}});
        }else{
          Profile.update({_id: key}, {$set: {nCanopies: 0}});
        }
      }

      // Object.keys(profileIds).forEach((key) => {
      //   Profile.update({_id: key}, {$inc: {nCanopies: -profileIds[key]}});
      // });

      await Canopy.remove({_id: {$in: getState().canopiesSetup.selected}}, {multi: true})
    } catch(err){
      console.error(err);
    }

    dispatch(Notifications.success({
      title: 'Groups Deleted',
      message: `Groups deleted with success`,
      autoDismiss: 10
    }));
    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    dispatch(Notifications.warning({
      title: `Delete ${getState().canopiesSetup.selected.length} canopies`,
      message: `Are you sure you want to delete ${getState().canopiesSetup.selected.length} groups?`,
      autoDismiss: 10,
      action: {
        label: 'Yes',
        callback: () => {
          dispatch(confirmDeleteMultiple())
        }
      }
    }));
  }
}

export const realtimeControl = (obj) => {
  return async (dispatch, getState) => {
    let canopyId = obj._id;
    let fixtures = await Fixture.cfind({serial_num: {$in: obj.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
    dispatch(Notifications.warning({
      title: 'Realtime Control',
      message: `Stopping scheduling for group: ${obj.name}.`,
      autoDismiss: 10
    }));
    let comm = getState().devices.comm;
    let fixturesLedState = null;
    if(fixtures.length > 0) {
      await runCommandOnFixtures(fixtures, comm, dispatch, async ({serial_num}) => {
        fixturesLedState = await comm.getLEDstate(serial_num, 'percentage');
        let ret = await comm.stopScheduling(serial_num);
      })
    }else{
      dispatch(Notifications.warning({
        title: 'Realtime Control',
        message: `This group has no fixtures assigned.`,
        autoDismiss: 10
      }));
      return;
    }

    let canopy;
    try {
      canopy = await Canopy.findOne({_id: canopyId});
    } catch(err) {
      console.error(err);
    }
    console.log(canopy.fInfo);

    let go = true;
    let test = {moxa: fixtures[0].moxa_mac, port: fixtures[0].port};
    for(let fix of fixtures){
      if(fix.moxa_mac !=  test.moxa || fix.port != test.port) go = false;
    };

    if(!go){
      log.error(`[real time control] All fixtures on a group must belong to the same moxa/port`);
      dispatch(Notifications.error({
        title: 'Realtime Control',
        message: `All fixtures on a group must belong to the same moxa:port.`,
        autoDismiss: 10
      }));
      return;
    }

    let openConnectionResult = await comm.openConnection(fixtures[0].port, fixtures[0].moxa_mac);
    if (openConnectionResult < 0) {
        log.error(`[real time control] Could not connect to ${fixtures[0].moxa_mac}:${fixtures[0].port}`);
        dispatch(Notifications.error({
          title: 'Realtime Control',
          message: `Could not connect to fixture: ${fixtures[0].serial_num}.`,
          autoDismiss: 10
        }));
    } else {
      let { sortAttribute, sortType, from, filters } = getState().canopiesSetup;
      if (from == 0) from = 1;
      let objects = await queryCanopies(sortAttribute, sortType, from, filters);
      for(let i = 0; i < objects.length; i++){
        if(objects[i]._id == canopyId){
          objects[i].scheduleState = 0;
        }
      }
      //console.log(canopies);
      await dispatch({
        type: 'CANOPIES_SETUP_REALTIME',
        canopyObj: obj,
        fixtureObj: {},
        fixturesLedState: fixturesLedState,
        fInfo: canopy.fInfo,
        realTimeMode: 'canopy'
      });

      let count = await countQuery(filters, Canopy);
      dispatch({
          type: "CANOPIES_SETUP_GET",
          objects,
          count,
          ...await getPaginationInfo(from, count)
      });

      dispatch(openSidebar('REALTIME_CONTROL_FORM', 'Realtime control', `${obj.name}`, 'xl'));
    }
  }
}

export const realtimeControlChange = ({uv, blue, green, hyperRed, farRed, white}) => {
  return async (dispatch, getState) => {
    let serials = getState().canopiesSetup.canopyObj.fixtures;
    let fixtures = await Fixture.cfind({serial_num: {$in: serials}}).sort({moxa_mac: 1, port: 1}).exec();
    //console.log(fixtures);
    let comm = getState().devices.comm;
    /*await runCommandOnFixtures(fixtures, comm, dispatch, async ({serial_num}) => {
      await comm.setLEDrt(serial_num, [uv, blue, green, hyperRed, farRed, white]);
    })*/

    for (let i = 0; i < fixtures.length; i++) {
        let fixture = fixtures[i];
        await comm.setLEDrt(fixture.serial_num, [uv, blue, green, hyperRed, farRed, white]);
        await delay(20);
    }
  }
}

export const realtimeFixture = (obj) => {
  return async (dispatch, getState) => {

    dispatch(Notifications.warning({
      title: 'Realtime Control',
      message: `Stopping scheduling for fixture: ${obj.serial_num}.`,
      autoDismiss: 10
    }));
    let comm = getState().devices.comm;
    let fixturesLedState = null, fInfo =null;
    await runCommandOnFixtures([obj], comm, dispatch, async ({serial_num}) => {
      fixturesLedState = await comm.getLEDstate(serial_num, 'percentage');
      fInfo = await comm.getFixtureInfo(serial_num);
      console.log(fInfo);
      if( fInfo['blueMax'] == -1 || isNaN(fInfo['blueMax'])){
        fInfo = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0};
        log.error(`[Realtime Control] Error getting maximum irradiance from fixture: ${serial_num}.`);
      }else if( fInfo['blueMax'] == 0){
        log.error(`[Realtime Control] fixture ${serial_num} returned 0 max. irradiance - probably missing factory calibration.`);
        fInfo = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0};
      }
      let ret = await comm.stopScheduling(serial_num);
    });

    let openConnectionResult = await comm.openConnection(obj.port, obj.moxa_mac);
    if (openConnectionResult < 0) {
        log.error(`[real time control] Could not connect to ${obj.moxa_mac}:${obj.port}`);
        dispatch(Notifications.error({
          title: 'Realtime Control',
          message: `Could not connect to fixture: ${obj.serial_num}.`,
          autoDismiss: 10
        }));
    } else {
      await dispatch({
        type: 'CANOPIES_SETUP_REALTIME',
        fixtureObj: obj,
        canopyObj: {},
        fixturesLedState: fixturesLedState,
        fInfo: fInfo,
        realTimeMode: 'fixture'
      })

      dispatch(openSidebar('REALTIME_CONTROL_FORM', 'Realtime control fixture', `${obj.serial_num}`, 'xl'));
    }

  }
}

export const realtimeFixtureChange = ({uv, blue, green, hyperRed, farRed, white}) => {
  return async (dispatch, getState) => {
    let fixture = getState().canopiesSetup.fixtureObj
    let comm = getState().devices.comm;
    await comm.setLEDrt(fixture.serial_num, [uv, blue, green, hyperRed, farRed, white]);
    // await runCommandOnFixtures([fixture], comm, dispatch, async ({serial_num}) => {
    //   await comm.setLEDrt(serial_num, [uv, blue, green, hyperRed, farRed, white]);
    // });
  }
}

export const resumeScheduleOnCanopy = (obj) => {
  return async (dispatch, getState) => {
    try {
      let fixtures = await getFixturesFromCanopy(obj);
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
        await getState().devices.comm.resumeScheduling(fixture.serial_num);
      })
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const stopScheduleOnCanopy = (obj) => {
  return async (dispatch, getState) => {
    try {
      let fixtures = await getFixturesFromCanopy(obj);
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        // await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
        await getState().devices.comm.stopScheduling(fixture.serial_num);
      });
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}


export const resumeScheduleOnMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let canopies = await Canopy.cfind({_id: {$in: getState().canopiesSetup.selected}}).exec();
      for(let i=0; i<canopies.length; i++) {
        let canopy = canopies[i];
        let fixtures = await getFixturesFromCanopy(canopy);
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
          await getState().devices.comm.resumeScheduling(fixture.serial_num);
        })
      }
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const stopScheduleOnMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let canopies = await Canopy.cfind({_id: {$in: getState().canopiesSetup.selected}}).exec();
      for(let i=0; i<canopies.length; i++) {
        let canopy = canopies[i];
        let fixtures = await getFixturesFromCanopy(canopy);
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          await getState().devices.comm.stopScheduling(fixture.serial_num);
        })
      }
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}
