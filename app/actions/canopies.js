// @flow
const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')
import validator from 'validator';
import Notifications from 'react-notification-system-redux';
import moment from 'moment';

import Profile from '../models/Profile';
import Canopy, { CanopyFormValidator, getFixturesFromCanopy } from '../models/Canopy';
import { getPageSize, getIrradianceToRead } from '../models/Settings';
import Schedule, { removeScheduleFromCanopy} from '../models/Schedule';
import Recipe from '../models/Recipe';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
  makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext,
  makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo, runCommandOnFixtures } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';

const defaultCanopy = {name: ""};

const queryCanopy = async (canopyId) => {
  let canopy;

  try {
    canopy = await Canopy.findOne({_id: canopyId});
  } catch(err) {
    console.error(err);
    return null;
  }

  return canopy;
}

const queryCanopies = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let canopies;


  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);

  try {
    canopies = await Canopy.cfind(filters).sort({name: 1}).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return canopies;
}

const queryCountCanopies = async(filters = {}) => {
  try {
    return Canopy.ccount(filters).exec();
  } catch(err){
    console.error(err);
    return 0;
  }
}

export const getObject = (canopyId) => {
  return async (dispatch, getState) => {
    let object = await queryCanopy(canopyId);
    if(object != null){
      let fixtures = await getFixturesFromCanopy(object);
      try {
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          object.ledState = await getState().devices.comm.getLEDstate(fixture.serial_num);
          if(object.ledState.uv == -1){
            log.error(`[getLEDstate] Error getting LED state from fixture: ${fixture.serial_num}`);
          }
          object.irradiance = object.ledState.uv+object.ledState.blue+object.ledState.green+object.ledState.farRed+object.ledState.hyperRed+object.ledState.white;

        });
      } catch(err){
        console.error(err);
      }
    }

    dispatch({
        type: "CANOPIES_GET_ONE",
        object
    })
  }
}

export const resetObject = () => {
  return async (dispatch, getState) => {
    dispatch({
        type: "CANOPIES_RESET_OBJ",
    });
  }
};

export const getObjects = () => {
  return async (dispatch, getState) => {
    let { sortAttribute, sortType, from, filters } = getState().canopies;
    let objects = await queryCanopies('name', 1, 1, {});
    for(let i=0; i<objects.length; i++){
        let canopy = objects[i];
        let fixtures = await getFixturesFromCanopy(canopy);

        try {
          await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
            objects[i].ledState = await getState().devices.comm.getLEDstate(fixture.serial_num);
            console.log(objects[i].ledState);
            if(objects[i].ledState.uv == -1){
                objects[i].irradiance = '?';
                log.error(`[getLEDstate] Error getting LED state from fixture: ${fixture.serial_num}`);
            }
            else{
              if(objects[i].fInfo != undefined && objects[i].fInfo != null){
                objects[i].irradiance = getIrradianceToRead(objects[i].ledState.uv, getState().settings.irradianceUnit, objects[i].fInfo['uvMax']);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.blue, getState().settings.irradianceUnit, objects[i].fInfo['blueMax']);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.green, getState().settings.irradianceUnit, objects[i].fInfo['greenMax']);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.farRed, getState().settings.irradianceUnit, objects[i].fInfo['farRedMax']);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.hyperRed, getState().settings.irradianceUnit, objects[i].fInfo['hyperRedMax']);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.white, getState().settings.irradianceUnit, objects[i].fInfo['whiteMax']);
                objects[i].irradiance = objects[i].irradiance.toFixed(0);
              }else{
                log.error(`Group ${objects[i].name} does not have max irradiance values! Using default..`);
                objects[i].irradiance = getIrradianceToRead(objects[i].ledState.uv, getState().settings.irradianceUnit, 50);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.blue, getState().settings.irradianceUnit, 250);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.green, getState().settings.irradianceUnit, 100);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.farRed, getState().settings.irradianceUnit, 250);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.hyperRed, getState().settings.irradianceUnit, 100);
                objects[i].irradiance += getIrradianceToRead(objects[i].ledState.white, getState().settings.irradianceUnit, 250);
                objects[i].irradiance = objects[i].irradiance.toFixed(0);
              }
            }
          });
        } catch(err){
          console.error(err);
        }

        let schedules = await Schedule.cfind({canopies: canopy._id}).exec();
        schedules = schedules.sort(function(a,b) {return moment(a.end_date) < moment(b.end_date) ? 1 : ((moment(b.end_date) < moment(a.end_date)) ? -1 : 0)} )
        if(schedules.length > 0){
          let currDate = moment();
          let currDay = moment(currDate).format("MM-DD-YYYY");

          let tmp = schedules.filter((schedule) => {
            let startDay = moment(schedule.start_date).format("MM-DD-YYYY");
            let endDay = moment(schedule.end_date).format("MM-DD-YYYY");
            return (moment(schedule.start_date).isBefore(currDate) || startDay == currDay) && (moment(schedule.end_date).isAfter(currDate) || endDay == currDay);
          });

          let currSchedule = null;
          let recipe = null;
          if (tmp.length > 0){
            currSchedule = tmp[0];
            recipe = await Recipe.findOne({_id: currSchedule.recipeId});
            objects[i].activeRecipeName = recipe.name;
            objects[i].activePeriod = await getActivePeriod(recipe);
            objects[i].scheduleTime = recipe.photoperiod ? recipe.photoperiod : null;
            console.log(`photoperiod: ${recipe.photoperiod}`);
            objects[i].scheduleTimeUnit = 'hours';
            objects[i].dailyLightIntegral = await getDailyLightIntegral(recipe.periods, objects[i].fInfo);
          }else{
            objects[i].activeRecipeName = 'No active schedule'
            objects[i].activePeriod = 0;
            objects[i].scheduleTime = 0;
            objects[i].scheduleTimeUnit = 'hours';
            objects[i].dailyLightIntegral = 0;
          }
          console.log(objects[i].activePeriod);
        }
    }

    let count = await countQuery(filters, Canopy);
    dispatch({
        type: "CANOPIES_GET",
        objects,
        count,
        ...await getPaginationInfo(from, count)
    });
  }
}

async function getActivePeriod(recipe){
  let periodValues = null;
  let current = new Date().toLocaleTimeString();
  let currentMinutes = parseInt(current.split(':')[0])*60+parseInt(current.split(':')[1]);
  console.log(`current time: ${current} and current minutes: ${currentMinutes}`);

  for(let period of recipe.periods){
    let startMinutes = parseInt(period.start.split(':')[0])*60+parseInt(period.start.split(':')[1]);
    let endMinutes = parseInt(period.end.split(':')[0])*60+parseInt(period.end.split(':')[1]);
    console.log(`start minutes: ${startMinutes} || end minutes: ${endMinutes}`);
    if((startMinutes <= currentMinutes) && (endMinutes >= currentMinutes)){
      periodValues = {'uv': period.uv, 'blue': period.blue, 'green': period.green, 'hyperRed':period.hyperRed, 'farRed':period.farRed, 'white': period.white};
    }
  }
  if(periodValues == null){
    periodValues = {'uv': recipe.periods[0].uv, 'blue': recipe.periods[0].blue, 'green': recipe.periods[0].green, 'hyperRed': recipe.periods[0].hyperRed, 'farRed': recipe.periods[0].farRed, 'white': recipe.periods[0].white};
  }

  return periodValues;
}

async function getActivePeriodOld(recipe){
  let currDate = moment();
  let periodValues = null;

  let current = new Date().toLocaleTimeString();
  let currentHours = Number(current.split(':')[0]);
  for(let period of recipe.periods){
    if((Number(period.start.split(':')[0]) <= currentHours) && (Number(period.end.split(':')[0]) >= currentHours)){
      periodValues = {'uv': period.uv, 'blue': period.blue, 'green': period.green, 'hyperRed':period.hyperRed, 'farRed':period.farRed, 'white': period.white};
    }
  }
  if(periodValues == null){
    periodValues = {'uv': recipe.periods[0].uv, 'blue': recipe.periods[0].blue, 'green': recipe.periods[0].green, 'hyperRed': recipe.periods[0].hyperRed, 'farRed': recipe.periods[0].farRed, 'white': recipe.periods[0].white};
  }

  return periodValues;
}

async function getDailyLightIntegral(periods, fInfo){
  let dailyLightIntegral = 0;
  for(let period of periods){
    let mols = 0;
    try{
      mols = Math.round(period.uv*fInfo.uvMax/100) +
            Math.round(period.blue*fInfo.blueMax/100) +
            Math.round(period.green*fInfo.greenMax/100) +
            Math.round(period.farRed*fInfo.farRedMax/100) +
            Math.round(period.hyperRed*fInfo.hyperRedMax/100) +
            Math.round(period.white*fInfo.whiteMax/100);
    }catch(err){
      log.error(`[Daily Light Integral] Fail - ${err}`);
    }
    let photoperiodSeconds = await getPhotoperiodSeconds(period);
    console.log(`mols: ${mols} -- seconds: ${photoperiodSeconds}`);
    dailyLightIntegral += (mols/100000)*photoperiodSeconds;
  }
  return dailyLightIntegral.toFixed(2);
}

async function getPhotoperiodSeconds(period){

  let end = period.end.split(':')
  let start = period.start.split(':')
  let hours = (parseInt(end[0])-parseInt(start[0]))*360;
  let minutes = (parseInt(end[1])-parseInt(start[1]))*60;
  return (hours+minutes); //in seconds
}
// export const getObjects = makeGetObjectsAction('CANOPIES_GET', 'canopies', Canopy, queryCanopies);
export const getObjectsAll = makeGetObjectsAllAction('CANOPIES_GET_ALL', 'canopies', Canopy, queryCanopies);
export const addFilter = makeAddFilterAction('CANOPIES_ADD_FILTER', 'canopies', getObjects);
export const removeFilter = makeRemoveFilterAction('CANOPIES_REMOVE_FILTER', 'canopies', getObjects);
