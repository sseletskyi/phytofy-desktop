import discovery from 'phytofy-drivers/lib/moxa-discovery';
import { RS485protocol } from 'phytofy-drivers/lib/rs485-protocol';
import Notifications from 'react-notification-system-redux';

import { getPageSize } from '../models/Settings';
import { makeSortAction, makePageBack, makePageNext, makePageGoTo, getPaginationInfo, runCommandOnFixtures } from './helpers';
import Moxa from '../models/Moxa';
import Fixture from '../models/Fixture';
import Canopy from '../models/Canopy';

import moment from 'moment';

const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')

const ip = require('ip');
const os = require('os');

const queryDevices = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
    let canopies;

    let sortVal = sortType == 'orderAsc' ? 1 : -1;
    let sortObj = {};
    if(sortAttribute){
      sortObj = {[sortAttribute]: sortVal}
    }

    let pageSize = await getPageSize();

    // let skip = await ( page ? (page - 1) * pageSize : 0);
    // let limit = await ( page ? (skip + pageSize) : pageSize);
    let skip = from - 1;
    let limit = pageSize;
    if(ignoreLimit){
      limit = 0;
    }

    if(Object.keys(sortObj).indexOf('name') == -1){
      sortObj['name'] = 1;
    }

    try {
      canopies = await Canopy.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
    } catch(err) {
      console.error(err);
      return [];
    }

    return canopies;
}


export const getObjects = (obj) => {
  return async (dispatch, getState) => {
    await dispatch({
      type: 'DEVICES_GET'
    })
  }
};

const createMoxaDisplay = async (moxas) => {
  let moxaDisplay = [];
    for(let i in moxas) {
      let moxa = moxas[i];
      let nFixtures = (await Fixture.cfind({moxa_mac: moxa.mac}).exec()).length;
      moxaDisplay.push({
        name: moxa.ip,
        ports: moxa.ports.length,
        fixtures: nFixtures,
        location: `/setup/devices/${moxa.mac}`
      });

  }

  return moxaDisplay;
}

export const getMoxas = (history) => {
  return async (dispatch, getState) => {
    let moxaList = await discovery.discoverMoxas('255.255.255.255');
    if(moxaList.length == 0){
      let myIp = ip.address();
      let interfaces = os.networkInterfaces();
      let netmask = [].concat.apply([], Object.keys(interfaces).map(key => interfaces[key])).filter(a => a.address === myIp)[0].netmask;
      console.log(`netmask: ${netmask}`);
      let broadcastIp = ip.or(myIp, ip.not(netmask));
      console.log(`broadcasting to: ${broadcastIp}`);
      moxaList = await discovery.discoverMoxas(broadcastIp);
    }
    console.log(moxaList);

    await Moxa.remove({}, {multi: true});
    await Moxa.insert(moxaList);

    let moxas = await Moxa.cfind({}).sort({'ip': 1}).exec()
    if(moxaList.length > 0 && history.location.pathname.split('/')[1] == 'setup' && history.location.pathname.split('/')[2] == 'devices'){
      history.replace(`/setup/devices/${moxas[0].mac}`)
      dispatch(getFixtures(moxas[0].mac));
    }

    let moxaDisplay = await createMoxaDisplay(moxas);
    // let fixtures = await Fixture.cfind({}).execute();

    let comm = getState().devices.comm;

    if(!comm) {
      comm = new RS485protocol(ip.address());
      comm.fixtureMap = await Fixture.cfind({}).exec();
    }

    comm.moxaList = moxas;

    if(moxas.length == 0){
      dispatch(Notifications.warning({
        title: 'Connection warning',
        message: `No Moxa servers found. `,
        autoDismiss: 10
      }));
    }

    await dispatch({
      type: 'DEVICES_GET_MOXAS',
      moxas,
      moxaDisplay,
      comm
    })
  }
}

const queryFixtures = async (sortAttribute, sortType, from, ignoreLimit) => {
  let fixtures;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    fixtures = await Fixture.cfind().sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return fixtures;

}

export const getTemperatures = () => {
  return async (dispatch, getState) => {
    let comm = getState().devices.comm;



    let tmpFixtures = getState().devices.fixtures; //.sort(function(a,b) {return (a.port > b.port) ? 1 : ((b.port > a.port) ? -1 : 0);} );
    let fixturesTemperatures = {};
    let fixturesInfo = {};


    await runCommandOnFixtures(tmpFixtures, comm, dispatch, async (fixture) => {
      let temperatures = await comm.getModuleTemperature(fixture.serial_num);
      let sum = 0;
      let validTempsCount = 0.0;

      if(Array.isArray(temperatures)) {
        temperatures.forEach((temp) => {
          if(isNaN(temp)) return;

          sum += temp;
          validTempsCount += 1;
        })

        let avg = '-';
        if(validTempsCount > 0) avg = (sum/validTempsCount).toFixed(2);
        fixturesTemperatures[fixture.serial_num] = avg;
      }else{
        log.error(`[get Temperatures from fixture: ${fixture.serial_num}] Failed!`)
      }

      let info = await comm.getFixtureInfo(fixture.serial_num);
      fixturesInfo[fixture.serial_num] = info;
    })

    let fixtures = [];

    getState().devices.fixtures.forEach((fixture) => {
      let tmp;

      if(fixturesTemperatures[fixture.serial_num] != undefined){
        tmp = {...fixture, temperature: fixturesTemperatures[fixture.serial_num], status: 0};
      } else {
        tmp = {...fixture, temperature: '-', status: 2};
      }

      if(fixturesInfo[fixture.serial_num] != undefined){
        tmp = {...tmp, firmware: fixturesInfo[fixture.serial_num].FW, hardware: fixturesInfo[fixture.serial_num].HW};
      } else {
        tmp = {...tmp, firmware: '-', hardware: '-', status: 2};
      }

      fixtures.push(tmp);
    })

    await dispatch({
      type: 'DEVICES_GET_TEMPERATURES',
      fixtures,
    })
  }
}

export const getFixtures = () => {
  return async (dispatch, getState) => {
    let { sortAttribute, sortType, from } = getState().devices;
    if(from == 0) from = 1;

    let count = (await Fixture.cfind().exec()).length;
    let moxas = await Moxa.cfind({}).sort({'ip': 1}).exec()
    let moxaDisplay = await createMoxaDisplay(moxas);

    let fixtures = await queryFixtures(sortAttribute, sortType, from);

    fixtures = fixtures.map((fixture) => {
      return {...fixture, temperature: '-', firmware: '-', hardware: '-', status: 1}
    })

    let allFixtures = await Fixture.cfind({}).exec();

    await dispatch({
      type: 'DEVICES_GET_FIXTURES',
      fixtures,
      allFixtures,
      moxaDisplay,
      count,
      ...await getPaginationInfo(from, count)
    })

    await dispatch(getTemperatures());
  }
}

export const getAllFixtures = () => {
  return async (dispatch, getState) => {
    let allFixtures = await Fixture.cfind({}).exec();

    await dispatch({
      type: 'DEVICES_GET_ALL_FIXTURES',
      allFixtures
    })
  };
}

export const commissionFixtures = () => {
  return async (dispatch, getState) => {
    let moxaList = await discovery.discoverMoxas('255.255.255.255');
    console.log(moxaList);
    if(moxaList.length == 0){
      let myIp = ip.address();
      let interfaces = os.networkInterfaces();
      let netmask = [].concat.apply([], Object.keys(interfaces).map(key => interfaces[key])).filter(a => a.address === myIp)[0].netmask;
      console.log(`netmask: ${netmask}`);
      let broadcastIp = ip.or(myIp, ip.not(netmask));
      console.log(`broadcasting to: ${broadcastIp}`);
      moxaList = await discovery.discoverMoxas(broadcastIp);
    }
    console.log(moxaList);

    if(moxaList.length == 0) {
      dispatch(Notifications.error({
        title: "Cannot reach Moxa server",
        message: `Impossible to connect to Moxa server`,
        autoDismiss: 10
      }));
      return;
    }
    dispatch(Notifications.success({
      title: `Fixture Commissioning`,
      message: `Starting commissioning, please wait...`,
      autoDismiss: 10
    }));

    await dispatch({
      type: 'DEVICES_COMMISS',
      commissioning: true
    });

    let comm = getState().devices.comm;
    let moxaPorts = [];
    let moxas = getState().devices.moxas;
    comm.fixtureMap = await Fixture.cfind().exec();
    console.log(comm.fixtureMap)
    for(let i=0; i<moxas.length; i++){
      let moxa = moxas[i];
      // if(moxa.mac == mac) moxaPorts = moxa.ports;

      await comm.closeConnection();

      // to remove
      // await Fixture.remove({}, {multi: true});
      let nFixtures = await comm.commissionFixtures(moxa.mac, moxa.ports);
      console.log(nFixtures, moxa);
      await Fixture.remove({moxa_mac: moxa.mac}, {multi: true});
      await Fixture.insert(comm.fixtureMap.filter((obj) => {return obj.moxa_mac == moxa.mac}).map((obj) => {return {...obj, name: obj.serial_num.toString()}}));
    }
    console.log(comm.fixtureMap)
    dispatch(getFixtures());
    dispatch(Notifications.success({
      title: `Fixture Commissioning`,
      message: `Done!`,
      autoDismiss: 10
    }));
    await dispatch({
      type: 'DEVICES_COMMISS',
      commissioning: false
    });
  }
}

export const removeFixture = (obj) => {
  return async (dispatch, getState) => {
    //check if fixture belongs to any canopy
    let serialNum = obj.serial_num;
    let canopies = await Canopy.find({fixtures: {$elemMatch: obj.serial_num}});
    console.log(canopies);
    if(canopies.length == 0){
      await Fixture.remove({_id: obj._id});
      dispatch(getFixtures());
      dispatch(Notifications.success({
        title: `Fixture removed`,
        message: `Removed fixture ${serialNum} from memory!`,
        autoDismiss: 10
      }));
    }else{
      dispatch(Notifications.error({
        title: `Fixture not removed`,
        message: `Could not Remove fixture ${serialNum}, it is assigned to group ${canopies[0].name}!`,
        autoDismiss: 10
      }));
    }
  }
}

export const automaticSyncTimeRef = () => {
  return async (dispatch, getState) => {
    let syncInterval = setInterval( async () => {
      let fixtures = await Fixture.cfind({}).exec();
      console.log('[automaticSyncTimeRef]');
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        let ack = await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
        if(ack == 0) log.error(`[Automatic Sync] Error setting time reference of Fixture: ${fixture.serial_num} - failed`);
      });
    }, 600000);
  }
};

export const automaticSync = () => {
  return async (dispatch, getState) => {
    let syncInterval = setInterval( async () => {
      let fixtures = await Fixture.cfind({}).exec();
      console.log('[automaticSync]');
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        let ack = await getState().devices.comm.setTimeReference(fixture.serial_num, moment().unix());
        if(ack == 0) log.error(`[Automatic Sync] Error setting time reference of Fixture: ${fixture.serial_num} - failed`);
        ack = await getState().devices.comm.resumeScheduling(fixture.serial_num);
        if(ack == 0) log.error(`[Automatic Sync] Error resuming scheduling of Fixture: ${fixture.serial_num} - failed`);
      });
    }, 600000);
  }
};

export const sort = makeSortAction('DEVICES_SORT', 'devices', getFixtures);
export const pageBack = makePageBack('DEVICES_PAGE_BACK', 'devices', getFixtures, Fixture);
export const pageNext = makePageNext('DEVICES_PAGE_NEXT', 'devices', getFixtures, Fixture);
export const goto = makePageGoTo('DEVICES_PAGE_GOTO', 'devices', getFixtures, Fixture);
