// @flow
import path from 'path';
import validator from 'validator';
import Notifications from 'react-notification-system-redux';
import moment from 'moment';

import Schedule, {ScheduleFormValidator, addScheduleToFixtures, removeScheduleFromFixtures, removeScheduleFromCanopy, validateSchedules} from '../models/Schedule';
import { getPageSize } from '../models/Settings';
import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
         makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext, makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo, runCommandOnFixtures} from './helpers';
import { openSidebar, closeSidebar } from './sidebar';
import FormValidator from '../utils/FormValidator';
import Canopy, { getFixturesFromCanopy } from '../models/Canopy';
import Recipe from '../models/Recipe';
import Fixture from '../models/Fixture';
import { getLengthConvertedToInches } from '../models/Settings';

import { irradianceMap, sumAndFormat } from 'phytofy-utils/lib/irradianceUtils'

const defaultSchedule = {name: "", canopies: [], start_date: '', end_date: ''};

const querySchedulesFromCanopy = async(canopyId) => {
  let schedules;

  try {
    schedules = await Schedule.cfind({canopies: canopyId}).exec();
    schedules = schedules.sort(function(a,b) {return moment(a.end_date) < moment(b.end_date) ? 1 : ((moment(b.end_date) < moment(a.end_date)) ? -1 : 0)} )
  } catch(err) {
    console.error(err);
    return [];
  }

  return schedules;
}

const querySchedules = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let schedules;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    schedules = await Schedule.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return schedules;
}

export const getObjects = makeGetObjectsAction('SCHEDULES_GET', 'schedules', Schedule, querySchedules);
export const getObjectsAll = makeGetObjectsAllAction('SCHEDULES_GET_ALL', 'schedules', Schedule, querySchedules);
export const sort = makeSortAction('SCHEDULES_SORT', 'schedules', getObjects);
export const addFilter = makeAddFilterAction('SCHEDULES_ADD_FILTER', 'schedules', getObjects);
export const removeFilter = makeRemoveFilterAction('SCHEDULES_REMOVE_FILTER', 'schedules', getObjects);
export const pageBack = makePageBack('SCHEDULES_PAGE_BACK', 'schedules', getObjects, Schedule);
export const pageNext = makePageNext('SCHEDULES_PAGE_NEXT', 'schedules', getObjects, Schedule);
export const select = makeSelectObject('SCHEDULES_SELECT', 'schedules');
export const deselect = makeDeselectObject('SCHEDULES_DESELECT', 'schedules');
export const selectAll = makeSelectAll('SCHEDULES_SELECT_ALL', 'schedules');
export const deselectAll = makeDeselectAll('SCHEDULES_DESELECT_ALL');
export const goto = makePageGoTo('SCHEDULES_PAGE_GOTO', 'schedules', getObjects, Schedule);

export const resetIrradMap = () => {
  return async (dispatch, getState) => {
    await dispatch({
      type: 'SCHEDULES_RESET_IRRAD_MAP',
      irradMap: null
    });
  }
};

export const getObjectsFromCanopy = (canopyId) => {
  return async (dispatch, getState) => {
      let channelsOn = [], channels = ['uv', 'blue', 'green', 'hyperred', 'farred', 'white'];
      let schedules = await querySchedulesFromCanopy(canopyId);
      let count = schedules.length;
      let {from} = getState().schedules;
      if(from == 0) from = 1;

      let currDate = moment();
      let periodValues = {'uv': 0, 'blue': 0, 'green': 0, 'hyperred':0, 'farred':0, 'white': 0};
      if(schedules.length > 0){
        let currDate = moment();
        let currDay = moment(currDate).format("MM-DD-YYYY");

        let tmp = schedules.filter((schedule) => {
          let startDay = moment(schedule.start_date).format("MM-DD-YYYY");
          let endDay = moment(schedule.end_date).format("MM-DD-YYYY");
          return (moment(schedule.start_date).isBefore(currDate) || startDay == currDay) && (moment(schedule.end_date).isAfter(currDate) || endDay == currDay);
        });
        let currSchedule = null;
        if (tmp.length > 0){
          currSchedule = tmp[0];
          let recipe = await Recipe.findOne({_id: currSchedule.recipeId});
          let current = new Date().toLocaleTimeString();
          let currentHours = Number(current.split(':')[0]);
          console.log(recipe);
          for(let period of recipe.periods){
            if(Number((period.start.split(':')[0]) <= currentHours) && (Number(period.end.split(':')[0]) >= currentHours)){
              periodValues = {'uv': period.irradianceUVA, 'blue': period.irradianceBlue, 'green': period.irradianceGreen, 'hyperred':period.irradianceHyperRed, 'farred':period.irradianceFarRed, 'white': period.irradianceWhite};
              for(let c=0; c<6; c++){
                if( period[channels[c]] > 0)
                  channelsOn.push(channels[c]);
              }
              break;
            }
          }
          if(!channelsOn.length){
            periodValues = {'uv': recipe.periods[0].irradianceUVA, 'blue': recipe.periods[0].irradianceBlue, 'green': recipe.periods[0].irradianceGreen, 'hyperred':recipe.periods[0].irradianceHyperRed, 'farred':recipe.periods[0].irradianceFarRed, 'white': recipe.periods[0].irradianceWhite};
            for(let c=0; c<6; c++){
              if( recipe.periods[0][channels[c]] > 0)
                channelsOn.push(channels[c]);
            }
          }
        }

      }
      console.log(channelsOn);

      await dispatch({
          type: 'SCHEDULES_GET_FROM_CANOPY',
          objects: schedules,
          channelArr: channelsOn,
          count: count,
          irradMap: null,
          ...await getPaginationInfo(from, count)
      })

      dispatch(getIrradianceMap(canopyId, channelsOn, periodValues));
  }
}

export const getIrradianceMap = (canopyId, channelArr, periodValues) => {
  return async (dispatch, getState) => {
    let object, retObj = null, map = null, maxIrrad = 0;
    try {
      object = await Canopy.findOne({_id: canopyId});
    } catch(err) {
      console.error(err);
      return null;
    }

    let w = getLengthConvertedToInches(object._profile.width, getState().settings.lengthUnit);
    let l = getLengthConvertedToInches(object._profile.length, getState().settings.lengthUnit);
    let delta = getLengthConvertedToInches(object._profile.deltaLength, getState().settings.lengthUnit);
    let height = getLengthConvertedToInches(object._profile.height, getState().settings.lengthUnit);
    const relPath = path.join(require('electron').remote.app.getAppPath(),'static');
    const absPath = "/Users/ricardo/Documents/GitStation/phytofy-dashboard/lib/phytofy-utils/src/FixtureMaps"

    console.log(relPath);
    if(channelArr != null && channelArr.length != 0){
      retObj = await irradianceMap(l,w, object._profile.config, height, channelArr, delta, relPath);
      //raw = true -> tooltip shows irradiance
      let colorScale = getState().settings.irradianceColorscale;
      console.log(colorScale);
      map = await sumAndFormat(retObj['canopyMap'], retObj['maximumValues'], periodValues ,channelArr, true, colorScale);
    }

    //let map = null;
    //if(retObj != null) map = retObj['canopyMap'];

    dispatch({
        type: "SCHEDULES_GET_IRRADIANCE_MAP",
        map
    })
  }
}

export const addCanopy = (obj) => {
    return async (dispatch, getState) => {
        await dispatch({
            type: 'SCHEDULES_ADD_CANOPY',
            canopiesSearch: [],
            scheduleObj: obj,
        })

        dispatch(openSidebar('ADD_CANOPY_TO_SCHEDULE_FORM'));
    }
}

export const getCanopies = (search) => {
    return async (dispatch, getState) => {
        let canopies = await Canopy.cfind({_id: {$nin: getState().schedules.scheduleObj.canopies}, name: new RegExp(search, 'i')}).sort({'name': 1}).exec();

        await dispatch({
            type: 'SCHEDULES_GET_CANOPIES',
            canopiesSearch: canopies
        })
    }
}

export const getAvailableCanopies = () => {
    return async (dispatch, getState) => {
        let canopies = await Canopy.cfind({_id: {$nin: getState().schedules.scheduleObj.canopies}}).exec();

        await dispatch({
            type: 'SCHEDULES_GET_CANOPIES',
            canopiesSearch: canopies
        })
    }
}

export const submitAddCanopy = (canopyId) => {
    return async (dispatch, getState) => {
        let scheduleObj = getState().schedules.scheduleObj;
        scheduleObj.canopies.push(canopyId);


        await dispatch({
            type: 'SCHEDULES_SUBMIT_EDIT_CANOPY',
            scheduleObj
        })

        let prevState = getState().sidebar.prevState;
        dispatch(openSidebar(prevState.component, prevState.label, prevState.title));
    }
}

export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let scheduleObj = obj ? obj : {...defaultSchedule};

    let recipes = await Recipe.cfind({}).sort({'name': 1}).exec();
    let canopies = await Canopy.cfind({}).sort({'name': 1}).exec();

    if(recipes.length == 0){
        dispatch(Notifications.error({
          title: `No recipes found!`,
          message: `Please create a recipe before trying to add a new schedule.`,
          autoDismiss: 10
        }));
        return;
    }

    if(!scheduleObj.recipeId){
        scheduleObj.recipeId = recipes[0]._id
    }

    await dispatch({
      type: 'SCHEDULES_CREATE',
      scheduleObj: scheduleObj,
      recipes,
      canopies,
      validation: {}
    })
    dispatch(openSidebar('CREATE_SCHEDULE_FORM', 'Create', 'New schedule'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    let validations = await ScheduleFormValidator.validate(obj);
    let scheduleId = '';
    if(validations.isValid){
      try {
        obj.status = 1;
        obj._recipe = await Recipe.findOne({_id: obj.recipeId});
        obj = await Schedule.insert(obj);
        scheduleId = obj._id;
      } catch(err){
        console.error(err);
      }
      let canopies = (await Canopy.cfind({_id: {$in: obj.canopies}}).exec());

      let show = true;
      dispatch(closeSidebar())
      if(canopies.length > 0){
        dispatch(Notifications.success({
          title: 'Saving schedule',
          message: `Communicating with fixtures please wait...`,
          autoDismiss: 10
        }));
      }else{
        show = false;
        dispatch(Notifications.success({
          title: 'Schedule saved',
          message: `No groups associated with schedule.`,
          autoDismiss: 10
        }));
      }

      let ack = -1;
      let fixturesArray = [];
      for(let canopyI=0; canopyI<canopies.length; canopyI++){
        let canopy = canopies[canopyI];
        fixturesArray = fixturesArray.concat(canopy.fixtures);
      }
      if(fixturesArray.length > 0){
        let fixtures = await Fixture.cfind({serial_num: {$in: fixturesArray}}).exec();
        console.log(fixtures);
        ack = await addScheduleToFixtures(obj, fixtures, getState().devices.comm, dispatch);
      }

      obj = {...defaultSchedule};
      //dispatch(closeSidebar())
      if(ack && show){
        dispatch(Notifications.success({
          title: 'Schedule created',
          message: `Schedule created with success.`,
          autoDismiss: 10
        }));
      }else if(!ack){
        await Schedule.update({_id: scheduleId}, {$set: {status: 0}});
        dispatch(Notifications.error({
          title: 'Schedule created',
          message: `There were issues communicating with at least one fixture. Check logs for more info.`,
          autoDismiss: 10
        }));
      }
    }

    dispatch(getObjects())

    dispatch({
      type: 'SCHEDULES_SUBMIT_CREATE',
      scheduleObj: obj,
      validation: validations,
    })

  }
}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;

    dispatch(create(newObj));
  }
}

export const edit = (obj) => {
  return async (dispatch, getState) => {

    let recipes = await Recipe.cfind({}).sort({'name': 1}).exec();
    let canopies = await Canopy.cfind({}).sort({'name': 1}).exec();

    await dispatch({
      type: 'SCHEDULES_EDIT',
      scheduleObj: obj,
      editObjId: obj._id,
      recipes,
      canopies,
      validation: {}
    })

    dispatch(openSidebar('EDIT_SCHEDULE_FORM', 'Edit', 'schedule'));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {
    let original = await Schedule.findOne({_id: getState().schedules.editObjId});
    let validations = await ScheduleFormValidator.validate(obj, getState().schedules.scheduleObj);

    if(validations.isValid){
      try {
        obj._recipe = await Recipe.findOne({_id: obj.recipeId});
        await Schedule.update({_id: getState().schedules.editObjId}, obj);
      } catch(err){
        console.error(err);
      }

      let show = true;
      dispatch(closeSidebar())
      if(original.canopies.length > 0 || obj.canopies.length > 0){
        dispatch(Notifications.success({
          title: 'Updating schedule',
          message: `Communicating with fixtures please wait...`,
          autoDismiss: 10
        }));
      }else{
        show = false;
        dispatch(Notifications.success({
          title: 'Schedule saved',
          message: `No groups associated with schedule.`,
          autoDismiss: 10
        }));
      }

      let ack = -1;
      for(let i=0; i<original.canopies.length; i++) {
        let canopyId = original.canopies[i];
        let canopy = await Canopy.findOne({_id: canopyId});

        if(!canopy) continue;

        let fixtures = await getFixturesFromCanopy(canopy);
        await removeScheduleFromFixtures(obj, fixtures, getState().devices.comm, dispatch);
      }

      let fixturesArray = [];
      for(let i=0; i<obj.canopies.length; i++) {
        let canopyId = obj.canopies[i];
        let canopy = await Canopy.findOne({_id: canopyId});

        if(!canopy) continue;
        fixturesArray = fixturesArray.concat(canopy.fixtures);
        //let fixtures = await getFixturesFromCanopy(canopy);
      }
      if(fixturesArray.length > 0){
        let fixtures = await Fixture.cfind({serial_num: {$in: fixturesArray}}).exec();
        console.log(fixtures);
        ack = await addScheduleToFixtures(obj, fixtures, getState().devices.comm, dispatch);
      }

      //dispatch(closeSidebar())
      if(ack && show){
        await Schedule.update({_id: obj._id}, {$set: {status: 1}});
        dispatch(Notifications.success({
          title: 'Schedule edited',
          message: `Schedule  edited with success.`,
          autoDismiss: 10
        }));
      }else if(!ack){
        await Schedule.update({_id: obj._id}, {$set: {status: 0}});
        dispatch(Notifications.error({
          title: 'Schedule created',
          message: `There were issues communicating with at least one fixture. Check logs for more info.`,
          autoDismiss: 10
        }));
      }

    }

    dispatch(getObjects());
    dispatch({
      type: 'SCHEDULES_SUBMIT_EDIT',
      scheduleObj: obj,
      editObjId: validations.isValid ? null : getState().schedules.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultSchedule};
    dispatch({
      type: 'SCHEDULES_CANCEL_FORM',
      scheduleObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const addScheduleToCanopy = (canopySchedules) => {
  return async (dispatch, getState) => {
    let takenIds = canopySchedules.map((schedule) => {
        return schedule._id
    });
    console.log(takenIds);
    let filters = getState().schedules.filters;
    let allSchedules = await querySchedules({}, null, 1, filters, true);
    let schedules = allSchedules.filter((element) => {
      let notIncluded = true;
      for(let id of takenIds){
        if(id == element._id) notIncluded = false;
      }
      return notIncluded;
    });
    console.log(schedules);
    //let schedules = canopySchedules;

    if(schedules.length == 0){
      dispatch(Notifications.warning({
        title: 'Add Schedule to Group',
        message: `There are no avaiable schedules to add to this group...`,
        autoDismiss: 10
      }));
      return;
    }

    await dispatch({
      type: 'SCHEDULES_ADD_TO_CANOPY',
      availableScheds: schedules,
      validation: {}
    })
    dispatch(openSidebar('ADD_SCHEDULE_TO_CANOPY_FORM', 'Add', 'Schedule'));
  }
}

export const submitAddToCanopy = (schedule) => {
  return async (dispatch, getState) => {
    let canopy = getState().canopies.object;
    let canopySchedules = await querySchedulesFromCanopy(canopy._id);
    let startDate = new Date(schedule.start_date);
    let endDate = new Date(schedule.end_date);
    let valid = await validateSchedules(canopySchedules, startDate.getTime(), endDate.getTime(), schedule._id);
    console.log(valid);
    if(!valid){
      dispatch(Notifications.error({
        title: 'Add schedule',
        message: `This schedule overlaps with other scheduling on this group.`,
        autoDismiss: 10
      }));
      return;
    }
    try{
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Saving schedule',
        message: `Communicating with fixtures please wait...`,
        autoDismiss: 10
      }));
      let fixtures = await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).exec();
      await Schedule.update({_id: schedule._id}, {$set: {canopies: [...schedule.canopies, canopy._id]}});
      let ack = await addScheduleToFixtures(schedule, fixtures, getState().devices.comm, dispatch);
      if(ack){
        dispatch(Notifications.success({
          title: 'Schedule created',
          message: `Schedule created with success.`,
          autoDismiss: 10
        }));
      }else{
        dispatch(Notifications.error({
          title: 'Schedule created',
          message: `There were issues communicating with at least one fixture. Check logs for more info.`,
          autoDismiss: 10
        }));
      }
    } catch(err){
      console.error(err);
    }

    //dispatch(closeSidebar())
    // dispatch(Notifications.success({
    //   title: 'Schedule added',
    //   message: `Schedule added with success.`,
    //   autoDismiss: 10
    // }));
    // dispatch(getObjectsFromCanopy())

    //is this necessary ?
    dispatch({
      type: 'SCHEDULES_SUBMIT_ADD_TO_CANOPY',
      schedule: schedule,
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      if(obj.fixtures != undefined){
        dispatch(Notifications.success({
          title: 'Deleting Schedule',
          message: `Communicating with fixtures please wait...`,
          autoDismiss: 10
        }));
        let fixtures = await Fixture.cfind({name: {$in: Object.keys(obj.fixtures)}}).exec();
        await removeScheduleFromFixtures(obj, fixtures, getState().devices.comm, dispatch);
      }
      await Schedule.remove({_id: obj._id})
      dispatch(Notifications.success({
        title: 'Schedule delete',
        message: `Schedule deleted with success!`,
        autoDismiss: 10
      }));
    } catch(err){
      console.error(err);
      dispatch(Notifications.error({
        title: 'Schedule delete',
        message: `There were issues deleting schedules!`,
        autoDismiss: 10
      }));
    }

    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    let scheduleId = obj._id;
    // let count = await Canopy.count({scheduleId: scheduleId});
    // if(count > 0){
    //   dispatch(Notifications.error({
    //     title: `Cannot delete ${obj.name}!`,
    //     message: `The schedule ${obj.name} has one or more canopies associated to it.`,
    //     autoDismiss: 10
    //   }));
    // } else {
      dispatch(Notifications.warning({
        title: `Delete ${obj.name}`,
        message: `Are you sure you want to delete ${obj.name}?`,
        autoDismiss: 10,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDelete(obj))
          }
        }
      }));
    // }
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      let schedules = await Schedule.cfind({_id: {$in: getState().schedules.selected}}).exec();
      dispatch(Notifications.success({
        title: 'Deleting Schedule',
        message: `Deleting multiple schedules please wait...`,
        autoDismiss: 10
      }));
      for(var i=0; i<schedules.length; i++) {
        let schedule = schedules[i];
        if( schedule.fixtures != null){
          let fixtures = await Fixture.cfind({name: {$in: Object.keys(schedule.fixtures)}}).exec();
          await removeScheduleFromFixtures(schedule, fixtures, getState().devices.comm, dispatch);
        }
      }
      let obj = await Schedule.remove({_id: {$in: getState().schedules.selected}}, {multi: true})
      dispatch(Notifications.success({
        title: 'Schedule delete',
        message: `Schedules deleted with success...`,
        autoDismiss: 10
      }));
    } catch(err){
      console.error(err);
      dispatch(Notifications.error({
        title: 'Schedule delete',
        message: `There were issues deleting schedules!`,
        autoDismiss: 10
      }));
    }

    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    // let count = await Canopy.count({scheduleId: {$in: getState().schedules.selected}});
    // if(count > 0){
    //   dispatch(Notifications.error({
    //     title: `Cannot delete schedules!`,
    //     message: `The selected schedules have one or more canopies associated to them.`,
    //     autoDismiss: 10
    //   }));
    // } else {
      dispatch(Notifications.warning({
        title: `Delete ${getState().schedules.selected.length} schedules`,
        message: `Are you sure you want to delete ${getState().schedules.selected.length} schedules?`,
        autoDismiss: 10,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDeleteMultiple())
          }
        }
      }));
    // }
  }
}

export const deleteObjFromCanopy = (sched) => {
  return async (dispatch, getState) => {
    let scheduleId = sched._id;
    let canopyId = getState().canopies.object._id;
    dispatch(Notifications.warning({
      title: `Delete ${sched.name}`,
      message: `Are you sure you want to delete this schedule?`,
      autoDismiss: 10,
      action: {
        label: 'Yes',
        callback: () => {
          dispatch(confirmDeleteFromCanopy(canopyId, sched))
        }
      }
    }));
  };
}

export const confirmDeleteFromCanopy = (canopyId, sched) => {
  return async (dispatch, getState) => {
    try {
      let canopy = await Canopy.findOne({_id: canopyId});
      const fixtures = await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
      console.log(fixtures);
      if(fixtures.length != 0){
        dispatch(Notifications.success({
          title: 'Deleting Schedule',
          message: `Communicating with fixtures please wait...`,
          autoDismiss: 10
        }));
        let errors = await removeScheduleFromCanopy(sched, canopyId, fixtures, getState().devices.comm, dispatch);
        if(errors > 0){
          dispatch(Notifications.error({
            title: 'Schedule delete Error',
            message: `Error comunicating with fixtures, check the logs for more info.`,
            autoDismiss: 10
          }));
        }else{
          dispatch(Notifications.success({
            title: 'Schedule delete',
            message: `Schedules deleted with success...`,
            autoDismiss: 10
          }));
        }
      }else{
        let idx = sched.canopies.indexOf(canopyId);
        await Schedule.update({_id: sched._id}, {$set: {canopies: [...sched.canopies.slice(0, idx), ...sched.canopies.slice(idx+1)]}});
        dispatch(Notifications.success({
          title: 'Schedule delete',
          message: `Schedules deleted with success...`,
          autoDismiss: 10
        }));
      }
    } catch(err){
      console.error(err);
      dispatch(Notifications.error({
        title: 'Schedule delete',
        message: `There were issues deleting schedules from group!`,
        autoDismiss: 10
      }));
    }

    //dispatch getObjects
    dispatch(getObjectsFromCanopy(canopyId));
  }
}

export const deleteMultipleFromCanopy = () => {
  return async (dispatch, getState) => {
      let canopyId = getState().canopies.object._id;

      dispatch(Notifications.warning({
        title: `Delete ${getState().schedules.selected.length} schedules`,
        message: `Are you sure you want to delete ${getState().schedules.selected.length} schedules?`,
        autoDismiss: 10,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDeleteMultipleFromCanopy(canopyId))
          }
        }
      }));
  }
}

export const confirmDeleteMultipleFromCanopy = (canopyId) => {
  return async (dispatch, getState) => {
    try {
      let canopy = await Canopy.findOne({_id: canopyId});
      const fixtures = await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
      if(fixtures != 0){
        dispatch(Notifications.success({
          title: 'Deleting Schedule',
          message: `Communicating with fixtures please wait...`,
          autoDismiss: 10
        }));
        let schedules = await Schedule.cfind({_id: {$in: getState().schedules.selected}}).exec();
        console.log(schedules);
        for(let i=0; i<schedules.length; i++) {
          let schedule = schedules[i];
          let errors = await removeScheduleFromCanopy(schedule, canopyId, fixtures, getState().devices.comm, dispatch);
          console.log(errors);
          let idx = schedule.canopies.indexOf(canopyId);
          await Schedule.update({_id: schedule._id}, {$set: {canopies: [...schedule.canopies.slice(0, idx), ...schedule.canopies.slice(idx+1)]}});
        }
        dispatch(Notifications.success({
          title: 'Schedule delete',
          message: `Schedules deleted with success...`,
          autoDismiss: 10
        }));
      }else{
        let schedules = await Schedule.cfind({_id: {$in: getState().schedules.selected}}).exec();
        for(let i=0; i<schedules.length; i++) {
          let schedule = schedules[i];
          let idx = schedule.canopies.indexOf(canopyId);
          await Schedule.update({_id: schedule._id}, {$set: {canopies: [...schedule.canopies.slice(0, idx), ...schedule.canopies.slice(idx+1)]}});
        }
        dispatch(Notifications.success({
          title: 'Schedule delete',
          message: `Schedules deleted with success...`,
          autoDismiss: 10
        }));
      }
    } catch(err){
      console.error(err);
      dispatch(Notifications.error({
        title: 'Schedule delete',
        message: `There were issues deleting schedules from group!`,
        autoDismiss: 10
      }));
    }

    dispatch(getObjectsFromCanopy(canopyId));
  }
}
