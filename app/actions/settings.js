const electron = require('electron');
const log = require('electron-log');
//const log = require('electron').remote.require('electron-log')

import Notifications from 'react-notification-system-redux';
import JSZip from 'jszip';
import fs from 'fs';
const path = require('path');
const ndjson = require('ndjson')
const LDJSONStream = require('ld-jsonstream');


import Settings, {getIrradianceColorscale, getIrradianceUnit, getLengthUnit, getPageSize, getLengthConvertedToInches, getLoggingMode, getSyncTimeFlag, getResumeSchedFlag } from '../models/Settings';
import Profile, {exportData as exportProfiles, importData as importProfiles} from '../models/Profile';
import Canopy, {exportData as exportCanopies, importData as importCanopies} from '../models/Canopy';
import Fixture from '../models/Fixture';
import Recipe, {exportData as exportRecipes, importData as importRecipes} from '../models/Recipe';
import Schedule, {exportData as exportSchedules, importData as importSchedules, addScheduleToFixtures} from '../models/Schedule';
import Moxa from '../models/Moxa';
import Device from '../models/Device';
import { runCommandOnFixtures } from './helpers';
import { irradianceMap, computeEnviron } from 'phytofy-utils/lib/irradianceUtils'


export const setSettings = (pageSize, lengthUnit, irradianceUnit, irradianceColorscale, schedResume) => {
    return async (dispatch, getState) => {
        await Settings.remove({}, { multi: true });
        await Settings.insert({name: 'pageSize', value: pageSize});
        await Settings.insert({name: 'lengthUnit', value: lengthUnit});
        await Settings.insert({name: 'irradianceUnit', value: irradianceUnit});
        await Settings.insert({name: 'irradianceColorscale', value: irradianceColorscale});
        let loggingMode = await getLoggingMode();
        await Settings.insert({name: 'loggingMode', value: loggingMode});
        await Settings.insert({name: 'schedResume', value: schedResume});

        dispatch({
            type: 'SETTINGS_SET',
            pageSize,
            lengthUnit,
            irradianceUnit,
            irradianceColorscale,
            schedResume
        })

        dispatch(Notifications.success({
            title: 'Preferences changed',
            message: `Preferences changed with success.`,
            autoDismiss: 10
        }));
    }
}

export const setLoggingMode = (loggingMode) => {
  return async (dispatch, getState) => {
    await Settings.remove({name: 'loggingMode'});
    await Settings.insert({name: 'loggingMode', value: loggingMode});

    dispatch({
        type: 'SETTINGS_SET_LOGMODE',
        loggingMode
    });

    dispatch(Notifications.success({
        title: 'Logging Mode changed',
        message: `The application needs to be restarted for this change to take effect.`,
        autoDismiss: 10
    }));
  }
}

/*
Not using this, configuration only for resume scheduling
and it is set on the preferences card,
simply on setSettings
*/
// export const setAutomaticSync = (syncTimeRef, schedResume) => {
//   return async (dispatch, getState) => {
//     await Settings.remove({name: 'syncTimeRef'});
//     await Settings.insert({name: 'syncTimeRef', value: syncTimeRef});
//     await Settings.remove({name: 'schedResume'});
//     await Settings.insert({name: 'schedResume', value: schedResume});
//     dispatch({
//         type: 'SETTINGS_SET_AUTOMATICSYNC',
//         syncTimeRef,
//         schedResume
//     });
//
//     dispatch(Notifications.success({
//         title: 'Automatic Sync updated',
//         message: `Updated automatic sync preferences.`,
//         autoDismiss: 10
//     }));
//   }
// };

export const getSettings = () => {
    return async (dispatch, getState) => {
        let pageSize = await getPageSize();
        let lengthUnit = await getLengthUnit();
        let irradianceUnit = await getIrradianceUnit();
        let irradianceColorscale = await getIrradianceColorscale();
        let loggingMode = await getLoggingMode();
        let schedResume = await getResumeSchedFlag();

        if(schedResume == 10) schedResume = 'disabled';

        dispatch({
            type: 'SETTINGS_GET',
            pageSize,
            lengthUnit,
            irradianceUnit,
            irradianceColorscale,
            loggingMode,
            schedResume
        });
    }
};

export const exportData = (profiles, recipes, canopies, schedules) => {
    return async (dispatch, getState) => {
        const {dialog} = require('electron').remote
        dialog.showSaveDialog({defaultPath: 'phytofyExport.zip'}, (async (path) => {
            let profilesCSV = profiles ? exportProfiles() : null;
            let recipesCSV = recipes ? exportRecipes() : null;
            let canopiesCSV = canopies ? exportCanopies() : null;
            let schedulesCSV = schedules ? exportSchedules() :null;

            var zip = new JSZip();
            if(profilesCSV) zip.file("phytofyExport/layouts.csv", profilesCSV);
            if(recipesCSV) zip.file("phytofyExport/recipes.csv", recipesCSV);
            if(canopiesCSV) zip.file("phytofyExport/groups.csv", canopiesCSV);
            if(schedulesCSV) zip.file("phytofyExport/schedules.csv", schedulesCSV);


            zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
            .pipe(fs.createWriteStream(path))
            .on('finish', function () {
                // JSZip generates a readable stream with a "end" event,
                // but is piped here in a writable stream which emits a "finish" event.
                dispatch(Notifications.success({
                    title: 'Data exported',
                    message: `Data exported with success.`,
                    autoDismiss: 10
                }));
            });
        }));
    }
}

export const importData = (files) => {
    return async (dispatch, getState) => {
        for (var i = 0; i < files.length; i++) {
            let file = files[i];

            fs.readFile(file.path, async function(err, data) {
                if (err) throw err;
                let errors = 0;
                // JSZip.loadAsync(data).then(function (zip) {
                //   // Get list of filenames, which are also keys for additional details
                //   let zipFiles = Object.keys(zip.files);
                //
                //   for(i=0; i<zipFiles.length; i++){
                //     zip.file(zipFiles[i]).async('nodebuffer').then(function(content) {
                //         var parse = require('csv-parse/lib/sync');
                //         var records = parse(content, {columns: true});
                //         importProfiles(records);
                //     });
                //   }
                // });
                var parse = require('csv-parse/lib/sync');
                var records = parse(data, {columns: true});
                console.log(records);
                if("start date" in records[0]){
                  let ret = await importSchedules(records);
                  console.log(ret);
                  errors = ret.errorCount;
                  dispatch(loadSchedules(ret.created));
                }
                else if("start" in records[0]){
                  let ret = await importRecipes(records);
                  console.log(ret);
                  errors = ret;
                }
                else if("layout name" in records[0]){
                  let ret = await importCanopies(records);
                  console.log(ret.created);
                  errors = ret.errorCount;

                  for(let g = 0; g < ret.created.length; g++){
                    if(ret.created[g].fixtures.length == 0) ret.created.splice(g, 1);
                  }
                  if(ret.created.length > 0){
                    console.log('commissioning canopies');
                    dispatch(commissionCanopies(ret.created));
                  }
                }
                else if("length" in records[0]){
                  let ret = await importProfiles(records);
                  errors = ret;
                }
                else{
                  dispatch(Notifications.error({
                      title: 'Data import Fail',
                      message: `File does not have the required fields.`,
                      autoDismiss: 10
                  }));
                  return;
                  errors = -1;
                }
                if(errors == 0){
                  dispatch(Notifications.success({
                      title: 'Data imported',
                      message: `Data imported with success.`,
                      autoDismiss: 10
                  }));
                }else if(errors != -1 && errors != 0){
                  dispatch(Notifications.error({
                      title: 'Data imported',
                      message: `There were ${errors} errors on import. Check the logs for more info.`,
                      autoDismiss: 10
                  }));
                }
            });
        }
    }
}

export const deleteData = () => {
    return async (dispatch, getState) => {
        dispatch(Notifications.warning({
            title: `Reset data`,
            message: `Are you sure you want to delete all the data?`,
            autoDismiss: 10,
            action: {
            label: 'Yes',
            callback: () => {
                dispatch(confirmDeleteData())
            }
            }
        }));
    };
}

export const confirmDeleteData = () => {
    return async (dispatch, getState) => {
      //get all fixtures
      let fixtures = await Fixture.cfind({}).exec();
      await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
        let ack = await getState().devices.comm.deleteAllSchedules(fixture.serial_num);
        if(ack == 0) log.error(`[Reset Data Fail] Remove all schedules from Fixture: ${fixture.serial_num} - failed`);
        let ack2 = await getState().devices.comm.stopScheduling(fixture.serial_num);
        if(ack2 == 0) log.error(`[Reset Data Fail] Pause scheduling at: ${fixture.serial_num} - failed`);
        let schedCount = await getState().devices.comm.getScheduleCount(fixture.serial_num);
        if(schedCount != 0){
          log.error(`[Reset Data] Removed all schedules from fixture ${fixture.serial_num} but there are still ${schedCount} items in memory. Re-sending delete all command.`);
          let ack3 = await getState().devices.comm.deleteAllSchedules(fixture.serial_num);
        }
      });
      Profile.remove({}, {multi: true});
      Canopy.remove({}, {multi: true});
      Fixture.remove({}, {multi: true});
      Schedule.remove({}, {multi: true});
      Recipe.remove({}, {multi: true});
      Device.remove({}, {multi: true});
      Settings.remove({}, {multi: true});
      Moxa.remove({}, {multi: true});

      dispatch(Notifications.success({
          title: 'Data deleted',
          message: `Data deleted with success.`,
          autoDismiss: 10
      }));
    };
}

export const restore = (files, resetFlag) => {
  return async (dispatch, getState) => {
    if(resetFlag) dispatch(deleteAllSchedulesFromFixtures());
    for (var i = 0; i < files.length; i++) {
      let file = files[i];

      fs.readFile(file.path, async function(err, data){
        if (err) throw err;
        JSZip.loadAsync(data).then(async function (zip) {
          dispatch(Notifications.success({
            title: 'Database Restore',
            message: `Restoring... may take a while depending on size of the database.`,
            autoDismiss: 10
          }));
          // Get list of filenames, which are also keys for additional details
          let zipFiles = Object.keys(zip.files);
          console.log(zipFiles);
          for(i=1; i<zipFiles.length; i++){
            let splitz = zipFiles[i].split('/');
            let name = splitz[splitz.length-1];
            const userDataPath = (electron.app || electron.remote.app).getPath('userData');
            zip.file(zipFiles[i]).async('nodebuffer').then(async function(content) {
              restoreDb(content, userDataPath, name);
              // if(name == 'recipes.db'){
              //   restoreRecipes(content, userDataPath);
              // }else if(name == 'canopies.db'){
              //   restoreGroups(content, userDataPath);
              // }else if(name == 'fixtures.db'){
              //   restoreFixtures(content, userDataPath);
              // }else if(name == 'profiles.db'){
              //   restoreProfiles(content, userDataPath);
              // }else if(name == 'schedules.db'){
              //   restoreSchedules(content, userDataPath);
              // }else if(name == 'moxas.db'){
              //   restoreMoxas(content, userDataPath);
              // }else if(name == 'settings.db'){
              //   restoreSettings(content, userDataPath);
              // }
            });
          }
          await loadDatabases();
          if(resetFlag) dispatch(syncSchedules());
          dispatch(Notifications.success({
            title: 'Database Restore',
            message: `Database restored with success.`,
            autoDismiss: 10
          }));
        });
      });
    };
  };
};


export const backup = () => {
  return async (dispatch, getState) => {
    const {dialog} = require('electron').remote
    dialog.showSaveDialog({defaultPath: 'phytofyBackup.zip'}, (async (path) => {
      if(path == undefined) return;
      dispatch(Notifications.success({
          title: 'Database Backup',
          message: `Backing up... may take a while depending on size of database.`,
          autoDismiss: 10
      }));
      await loadDatabases();
      var zip = new JSZip();
      const userDataPath = (electron.app || electron.remote.app).getPath('userData');
      console.log(`${userDataPath}`);
      let profiles = fs.readFileSync(`${userDataPath}/profiles.db`);
      let groups = fs.readFileSync(`${userDataPath}/canopies.db`);
      let recipes = fs.readFileSync(`${userDataPath}/recipes.db`);
      let schedules = fs.readFileSync(`${userDataPath}/schedules.db`);
      let devices = fs.readFileSync(`${userDataPath}/devices.db`);
      let fixtures = fs.readFileSync(`${userDataPath}/fixtures.db`);
      let moxas = fs.readFileSync(`${userDataPath}/moxas.db`);
      let settings = fs.readFileSync(`${userDataPath}/settings.db`);

      zip.file("phytofyBackup/profiles.db", profiles);
      zip.file("phytofyBackup/canopies.db", groups);
      zip.file("phytofyBackup/recipes.db", recipes);
      zip.file("phytofyBackup/schedules.db", schedules);
      zip.file("phytofyBackup/devices.db", devices);
      zip.file("phytofyBackup/fixtures.db", fixtures);
      zip.file("phytofyBackup/moxas.db", moxas);
      zip.file("phytofyBackup/settings.db", settings);

      zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
      .pipe(fs.createWriteStream(path))
      .on('finish', function () {
          // JSZip generates a readable stream with a "end" event,
          // but is piped here in a writable stream which emits a "finish" event.
          dispatch(Notifications.success({
              title: 'Database Backup',
              message: `Database files saved with success.`,
              autoDismiss: 10
          }));
      });
    }));
  };
};

// async function restoreRecipes(content, userDataPath){
//   await deleteDB('recipes', userDataPath);
//   let recipesStream = new LDJSONStream();
//   recipesStream.on('data', function(obj){
//     console.log(obj)
//     Recipe.insert(obj);
//   });
//   recipesStream.write(content.toString());
// }

async function deleteDB(dbname, userDataPath){
  fs.truncateSync(`${userDataPath}/${dbname}`, 0, function(){console.log(`Done deleting ${dbname}`)})
}

async function restoreDb(content, userDataPath, dbname){
  await deleteDB(dbname, userDataPath);
  fs.appendFile(`${userDataPath}/${dbname}`, content.toString(), function (err){
    if(err){
      log.error(`Error restoring db: ${dbname}`);
    }
  });
}

export const deleteAllSchedulesFromFixtures = () => {
  return async (dispatch, getState) => {
    //call at begining of restore
    let fixtures = await Fixture.cfind({}).exec();
    let errorCount = 0;
    await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
      let ack = await getState().devices.comm.deleteAllSchedules(fixture.serial_num);
      if(ack == 0) errorCount++;
    });
    if(errorCount > 0)  log.error(`[Restore Schedules] Error removing schedules from ${errorCount} fixtures!`);
  }
}
export const syncSchedules = () => {
  return async (dispatch, getState) => {
    //call only after loadDatabases !

    let schedules = await Schedule.cfind({}).exec();
    for(let schedule of schedules){
      await Schedule.update({_id: schedule._id}, {$set: {fixtures: {} }});
      await Schedule.update({_id: schedule._id}, {$set: {canopies: [] }});
    }
  }
}

async function loadDatabases(){
  await Recipe.loadDatabase();
  await Schedule.loadDatabase();
  await Fixture.loadDatabase();
  await Profile.loadDatabase();
  await Canopy.loadDatabase();
  await Moxa.loadDatabase();
  await Settings.loadDatabase();
}


export const commissionCanopies = (canopies) => {
  return async (dispatch, getState) => {

    for(let obj of canopies){
      let canopy = await Canopy.findOne({name: obj.name});
      console.log(canopy);
      //compute environment variabls
      let env = {}, retObj = null;
      let channelArr = ['uv', 'blue', 'green', 'hyperred', 'farred', 'white'];
      let w = getLengthConvertedToInches(canopy._profile.width, getState().settings.lengthUnit);
      let l = getLengthConvertedToInches(canopy._profile.length, getState().settings.lengthUnit);
      let delta = getLengthConvertedToInches(canopy._profile.deltaLength, getState().settings.lengthUnit);
      let height = getLengthConvertedToInches(canopy._profile.height, getState().settings.lengthUnit);
      const relPath = path.join(require('electron').remote.app.getAppPath(),'static');
      const absPath = "/Users/ricardo/Documents/GitStation/phytofy-dashboard/lib/phytofy-utils/src/FixtureMaps"
      retObj = await irradianceMap(l,w, canopy._profile.config, height, channelArr, delta, relPath);
      env = await computeEnviron(canopy._profile.config, retObj.luf, l, w, height);
      const fixtures = await Fixture.cfind({serial_num: {$in: canopy.fixtures}}).sort({moxa_mac: 1, port: 1}).exec();
      //send FC24(illuminance configuration) and FC9 getInfo to get maximum irradiance
      try {
        let fixturesInfo = [];
        await runCommandOnFixtures(fixtures, getState().devices.comm, dispatch, async (fixture) => {
          console.log(env);
          let resp = await getState().devices.comm.setIlluminanceConfiguration(fixture.serial_num, env);
          if(resp[0] == 1){
            log.info(`[CommissionCanopy] Set illuminance configuration success.`);
            let info = await getState().devices.comm.getFixtureInfo(fixture.serial_num);
            console.log(info);
            if( info['blueMax'] == -1 ){
              info = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0};
              log.error(`[CommissionCanopy] Error getting maximum irradiance from fixture: ${fixture.serial_num}.`);
            }else if( info['blueMax'] == 0){
              log.error(`[CommissionCanopy] fixture ${fixture.serial_num} returned 0 max. irradiance - probably missing factory calibration.`);
              info = {"FW": -1, "HW": -1, "uvMax": 50.0, "blueMax": 250., "greenMax": 100.0, "hyperRedMax": 250.0, "farRedMax": 100.0, "whiteMax": 250.0};
            }
            fixturesInfo.push(info);
          }else{
            log.error(`[CommissionCanopy] Illuminance configuration unacklowedged.`);
          }
        });
        //choose minimum max irradiances for each channel:
        canopy.fInfo = fixturesInfo[0];
        fixturesInfo.forEach((element) => {
          if(element['uvMax'] < canopy.fInfo['uvMax']) canopy.fInfo['uvMax'] = element['uvMax'];
          if(element['blueMax'] < canopy.fInfo['blueMax']) canopy.fInfo['blueMax'] = element['blueMax'];
          if(element['greenMax'] < canopy.fInfo['greenMax']) canopy.fInfo['greenMax'] = element['greenMax'];
          if(element['hyperRedMax'] < canopy.fInfo['hyperRedMax']) canopy.fInfo['hyperRedMax'] = element['hyperRedMax'];
          if(element['farRedMax'] < canopy.fInfo['farRedMax']) canopy.fInfo['farRedMax'] = element['farRedMax'];
          if(element['whiteMax'] < canopy.fInfo['whiteMax']) canopy.fInfo['whiteMax'] = element['whiteMax'];
        });
        console.log(canopy.fInfo);

      } catch(err){
        console.error(err);
      }
      let ret = await Canopy.update({_id: canopy._id}, canopy);
      console.log(ret);

    }
  }
}

export const loadSchedules = (schedules) => {
  return async (dispatch, getState) => {
    const defaultSchedule = {name: "", canopies: [], start_date: '', end_date: ''};
    for(let obj of schedules){

      let canopies = (await Canopy.cfind({_id: {$in: obj.canopies}}).exec());

      if(canopies.length > 0){
        dispatch(Notifications.success({
          title: 'Saving schedule',
          message: `Communicating with fixtures please wait...`,
          autoDismiss: 10
        }));
      }else{
        dispatch(Notifications.success({
          title: 'Schedule saved',
          message: `No groups associated with schedule.`,
          autoDismiss: 10
        }));
      }

      let ack = -1;
      let fixturesArray = [];
      for(let canopyI=0; canopyI<canopies.length; canopyI++){
        let canopy = canopies[canopyI];
        fixturesArray = fixturesArray.concat(canopy.fixtures);
      }
      if(fixturesArray.length > 0){
        let fixtures = await Fixture.cfind({serial_num: {$in: fixturesArray}}).exec();
        console.log(fixtures);
        ack = await addScheduleToFixtures(obj, fixtures, getState().devices.comm, dispatch);
      }

      obj = {...defaultSchedule};
      //dispatch(closeSidebar())
      if(ack){
        dispatch(Notifications.success({
          title: 'Schedule created',
          message: `Schedule created with success.`,
          autoDismiss: 10
        }));
      }else{
        dispatch(Notifications.error({
          title: 'Schedule created',
          message: `There were issues communicating with at least one fixture. Check logs for more info.`,
          autoDismiss: 10
        }));
      }
    }
  }
}
