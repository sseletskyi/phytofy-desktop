// @flow
import validator from 'validator';
import Notifications from 'react-notification-system-redux';

import Recipe, {RecipeFormValidator, importJSON, getPhotoperiod} from '../models/Recipe';
import { getPageSize } from '../models/Settings';
import Schedule from '../models/Schedule';

import { getPaginationInfo, getNewSortType, countQuery, makeSortAction,
         makeAddFilterAction, makeRemoveFilterAction, makePageBack, makePageNext, makeSelectObject, makeDeselectObject, makeSelectAll, makeDeselectAll, makeGetObjectsAction, makeGetObjectsAllAction, makePageGoTo } from './helpers';
import { openSidebar, closeSidebar } from './sidebar';
import FormValidator from '../utils/FormValidator';

import JSZip from 'jszip';
import fs from 'fs';
import moment from 'moment';

const defaultRecipe = {name: ""};

const queryRecipes = async (sortAttribute, sortType, from, filters, ignoreLimit) => {
  let recipes;

  let sortVal = sortType == 'orderAsc' ? 1 : -1;
  let sortObj = {};
  if(sortAttribute){
    sortObj = {[sortAttribute]: sortVal}
  }

  let pageSize = await getPageSize();
  // let skip = await ( page ? (page - 1) * pageSize : 0);
  // let limit = await ( page ? (skip + pageSize) : pageSize);
  let skip = from - 1
  let limit = pageSize;
  if(ignoreLimit){
    limit = 0;
  }

  if(Object.keys(sortObj).indexOf('name') == -1){
    sortObj['name'] = 1;
  }

  try {
    recipes = await Recipe.cfind(filters).sort(sortObj).skip(skip).limit(limit).exec();
  } catch(err) {
    console.error(err);
    return [];
  }

  return recipes;
}

export const getObjects = makeGetObjectsAction('RECIPES_GET', 'recipes', Recipe, queryRecipes);
export const getObjectsAll = makeGetObjectsAllAction('RECIPES_GET_ALL', 'recipes', Recipe, queryRecipes);
export const sort = makeSortAction('RECIPES_SORT', 'recipes', getObjects);
export const addFilter = makeAddFilterAction('RECIPES_ADD_FILTER', 'recipes', getObjects);
export const removeFilter = makeRemoveFilterAction('RECIPES_REMOVE_FILTER', 'recipes', getObjects);
export const pageBack = makePageBack('RECIPES_PAGE_BACK', 'recipes', getObjects, Recipe);
export const pageNext = makePageNext('RECIPES_PAGE_NEXT', 'recipes', getObjects, Recipe);
export const select = makeSelectObject('RECIPES_SELECT', 'recipes');
export const deselect = makeDeselectObject('RECIPES_DESELECT', 'recipes');
export const selectAll = makeSelectAll('RECIPES_SELECT_ALL', 'recipes');
export const deselectAll = makeDeselectAll('RECIPES_DESELECT_ALL');
export const goto = makePageGoTo('RECIPES_PAGE_GOTO', 'recipes', getObjects, Recipe);


export const create = (obj) => {
  return async (dispatch, getState) => {
    if(obj){
      obj.name = `Copy of ${obj.name}`;
    }

    let recipeObj = obj ? obj : {...defaultRecipe};
    await dispatch({
      type: 'RECIPES_CREATE',
      recipeObj: recipeObj,
      validation: {}
    })
    dispatch(openSidebar('CREATE_RECIPE_FORM', 'Create', 'New  day recipe', 'xl'));
  }
};

export const submitCreate = (obj) => {
  return async (dispatch, getState) => {
    let validations = await RecipeFormValidator.validate(obj);
    obj.photoperiod = await getPhotoperiod(obj.periods);
    console.log(obj.photoperiod);

    if(validations.isValid){
      try {
        await Recipe.insert(obj);
      } catch(err){
        console.error(err);
      }
      obj = {...defaultRecipe};
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Recipe created',
        message: `Recipe ${obj.name} created with success.`,
        autoDismiss: 10
      }));
    }

    dispatch(getObjects())

    dispatch({
      type: 'RECIPES_SUBMIT_CREATE',
      recipeObj: obj,
      validation: validations,
    })
  }
}

export const duplicate = (obj) => {
  return async (dispatch, getState) => {
    let newObj = {...obj};
    delete newObj._id;

    dispatch(create(newObj));
  }
}

export const edit = (obj) => {
  return async (dispatch, getState) => {
    let recipeId = obj._id;

    await dispatch({
      type: 'RECIPES_EDIT',
      recipeObj: obj,
      editObjId: obj._id,
      validation: {}
    })

    dispatch(openSidebar('EDIT_RECIPE_FORM', 'Edit', `${obj.name}`, 'xl'));
  }
}

export const submitEdit = (obj) => {
  return async (dispatch, getState) => {
    let count = await Schedule.count({recipeId: obj._id});

    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot edit ${obj.name}!`,
        message: `The recipe ${obj.name} has one or more schedules associated to it.`,
        autoDismiss: 10
      }));

      return;
    }

    let validations = await RecipeFormValidator.validate(obj, getState().recipes.recipeObj);
    obj.photoperiod = await getPhotoperiod(obj.periods);
    console.log(obj.photoperiod);

    if(validations.isValid){
      try {
        await Recipe.update({_id: getState().recipes.editObjId}, obj);
      } catch(err){
        console.error(err);
      }
      dispatch(closeSidebar())
      dispatch(Notifications.success({
        title: 'Recipe edited',
        message: `Recipe ${obj.name} edited with success.`,
        autoDismiss: 10
      }));
    }

    dispatch(getObjects())
    dispatch({
      type: 'RECIPES_SUBMIT_EDIT',
      recipeObj: obj,
      editObjId: validations.isValid ? null : getState().recipes.editObjId,
      validation: validations
    })
  }
}

export const cancelForm = () => {
  return async (dispatch, getState) => {
    dispatch(closeSidebar())
    let obj = {...defaultRecipe};
    dispatch({
      type: 'RECIPES_CANCEL_FORM',
      recipeObj: obj,
      editObjId: null,
      validation: {}
    })
  }
}

export const confirmDelete = (obj) => {
  return async (dispatch, getState) => {
    try {
      await Recipe.remove({_id: obj._id})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteObj = (obj) => {
  return async (dispatch, getState) => {
    let recipeId = obj._id;
    let count = await Schedule.count({recipeId: recipeId});
    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot delete ${obj.name}!`,
        message: `The recipe ${obj.name} has one or more schedules associated to it.`,
        autoDismiss: 10
      }));
    } else {
      dispatch(Notifications.warning({
        title: `Delete ${obj.name}`,
        message: `Are you sure you want to delete ${obj.name}?`,
        autoDismiss: 10,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDelete(obj))
          }
        }
      }));
    }
  };
}

export const confirmDeleteMultiple = () => {
  return async (dispatch, getState) => {
    try {
      obj = await Recipe.remove({_id: {$in: getState().recipes.selected}}, {multi: true})
    } catch(err){
      console.error(err);
    }

    dispatch(getObjects());
  }
}

export const deleteMultiple = () => {
  return async (dispatch, getState) => {
    let count = await Schedule.count({recipeId: {$in: getState().recipes.selected}});
    if(count > 0){
      dispatch(Notifications.error({
        title: `Cannot delete recipes!`,
        message: `The selected recipes have one or more schedules associated to them.`,
        autoDismiss: 10
      }));
    } else {
      dispatch(Notifications.warning({
        title: `Delete ${getState().recipes.selected.length} recipes`,
        message: `Are you sure you want to delete ${getState().recipes.selected.length} recipes?`,
        autoDismiss: 10,
        action: {
          label: 'Yes',
          callback: () => {
            dispatch(confirmDeleteMultiple())
          }
        }
      }));
    }
  }
}

export const exportJsonRecipe = () => {
  return async (dispatch, getState) => {
    const {dialog} = require('electron').remote
    dialog.showSaveDialog({defaultPath: 'phytofyRecipes.zip'}, (async (path) => {
      let recipes = null;
      try {
        recipes = await Recipe.cfind({_id: {$in: getState().recipes.selected}}).exec()
      } catch(err){
        console.error(err);
      }
      console.log(recipes);
      var zip = new JSZip();
      for(let recipe of recipes){
        await recipeClean(recipe);
        zip.file(`phytofyJsonRecipes/${recipe.name}.json`, JSON.stringify(recipe, null, 2));
      }
      zip.generateNodeStream({type:'nodebuffer',streamFiles:true})
      .pipe(fs.createWriteStream(path))
      .on('finish', function () {
          // JSZip generates a readable stream with a "end" event,
          // but is piped here in a writable stream which emits a "finish" event.
          dispatch(Notifications.success({
              title: 'Recipe export',
              message: `JSON format recipes exported with success`,
              autoDismiss: 10
          }));
      });
    }));
  }
}

async function recipeClean(recipe) {
  // Filtering out properties
  delete recipe._id;
  delete recipe.ignore_whitespace;
  for(let i = 0; i < recipe.periods.length; i++){
    delete recipe.periods[i].errors;
    delete recipe.periods[i].irradianceUVA;
    delete recipe.periods[i].irradianceBlue;
    delete recipe.periods[i].irradianceGreen;
    delete recipe.periods[i].irradianceFarRed;
    delete recipe.periods[i].irradianceHyperRed;
    delete recipe.periods[i].irradianceWhite;
    delete recipe.periods[i].totalIrradiance;
  }
}

export const importJsonRecipe = () => {
  return async (dispatch, getState) => {
    const {dialog} = require('electron').remote
    dialog.showOpenDialog({properties: ['openFile', 'openDirectory']}, (async (path) => {
      fs.readFile(path[0], async function(err, data) {
        if (err) throw err;
        let obj = JSON.parse(data);
        let ret = await importJSON(obj);
        if(ret == 1){
          dispatch(Notifications.success({
              title: 'Recipe Import',
              message: `JSON format recipe ${obj.name} imported with success.`,
              autoDismiss: 10
          }));
          dispatch(getObjects());
        }else if( ret == -2){
          dispatch(Notifications.error({
              title: 'Recipe Import',
              message: `Failed importing because there are overlapping periods.`,
              autoDismiss: 10
          }));
        }else{
          dispatch(Notifications.error({
              title: 'Recipe Import',
              message: `Failed importing because ${obj.name} already exists.`,
              autoDismiss: 10
          }));
        }
      });
    }));
  }
}
